{ nixpkgs ? import ./nixpkgs.nix {} }:

import ./common.nix {
  src = [];
  inherit nixpkgs;
}
