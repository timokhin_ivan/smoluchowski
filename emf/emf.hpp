#ifndef _EMF_HPP__
#define _EMF_HPP__

#include <chrono>
#include <cstddef>
#include <filesystem>
#include <fstream>
#include <functional> // for std::function
#include <iomanip>
#include <iterator> // for std::reverse_iterator
#include <limits>
#include <memory> // for std::unique_ptr
#include <mutex>  // for std::mutex and std::lock_guard
#include <sstream>
#include <stdexcept>
#include <string>
#include <unordered_map>
#include <vector> // for std::vector

namespace emf {

// Using fmt would be a strictly better option, but that means
// either a dependency or C++20.
std::string strprintf(const char *fmt, ...);

template <typename T> struct type {};

bool parse(type<bool>, const char *str, const char *&endparse);

char parse(type<char>, const char *str, const char *&endparse);
signed char parse(type<signed char>, const char *str, const char *&endparse);
unsigned char parse(type<unsigned char>, const char *str,
                    const char *&endparse);

short parse(type<short>, const char *str, const char *&endparse);
int parse(type<int>, const char *str, const char *&endparse);
long parse(type<long>, const char *str, const char *&endparse);
long long parse(type<long long>, const char *str, const char *&endparse);

unsigned short parse(type<unsigned short>, const char *str,
                     const char *&endparse);
unsigned parse(type<unsigned>, const char *str, const char *&endparse);
unsigned long parse(type<unsigned long>, const char *str,
                    const char *&endparse);
unsigned long long parse(type<unsigned long long>, const char *str,
                         const char *&endparse);

float parse(type<float>, const char *str, const char *&endparse);
double parse(type<double>, const char *str, const char *&endparse);
long double parse(type<long double>, const char *str, const char *&endparse);

std::string parse(type<std::string>, const char *str, const char *&endparse);

template <typename T>
std::vector<T> parse(type<std::vector<T>>, const char *str,
                     const char *&endparse) {
  char endbracket = '\0';
  if (*str == '{')
    endbracket = '}';
  else if (*str == '[')
    endbracket = ']';
  else
    return {};

  ++str;

  std::vector<T> parsed;
  while (*str != endbracket) {
    const char *elend = str;
    parsed.push_back(parse(type<T>{}, str, elend));

    if (elend == str)
      return {};
    else
      str = elend;

    if (*str == ',')
      ++str;
    else if (*str == endbracket) {
      break;
    } else
      return {};
  }

  ++str;
  endparse = str;
  return parsed;
}

template <typename T> std::string render(const T &val) {
  // std::to_string would be an obvious alternative, but it uses
  // fixed-point formatting for floats for some bizarre reason.
  std::ostringstream os;
  os << std::boolalpha << val;
  return os.str();
}

template <typename T> std::string render(const std::vector<T> &val) {
  std::ostringstream os;
  os << '[';

  if (!val.empty()) {
    os << render(val.front());

    for (auto it = val.cbegin() + 1; it != val.cend(); ++it) {
      os << ", " << render(*it);
    }
  }

  os << ']';
  return os.str();
}

std::string render(const std::string &val);

std::string render(const char *val);

enum class severity { debug, info, notice, warning, error };

class experiment {
private:
  struct option {
    const char *long_name;
    std::function<void(const char *)> parse_callback;
    std::function<std::string()> render_callback;
    bool loud;
  };

  std::vector<option> options;
  std::vector<std::function<void()>> pre_run_callbacks;

  struct arguments_t {
  private:
    const char *const *args = nullptr;
    size_t args_count = 0;

    arguments_t() = default;
    arguments_t(const char *const *ptr, size_t n) : args(ptr), args_count(n) {}

    friend class experiment;

  public:
    using value_type = const char *;
    using pointer = value_type *;
    using const_pointer = const value_type *;
    using reference = value_type &;
    using const_reference = const value_type &;
    using const_iterator = const_pointer; // TODO: consider a more
                                          // sophisticated iterator,
                                          // with bounds checking and
                                          // everything.
    using iterator = const_iterator;
    using const_reverse_iterator = std::reverse_iterator<const_iterator>;
    using reverse_iterator = const_reverse_iterator;
    using size_type = std::size_t;
    using difference_type = std::ptrdiff_t;

    const_iterator cbegin() const { return args; }

    const_iterator cend() const { return args + args_count; }

    const_iterator begin() const { return cbegin(); }

    const_iterator end() const { return cend(); }

    const_reverse_iterator crbegin() const {
      return const_reverse_iterator(cend());
    }

    const_reverse_iterator crend() const {
      return const_reverse_iterator(cbegin());
    }

    const_reverse_iterator rbegin() const { return crbegin(); }

    const_reverse_iterator rend() const { return crend(); }

    // Pointers are small and therefore OK to copy
    value_type operator[](size_type i) const { return at(i); }

    value_type at(size_type i) const {
      if (i > size())
        throw std::out_of_range(
            "Index out of range in emf::experiment::arguments_t::at");
      return args[i];
    }

    value_type front() const { return at(0); }

    value_type back() const { return at(size() - 1); }

    const_pointer data() const { return args; }

    size_type size() const { return args_count; }

    size_type length() const { return size(); }

    size_type max_size() const { return std::numeric_limits<size_type>::max(); }

    bool empty() const { return size() == 0; }
  };

  arguments_t args;
  std::filesystem::path results_directory, meta_file, log_file, data_directory;
  std::ofstream log_stream;
  std::mutex log_mutex;

  std::chrono::steady_clock::time_point experiment_start, last_log_message;
  std::unordered_map<std::string, std::chrono::steady_clock::time_point>
      last_tagged_message_times;

  int do_main(int argc, char **argv);
  static int do_multi_main(int argc, char **argv,
                           std::unique_ptr<experiment> *candidates,
                           size_t candidates_count);

  void init(int argc, char **argv);
  void setup_directory();
  void setup_paths();
  void write_meta_start(const std::vector<std::string> &args);
  void write_meta_end();

  void do_log(severity sev, const char *tag, const char *msg);

  // TODO: replace with some sort of a trie
  std::unordered_map<std::string, severity> log_cutoffs;

  severity get_cutoff(const std::string &tag) {
    auto it = log_cutoffs.find(tag);
    return it == log_cutoffs.end() ? severity::notice : it->second;
  }

  static std::unique_ptr<experiment> instance;
  static std::mutex experiment_mutex;
  static std::recursive_mutex instance_mutex;

  class instance_access_guard {
  private:
    std::lock_guard<std::recursive_mutex> guard;
    experiment *e;

  public:
    instance_access_guard();

    experiment &get() { return *e; }
  };

  template <typename E> struct instance_guard {
    std::lock_guard<std::mutex> lock;

    instance_guard() : lock(experiment_mutex) {
      std::lock_guard<std::recursive_mutex> guard{instance_mutex};
      instance = std::make_unique<E>();
    }

    instance_guard(std::unique_ptr<E> ptr) : lock(experiment_mutex) {
      std::lock_guard<std::recursive_mutex> guard{instance_mutex};
      instance = std::move(ptr);
    }

    instance_guard(const instance_guard<E> &) = delete;
    void operator=(const instance_guard<E> &) = delete;
    instance_guard(instance_guard<E> &&) = delete;
    void operator=(instance_guard<E> &&) = delete;

    ~instance_guard() {
      std::lock_guard<std::recursive_mutex> guard{instance_mutex};
      instance.reset();
    }
  };

  static void check_parse_errors(const char *arg, const char *endparse);
  static void report_missing_argument(const char *name);

protected:
  template <typename T> class parameter {
  private:
    std::optional<T> opt_value;
    const char *pname;

    template <typename... U> void emplace(U &&... args) {
      opt_value.emplace(std::forward<U>(args)...);
    }

    explicit parameter(const char *name) : pname(name) {}

  public:
    friend class experiment;

    // Implicit conversions are nice and all, but what about
    // iostreams? fmt? templates? (this last one may be very nearly
    // unsolveable)
    operator const T &() const & {
      if (!opt_value.has_value())
        report_missing_argument(pname);
      return opt_value.value();
    }

    operator const T &&() const && {
      if (!opt_value.has_value())
        report_missing_argument(pname);
      return static_cast<const std::optional<T> &&>(opt_value).value();
    }
  };

  template <typename T, typename P, typename Render, typename Emplace>
  P register_long_argument_generic(const char *name, P &dest, P initial_value,
                                   Emplace emplace, Render render,
                                   bool loud = true) {
    options.push_back({name,
                       [&dest, emplace](const char *arg) {
                         const char *endparse = arg;
                         T val = parse(type<T>{}, arg, endparse);
                         check_parse_errors(arg, endparse);
                         emplace(std::move(val));
                       },
                       [&dest, render]() -> std::string {
                         return render(static_cast<const T &>(dest));
                       },
                       loud});
    return initial_value;
  }

  template <typename T, typename F>
  parameter<T> register_long_argument(const char *name, parameter<T> &dest,
                                      F render, bool loud = true) {
    return register_long_argument_generic<T>(
        name, dest, parameter<T>{name},
        [&dest](auto &&val) { dest.emplace(std::move(val)); }, render, loud);
  }

  template <typename T, typename F>
  T register_long_optional_argument(const char *name, T &dest, T initial_value,
                                    F render, bool loud = true) {
    return register_long_argument_generic<T>(
        name, dest, initial_value,
        [&dest](T &&value) { dest = std::move(value); }, render, loud);
  }

  template <typename F> void register_pre_run_callback(F f) {
    pre_run_callbacks.emplace_back(std::move(f));
  }

public:
  virtual int run() = 0;

  virtual const char *name() const { return "experiment"; };

  virtual ~experiment();

  const arguments_t &arguments() const { return args; }

  template <typename E> static int main(int argc, char **argv) {
    instance_guard<E> guard;
    return static_cast<experiment *>(instance.get())->do_main(argc, argv);
  }

  template <typename... Es> static int multi_main(int argc, char **argv) {
    std::unique_ptr<experiment> candidates[sizeof...(Es)] = {
        std::make_unique<Es>()...};
    return do_multi_main(argc, argv, candidates, sizeof...(Es));
  }

  static void log(severity s, const char *tag, const char *msg);

  template <typename... Args>
  static void logf(severity s, const char *tag, const char *fmt, Args... args) {
    log(s, tag, strprintf(fmt, args...).c_str());
  }

  class logger {
  private:
    std::string tag;
    severity threshold;
    friend class experiment;

    logger(const char *tag_, severity threshold_)
        : tag(tag_), threshold(threshold_) {}

  public:
    logger() = delete;

    void log(severity s, const char *msg) {
      if (s >= threshold)
        experiment::log(s, tag.c_str(), msg);
    }

    template <typename... Args>
    void logf(severity s, const char *fmt, Args... args) {
      if (s >= threshold)
        experiment::logf(s, tag.c_str(), fmt, args...);
    }
  };

  // TODO: entire logging thing is chock full of temporary string
  // allocations.  Figure something out.
  static logger make_logger(const char *tag) {
    return logger(tag, instance_access_guard{}.get().get_cutoff(tag));
  }

  static std::filesystem::path datadir();
  static std::ofstream data_file(const char *name);

  template <typename... Args>
  static std::ofstream data_file_f(const char *fmt, Args... args) {
    return data_file(strprintf(fmt, args...).c_str());
  }
};

#define EMF_PARAMETER(ty, name)                                                \
  ::emf::experiment::parameter<ty> name = this->register_long_argument(        \
      #name, this->name, [](const auto &val) -> ::std::string {                \
        using ::emf::render;                                                   \
        return render(val);                                                    \
      })

#define EMF_PARAMETER_QUIET(ty, name)                                          \
  ::emf::experiment::parameter<ty> name = this->register_long_argument(        \
      #name, this->name, [](const auto &) -> ::std::string { return {}; },     \
      false)

#define EMF_PARAMETER_OPTIONAL(ty, name, default_value)                        \
  ty name = this->register_long_optional_argument(                             \
      #name, this->name, (default_value),                                      \
      [](const auto &val) -> ::std::string {                                   \
        using ::emf::render;                                                   \
        return render(val);                                                    \
      })

#define EMF_PARAMETER_OPTIONAL_QUIET(ty, name, default_value)                  \
  ty name = this->register_long_optional_argument(                             \
      #name, this->name, (default_value),                                      \
      [](const auto &) -> ::std::string { return {}; }, false)

#define EMF_MAIN(E)                                                            \
  int main(int argc, char **argv) {                                            \
    return ::emf::experiment::main<E>(argc, argv);                             \
  }

#define EMF_MULTI_MAIN(...)                                                    \
  int main(int argc, char **argv) {                                            \
    return ::emf::experiment::multi_main<__VA_ARGS__>(argc, argv);             \
  }

} // namespace emf

#endif // _EMF_HPP__
