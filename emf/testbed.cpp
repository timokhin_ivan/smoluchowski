#include <chrono>
#include <iomanip>
#include <ios>
#include <iostream>
#include <thread>

#include "emf.hpp"

struct T {};

T parse(emf::type<T>, const char *, const char *&) { return {}; }

struct E : public emf::experiment {
  EMF_PARAMETER_OPTIONAL_QUIET(bool, announce, true);

  E() {
    register_pre_run_callback([this]() {
      if (announce) {
        std::cout << "Running an experiment...\n";
      }
    });
  }
};

struct TE : public E {
  EMF_PARAMETER(size_t, size);
  EMF_PARAMETER_QUIET(T, t);
  EMF_PARAMETER(double, x);
  EMF_PARAMETER(short, n);
  EMF_PARAMETER(std::string, nm);
  EMF_PARAMETER_OPTIONAL(std::vector<double>, xs, std::vector<double>{});
  EMF_PARAMETER_OPTIONAL(std::vector<std::vector<double>>, xss,
                         std::vector<std::vector<double>>{});
  EMF_PARAMETER_OPTIONAL(double, y, 0.5);
  EMF_PARAMETER_OPTIONAL_QUIET(bool, b, false);

  int run() override {
    std::cout << "Hello, world!" << std::endl;
    std::cout << "size = " << size << std::endl;
    std::cout << "x = " << x << std::endl;
    std::cout << "n = " << n << std::endl;
    std::cout << "nm = " << std::quoted(static_cast<const std::string &>(nm))
              << std::endl;

    for (auto arg : arguments()) {
      std::cout << arg << std::endl;
    }

    double sum = 0.0;
    for (auto x : xs)
      sum += x;

    for (const auto &s : xss)
      for (auto x : s)
        sum += x;

    std::cout << "sum = " << sum << std::endl;

    auto q = make_logger("Q");

    using namespace std::literals;
    std::this_thread::sleep_for(5s);
    log(emf::severity::debug, "T", "First log message");
    std::this_thread::sleep_for(3s);
    q.log(emf::severity::info, "Second log message");
    std::this_thread::sleep_for(2s);
    log(emf::severity::warning, "T", "Third log message");
    std::this_thread::sleep_for(1s);
    q.logf(emf::severity::error, "Log message #%d", 4);

    {
      auto out = data_file("a");
      std::cerr << std::boolalpha << out.is_open() << std::endl;
      out << 10 << '\n';
    }

    {
      auto out = data_file_f("%s", "b");
      out << 12 << '\n';
    }

    return 0;
  }

  const char *name() const override { return "TE"; }
};

struct A : public E {
  EMF_PARAMETER(double, a);

  const char *name() const override { return "A"; }

  int run() override {
    std::cout << "a = " << a << std::endl;
    return 0;
  }
};

struct B : public E {
  EMF_PARAMETER(double, b);

  const char *name() const override { return "B"; }

  int run() override {
    std::cout << "b = " << b << std::endl;
    return 0;
  }
};

EMF_MULTI_MAIN(TE, A, B)
