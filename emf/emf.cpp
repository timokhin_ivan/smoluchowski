#include "emf.hpp"
#include <cerrno> // for errno
#include <chrono>
#include <cstdarg>
#include <cstddef>
#include <cstdio>
#include <cstdlib> // for strtoXX
#include <cstring>
#include <ctime>
#include <exception>
#include <filesystem>
#include <fstream>
#include <functional>
#include <getopt.h> // for getopt-related stuff
#include <iomanip>
#include <ios>
#include <iostream> // for std::cerr
#include <limits>
#include <memory>
#include <mutex>
#include <ostream>
#include <stdexcept>
#include <string>
#include <system_error>

#include <fcntl.h>
#include <unistd.h>

std::string emf::strprintf(const char *fmt, ...) {
  std::va_list args;

  va_start(args, fmt);
  int required = std::vsnprintf(nullptr, 0, fmt, args);
  va_end(args);

  std::string result;
  // vsnprintf absolutely insists on writing the null byte, to the
  // point of truncating the output to fit it in, so…
  result.resize(required + 1);
  va_start(args, fmt);
  std::vsnprintf(result.data(), result.size(), fmt, args);
  va_end(args);

  // but, of course, we don't actually need the null byte in our data.
  result.pop_back();

  return result;
}

namespace {
template <typename T> std::string to_string_f(T v) {
  return emf::strprintf("%+.5Lg", static_cast<long double>(v));
}

std::tm current_local_time() {
  std::time_t time;
  std::time(&time);
  std::tm result;
  localtime_r(&time, &result);
  return result;
}

std::string rfc3339(const std::tm &time) {
  char buffer[26];
  std::strftime(buffer, sizeof(buffer), "%FT%T%z", &time);
  std::string result = buffer;
  // TODO: check if the timezone information was actually written
  result.insert(result.end() - 2, ':');
  return result;
}

template <typename Rep, typename Period>
std::string render_interval(std::chrono::duration<Rep, Period> d) {
  using namespace std::chrono_literals;
  char sign;
  if (d < 0s) {
    d = -d;
    sign = '-';
  } else {
    sign = '+';
  }

  auto d_s = std::chrono::duration_cast<std::chrono::seconds>(d);
  auto d_h = std::chrono::duration_cast<std::chrono::hours>(d_s);
  d_s -= d_h;
  auto d_m = std::chrono::duration_cast<std::chrono::minutes>(d_s);
  d_s -= d_m;

  return emf::strprintf("%c%02lu:%02lu:%02lu", sign,
                        static_cast<unsigned long>(d_h.count()),
                        static_cast<unsigned long>(d_m.count()),
                        static_cast<unsigned long>(d_s.count()));
}
} // namespace

namespace emf {

using std::literals::operator""s;

class cli_argument_out_of_range : public std::runtime_error {
public:
  template <typename T>
  cli_argument_out_of_range(type<T>, const char *arg, size_t arglen)
      : std::runtime_error(
            "The supplied value " + std::string(arg, arglen) +
            " is out of the valid range [" +
            std::to_string(std::numeric_limits<T>::min()) + ", " +
            std::to_string(std::numeric_limits<T>::max()) + "]") {}

  struct float_t {};

  template <typename T>
  cli_argument_out_of_range(float_t, type<T>, const char *arg, size_t arglen)
      : std::runtime_error(
            "The supplied value " + std::string(arg, arglen) +
            " is out of the valid ranges [" +
            ::to_string_f(-std::numeric_limits<T>::max()) + ", " +
            ::to_string_f(-std::numeric_limits<T>::min()) + "] and [" +
            ::to_string_f(std::numeric_limits<T>::min()) + ", " +
            ::to_string_f(std::numeric_limits<T>::max())) {}
};
} // namespace emf

template <typename T, T (*f)(const char *, char **, int)>
T strtoparse_base(const char *str, const char *&endparse) {
  errno = 0;
  T val = f(str, const_cast<char **>(&endparse), 0);
  if (errno == ERANGE) {
    errno = 0;
    throw emf::cli_argument_out_of_range(emf::type<T>{}, str, endparse - str);
  }
  return val;
}

template <typename T, typename U, U (*f)(const char *, char **, int)>
T strtoparse_base_narrow(const char *str, const char *&endparse) {
  errno = 0;
  U val = f(str, const_cast<char **>(&endparse), 0);
  if (errno == ERANGE || val > static_cast<U>(std::numeric_limits<T>::max()) ||
      val < static_cast<U>(std::numeric_limits<T>::lowest())) {
    errno = 0;
    throw emf::cli_argument_out_of_range(emf::type<T>{}, str, endparse - str);
  }
  return static_cast<T>(val);
}

template <typename T, T (*f)(const char *, char **)>
T strtoparse_float(const char *str, const char *&endparse) {
  errno = 0;
  T val = f(str, const_cast<char **>(&endparse));
  if (errno == ERANGE) {
    errno = 0;
    throw emf::cli_argument_out_of_range(
        emf::cli_argument_out_of_range::float_t{}, emf::type<T>{}, str,
        endparse - str);
  }
  return val;
}

unsigned long emf::parse(emf::type<unsigned long>, const char *str,
                         const char *&endparse) {
  return strtoparse_base<unsigned long, std::strtoul>(str, endparse);
}

unsigned long long emf::parse(emf::type<unsigned long long>, const char *str,
                              const char *&endparse) {
  return strtoparse_base<unsigned long long, std::strtoull>(str, endparse);
}

long emf::parse(emf::type<long>, const char *str, const char *&endparse) {
  return strtoparse_base<long, std::strtol>(str, endparse);
}

long long emf::parse(emf::type<long long>, const char *str,
                     const char *&endparse) {
  return strtoparse_base<long long, std::strtoll>(str, endparse);
}

bool emf::parse(emf::type<bool>, const char *str, const char *&endparse) {
  switch (*str) {
  case 'y':
    if (std::strncmp("es", str + 1, 2) == 0) {
      endparse = str + 3;
    } else {
      endparse = str + 1;
    }
    return true;
  case 'n':
    if (*(str + 1) == 'o') {
      endparse = str + 2;
    } else {
      endparse = str + 1;
    }
    return false;
  case 't':
    if (std::strncmp("rue", str + 1, 3) == 0) {
      endparse = str + 4;
    } else {
      endparse = str + 1;
    }
    return true;
  case 'f':
    if (std::strncmp("alse", str + 1, 4) == 0) {
      endparse = str + 5;
    } else {
      endparse = str + 1;
    }
    return false;
  default:
    endparse = str;
    return false;
  }
}

char emf::parse(emf::type<char>, const char *str, const char *&endparse) {
  if (*str) {
    endparse = str + 1;
    return *str;
  } else {
    endparse = str;
    return '\0';
  }
}

signed char emf::parse(emf::type<signed char>, const char *str,
                       const char *&endparse) {
  return static_cast<signed char>(emf::parse(emf::type<char>{}, str, endparse));
}

unsigned char emf::parse(emf::type<unsigned char>, const char *str,
                         const char *&endparse) {
  return static_cast<unsigned char>(
      emf::parse(emf::type<char>{}, str, endparse));
}

short emf::parse(emf::type<short>, const char *str, const char *&endparse) {
  return strtoparse_base_narrow<short, long, std::strtol>(str, endparse);
}

int emf::parse(emf::type<int>, const char *str, const char *&endparse) {
  return strtoparse_base_narrow<int, long, std::strtol>(str, endparse);
}

unsigned short emf::parse(emf::type<unsigned short>, const char *str,
                          const char *&endparse) {
  return strtoparse_base_narrow<unsigned short, unsigned long, std::strtoul>(
      str, endparse);
}

unsigned emf::parse(emf::type<unsigned>, const char *str,
                    const char *&endparse) {
  return strtoparse_base_narrow<unsigned, unsigned long, std::strtoul>(
      str, endparse);
}

float emf::parse(emf::type<float>, const char *str, const char *&endparse) {
  return strtoparse_float<float, std::strtof>(str, endparse);
}

double emf::parse(emf::type<double>, const char *str, const char *&endparse) {
  return strtoparse_float<double, std::strtod>(str, endparse);
}

long double emf::parse(emf::type<long double>, const char *str,
                       const char *&endparse) {
  return strtoparse_float<long double, std::strtold>(str, endparse);
}

std::string emf::parse(emf::type<std::string>, const char *str,
                       const char *&endparse) {
  std::string result{str};
  endparse = str + result.size();
  return result;
}

std::string emf::render(const std::string &val) {
  std::ostringstream os;
  os << std::quoted(val);
  return os.str();
}

std::string emf::render(const char *val) {
  std::ostringstream os;
  os << std::quoted(val);
  return os.str();
}

namespace {
void print_exception(const std::exception &e, size_t level) {
  std::cerr << std::string(2 * level, ' ') << e.what() << std::endl;

  try {
    std::rethrow_if_nested(e);
  } catch (const std::exception &nested) {
    print_exception(nested, level + 1);
  } catch (...) {
    // TODO: do something appropriate.  Maybe duplicate the dispatch
    // hell from do_main
  }
}
} // namespace

namespace emf {

class missing_experiment_name : public std::runtime_error {
public:
  missing_experiment_name()
      : std::runtime_error("Experiment name not provided on the command line") {
  }
};

class unrecognised_experiment_name : public std::runtime_error {
public:
  unrecognised_experiment_name(const char *name)
      : std::runtime_error("Experiment name not recognised: "s + name) {}
};

} // namespace emf

int emf::experiment::do_multi_main(int argc, char **argv,
                                   std::unique_ptr<emf::experiment> *candidates,
                                   size_t candidates_count) {
  if (argc < 2) {
    throw emf::missing_experiment_name{};
  }
  const char *ename = argv[1];

  size_t i = 0;
  for (; i < candidates_count; ++i) {
    if (std::strcmp(ename, candidates[i]->name()) == 0) {
      break;
    } else {
      candidates[i].reset();
    }
  }

  if (i >= candidates_count)
    throw emf::unrecognised_experiment_name{ename};

  std::unique_ptr<emf::experiment> selected{std::move(candidates[i])};

  for (++i; i < candidates_count; ++i) {
    candidates[i].reset();
  }

  instance_guard<emf::experiment> guard{std::move(selected)};
  return instance.get()->do_main(argc, argv);
}

int emf::experiment::do_main(int argc, char **argv) {
  constexpr const char *const msg = "Terminating due to an uncaught exception:";
  auto report_short_error = [](auto x) {
    std::cerr << msg << " " << x << std::endl;
  };

  try {
    // Start by saving the arguments before they are mangled by getopt
    std::vector<std::string> args;
    for (int i = 0; i < argc; ++i)
      args.emplace_back(argv[i]);

    init(argc, argv);
    setup_directory();
    write_meta_start(args);
    // Get rid of the arguments so that they don't hang around in
    // memory pointlessly.
    args.clear();

    last_log_message = experiment_start = std::chrono::steady_clock::now();
    do_log(severity::notice, nullptr, "experiment started");

    for (const auto &f : pre_run_callbacks) {
      f();
    }

    return run();
  } catch (const std::exception &e) {
    std::cerr << msg << std::endl;
    print_exception(e, 1);
  } catch (const char *str) {
    std::cerr << msg << "\n " << str << std::endl;
  } catch (const std::string &s) {
    std::cerr << msg << "\n " << s << std::endl;
  } catch (bool b) {
    report_short_error(b);
  } catch (short i) {
    report_short_error(i);
  } catch (unsigned short u) {
    report_short_error(u);
  } catch (int i) {
    report_short_error(i);
  } catch (unsigned int u) {
    report_short_error(u);
  } catch (long i) {
    report_short_error(i);
  } catch (unsigned long u) {
    report_short_error(u);
  } catch (long long i) {
    report_short_error(i);
  } catch (unsigned long long u) {
    report_short_error(u);
  } catch (float f) {
    report_short_error(f);
  } catch (double f) {
    report_short_error(f);
  } catch (long double f) {
    report_short_error(f);
  } catch (...) {
    std::cerr << "Terminating due to an unrecognised uncaught exception\n";
  }

  return EXIT_FAILURE;
}

namespace emf {
class argument_parsing_error : public std::runtime_error {
public:
  argument_parsing_error(const char *name)
      : std::runtime_error("Error when parsing argument "s + name) {}
};
} // namespace emf

void emf::experiment::init(int argc, char **argv) {
  std::vector<struct ::option> getopt_options;
  getopt_options.reserve(options.size() + 6);

  for (int i = 0; i < static_cast<int>(options.size()); ++i) {
    const option &opt = options.at(i);
    ::option option = {};
    option.name = opt.long_name;
    option.has_arg = 1;
    option.flag = nullptr;
    option.val = 0;
    getopt_options.push_back(option);
  }

  std::tuple<severity, const char *> severities[] = {
      {severity::debug, "enable-log-debug"},
      {severity::info, "enable-log-info"},
      {severity::notice, "enable-log-notice"},
      {severity::warning, "enable-log-warning"},
      {severity::error, "enable-log-error"}};
  for (auto [s, str] : severities) {
    ::option option = {};
    option.name = str;
    option.has_arg = 1;
    option.flag = nullptr;
    option.val = 0;
    getopt_options.push_back(option);
  }

  ::option sentinel = {};
  getopt_options.push_back(sentinel);

  int retval;
  int longindex = 0;
  while ((retval = getopt_long(argc, argv, "", getopt_options.data(),
                               &longindex)) != -1) {
    if (retval == 0) {
      if (longindex < static_cast<int>(options.size())) {
        const option &opt = options.at(longindex);

        try {
          opt.parse_callback(optarg);
        } catch (...) {
          // nested_exception automatically picks up the current
          // exception from the global variables.  Very convenient, but
          // very confusing.
          std::throw_with_nested(emf::argument_parsing_error(opt.long_name));
        }
      } else {
        severity s = static_cast<severity>(longindex - options.size());
        auto [it, inserted] = log_cutoffs.emplace(optarg, s);
        if (!inserted) {
          it->second = std::min(it->second, s);
        }
      }
    }
  }

  args = arguments_t{argv + optind,
                     optind > argc ? 0 : static_cast<size_t>(argc - optind)};
}

void emf::experiment::setup_directory() {
  // It's totally fine if it already exists.
  std::filesystem::create_directory("results");

  std::filesystem::path own_directory = "results";

  std::string dname = name(); // TODO: extend
  own_directory.append(dname);

  for (const auto &opt : options) {
    if (opt.loud) {
      own_directory.concat("_"s + opt.long_name + "=" + opt.render_callback());
    }
  }

  own_directory.concat("@" + rfc3339(current_local_time()));

  // This one, however, we want to be unique.
  if (!std::filesystem::create_directory(own_directory)) {
    own_directory.concat("@");
    size_t disambiguator = 0;

    std::filesystem::path attempt;

    do {
      attempt = own_directory;
      attempt.concat(std::to_string(disambiguator));
      ++disambiguator;
    } while (!std::filesystem::create_directory(attempt));

    own_directory = std::move(attempt);
  }

  // Only set at the end so that it remains unset if there's an
  // exception.
  results_directory = std::move(own_directory);
  setup_paths();
}

void emf::experiment::setup_paths() {
  meta_file = results_directory / "meta.toml";
  log_file = results_directory / "log";
  data_directory = results_directory / "data";
  std::filesystem::create_directory(data_directory);
}

// What follows is a horrible hack used to detect the presence of a C
// OpenMP library linked into the final executable.  I've learned it
// from https://snf.github.io/2019/02/13/shared-ptr-optimization/
// but it already describes its use in libstdc++ to detect
// multithreading, so the origins are likely way older.
using omp_get_max_threads_t = int(void);

// TODO: check how this works with separate compilation
static omp_get_max_threads_t _emf_omp_get_max_threads
    __attribute__((__weakref__("omp_get_max_threads")));

static void write_openmp_data(std::ostream &out) {
  if (int (*_omp_available_ptr)(void) = &_emf_omp_get_max_threads) {
    out << "[openmp]\n";
    out << "max_threads = " << _omp_available_ptr() << "\n\n";
  }
}

static void write_slurm_data(std::ostream &out) {
  if (const char *job_id = std::getenv("SLURM_JOB_ID")) {
    out << "[slurm]\n";
    out << "JOB_ID = " << std::quoted(job_id) << '\n';

    std::string variables[] = {"JOB_NAME",
                               "JOB_ACCOUNT",
                               "JOB_PARTITION",
                               "JOB_DEPENDENCY",
                               "JOB_QOS",
                               "JOB_RESERVATION",
                               "JOB_NUM_NODES",
                               "JOB_NODELIST",
                               "NODE_ALIASES",
                               "TASKS_PER_NODE",
                               "NODENAME",
                               "NODEID",
                               "GTIDS",
                               "LOCALID",
                               "NTASKS",
                               "NTASKS_PER_CORE",
                               "NTASKS_PER_NODE",
                               "NTASKS_PER_SOCKET",
                               "JOB_CPUS_PER_NODE",
                               "CPUS_ON_NODE",
                               "CPUS_PER_TASK",
                               "CPUS_PER_GPU",
                               "GPUS",
                               "GPU_BIND",
                               "GPU_FREQ",
                               "GPUS_PER_NODE",
                               "GPUS_PER_SOCKET",
                               "GPUS_PER_TASK",
                               "ARRAY_JOB_ID",
                               "ARRAY_TASK_COUNT",
                               "ARRAY_TASK_ID",
                               "ARRAY_TASK_MIN",
                               "ARRAY_TASK_MAX",
                               "ARRAY_TASK_STEP",
                               "TASK_PID",
                               "PROCID",
                               "SUBMIT_DIR",
                               "SUBMIT_HOST",
                               "RESTART_COUNT",
                               "PROFILE",
                               "PRIO_PORCESS",
                               "MEM_PER_CPU",
                               "MEM_PER_GPU",
                               "MEM_PER_NODE",
                               "MEM_BIND",
                               "MEM_BIND_LIST",
                               "MEM_BIND_PREFER",
                               "MEM_BIND_TYPE",
                               "MEM_BIND_VERBOSE",
                               "TOPOLOGY_ADDR",
                               "TOPOLOGY_ADDR_PATTERN",
                               "EXPORT_ENV",
                               "DISTRIBUTION",
                               "DIST_PLANESIZE",
                               "CLUSTER_NAME"};

    for (const std::string &v : variables) {
      std::string envname = "SLURM_" + v;
      if (const char *val = std::getenv(envname.c_str())) {
        out << v << " = " << std::quoted(val) << '\n';
      }
    }
    out << '\n';
  }
}

static bool valid_toml_key(const char *str) {
  for (; *str; ++str) {
    if ((*str < 'a' || *str > 'z') && (*str < 'A' || *str > 'Z') &&
        (*str < '0' || *str > '9') && *str != '_' && *str != '-') {
      return false;
    }
  }
  return true;
}

static bool valid_toml_number(const char *str) {
  const char *endparse = str;
  emf::parse(emf::type<long double>{}, str, endparse);
  return endparse != str && endparse != nullptr && *endparse == '\0';
}

static void write_cache_data(std::ostream &out, int cpu) {
  std::filesystem::path base =
      "/sys/devices/system/cpu/cpu" + std::to_string(cpu) + "/cache";

  auto status = std::filesystem::status(base);
  if (!std::filesystem::exists(status) ||
      !std::filesystem::is_directory(status))
    return;

  for (size_t index = 0;; ++index) {
    std::filesystem::path cache_base = base / ("index" + std::to_string(index));

    status = std::filesystem::status(cache_base);
    if (!std::filesystem::exists(status) ||
        !std::filesystem::is_directory(status)) {
      break;
    }

    out << "\n[[processor.cache]]\n";

    std::string value;
    {
      std::ifstream level{cache_base / "level"};
      std::getline(level, value);
      out << "level = " << value << '\n';
    }
    {
      std::ifstream type{cache_base / "type"};
      std::getline(type, value);
      out << "type = " << std::quoted(value) << '\n';
    }
    {
      std::ifstream size{cache_base / "size"};
      std::getline(size, value);
      out << "size = " << std::quoted(value) << '\n';
    }
    {
      std::ifstream line{cache_base / "coherency_line_size"};
      std::getline(line, value);
      out << "line_size = " << value << '\n';
    }
    {
      std::ifstream cpus{cache_base / "shared_cpu_list"};
      std::getline(cpus, value);
      out << "cpus = " << std::quoted(value) << '\n';
    }
  }
}

static void write_cpu_data(std::ostream &out) {
  std::ifstream cpuinfo("/proc/cpuinfo");

  bool in_table = false;
  int processor = -1;
  for (std::string line; std::getline(cpuinfo, line);) {
    if (line.empty()) {
      in_table = false;
      continue;
    }

    if (!in_table) {
      if (processor >= 0) {
        write_cache_data(out, processor);
        processor = -1;
      }

      out << "\n[[processor]]\n";
      in_table = true;
    }

    size_t separator = line.find(':');
    if (separator == line.npos)
      continue;

    size_t key_start = line.find_first_not_of(" \t");
    size_t key_end = line.find_last_not_of(" \t", separator - 1);
    size_t value_start = line.find_first_not_of(" \t", separator + 1);
    size_t value_end = line.find_last_not_of(" \t");

    if (key_start == line.npos || key_end == line.npos ||
        value_start == line.npos || value_end == line.npos ||
        key_start >= key_end || value_start > value_end)
      continue;

    std::string key = line.substr(key_start, key_end - key_start + 1);
    std::string value = line.substr(value_start, value_end - value_start + 1);

    if (key == "processor")
      processor = std::stoi(value);

    if (key == "flags" || key == "bugs") {
      out << key << " = [ ";

      size_t vend = 0;
      for (size_t vstart = 0; vstart != value.npos;
           vstart = value.find_first_not_of(" \t", vend)) {
        vend = value.find_first_of(" \t", vstart);
        std::string v = value.substr(vstart, vend - vstart);
        out << std::quoted(v) << ", ";
      }
      out << "]";
    } else {

      if (valid_toml_key(key.c_str())) {
        out << key;
      } else {
        out << std::quoted(key);
      }

      out << " = ";

      if (valid_toml_number(value.c_str())) {
        out << value;
      } else {
        out << std::quoted(value);
      }
    }
    out << '\n';
  }

  if (processor >= 0)
    write_cache_data(out, processor);

  out << '\n';
}

void emf::experiment::write_meta_start(const std::vector<std::string> &args) {
  std::ofstream out{meta_file};

  out << "name = " << std::quoted(name()) << "\n\n";

  out << "command_line = [\n";
  for (const std::string &arg : args) {
    out << "  " << std::quoted(arg) << ",\n";
  }
  out << "]\n\n";

  out << "[parameters]\n";
  for (const option &opt : options) {
    if (opt.loud) {
      out << opt.long_name << " = " << opt.render_callback() << '\n';
    }
  }
  out << '\n';

  write_slurm_data(out);
  write_openmp_data(out);

  out << "[time]\nstart = " << rfc3339(current_local_time()) << '\n';

  std::ofstream system{results_directory / "system.toml"};
  write_cpu_data(system);
}

void emf::experiment::write_meta_end() {
  std::ofstream out{meta_file, std::ios_base::out | std::ios_base::app};

  out << "end = " << rfc3339(current_local_time()) << '\n';
}

static const char *severity_str(emf::severity s) {
  switch (s) {
  case emf::severity::debug:
    return " DEBUG";
  case emf::severity::info:
    return "  INFO";
  case emf::severity::notice:
    return "NOTICE";
  case emf::severity::warning:
    return "  WARN";
  case emf::severity::error:
    return " ERROR";
  default:
    return "UNKNWN";
  }
}

void emf::experiment::do_log(emf::severity sev, const char *tag,
                             const char *msg) {
  if (tag != nullptr) {
    if (sev < get_cutoff(tag))
      return;
  }

  std::lock_guard<std::mutex> guard(log_mutex);

  if (!log_stream.is_open()) {
    // Check that log_file is set.
    log_stream.open(log_file, std::ios::out | std::ios::app);
  }

  auto timestamp = std::chrono::steady_clock::now();

  log_stream << '[' << rfc3339(current_local_time()) << "]["
             << render_interval(timestamp - experiment_start) << "]["
             << render_interval(timestamp - last_log_message) << "]["
             << severity_str(sev) << "]";

  if (tag != nullptr) {
    auto last_tagged_message_it = last_tagged_message_times.find(tag);

    auto last_tagged_message_time =
        (last_tagged_message_it == last_tagged_message_times.end())
            ? timestamp
            : last_tagged_message_it->second;

    log_stream << "[" << render_interval(timestamp - last_tagged_message_time)
               << "]";

    last_tagged_message_times.insert_or_assign(tag, timestamp);

    log_stream << "<" << tag << ">";
  }

  log_stream << " " << msg << std::endl;

  last_log_message = timestamp;
}

void emf::experiment::log(emf::severity s, const char *tag, const char *msg) {
  instance_access_guard{}.get().do_log(s, tag, msg);
}

std::filesystem::path emf::experiment::datadir() {
  return instance_access_guard{}.get().data_directory;
}

namespace emf {

class duplicate_data_file : public std::logic_error {
public:
  duplicate_data_file(const char *name)
      : std::logic_error("Attempt to create existing data file "s + name) {}
};

} // namespace emf

std::ofstream emf::experiment::data_file(const char *name) {
  auto path = datadir() / name;
  const char *cpath = path.c_str();

  // Alas, for some reason C++ standard does not mandate a way to
  // create ofstream exclusively.  So we do the next best thing:
  // create a file using OS-specific interface, check that it didn't
  // exist before, and then quickly close and reopen it using C++
  // interface.  If nobody else uses the data directory, this should work.
  errno = 0;
  int fd = open(cpath, O_CLOEXEC | O_WRONLY | O_CREAT | O_EXCL,
                S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH);
  if (fd != -1) {
    close(fd);
  } else {
    int err = errno;
    errno = 0;
    if (err == EEXIST) {
      throw emf::duplicate_data_file(cpath);
    } else {
      throw std::system_error{std::error_code{err, std::system_category()},
                              cpath};
    }
  }

  return std::ofstream{path};
}

emf::experiment::~experiment() {
  write_meta_end();
  do_log(emf::severity::notice, nullptr, "experiment finished");
}

namespace emf {

class incomplete_parse : public std::runtime_error {
public:
  incomplete_parse(const char *leftover)
      : std::runtime_error("Incomplete parse: \""s + leftover +
                           "\" left as a leftover") {}
};

class no_parse : public std::runtime_error {
public:
  no_parse(const char *arg)
      : std::runtime_error("Unable to parse \""s + arg + "\"") {}
};

} // namespace emf

void emf::experiment::check_parse_errors(const char *arg,
                                         const char *endparse) {
  if (arg == endparse)
    throw emf::no_parse(arg);
  if (*endparse != '\0')
    throw emf::incomplete_parse(endparse);
}

namespace emf {
class missing_argument : public std::runtime_error {
public:
  missing_argument(const char *name)
      : std::runtime_error("No value provided for the required argument "s +
                           name) {}
};
} // namespace emf

void emf::experiment::report_missing_argument(const char *name) {
  throw emf::missing_argument(name);
}

namespace emf {
class uninitialised_experiment_instance : public std::logic_error {
public:
  uninitialised_experiment_instance()
      : std::logic_error("Global instance of emf::experiment is missing") {}
};
} // namespace emf

emf::experiment::instance_access_guard::instance_access_guard()
    : guard(instance_mutex), e(instance.get()) {
  if (e == nullptr)
    throw uninitialised_experiment_instance{};
}

std::unique_ptr<emf::experiment> emf::experiment::instance;
std::mutex emf::experiment::experiment_mutex;
std::recursive_mutex emf::experiment::instance_mutex;
