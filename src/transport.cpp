#include "../include/transport.hpp"
#include "../include/ttsuite/blapack.hpp"
#include "../include/ttsuite/log.hpp"
#include "include/ttsuite/low_dimensional.hpp"
#include "include/ttsuite/matrix_cross.hpp"
#include <algorithm>
#include <cmath>
#include <complex>
#include <limits>
#include <mkl.h>
#include <random>
#include <tuple>
#include <utility>

namespace Direct {

namespace Transfer {

namespace {
inline double MCFL(const double previous, const double current,
                   const double next) {
  double dp = current - previous, dn = next - current;

  if (std::signbit(dp) != std::signbit(dn)) {
    return 0.0;
  }
  // The formula is given in terms of their ratio, so, if the signs
  // match, they are irrelevant.
  dp = std::abs(dp);
  dn = std::abs(dn);

  if (dp >= 3 * dn) {
    return 2.0;
  }

  // At this point dp and dn are of the same sign, /and/ dn is
  // non-zero (otherwise dp >= 3*dn regardless of the value of dp) and
  // 0 <= dp < 3*dn => 0 <= r < 3
  const double r = dp / dn;

  if (r <= 1.0 / 3.0)
    return 2 * r;
  else
    return (1 + r) / 2;

  // const double r = (current - previous) / (next - current);
  // return std::max(0.0, std::min(2 * r, std::min(2.0, (1 + r) / 2)));
}

inline double transfer_df(double pre_f, double f, double nxt_f, double v,
                          double dt, double dx, double pre_C, double C) {
  const double df1 = C * nxt_f + (2.0 - C - pre_C) * f - (2.0 - pre_C) * pre_f;
  const double df2 = C * nxt_f - (C + pre_C) * f + pre_C * pre_f;

  return v * (-df1 + v * dt * df2 / dx) / (2.0 * dx);
}

void full_derivative(TT::LD::MatrixRef<double> df,
                     TT::LD::MatrixRef<const double> f,
                     TT::LD::VectorRef<const double> velocity, const double dt,
                     const double dx, Smoluchowski::Operator &S,
                     const PML &pml) {
  Impl::aggregation(df, f, S);
  Impl::transfer(df, f, velocity, dt, dx, pml);
}
} // namespace

namespace Impl {

void transfer(TT::LD::MatrixRef<double> df, TT::LD::MatrixRef<const double> f,
              TT::LD::VectorRef<const double> velocity, const double dt,
              const double dx, const PML &pml) {
  const size_t N = df.size(0);
  const size_t W = df.size(1);

// We assume the edges are filled from boundary conditions
#pragma omp parallel for
  for (size_t ix = 1; ix < W - 1; ++ix) {
    const double x = ix * dx;
    const double sigma = pml(x);
    for (size_t k = 0; k < N; ++k) {
      const double fm1 = f(k, ix - 1);
      const double f0 = f(k, ix);
      const double fp1 = f(k, ix + 1);
      const double fm2 = ix == 1 ? f(k, 0) : f(k, ix - 2);

      const double C = MCFL(fm1, f0, fp1);

      const double Cp = MCFL(fm2, fm1, f0);

      // const double df1 = C * fp1 + (2.0 - C - Cp) * f0 - (2.0 - Cp) * fm1;
      // const double df2 = C * fp1 - (C + Cp) * f0 + Cp * fm1;
      const double df0 = velocity(k) * sigma * f0;
      const double dff =
          transfer_df(fm1, f0, fp1, velocity(k), dt, dx, Cp, C)
          // -velocity(k) * df1 / (2.0 * dx) +
          // velocity(k) * velocity(k) * dt * df2 / (2.0 * dx * dx)
          - df0;

      df(k, ix) += dff;

      assert(df(k, ix) == df(k, ix));
    }
  }
}

void aggregation(TT::LD::MatrixRef<double> df,
                 TT::LD::MatrixRef<const double> f, Smoluchowski::Operator &S) {
  const size_t W = df.size(1);

#pragma omp parallel for
  for (size_t ix = 1; ix < W - 1; ++ix) {
    S.eval_quadratic(f.index_last(ix), df.index_last(ix));
  }
}

} // namespace Impl

void time_step(TT::LD::MatrixRef<double> df, TT::LD::MatrixRef<double> f,
               TT::LD::VectorRef<const double> velocity, const double dt,
               const double dx, Smoluchowski::Operator &S, const PML &pml) {
  full_derivative(df, f.cref(), velocity, dt, dx, S, pml);
  TT::BLAS::L1::axpy(dt, df.cref().flatten().strided(), f.flatten().strided());

  for (double &x : f) {
    if (x < 0)
      x = 0.0;
  }
}

void Simulation::step(double dt) {
  time_step(m_df.ref(), m_f.ref(), m_velocity.cref(), dt, m_dx, m_S, m_pml);
}

double Simulation::courant_dt() const {
  double maximum_velocity =
      *std::max_element(m_velocity.cbegin(), m_velocity.cend());
  return m_dx / maximum_velocity;
}

namespace LowRank {

void Matrix::round(double rtol, double atol, size_t max_rank) {
  const size_t r = rank();

  if (r >= m_U.size(0) || r >= m_V.size(0)) {
    // At least one of the factors is as large as the full matrix
    // would be.  So just compute that and do an SVD

    TT::LD::MatrixD full = materialise();
    const auto min_size = std::min(full.size(0), full.size(1));
    TT::LD::MatrixD aux{{min_size, min_size}};

    TT::LD::MatrixD *left = nullptr, *right = nullptr;

    if (full.size(0) >= full.size(1)) {
      left = &full;
      right = &aux;
    } else {
      left = &aux;
      right = &full;
    }
    TT::LD::VectorD sigma{{min_size}};
    TT::LD::StridedTensorRef<double, 2> left_ref = left->ref().strided(),
                                        right_ref = right->ref().strided();

    TT::LAPACK::gesdd(TT::LAPACK::svd_vectors::overwrite, full.ref().strided(),
                      sigma.ref(), &left_ref, &right_ref);

    const double tol = std::max(rtol * sigma(0), atol);
    const size_t new_rank = std::min(
        max_rank,
        static_cast<size_t>(std::find_if(sigma.cbegin(), sigma.cend(),
                                         [tol](double s) { return s < tol; }) -
                            sigma.cbegin()));

    left->resize_last(new_rank);
    m_U = std::move(*left);
    for (size_t j = 0; j < new_rank; ++j) {
      TT::BLAS::L1::scal(sigma(j), m_U.ref().index_last(j).strided());
    }

    m_V.resize_last(new_rank);
    m_V.ref().iterate(
        [right](double &x, size_t i, size_t j) { x = (*right)(j, i); });
    return;
  }

  // A = U V^T = Q R V^T = Q (V R^T)^T
  TT::LD::VectorD tau{{r}};
  TT::LAPACK::geqrf(m_U.ref().strided(), tau.ref());

  auto R = m_U.cref().strided().subslice({0, 0}, {r, r});
  TT::BLAS::L3::trmm(TT::BLAS::side::right, TT::BLAS::uplo::upper,
                     TT::BLAS::transpose::yes, TT::BLAS::diag::nonunit, 1.0, R,
                     m_V.ref().strided());

  // VR^T = X Σ Y^T; X gets written to V's storage
  TT::LD::VectorD sigma{{r}};
  TT::LD::MatrixD Yt{{r, r}};
  auto Ytref = Yt.ref().strided();
  TT::LAPACK::gesdd(TT::LAPACK::svd_vectors::overwrite, m_V.ref().strided(),
                    sigma.ref(), nullptr, &Ytref);

  // Truncate the SVD
  const double tol = std::max(rtol * sigma(0), atol);
  const size_t new_rank = std::min(
      max_rank,
      static_cast<size_t>(std::find_if(sigma.cbegin(), sigma.cend(),
                                       [tol](double s) { return s < tol; }) -
                          sigma.cbegin()));

  sigma.resize_last(new_rank);
  m_V.resize_last(new_rank);

  // A = Q (X Σ Y^T)^T = QY Σ X^T
  TT::LD::MatrixD new_U{{m_U.size(0), new_rank}};
  std::fill(new_U.begin(), new_U.end(), 0.0);
  for (size_t j = 0; j < new_rank; ++j)
    for (size_t i = 0; i < r; ++i) {
      new_U(i, j) = Yt(j, i);
    }

  TT::LAPACK::ormqr(TT::BLAS::side::left, TT::BLAS::transpose::no,
                    m_U.cref().strided(), tau.cref(), new_U.ref().strided());

  m_U = std::move(new_U);

  for (size_t j = 0; j < new_rank; ++j) {
    TT::BLAS::L1::scal(sigma(j), m_U.ref().index_last(j).strided());
  }
}

void Matrix::round_cross(double eps, size_t max_rank) {
  max_rank = std::min(max_rank, rank());
  std::pair<size_t, size_t> extent = nonzero_extent();

  std::mt19937_64 gen{};

  auto cross = TT::MatrixCross<double>::cross(
      [this](size_t row, TT::LD::VectorRefD dest) {
        TT::BLAS::L2::gemv(
            TT::BLAS::transpose::no, 1.0,
            m_V.cref().strided().subslice({0, 0}, {dest.size(), rank()}),
            m_U.cref().strided().template fix_idx<0>(row), 0.0, dest.strided());
      },
      [this](size_t col, TT::LD::VectorRefD dest) {
        TT::BLAS::L2::gemv(
            TT::BLAS::transpose::no, 1.0,
            m_U.cref().strided().subslice({0, 0}, {dest.size(), rank()}),
            m_V.cref().strided().template fix_idx<0>(col), 0.0, dest.strided());
      },
      std::min(extent.first + 1, m_U.size(0)),
      std::min(extent.second + 1, m_V.size(0)), eps, max_rank, gen, true);

  *this = from_cross(cross);
}

Matrix Matrix::apply(const Smoluchowski::Operator &S) const {
  const size_t r = rank();
  std::vector<Smoluchowski::Partial> partial;

  partial.reserve(r);
  for (size_t j = 0; j < r; ++j) {
    partial.push_back(S.partial(m_U.cref().index_last(j)));
  }

  const size_t new_rank = r * (r + 1) / 2;
  TT::LD::MatrixD new_U{{m_U.size(0), new_rank}},
      new_V{{m_V.size(0), new_rank}};

  using task = std::tuple<double, size_t, size_t>;
  std::vector<task> tasks;
  tasks.reserve(new_rank);
  for (size_t i = 0; i < r; ++i) {
    tasks.emplace_back(1.0, i, i);

    for (size_t j = i + 1; j < r; ++j) {
      tasks.emplace_back(2.0, i, j);
    }
  }

#pragma omp parallel for
  for (size_t i = 0; i < new_rank; ++i) {
    double c;
    size_t k, l;
    std::tie(c, k, l) = tasks.at(i);
    vdMul(static_cast<MKL_INT>(m_V.size(0)), m_V.cref().index_last(k).data(),
          m_V.cref().index_last(l).data(), new_V.ref().index_last(i).data());

    // Avoid overwriting border conditions
    new_V(0, i) = new_V(m_V.size(0) - 1, i) = 0.0;
    TT::BLAS::L1::scal(c, new_V.ref().index_last(i).strided());

    S.combine(partial.at(k), partial.at(l), new_U.ref().index_last(i));
  }

  return Matrix(new_U, new_V);
}

Matrix Matrix::hadamard(const Matrix &m) const {
  const size_t r1 = rank();
  const size_t r2 = m.rank();
  const size_t M = m_U.size(0);
  const size_t N = m_V.size(0);

  assert(M == m.m_U.size(0));
  assert(N == m.m_V.size(0));

  TT::LD::MatrixD new_U{{M, r1 * r2}}, new_V{{N, r1 * r2}};
  auto new_U_ref = new_U.ref().template reshape<3>({M, r1, r2});
  auto new_V_ref = new_V.ref().template reshape<3>({N, r1, r2});

  for (size_t j = 0; j < r2; ++j) {
    for (size_t i = 0; i < r1; ++i) {
      vdMul(static_cast<MKL_INT>(M), m_U.cref().index_last(i).data(),
            m.m_U.cref().index_last(j).data(),
            new_U_ref.index_last(j).index_last(i).data());
      vdMul(static_cast<MKL_INT>(N), m_V.cref().index_last(i).data(),
            m.m_V.cref().index_last(j).data(),
            new_V_ref.index_last(j).index_last(i).data());
    }
  }

  return Matrix(std::move(new_U), std::move(new_V));
}

Matrix Matrix::lcom(double a, const Matrix &other, double b) const {
  const size_t r1 = rank();
  const size_t r2 = other.rank();
  const size_t M = m_U.size(0);
  const size_t N = m_V.size(0);

  assert(M == other.m_U.size(0));
  assert(N == other.m_V.size(0));

  TT::LD::MatrixD new_U{{M, r1 + r2}}, new_V{{N, r1 + r2}};

  TT::BLAS::L1::copy(m_U.cref().flatten().strided(),
                     new_U.ref().slice_last(0, r1).flatten().strided());
  TT::BLAS::L1::copy(other.m_U.cref().flatten().strided(),
                     new_U.ref().slice_last(r1, r2).flatten().strided());

  TT::BLAS::L1::scal(a, new_U.ref().slice_last(0, r1).flatten().strided());
  TT::BLAS::L1::scal(b, new_V.ref().slice_last(r1, r2).flatten().strided());

  TT::BLAS::L1::copy(m_V.cref().flatten().strided(),
                     new_V.ref().slice_last(0, r1).flatten().strided());
  TT::BLAS::L1::copy(other.m_V.cref().flatten().strided(),
                     new_V.ref().slice_last(r1, r2).flatten().strided());

  return Matrix(std::move(new_U), std::move(new_V));
}

void copy_padded(TT::LD::MatrixRef<const double> src, TT::LD::MatrixRefD dest) {
  if (src.size(0) == dest.size(0)) {
    TT::BLAS::L1::copy(src.flatten().strided(), dest.flatten().strided());
  } else {
    for (size_t j = 0; j < src.size(1); ++j) {
      auto src_col = src.index_last(j);
      auto dest_col = dest.index_last(j);
      TT::BLAS::L1::copy(src_col.strided(),
                         dest_col.slice_last(0, src_col.size()).strided());
      std::fill(dest_col.begin() + src_col.size(), dest_col.end(), 0.0);
    }
  }
}

TT::LD::MatrixD hcat_padded(TT::LD::MatrixRef<const double> A,
                            TT::LD::MatrixRef<const double> B) {
  const size_t M1 = A.size(0);
  const size_t N1 = A.size(1);
  const size_t M2 = B.size(0);
  const size_t N2 = B.size(1);

  const size_t M = std::max(M1, M2);

  TT::LD::MatrixD result{{M, N1 + N2}};

  copy_padded(A, result.ref().slice_last(0, N1));
  copy_padded(B, result.ref().slice_last(N1, N2));

  return result;
}

Matrix Matrix::lcom_padded(TT::LD::MatrixRef<const double> aU,
                           TT::LD::MatrixRef<const double> aV, double a,
                           TT::LD::MatrixRef<const double> bU,
                           TT::LD::MatrixRef<const double> bV, double b) {
  const size_t r1 = aU.size(1);
  assert(r1 == aV.size(1));
  const size_t r2 = bU.size(1);
  assert(r2 == bV.size(1));

  TT::LD::MatrixD new_U = hcat_padded(aU, bU), new_V = hcat_padded(aV, bV);

  TT::BLAS::L1::scal(a, new_U.ref().slice_last(0, r1).flatten().strided());
  TT::BLAS::L1::scal(b, new_U.ref().slice_last(r1, r2).flatten().strided());

  return Matrix(std::move(new_U), std::move(new_V));
}

Matrix Matrix::lcom_padded(double a, const Matrix &other, double b) const {
  return lcom_padded(m_U.cref(), m_V.cref(), a, other.m_U.cref(),
                     other.m_V.cref(), b);
}

Matrix Matrix::lcom_padded(double a, const TT::MatrixCross<double> &cross,
                           double b) const {
  return lcom_padded(m_U.cref(), m_V.cref(), a, cross.u(), cross.v(), b);
}

Matrix Matrix::pad(size_t M, size_t N) const {
  const size_t R = rank();
  M = std::max(m_U.size(0), M);
  N = std::max(m_V.size(0), N);

  TT::LD::MatrixD new_U{{M, R}}, new_V{{N, R}};

  copy_padded(m_U.cref(), new_U.ref());
  copy_padded(m_V.cref(), new_V.ref());
  return Matrix(std::move(new_U), std::move(new_V));
}

size_t nonzero_row_extent(TT::LD::MatrixRef<const double> A) {
  const double nrm = TT::BLAS::L1::nrm2(A.flatten().strided());

  size_t extent = 0;
  const double threshold = nrm * std::numeric_limits<double>::epsilon();
  for (size_t j = 0; j < A.size(1); ++j) {
    size_t i = A.size(0);
    for (; i > 0; --i) {
      if (std::abs(A(i - 1, j)) > threshold)
        break;
    }

    if (i > extent)
      extent = i;
  }

  return extent;
}

std::pair<size_t, size_t> Matrix::nonzero_extent() const {
  return std::make_pair(nonzero_row_extent(m_U.cref()),
                        nonzero_row_extent(m_V.cref()));
}

void Matrix::write_transport_df_row_partial(
    size_t row, TT::LD::VectorRefD dest,
    TT::LD::VectorRef<const double> velocity, const double dt,
    const double dx) const {
  const size_t R = m_U.size(1), n = dest.size();

  TT::BLAS::L2::gemv(TT::BLAS::transpose::no, 1.0,
                     m_V.cref().strided().subslice({0, 0}, {n, R}),
                     m_U.cref().strided().template fix_idx<0>(row), 0.0,
                     dest.strided());

  const double v = velocity(row);

  double pre_f = dest(0);
  double f = pre_f;
  double nxt_f = dest(1);
  //dest(0) = 0.0;
  double pre_C = MCFL(pre_f, f, nxt_f);

  for (size_t i = 1; i < n && i < m_V.size(0) - 1; ++i) {
    pre_f = f;
    f = nxt_f;
    nxt_f = (i == n - 1)
                ? (n == m_V.size(0)
                       ? 0.0
                       : TT::BLAS::L1::dot(
                             m_V.cref().strided().template fix_idx<0>(i + 1),
                             m_U.cref().strided().template fix_idx<0>(row)))
                : dest(i + 1);
    double C = MCFL(pre_f, f, nxt_f);
    dest(i) = f + dt * transfer_df(pre_f, f, nxt_f, v, dt, dx, pre_C, C);
    pre_C = C;
  }

  if (m_V.size(0) == n) {
    //dest(n - 1) = 0.0;
  }
}

void Matrix::write_transport_df_col_partial(
    size_t col, TT::LD::VectorRefD dest,
    TT::LD::VectorRef<const double> velocity, const double dt, const double dx,
    TT::LD::VectorD &work) const {
  const size_t N = m_V.size(0), R = m_U.size(1), m = dest.size();

  if (col == 0 || col == N - 1) {
    TT::BLAS::L2::gemv(TT::BLAS::transpose::no, 1.0,
                       m_U.cref().strided().subslice({0, 0}, {m, R}),
                       m_V.cref().strided().template fix_idx<0>(col), 0.0,
                       dest.strided());
    // std::fill(dest.begin(), dest.end(), 0.0);
    return;
  }

  work.resize_last(4 * m);
  auto work_ref = work.ref().template reshape<2>({m, 4});

  size_t base_idx = std::max(col, static_cast<size_t>(2)) - 2;
  size_t last_idx = col + 1;

  TT::BLAS::L3::gemm(TT::BLAS::transpose::no, TT::BLAS::transpose::yes, 1.0,
                     m_U.cref().strided().subslice({0, 0}, {m, R}),
                     m_V.cref().strided().subslice(
                         {base_idx, 0}, {last_idx - base_idx + 1, R}),
                     0.0,
                     work_ref.strided().subslice({0, base_idx + 2 - col},
                                                 {m, last_idx - base_idx + 1}));

  if (base_idx + 1 == col) {
    TT::BLAS::L1::copy(work_ref.cref().index_last(1).strided(),
                       work_ref.index_last(0).strided());
  }

  for (size_t i = 0; i < m; ++i) {
    const double prepre_f = work_ref(i, 0), pre_f = work_ref(i, 1),
                 f = work_ref(i, 2), nxt_f = work_ref(i, 3);
    const double pre_C = MCFL(prepre_f, pre_f, f), C = MCFL(pre_f, f, nxt_f);
    dest(i) =
        f + dt * transfer_df(pre_f, f, nxt_f, velocity(i), dt, dx, pre_C, C);
  }
}

Matrix Matrix::from_pml(size_t M, size_t N, double dx, const PML &pml,
                        TT::LD::VectorRef<const double> velocity) {
  TT::LD::MatrixD U{{M, 1}}, V{{N, 1}};
  TT::BLAS::L1::copy(velocity.strided(), U.ref().flatten().strided());
  for (size_t i = 0; i < N; ++i) {
    V(i, 0) = pml(dx * i);
  }

  return Matrix(std::move(U), std::move(V));
}

Matrix Matrix::rank_1(TT::LD::VectorRef<const double> u,
                      TT::LD::VectorRef<const double> v) {
  return Matrix(u.template reshape<2>({u.size(), 1}),
                v.template reshape<2>({v.size(), 1}));
}

Matrix Matrix::from_cross(const TT::MatrixCross<double> &cross) {
  return Matrix(cross.u(), cross.v());
}

TT::LD::MatrixD Matrix::materialise() const {
  TT::LD::MatrixD result{{m_U.size(0), m_V.size(0)}};
  TT::BLAS::L3::gemm(TT::BLAS::transpose::no, TT::BLAS::transpose::yes, 1.0,
                     m_U.cref().strided(), m_V.cref().strided(), 0.0,
                     result.ref().strided());
  return result;
}

void Simulation::step(double dt, double rtol, double atol, size_t max_rank) {
  TT::LD::VectorD tmp;
  auto nz_extent = m_f.nonzero_extent();
  nz_extent.second = std::min(nz_extent.second + 1, m_pml.column_count());
  auto transport_df = TT::MatrixCross<double>::cross(
      [this, dt](size_t row, TT::LD::VectorRefD dest) {
        m_f.write_transport_df_row_partial(row, dest, m_velocity.cref(), dt,
                                           m_dx);
      },
      [this, dt, &tmp](size_t col, TT::LD::VectorRefD dest) mutable {
        m_f.write_transport_df_col_partial(col, dest, m_velocity.cref(), dt,
                                           m_dx, tmp);
      },
      nz_extent.first, nz_extent.second, rtol, max_rank, m_gen, true);

  auto pml_df = m_pml.hadamard(m_f);
  pml_df.round(rtol, atol, max_rank);

  auto S_df = m_f.apply(m_S);
  // TODO: more sensible precision control in cross approximation
  S_df.round_cross(1e-10);
  if (S_df.rank() > 0) {
    S_df.round(rtol, atol, max_rank);
  }

  m_f = Matrix::from_cross(transport_df);
  m_f.round(rtol, atol, max_rank);

  m_f = m_f.lcom_padded(1.0, pml_df, -dt);
  m_f.round(rtol, atol, max_rank);

  m_f = m_f.lcom_padded(1.0, S_df, dt);
  m_f.round(rtol, atol, max_rank);
}

} // namespace LowRank

} // namespace Transfer

} // namespace Direct
