#include "../include/jacobi_solver.hpp"
#include "../include/preconditioner.hpp"
#include "../include/ttsuite/gmres.hpp"
#include "../include/ttsuite/log.hpp"
#include <fstream>
#include <sstream>

#define TRACKING 0

namespace Direct {

namespace {
void write_vector(std::ostream &os,
                  TT::LowDimensional::VectorRef<const double> v) {
  for (size_t i = 0; i < v.size(); ++i)
    os << i << '\t' << v(i) << std::endl;
}
} // namespace

void solve_jacobi(double Icoef, double Jcoef,
                  TT::LowDimensional::VectorRef<const double> at,
                  TT::LowDimensional::VectorRef<const double> rhs,
                  TT::LowDimensional::VectorRefD x, const size_t max_iters,
                  const double eps, const size_t p,
                  TT::MatrixCross<double> &kernel,
                  Smoluchowski::Operator &op) {
  auto prec = Jacobi::Preconditioner(rhs.size(), kernel.rank(), kernel, 0.0, at,
                                     p, Icoef, Jcoef);

  ttlog_debug("Solving eq. with Icoef = %g, Jcoef = %g", Icoef, Jcoef);

  auto left = op.partial(at);
  auto iters = TT::gmres(
      [&left, &op, Icoef,
       Jcoef](TT::LowDimensional::VectorRef<const double> x,
              TT::LowDimensional::VectorRefD y) {
        auto right = op.partial(x);
        op.combine(left, right, y);
        TT::BLAS::L1::scal(2.0 * Jcoef, y.strided());
        TT::BLAS::L1::axpy(Icoef, x.strided(), y.strided());
      },
      [&prec](TT::LowDimensional::VectorRefD x) { prec.apply(x.data()); },
      rhs.cref(), x, max_iters, max_iters, eps);
  ttlog_debug("GMRES took %u iterations", (unsigned)iters);

#if TRACKING
  {
    static unsigned invocation = 0;
    std::ostringstream ostr;
    ostr << "snapshots/solve_jacobi/" << invocation << "/";

    {
      std::ofstream at_f(ostr.str() + "at.data");
      write_vector(at_f, at);
    }
    {
      std::ofstream rhs_f(ostr.str() + "rhs.data");
      write_vector(rhs_f, rhs);
    }
    {
      std::ofstream x_f(ostr.str() + "x.data");
      write_vector(x_f, x.cref());
    }
    ++invocation;
  }
#endif
}

} // namespace Direct
