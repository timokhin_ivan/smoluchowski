#include "../include/convolution.hpp"
#include "../include/csv.hpp"
#include "../include/integration.hpp"
#include "../include/jacobi_solver.hpp"
#include "../include/qkrylov.hpp"
#include "../include/reduction.hpp"
#include "../include/smoluchowski.hpp"
#include "../include/ttsuite/blapack.hpp"
#include "../include/ttsuite/log.hpp"
#include <algorithm>
#include <chrono>
#include <fstream>
#include <iomanip>
#include <iterator>
#include <sstream>
#include <string>

int main_tt(const double a, const size_t logN, const size_t maxR,
            const double lambda = 0.0) {
  const size_t N = size_t{1} << logN;
  const size_t K = 50;

#if 1
  auto kernel_f = [a](std::array<size_t, 2> idx) {
    auto i = idx[0] + 1, j = idx[1] + 1;
    auto i_a = std::pow(i, a), j_a = std::pow(j, a);
    return i_a / j_a + j_a / i_a;
  };
#else
  auto kernel_f = [](std::array<size_t, 2>) -> double {
    return 1; // idx[0] + idx[1];
  };
#endif

  std::mt19937_64 gen{};

  auto start = std::chrono::steady_clock::now();
  auto kernel =
      TT::TTT<double, 2>::dmrg_cross(kernel_f, {N, N}, 1e-8, 100, 200, gen);
  auto end = std::chrono::steady_clock::now();
  std::chrono::duration<double> diff = end - start;
  kernel.round(0.0, maxR);
  std::cerr << kernel.max_rank() << '\t' << diff.count() << std::endl;

  std::uniform_int_distribution<size_t> dist{0, N - 1};

  const size_t M = 100000;
  double diff_nrm = 0.0, nrm = 0.0;
  for (size_t i = 0; i < M; ++i) {
    std::array<size_t, 2> idx = {dist(gen), dist(gen)};
    double ref = kernel_f(idx);
    double d = kernel.element_at(idx.begin(), idx.end()) - ref;
    diff_nrm += d * d;
    nrm += ref * ref;
  }

  std::cerr << " || " << std::sqrt(diff_nrm / nrm) << std::endl;

  start = std::chrono::steady_clock::now();
  auto op = kernel.extend_repeating(N) *
            (Smoluchowski::production<double>(N) -
             Smoluchowski::consumption<double>(N, false) * (1 + lambda) -
             Smoluchowski::consumption<double>(N, true) * (1 + lambda) +
             lambda * Smoluchowski::fragmentation_products<double>(N)) /
            2;
  auto err = op.round(1e-10);
  op.flip3dim();
  end = std::chrono::steady_clock::now();
  diff = end - start;

  std::cerr << op.max_rank() << '\t' << diff.count() << std::endl;
  std::cerr << op.internal_dimensionality() << std::endl;
  std::cerr << "Total rounding error: " << err << std::endl;

  auto mass_tester = TT::TTT<double, 1>::dmrg_cross(
      [](std::array<size_t, 1> idx) -> double { return idx[0] + 1; }, {N}, 1e-8,
      100, 200, gen);
  auto sum_tester = TT::TTT<double, 1>::dmrg_cross(
      [](std::array<size_t, 1>) -> double { return 1; }, {N}, 1e-8, 100, 200,
      gen);
  auto source = TT::TTT<double, 1>::delta({N}, {0});// + (1.0/8) * TT::TTT<double, 1>::delta({N}, {256});
  source.round(1e-18);

  // auto op_f = [&op, &source](const TT::TTT<double, 1> x) -> TT::TTT<double,
  // 1> {
  //   auto temp = op.tdot(x);
  //   temp.round(1e-10);
  //   return temp.tdot(x) + source;
  // };

  auto x = sum_tester / (static_cast<double>(N));
  std::cerr << x.dot(mass_tester) << std::endl;
  auto olddir = x;

  start = std::chrono::steady_clock::now();
  for (size_t k = 0; k < K; ++k) {
    // x = Smoluchowski::ttt_rk4_step(op_f, dt, x, 1e-12, 200);
    auto interm = op.tdot(x);
    interm.round(1e-18);

    auto S = interm.tdot(x) + source;
    S.round(1e-18);

    auto J = 2.0 * interm;

    if (k == 0)
      olddir = S;
    auto dir = olddir;
    dir.round(1e-18);
    auto rhs_norm = std::sqrt(S.dot(S));
    if (rhs_norm < 5e-12)
      break;
    J.amen(S, dir, 1e-6 * rhs_norm, 1e-10 * rhs_norm, 6, 16, 256,
           1e-10 * rhs_norm);

    x = x - dir;
    x.round(std::min(1e-8, rhs_norm * 1e-5), maxR);

    // size_t negatives = 0;
    // for (std::array<size_t, 1> ix{0}; ix[0] < N; ++ix[0]) {
    //   if (x.element_at(ix.cbegin(), ix.cend()) < 0)
    //     ++negatives;
    // }

    std::cerr << k << '\t' << x.max_rank() << '\t' << '\t' << rhs_norm << '\t'
              << "\t % " << "??" << std::endl;
    olddir = dir;
  }
  end = std::chrono::steady_clock::now();
  diff = end - start;
  std::cerr << diff.count() << std::endl;

  std::ostringstream ostr;
  ostr << "data/solution_" << a << "_" << logN << "_" << maxR << "_" << lambda;
  std::ofstream fout(ostr.str());
  for (std::array<size_t, 1> i = {0}; i[0] < std::min(size_t{1024}, N);
       ++i[0]) {
    fout << i[0] + 1 << '\t' << x.element_at(i.cbegin(), i.cend()) << std::endl;
  }

  for (std::array<size_t, 1> i = {1024}; i[0] < N; i[0] = (i[0] * 31) / 30) {
    fout << i[0] + 1 << '\t' << x.element_at(i.cbegin(), i.cend()) << std::endl;
  }

  std::array<size_t, 1> i = {N - 1};
  fout << N << '\t' << x.element_at(i.cbegin(), i.cend()) << std::endl;

  // std::ostringstream ostr2;
  // ostr2 << "data/negatives_" << a << "_" << logN << "_" << maxR;
  // std::ofstream negs(ostr2.str());
  // for (std::array<size_t, 1> i = {0}; i[0] < N; ++i[0]) {
  //   const double val = x.element_at(i.cbegin(), i.cend());
  //   if (i[0] % 1000000 == 0)
  //     std::cout << i[0] << '\t' << N << std::endl;
  //   if (val < 0) {
  //     negs << i[0] + 1 << '\t' << val << std::endl;
  //     std::cerr << "HIT!\n";
  //   }
  // }

  // for (std::array<size_t, 1> i = {0}; i[0] < N; ++i[0]) {
  //   std::cout << i[0] + 1 << '\t' << x.element_at(i.cbegin(), i.cend())
  //             << std::endl;
  // }

  return 0;
}

int main_direct() {
  const size_t N = 65536;
  const double a = 0.3;
  auto kernel = [a](size_t i, size_t j) -> double {
    const double i_a = std::pow(i + 1, a), j_a = std::pow(j + 1, a);
    return i_a / j_a + j_a / i_a;
  };

  const double safe_dt = 1.0 / 256;

  auto gen = std::mt19937_64{};
  auto kernel_cross =
      TT::MatrixCross<double>::cross(kernel, N, N, 1e-10, 4, gen);
  auto S = Direct::Smoluchowski::Operator(kernel_cross.u(), kernel_cross.v());

  const double T = 40.0;
  const double sampling_dt = 2.0;
  const size_t TotalSamples =
      static_cast<size_t>(std::ceil(T / sampling_dt)) + 1;

  using namespace TT::LowDimensional;

  if (0) {
    MatrixD reference_samples{{N, TotalSamples}};
    {
      VectorD x{{N}};
      x(0) = 1.0;
      std::fill(x.begin() + 1, x.end(), 0.0);
      double last_sample = -1.0;
      size_t next_sample_ix = 0;

      std::cerr << "Integrating... " << std::endl;
      Integration::Explicit::midpoint(
          [&S](double, VectorRef<const double> x, VectorRefD y) mutable {
            S.eval_quadratic(x, y);
            y(0) += 1.0;
          },
          x.ref(), 0.0, T, safe_dt,
          [&last_sample, &reference_samples, &next_sample_ix, sampling_dt,
           TotalSamples](VectorRefD x, double t, size_t) mutable {
            for (auto &v : x)
              if (v < 0)
                v = 0;
            if (t >= last_sample + sampling_dt) {
              TT::BLAS::L1::copy(
                  x.cref().strided(),
                  reference_samples.ref().index_last(next_sample_ix).strided());
              std::cerr << "Sample " << next_sample_ix << " out of "
                        << TotalSamples << std::endl;
              ++next_sample_ix;
              last_sample = t;
            }
          });
    }

    // For now, we just write out the results to files
    for (size_t i = 0; i < TotalSamples; ++i) {
      std::ostringstream ostr;
      ostr << "snapshots/reference_" << i << ".data";
      std::ofstream out(ostr.str());
      for (size_t j = 0; j < N; ++j)
        out << j + 1 << '\t' << reference_samples(j, i) << std::endl;
    }
  }

  if (0) {
    MatrixD samples{{N, TotalSamples}};
    for (double dt = safe_dt; dt < sampling_dt / 2; dt *= 2) {
      {
        VectorD x{{N}};
        x(0) = 1.0;
        std::fill(x.begin() + 1, x.end(), 0.0);
        double last_sample = -1.0;
        size_t next_sample_ix = 0;

        std::cerr << "Integrating... " << std::endl;
        Integration::Explicit::midpoint(
            [&S](double, VectorRef<const double> x, VectorRefD y) mutable {
              S.eval_quadratic(x, y);
              y(0) += 1.0;
            },
            x.ref(), 0.0, T, dt,
            [&last_sample, &samples, &next_sample_ix, sampling_dt,
             TotalSamples](VectorRefD x, double t, size_t) mutable {
              for (auto &v : x)
                if (v < 0)
                  v = 0;
              if (t >= last_sample + sampling_dt) {
                TT::BLAS::L1::copy(
                    x.cref().strided(),
                    samples.ref().index_last(next_sample_ix).strided());
                std::cerr << "Sample " << next_sample_ix << " out of "
                          << TotalSamples << std::endl;
                ++next_sample_ix;
                last_sample = t;
              }
            });
      }

      // For now, we just write out the results to files
      for (size_t i = 0; i < TotalSamples; ++i) {
        std::ostringstream ostr;
        ostr << "snapshots/direct_" << dt << "_" << i << ".data";
        std::ofstream out(ostr.str());
        for (size_t j = 0; j < N; ++j)
          out << j + 1 << '\t' << samples(j, i) << std::endl;
      }
    }
  }

  if (0) {
    MatrixD samples{{N, TotalSamples}};
    for (double dt = 16 * safe_dt; dt < sampling_dt / 2; dt *= 2) {
      {
        VectorD x{{N}};
        x(0) = 1.0;
        std::fill(x.begin() + 1, x.end(), 0.0);
        double last_sample = -1.0;
        size_t next_sample_ix = 0;

        std::cerr << "Integrating... " << std::endl;
        Integration::Rosenbrock::midpoint(
            [&S](double, VectorRef<const double> x, VectorRefD y) mutable {
              S.eval_quadratic(x, y);
              y(0) += 1.0;
            },
            [&S, &kernel_cross](double Icoef, double Jcoef,
                                VectorRef<const double> at,
                                VectorRef<const double> rhs, VectorRefD shift) {
              Direct::solve_jacobi(Icoef, Jcoef, at, rhs, shift, 512, 1e-10,
                                   256, kernel_cross, S);
            },
            x.ref(), 0.0, T, dt,
            [&last_sample, &samples, &next_sample_ix, sampling_dt,
             TotalSamples](VectorRefD x, double t, size_t) mutable {
              for (auto &v : x)
                if (v < 0)
                  v = 0;
              if (t >= last_sample + sampling_dt) {
                TT::BLAS::L1::copy(
                    x.cref().strided(),
                    samples.ref().index_last(next_sample_ix).strided());
                std::cerr << "Sample " << next_sample_ix << " out of "
                          << TotalSamples << std::endl;
                ++next_sample_ix;
                last_sample = t;
              }
            });
      }

      // For now, we just write out the results to files
      for (size_t i = 0; i < TotalSamples; ++i) {
        std::ostringstream ostr;
        ostr << "snapshots/rosenbrock_" << dt << "_" << i << ".data";
        std::ofstream out(ostr.str());
        for (size_t j = 0; j < N; ++j)
          out << j + 1 << '\t' << samples(j, i) << std::endl;
      }
    }
  }

  if (1) {
    MatrixD samples{{N, TotalSamples}};
    for (double dt = 16 * safe_dt; dt < sampling_dt / 2; dt *= 2) {
      {
        VectorD x{{N}};
        x(0) = 1.0;
        std::fill(x.begin() + 1, x.end(), 0.0);
        double last_sample = -1.0;
        size_t next_sample_ix = 0;

        std::cerr << "Integrating... " << std::endl;
        Integration::Implicit::midpoint_newton(
            [&S](double, VectorRef<const double> x, VectorRefD y) mutable {
              S.eval_quadratic(x, y);
              y(0) += 1.0;
            },
            [&S, &kernel_cross](double Icoef, double Jcoef,
                                VectorRef<const double> at,
                                VectorRef<const double> rhs, VectorRefD shift) {
              Direct::solve_jacobi(Icoef, Jcoef, at, rhs, shift, 512, 1e-10,
                                   256, kernel_cross, S);
            },
            64, 1e-10, x.ref(), 0.0, T, dt,
            [&last_sample, &samples, &next_sample_ix, sampling_dt,
             TotalSamples](VectorRefD x, double t, size_t) mutable {
              for (auto &v : x)
                if (v < 0)
                  v = 0;
              if (t >= last_sample + sampling_dt) {
                TT::BLAS::L1::copy(
                    x.cref().strided(),
                    samples.ref().index_last(next_sample_ix).strided());
                std::cerr << "Sample " << next_sample_ix << " out of "
                          << TotalSamples << std::endl;
                ++next_sample_ix;
                last_sample = t;
              }
            });
      }

      // For now, we just write out the results to files
      for (size_t i = 0; i < TotalSamples; ++i) {
        std::ostringstream ostr;
        ostr << "snapshots/newton_" << dt << "_" << i << ".data";
        std::ofstream out(ostr.str());
        for (size_t j = 0; j < N; ++j)
          out << j + 1 << '\t' << samples(j, i) << std::endl;
      }
    }
  }

  return 0;
}

int main_red() {
  using namespace TT;
  using namespace TT::LowDimensional;

  const size_t N = 65536;
  const double a = 0.3;
  auto kernel = [a](size_t i, size_t j) -> double {
    const double i_a = std::pow(i + 1, a), j_a = std::pow(j + 1, a);
    return i_a / j_a + j_a / i_a;
  };

  const double safe_dt = 1.0 / 4096;

  auto gen = std::mt19937_64{};
  auto kernel_cross =
      TT::MatrixCross<double>::cross(kernel, N, N, 1e-10, 4, gen);
  auto S = Direct::Smoluchowski::Operator(kernel_cross.u(), kernel_cross.v());

  const double T = 360.0;
  const double sampling_dt = 1.0 / 32;
  const size_t TotalSamples =
      static_cast<size_t>(std::ceil(T / sampling_dt)) + 1;

  using namespace TT::LowDimensional;

  MatrixD reference_samples{{N, TotalSamples}};
  size_t actual_samples = 0;
  std::chrono::duration<double> reference_time, reduced_time;
  if (1) {
    {
      VectorD x{{N}};
      x(0) = 1.0;
      std::fill(x.begin() + 1, x.end(), 0.0);
      double last_sample = -1.0;
      size_t next_sample_ix = 0;

      std::cerr << "Integrating... " << std::endl;
      auto start = std::chrono::steady_clock::now();
      Integration::Explicit::midpoint(
          [&S](double, VectorRef<const double> x, VectorRefD y) mutable {
            S.eval_quadratic(x, y);
            y(0) += 1.0;
          },
          x.ref(), 0.0, T, safe_dt,
          [&last_sample, &reference_samples, &next_sample_ix, sampling_dt,
           TotalSamples](VectorRefD x, double t, size_t step) mutable {
            for (auto &v : x) {
              if (v < 0)
                v = 0;
              if (v != v)
                throw step;
            }
            if (t >= last_sample + sampling_dt) {
              TT::BLAS::L1::copy(
                  x.cref().strided(),
                  reference_samples.ref().index_last(next_sample_ix).strided());
              std::cerr << "Sample " << next_sample_ix << " out of "
                        << TotalSamples << std::endl;
              ++next_sample_ix;
              last_sample = t;
            }
          });
      actual_samples = next_sample_ix;
      reference_samples.resize_last(actual_samples);
      reference_time = std::chrono::steady_clock::now() - start;
    }

    // For now, we just write out the results to files
    // for (size_t i = 0; i < actual_samples; ++i) {
    //   std::ostringstream ostr;
    //   ostr << "snapshots/reference_" << i << ".data";
    //   std::ofstream out(ostr.str());
    //   for (size_t j = 0; j < N; ++j)
    //     out << j + 1 << '\t' << reference_samples(j, i) << std::endl;
    // }
  }

  for (size_t samples_used = actual_samples; samples_used > 500;
       samples_used -= 500) {

    MatrixD used_samples = reference_samples;
    used_samples.resize_last(samples_used);

    MatrixD basis{{N, samples_used}};
    auto bref = basis.ref().strided();
    VectorD s{{samples_used}}, superb{{samples_used}};
    TT::LAPACK::gesvd(
        TT::LAPACK::svd_vectors::significant, TT::LAPACK::svd_vectors::none,
        used_samples.ref().strided(), s.ref(), &bref, nullptr, superb.ref());

    double eps = s(0) * 1e-10;
    std::size_t rank = std::min(
        samples_used, static_cast<size_t>(
                          std::find_if(s.begin(), s.end(),
                                       [eps](double sv) { return sv < eps; }) -
                          s.begin()));
    basis.resize_last(rank);
    std::cerr << "Rank = " << rank << std::endl;

    auto reduced = Reduction::reduced_operator(S, basis.cref());

    MatrixD reduced_samples{{rank, actual_samples}};

    {
      VectorD x{{N}};
      x(0) = 1.0;
      std::fill(x.begin() + 1, x.end(), 0.0);

      VectorD y{{rank}};
      BLAS::L2::gemv(BLAS::transpose::yes, 1.0, basis.cref().strided(),
                     x.cref().strided(), 0.0, y.ref().strided());
      VectorD source = y;

      double last_sample = -1.0;
      size_t next_sample_ix = 0;

      std::cerr << "Integrating..." << std::endl;
      MatrixD tmp{{rank, rank}};
      auto start = std::chrono::steady_clock::now();
      Integration::Explicit::midpoint(
          [&reduced, &tmp, rank, &source](double, VectorRef<const double> x,
                                          VectorRefD y) mutable {
            BLAS::L1::copy(source.cref().strided(), y.strided());
            BLAS::L2::gemv(
                BLAS::transpose::no, 1.0,
                reduced.cref()
                    .template reshape<2>({rank * rank, rank})
                    .strided(),
                x.strided(), 0.0,
                tmp.ref().template reshape<1>({rank * rank}).strided());
            BLAS::L2::gemv(BLAS::transpose::no, 1.0, tmp.cref().strided(),
                           x.strided(), 1.0, y.strided());
          },
          y.ref(), 0.0, T, safe_dt,
          [&last_sample, &reduced_samples, &next_sample_ix,
           sampling_dt](VectorRefD x, double t, size_t) mutable {
            if (t >= last_sample + sampling_dt) {
              TT::BLAS::L1::copy(
                  x.cref().strided(),
                  reduced_samples.ref().index_last(next_sample_ix).strided());
              ++next_sample_ix;
              last_sample = t;
            }
          });
      reduced_time = std::chrono::steady_clock::now() - start;
    }

    VectorD decompressedSample{{N}};
    // For now, we just write out the results to files
    // for (size_t i = 0; i < actual_samples; ++i) {
    //   std::ostringstream ostr;
    //   ostr << "snapshots/reduced_" << samples_used << "_" << i << ".data";
    //   std::ofstream out(ostr.str());
    //   BLAS::L2::gemv(BLAS::transpose::no, 1.0, basis.cref().strided(),
    //                  reduced_samples.cref().index_last(i).strided(), 0.0,
    //                  decompressedSample.ref().strided());
    //   for (size_t j = 0; j < N; ++j)
    //     out << j + 1 << '\t' << decompressedSample(j) << std::endl;
    // }

    std::cerr << reference_time.count() << '\t' << reduced_time.count() << '\t'
              << samples_used << std::endl;
  }

  return 0;
}

int main_windowed_reduction(const size_t N = 32768, const double a = 0.8,
                            const size_t k = 1, const double lambda = 0.0,
                            const double eps = 1e-11, const double T = 256.0,
                            const size_t window_count = 128) {
  using namespace TT;
  using namespace TT::LowDimensional;

  std::string suffix;
  {
    std::ostringstream suffixS;
    suffixS << "_" << N << "_" << a << "_" << k << "_" << lambda << "_" << eps
            << "_" << T << "_" << window_count;
    suffix = suffixS.str();
  }

  auto gen = std::mt19937_64{};

  const double safe_dt = (lambda > 0) ? (1.0 / 8192) : (1.0 / 4096);
  const double window_t = T / window_count;
  const size_t window_samples = 65;

  std::vector<MatrixD> bases;

  VectorD x{{N}};
  std::fill(x.begin(), x.end(), 0.0);
  std::fill(x.begin(), x.begin() + k, 2.0 / (k * (k + 1)));

  auto kernel = [a](size_t i, size_t j) -> double {
    const double i_a = std::pow(i + 1, a), j_a = std::pow(j + 1, a);
    return i_a / j_a + j_a / i_a;
  };
  auto kernel_cross =
      TT::MatrixCross<double>::cross(kernel, N, N, 1e-10, 4, gen);

  auto S = Direct::Smoluchowski::Operator(kernel_cross.u(), kernel_cross.v(),
                                          lambda);

  std::ofstream basisLogF("data/basis_log" + suffix);
  CSV::Writer<size_t, double, size_t, double, double, size_t, size_t, double,
              double, size_t, bool, double, double>
      basisLog{basisLogF};
  auto res = Reduction::find_windowed_basis_for(
      0.0, T, safe_dt, x.ref(), S, window_t, window_samples, eps * 1e3, eps,
      [&basisLog, N, a, k, lambda, T, window_count, window_samples, safe_dt,
       eps](size_t size, bool ext, double norm_ratio, double t) {
        basisLog.write(N, a, k, lambda, T, window_count, window_samples,
                       safe_dt, eps, size, ext, norm_ratio, t);
      });
  ttlog_info("Found basis for %g(%u) over [0,%g] with %u vectors", a,
             static_cast<unsigned>(k), res.second,
             static_cast<unsigned>(res.first.size(1)));

  {
    std::ofstream basisF("data/basis" + suffix);
    CSV::Writer<size_t, double, size_t, double, double, size_t, size_t, double,
                double, double, size_t>
        basis{basisF};
    basis.write(N, a, k, lambda, T, window_count, window_samples, safe_dt, eps,
                res.second, res.first.size(1));
  }

  std::ofstream solutionTF("data/solve_meta" + suffix);
  CSV::Writer<size_t, double, size_t, double, double, size_t, size_t, double,
              double, double, const char *>
      solutionT{solutionTF};

  MatrixD full_snapshots{{N, 1025}};
  {
    std::fill(x.begin(), x.end(), 0.0);
    std::fill(x.begin(), x.begin() + k, 2.0 / (k * (k + 1)));

    MatrixRefD refs = full_snapshots.ref();

    auto start = std::chrono::steady_clock::now();
    Reduction::integrate_full_from_to(0.0, T, safe_dt, x.ref(), S, &refs);
    std::chrono::duration<double> total =
        std::chrono::steady_clock::now() - start;
    ttlog_info("Solved full system for %g(%u) over [0,%g] in %g seconds", a,
               static_cast<unsigned>(k), T, total.count());
    solutionT.write(N, a, k, lambda, T, window_count, window_samples, safe_dt,
                    eps, total.count(), "full");
  }

  MatrixD reduced_snapshots{{N, 1025}};
  {
    const size_t rank = res.first.size(1);
    std::fill(x.begin(), x.end(), 0.0);
    std::fill(x.begin(), x.begin() + k, 2.0 / (k * (k + 1)));
    auto basis = res.first.cref();
    MatrixD small_samples{{rank, 1025}};
    VectorD reduced_x{{rank}};

    auto Sred = Reduction::reduced_operator(S, basis);
    BLAS::L2::gemv(BLAS::transpose::yes, 1.0, basis.strided(),
                   x.cref().strided(), 0.0, reduced_x.ref().strided());
    VectorD source = reduced_x; // Assumes k == 1
    MatrixRefD refs = small_samples.ref();

    auto start = std::chrono::steady_clock::now();
    Reduction::integrate_reduced_from_to(0, T, safe_dt, reduced_x.ref(),
                                         Sred.cref(), source.cref(), &refs);
    std::chrono::duration<double> total =
        std::chrono::steady_clock::now() - start;
    ttlog_info("Solved reduced system for %g(%u) over [0,%g] in %g seconds", a,
               static_cast<unsigned>(k), T, total.count());
    solutionT.write(N, a, k, lambda, T, window_count, window_samples, safe_dt,
                    eps, total.count(), "reduced");

    BLAS::L3::gemm(BLAS::transpose::no, BLAS::transpose::no, 1.0,
                   basis.strided(), small_samples.cref().strided(), 0.0,
                   reduced_snapshots.ref().strided());
  }

  std::ofstream solution_diffF{"data/solution_diff" + suffix};
  CSV::Writer<size_t, double, size_t, double, double, size_t, size_t, double,
              double, size_t, double, double>
      solution_diff{solution_diffF};

  for (size_t i = 0; i < full_snapshots.size(1); ++i) {
    VectorD diff{{N}};
    BLAS::L1::copy(full_snapshots.cref().index_last(i).strided(),
                   diff.ref().strided());
    double reference_norm = BLAS::L1::nrm2(diff.cref().strided());
    BLAS::L1::axpy(-1.0, reduced_snapshots.cref().index_last(i).strided(),
                   diff.ref().strided());
    double err_norm = BLAS::L1::nrm2(diff.cref().strided());
    ttlog_info("Relative error at snapshot %u is %g", static_cast<unsigned>(i),
               err_norm / reference_norm);
    solution_diff.write(N, a, k, lambda, T, window_count, window_samples,
                        safe_dt, eps, i, err_norm, reference_norm);
  }

  std::ofstream solutionF{"data/solution" + suffix};
  CSV::Writer<size_t, size_t, double, const char *> solution_writer{solutionF};

  const size_t time_stride = 16;
  full_snapshots.cref()
      .strided()
      .strided({1, time_stride})
      .ijfor([&](size_t i, size_t j, double x) {
        solution_writer.write(i, j * time_stride, x, "full");
      });
  reduced_snapshots.cref()
      .strided()
      .strided({1, time_stride})
      .ijfor([&](size_t i, size_t j, double x) {
        solution_writer.write(i, j * time_stride, x, "reduced");
      });

  return 0;
}

int main_windowed_reduction_change_size() {
  using namespace TT;
  using namespace TT::LowDimensional;

  auto gen = std::mt19937_64{};
  const size_t N = 8192;

  const double safe_dt = 1.0 / 2048;
  const double T = 512.0;
  const size_t window_count = 256;
  const double window_t = T / window_count;
  const size_t window_samples = 65;

  const double a = 0.8;
  const size_t k = 1;
  VectorD x{{N}};
  std::fill(x.begin(), x.end(), 0.0);
  std::fill(x.begin(), x.begin() + k, 2.0 / (k * (k + 1)));

  auto kernel = [a](size_t i, size_t j) -> double {
    const double i_a = std::pow(i + 1, a), j_a = std::pow(j + 1, a);
    return i_a / j_a + j_a / i_a;
  };
  auto kernel_cross =
      TT::MatrixCross<double>::cross(kernel, N, N, 1e-10, 4, gen);

  auto S = Direct::Smoluchowski::Operator(kernel_cross.u(), kernel_cross.v());

  auto basis_t = Reduction::find_windowed_basis_for(
      0, T, safe_dt, x.ref(), S, window_t, window_samples, 1e-10, 1e-12);

  const size_t smaller_sizes[] = {128, 192, 256, 384, 512, 768, 1024, 1536};

  std::cout << "N" << '\t' << "snapshot" << '\t' << "norm" << '\t'
            << "abserr" << '\t' << "relerr" << '\n';
  for (const size_t Nsmall : smaller_sizes) {

    if (Nsmall < basis_t.first.size(1))
      continue;

    MatrixD smaller_basis = basis_t.first.cref().strided().subslice(
        {0, 0}, {Nsmall, basis_t.first.size(1)});
    VectorD tau{{smaller_basis.size(1)}};
    LAPACK::geqrf(smaller_basis.ref().strided(), tau.ref());
    LAPACK::orgqr(smaller_basis.ref().strided(), tau.cref());

    VectorD xsmall{{Nsmall}};
    std::fill(xsmall.begin(), xsmall.end(), 0.0);
    std::fill(xsmall.begin(), xsmall.begin() + k, 2.0 / (k * (k + 1)));

    VectorD xred{{smaller_basis.size(1)}};

    BLAS::L2::gemv(BLAS::transpose::yes, 1.0, smaller_basis.cref().strided(),
                   xsmall.cref().strided(), 0.0, xred.ref().strided());
    auto source = xred;

    auto kernel_cross_small =
        TT::MatrixCross<double>::cross(kernel, Nsmall, Nsmall, 1e-10, 4, gen);

    auto S_small = Direct::Smoluchowski::Operator(kernel_cross_small.u(),
                                                  kernel_cross_small.v());
    auto Sred = Reduction::reduced_operator(S_small, smaller_basis.cref());

    MatrixD samples{{smaller_basis.size(1), 8192}};
    auto samples_ref = samples.ref();
    Reduction::integrate_reduced_from_to(0, T, safe_dt, xred.ref(), Sred.cref(),
                                         source.cref(), &samples_ref);

    MatrixD full_samples{{smaller_basis.size(0), samples.size(1)}};
    BLAS::L3::gemm(BLAS::transpose::no, BLAS::transpose::no, 1.0,
                   smaller_basis.cref().strided(), samples.cref().strided(),
                   0.0, full_samples.ref().strided());

    MatrixD unred_samples{{smaller_basis.size(0), samples.size(1)}};
    auto ref = unred_samples.ref();
    Reduction::integrate_full_from_to(0.0, T, safe_dt, xsmall.ref(), S_small, &ref);
    for (size_t i = 0; i < full_samples.size(1); ++i) {
      VectorD diff{{Nsmall}};
      BLAS::L1::copy(unred_samples.cref().index_last(i).strided(),
                     diff.ref().strided());
      double reference_norm = BLAS::L1::nrm2(diff.cref().strided());
      BLAS::L1::axpy(-1.0, full_samples.cref().index_last(i).strided(),
                     diff.ref().strided());
      double err_norm = BLAS::L1::nrm2(diff.cref().strided());
      std::cout << Nsmall << '\t' << i << '\t' << reference_norm << '\t'
                << err_norm << '\t' << err_norm / reference_norm << '\n';
      // std::ostringstream filename;
      // filename << "samples/small_sample_" << std::setw(4) <<
      // std::setfill('0')
      //          << i << ".out";
      // std::ofstream out(filename.str());

      // for (size_t j = 0; j < full_samples.size(0); ++j) {
      //   out << j + 1 << '\t' << full_samples(j, i) << '\n';
      // }
    }
  }

  return 0;
}

int main_stationary() {

  using namespace TT;
  using namespace TT::LowDimensional;

  const size_t N = 131072;
  const double a = 0.4;
  auto kernel = [a](size_t i, size_t j) -> double {
    const double i_a = std::pow(i + 1, a), j_a = std::pow(j + 1, a);
    return i_a / j_a + j_a / i_a;
  };

  auto gen = std::mt19937_64{};
  auto kernel_cross =
      TT::MatrixCross<double>::cross(kernel, N, N, 1e-10, 4, gen);
  auto S = Direct::Smoluchowski::Operator(kernel_cross.u(), kernel_cross.v());

  const double Icoef = 0.0;
  const double Jcoef = 1.0;

  VectorD x{{N}};
  std::fill(x.begin(), x.end(), 0.0);
  std::fill(x.begin(), x.begin() + 100, 1.0 / 5050);
  VectorD source{{N}};
  std::fill(source.begin(), source.end(), 0.0);
  source(0) = 1.0;

  VectorD offset{{N}};
  std::fill(offset.begin(), offset.end(), 0.0);

  Integration::Implicit::newton_step_size_bound_below(
      [&](VectorRef<const double> x, VectorRefD y) {
        S.eval_quadratic(x, y);
        y(0) += 1.0;
        TT::BLAS::L1::scal(Jcoef, y.strided());
        TT::BLAS::L1::axpy(Icoef, x.strided(), y.strided());
      },
      [&](VectorRef<const double> at, VectorRef<const double> rhs,
          VectorRefD sol) {
        Direct::solve_jacobi(Icoef, Jcoef, at, rhs, sol, 256, 1e-8, 196,
                             kernel_cross, S);
      },
      [&](VectorRef<const double> at, VectorRef<const double> val_at,
          const double norm_val_at,
          VectorRef<const double> direction) -> double {
        return Direct::Smoluchowski::choose_newton_step_size(
            source.cref(), S, Icoef, Jcoef, at, offset.cref(), val_at,
            norm_val_at, direction);
      },
      offset.cref(), 200, 1e-12, x.ref());

  double nrm = TT::BLAS::L1::nrm2(x.cref().strided());
  std::cout << nrm << std::endl;

  for (double eps = 0.5; eps > 1e-15; eps /= 2) {
    auto tensorised = TT::TTT<double, 1>::ttt_svd(x.cref(), eps * nrm, 200);
    std::cout << tensorised.max_rank() << '\t' << eps << std::endl;
  }

  return 0;
}

int main_dynamic() {
  using namespace TT;
  using namespace TT::LowDimensional;

  const size_t N = 131072;
  const double a = 0.9;
  auto kernel = [a](size_t i, size_t j) -> double {
    const double i_a = std::pow(i + 1, a), j_a = std::pow(j + 1, a);
    return i_a / j_a + j_a / i_a;
  };

  auto gen = std::mt19937_64{};
  auto kernel_cross =
      TT::MatrixCross<double>::cross(kernel, N, N, 1e-10, 4, gen);
  auto S = Direct::Smoluchowski::Operator(kernel_cross.u(), kernel_cross.v());

  const double T = 4.0;
  const double sampling_dt = 0.25;
  const double initial_dt = 1.0 / (8192 * 32);
  const size_t SamplesBound =
      static_cast<size_t>(std::ceil(T / sampling_dt) + 1);

  MatrixD samples{{N, SamplesBound}};
  std::chrono::duration<double> rk4_time, norsett_time, dirk43_time;
  if (1) {
    VectorD x{{N}};
    x(0) = 1.0;
    std::fill(x.begin() + 1, x.end(), 0.0);
    size_t next_sample_ix = 0;
    double final_dt = initial_dt;

    auto start = std::chrono::steady_clock::now();
    Integration::Explicit::rk4(
        [&](double, VectorRef<const double> x, VectorRefD y) mutable {
          S.eval_quadratic(x, y);
          y(0) += 1.0;
        },
        x.ref(), 0.0, T, initial_dt, 1e-12, 1e-12,
        [&](VectorRefD x, double t, double) mutable {
          while (t >= sampling_dt * next_sample_ix) {
            ttlog_info("Writing sample at %g", t);
            TT::BLAS::L1::copy(
                x.cref().strided(),
                samples.ref().index_last(next_sample_ix).strided());
            ++next_sample_ix;
          }
        },
        [&](double t, double old_dt, double new_dt) mutable {
          ttlog_info("Switching step from %g to %g at %g", old_dt, new_dt, t);
          final_dt = new_dt;
        });
    rk4_time = std::chrono::steady_clock::now() - start;

    for (size_t i = 0; i < next_sample_ix; ++i) {
      std::ostringstream ostr;
      ostr << "snapshots/rk4_" << i << ".data";
      std::ofstream out(ostr.str());
      for (size_t j = 0; j < N; ++j) {
        out << j + 1 << '\t' << samples(j, i) << std::endl;
      }
    }

    std::cout << "RK4 took " << rk4_time.count() << " with dt = " << final_dt
              << std::endl;
  }

  VectorD source{{N}};
  source(0) = 1.0;
  std::fill(source.begin() + 1, source.end(), 0.0);

  Integration::Implicit::DIRKConfig config;
  config.f = [&](double, VectorRef<const double> x, VectorRefD y) mutable {
    S.eval_quadratic(x, y);
    y(0) += 1.0;
  };
  config.jacobi_solver = [&](double Icoef, double Jcoef,
                             VectorRef<const double> at,
                             VectorRef<const double> rhs, VectorRefD sol) {
    for (size_t i = 0; i < at.size(); ++i)
      if (at(i) < 0)
        ttlog_error("at(%d) = %g is negative", static_cast<int>(i), at(i));

    Direct::solve_jacobi(Icoef, Jcoef, at, rhs, sol, 512, 1e-11, 256,
                         kernel_cross, S);
  };
  config.step_size_oracle =
      [&](double Icoef, double Jcoef, VectorRef<const double> at,
          VectorRef<const double> offset, VectorRef<const double> val_at,
          const double norm_val_at, VectorRef<const double> direction) {
        return Direct::Smoluchowski::choose_newton_step_size(
            source.cref(), S, Icoef, Jcoef, at, offset, val_at, norm_val_at,
            direction);
      };
  config.max_newton_iter = 80;
  config.newton_eps = 1e-14;
  config.reps = 1e-12;
  config.aeps = 1e-12;
  config.callback = [](VectorRefD, double, double) {};
  config.step_change_callback = [](double, double, double) {};

  config.project = [](VectorRef<const double> offset, VectorRef<double> x) {
    for (size_t i = 0; i < offset.size(); ++i) {
      if (x(i) <= -offset(i)) {
        x(i) = -offset(i);
      }
    }
  };

  if (0) {
    VectorD x{{N}};
    x(0) = 1.0;
    std::fill(x.begin() + 1, x.end(), 0.0);
    size_t next_sample_ix = 0;
    double final_dt = initial_dt;

    config.callback = [&](VectorRefD x, double t, double) mutable {
      while (t >= sampling_dt * next_sample_ix) {
        ttlog_info("Writing sample at %g", t);
        TT::BLAS::L1::copy(x.cref().strided(),
                           samples.ref().index_last(next_sample_ix).strided());
        ++next_sample_ix;
      }
    };
    config.step_change_callback = [&](double t, double old_dt,
                                      double new_dt) mutable {
      ttlog_info("Switching step from %g to %g at %g", old_dt, new_dt, t);
      final_dt = new_dt;
    };

    auto start = std::chrono::steady_clock::now();
    Integration::Implicit::norsett(config, x.ref(), 0.0, T, initial_dt);
    norsett_time = std::chrono::steady_clock::now() - start;

    for (size_t i = 0; i < next_sample_ix; ++i) {
      std::ostringstream ostr;
      ostr << "snapshots/norsett_" << i << ".data";
      std::ofstream out(ostr.str());
      for (size_t j = 0; j < N; ++j) {
        out << j + 1 << '\t' << samples(j, i) << std::endl;
      }
    }

    std::cout << "Norsett took " << norsett_time.count()
              << " with dt = " << final_dt << std::endl;
  }

  if (1) {
    VectorD x{{N}};
    x(0) = 1.0;
    std::fill(x.begin() + 1, x.end(), 0.0);
    size_t next_sample_ix = 0;
    double final_dt = initial_dt;
    VectorD source = x;

    config.callback = [&](VectorRefD x, double t, double) mutable {
      while (t >= sampling_dt * next_sample_ix) {
        ttlog_info("Writing sample at %g", t);
        TT::BLAS::L1::copy(x.cref().strided(),
                           samples.ref().index_last(next_sample_ix).strided());
        ++next_sample_ix;
      }
    };
    config.step_change_callback = [&](double t, double old_dt,
                                      double new_dt) mutable {
      ttlog_info("Switching step from %g to %g at %g", old_dt, new_dt, t);
      final_dt = new_dt;
    };

    auto start = std::chrono::steady_clock::now();
    Integration::Implicit::dirk43(config, x.ref(), 0.0, T, initial_dt);
    dirk43_time = std::chrono::steady_clock::now() - start;

    for (size_t i = 0; i < next_sample_ix; ++i) {
      std::ostringstream ostr;
      ostr << "snapshots/dirk43_" << i << ".data";
      std::ofstream out(ostr.str());
      for (size_t j = 0; j < N; ++j) {
        out << j + 1 << '\t' << samples(j, i) << std::endl;
      }
    }

    std::cout << "DIRK43 took " << dirk43_time.count()
              << " with dt = " << final_dt << std::endl;
  }

  return 0;
}

int main_qkrylov() {
  using namespace TT;
  using namespace TT::LowDimensional;

  const size_t N = 131072;
  const double a = 0.9;
  auto kernel = [a](size_t i, size_t j) -> double {
    const double i_a = std::pow(i + 1, a), j_a = std::pow(j + 1, a);
    return i_a / j_a + j_a / i_a;
  };

  auto gen = std::mt19937_64{};
  auto kernel_cross =
      TT::MatrixCross<double>::cross(kernel, N, N, 1e-10, 4, gen);
  auto S = Direct::Smoluchowski::Operator(kernel_cross.u(), kernel_cross.v());

  MatrixD basis({N, 1});
  basis(0, 0) = 1.0;
  for (size_t i = 1; i < N; ++i) {
    basis(i, 0) = 0.0;
  }

  Direct::Smoluchowski::Reduction::QKrylov::expand_basis(basis, S, 128, 200,
                                                         1e-8);

  return 0;
}

int main(int argc, char **argv) {
  return main_tt(0.2, 26, 5);
  //return main_windowed_reduction_change_size();
  // const size_t N = argc >= 2 ? std::stoul(argv[1]) : 32768;
  // const double a = argc >= 3 ? std::stod(argv[2]) : 0.8;
  // const size_t k = argc >= 4 ? std::stoul(argv[3]) : 1;
  // const double lambda = argc >= 5 ? std::stod(argv[4]) : 0.0;
  // const double eps = argc >= 6 ? std::stod(argv[5]) : 1e-13;
  // const double T = argc >= 7 ? std::stod(argv[6]) : 256.0;
  // const size_t window_count = argc >= 8 ? std::stod(argv[7]) : 128;

  // std::cout << "Running windowed reduction with parameters:"
  //           << "\n\tN = " << N << "\n\ta = " << a << "\n\tk = " << k
  //           << "\n\tlambda = " << lambda << "\n\teps = " << eps
  //           << "\n\tT = " << T << "\n\twindow count = " << window_count
  //           << std::endl;

  // return main_windowed_reduction(N, a, k, lambda, eps, T, window_count);
}
