#include "../include/reduction.hpp"
#include "../include/integration.hpp"
#include "../include/ttsuite/blapack.hpp"
#include "../include/ttsuite/log.hpp"
#include "include/convolution.hpp"
#include "include/ttsuite/low_dimensional.hpp"
#include <algorithm>

namespace Reduction {
using namespace TT::LowDimensional;
using namespace Direct;

// The code in this file has *not* been revised when Smoluchowski's
// Partial became symmetric.  Expect some eyebrow-raising inefficiencies.

Tensor<double, 3> reduced_operator(const Smoluchowski::Operator &op,
                                   MatrixRef<const double> base) {
  const size_t N = base.size(0);
  const size_t M = base.size(1);

  std::vector<Smoluchowski::Partial> partials;

  for (size_t i = 0; i < M; ++i) {
    auto vec = base.index_last(i);
    partials.emplace_back(op.partial(vec));
  }

  VectorD prod{{N}};
  Tensor<double, 3> result{{M, M, M}};
  for (size_t i = 0; i < M; ++i) {
    for (size_t j = 0; j <= i; ++j) {
      op.combine(partials.at(i), partials.at(j), prod.ref());
      TT::BLAS::L2::gemv(TT::BLAS::transpose::yes, 1.0, base.strided(),
                         prod.cref().strided(), 0.0,
                         result.ref().index_last(j).index_last(i).strided());

      if (j != i) {
        TT::BLAS::L1::copy(result.cref().index_last(j).index_last(i).strided(),
                           result.ref().index_last(i).index_last(j).strided());
      }
    }
  }

  return result;
}

std::tuple<VectorD, VectorD, Tensor<double, 3>>
prepare_reduced_problem(MatrixRef<const double> base,
                        StridedVectorRef<const double> full_source,
                        StridedVectorRef<const double> full_x0,
                        const Smoluchowski::Operator &full_op) {
  const size_t N = base.size(0);
  const size_t R = base.size(1);
  if (N != full_source.size() || N != full_x0.size())
    throw "Size mismatch in prepare_reduced_problem";

  VectorD reduced_source{{R}}, reduced_x0{{R}};

  TT::BLAS::L2::gemv(TT::BLAS::transpose::yes, 1.0, base.strided(), full_source,
                     0.0, reduced_source.ref().strided());
  TT::BLAS::L2::gemv(TT::BLAS::transpose::yes, 1.0, base.strided(), full_x0,
                     0.0, reduced_x0.ref().strided());

  return {std::move(reduced_source), std::move(reduced_x0),
          reduced_operator(full_op, base)};
}

bool extend_basis(MatrixD &basis, MatrixRef<const double> extension,
                  const double eps, double *const norm_ratio) {
  const size_t N = basis.size(0);
  const size_t old_rank = basis.size(1);
  const size_t extension_size = extension.size(1);
  ttlog_info("N = %zu, old_rank = %zu, extension_size = %zu × %zu, eps = %g", N,
             old_rank, extension.size(0), extension_size, eps);
  ttlog_info("extension_norm = %zu",
             TT::BLAS::L1::nrm2(extension.flatten().strided()));

  basis.resize_last(old_rank + extension_size);
  TT::BLAS::L1::copy(
      extension.cref().flatten().strided(),
      basis.ref().slice_last(old_rank, extension_size).flatten().strided());

  VectorD s({std::min(extension_size, N)});
  VectorD superb({s.size()});

  ttlog_info("now extension norm = %zu",
             TT::BLAS::L1::nrm2(basis.cref()
                                    .slice_last(old_rank, extension_size)
                                    .flatten()
                                    .strided()));

  TT::LAPACK::gesvd(TT::LAPACK::svd_vectors::overwrite,
                    TT::LAPACK::svd_vectors::none,
                    basis.ref().slice_last(old_rank, extension_size).strided(),
                    s.ref(), nullptr, nullptr, superb.ref());
  ttlog_info("extension singular values = %g, %g, %g, ...", s(0), s(1), s(2));
  const size_t extension_rank =
      std::find_if(s.cbegin(), s.cend(),
                   [eps](double sv) { return sv < eps; }) -
      s.cbegin();
  ttlog_info("extension_rank = %zu", extension_rank);
  basis.resize_last(old_rank + extension_rank);

  MatrixD projected({old_rank, extension_rank});
  TT::BLAS::L3::gemm(
      TT::BLAS::transpose::yes, TT::BLAS::transpose::no, 1.0,
      basis.cref().slice_last(0, old_rank).strided(),
      basis.cref().slice_last(old_rank, extension_rank).strided(), 0.0,
      projected.ref().strided());
  const double projected_norm =
      TT::BLAS::L1::nrm2(projected.cref()
                             .template reshape<1>({old_rank * extension_rank})
                             .strided());
  const double extension_norm =
      TT::BLAS::L1::nrm2(basis.cref()
                             .slice_last(old_rank, extension_rank)
                             .template reshape<1>({extension_rank * N})
                             .strided());

  ttlog_info("Projected norm %g residue is %g%% of extension norm %g",
             projected_norm, (1.0 - projected_norm / extension_norm) * 100,
             extension_norm);
  if (norm_ratio != nullptr)
    *norm_ratio = projected_norm / extension_norm;

  if (projected_norm >= (1 - eps) * extension_norm) {
    // The modification is small enough that we get to discard it entirely
    basis.resize_last(old_rank);
    return false;
  }

  // Otherwise we re-approximate the basis
  s.resize_last(std::min(N, basis.size(1)));
  superb.resize_last(s.size());
  TT::LAPACK::gesvd(TT::LAPACK::svd_vectors::overwrite,
                    TT::LAPACK::svd_vectors::none, basis.ref().strided(),
                    s.ref(), nullptr, nullptr, superb.ref());
  const size_t resulting_rank =
      std::find_if(s.cbegin(), s.cend(),
                   [eps](double sv) { return sv < eps; }) -
      s.cbegin();
  basis.resize_last(resulting_rank);
  return true;
}

void integrate_full_from_to(const double t0, const double t1, const double dt,
                            TT::LowDimensional::VectorRefD x0,
                            Direct::Smoluchowski::Operator &S,
                            TT::LowDimensional::MatrixRefD *samples) {
  size_t next_sample = 0;
  const size_t NSamples = samples == nullptr ? 0 : samples->size(1);
  const double sample_dt = (t1 - t0) / (NSamples - 1);

  Integration::Explicit::midpoint(
      [&S](double t, VectorRef<const double> x, VectorRefD y) mutable {
        S.eval_quadratic(x, y);
        y(0) += 1.0;
      },
      x0, t0, t1, dt,
      [&next_sample, sample_dt, &samples, t0](VectorRefD x, double t,
                                              size_t step) mutable {
        for (auto &v : x) {
          if (v < 0)
            v = 0;
        }

        if (samples != nullptr && t >= t0 + sample_dt * next_sample) {
          TT::BLAS::L1::copy(x.cref().strided(),
                             samples->index_last(next_sample).strided());
          ++next_sample;
        }
      });
}

void reduce_and_integrate(const double t0, const double t1, const double dt,
                          TT::LowDimensional::VectorRef<const double> x0,
                          TT::LowDimensional::VectorRef<const double> source,
                          Direct::Smoluchowski::Operator &S,
                          TT::LowDimensional::MatrixRef<const double> basis,
                          TT::LowDimensional::MatrixRefD *samples) {
  const size_t full_size = x0.size();
  assert(S.size() == full_size);
  assert(source.size() == full_size);

  if (basis.size(0) != full_size) {
    auto rescaled_basis = rescale_basis_for_size(basis.strided(), full_size);
    reduce_and_integrate(t0, t1, dt, x0, source, S, rescaled_basis.cref(),
                         samples);
    return;
  }

  const size_t reduced_size = basis.size(1);
  VectorD x0_reduced{{reduced_size}}, source_reduced{{reduced_size}};
  auto S_reduced = reduced_operator(S, basis.cref());

  TT::BLAS::L2::gemv(TT::BLAS::transpose::yes, 1.0, basis.strided(),
                     x0.strided(), 0.0, x0_reduced.ref().strided());
  TT::BLAS::L2::gemv(TT::BLAS::transpose::yes, 1.0, basis.strided(),
                     source.strided(), 0.0, source_reduced.ref().strided());

  integrate_reduced_from_to(t0, t1, dt, x0_reduced.ref(), S_reduced.cref(),
                            source_reduced.cref(), samples);
}

void integrate_reduced_from_to(
    const double t0, const double t1, const double dt,
    TT::LowDimensional::VectorRefD x0,
    TT::LowDimensional::TensorRef<const double, 3> reduced,
    TT::LowDimensional::VectorRef<const double> sourceR,
    TT::LowDimensional::MatrixRefD *samples) {
  size_t next_sample = 0;
  const size_t NSamples = samples == nullptr ? 0 : samples->size(1);
  const double sample_dt = (t1 - t0) / (NSamples - 1);
  const size_t Nr = x0.size();
  MatrixD tmp{{Nr, Nr}};

  Integration::Explicit::midpoint(
      [&reduced, &tmp, &sourceR, Nr](double t, VectorRef<const double> x,
                                     VectorRefD y) mutable {
        TT::BLAS::L1::copy(sourceR.cref().strided(), y.strided());
        TT::BLAS::L2::gemv(TT::BLAS::transpose::no, 1.0,
                           reduced.template reshape<2>({Nr * Nr, Nr}).strided(),
                           x.strided(), 0.0,
                           tmp.ref().template reshape<1>({Nr * Nr}).strided());
        TT::BLAS::L2::gemv(TT::BLAS::transpose::no, 1.0, tmp.cref().strided(),
                           x.strided(), 1.0, y.strided());
      },
      x0, t0, t1, dt,
      [&next_sample, sample_dt, &samples, t0](VectorRefD x, double t,
                                              size_t step) mutable {
        if (samples != nullptr && t >= t0 + sample_dt * next_sample) {
          TT::BLAS::L1::copy(x.cref().strided(),
                             samples->index_last(next_sample).strided());
          ++next_sample;
        }
      });
}

std::pair<TT::LowDimensional::MatrixD, double>
find_windowed_basis_from_initial_approx(
    const double t0, const double tmax, const double dt,
    TT::LowDimensional::VectorRefD x0, Direct::Smoluchowski::Operator &S,
    const double window_size, const size_t window_samples,
    const double extension_eps, const double finished_eps,
    TT::LowDimensional::MatrixD initial_basis,
    std::function<void(size_t, bool, double, double)> progress_log) {
  const size_t max_windows =
      static_cast<size_t>(std::floor((tmax - t0) / window_size));
  const size_t N = x0.size();
  MatrixD basis = std::move(initial_basis);
  if (basis.size(0) != N)
    ttlog_error("Basis vector size is %zu, should be %zu", basis.size(0), N);

  for (size_t current_window = 0; current_window < max_windows;
       ++current_window) {
    MatrixD samples{{N, window_samples}};
    auto samples_ref = samples.ref();

    const double window_start = t0 + current_window * window_size;
    const double window_end = t0 + (current_window + 1) * window_size;
    Reduction::integrate_full_from_to(window_start, window_end, dt, x0, S,
                                      &samples_ref);

    double norm_ratio = 0.0;

    bool extended = Reduction::extend_basis(basis, samples.cref(),
                                            extension_eps, &norm_ratio);
    progress_log(basis.size(1), extended, 1.0 - norm_ratio, window_end);

    if (norm_ratio > 1 - finished_eps) {
      return std::pair<TT::LowDimensional::MatrixD, double>{std::move(basis),
                                                            window_end};
    }
  }

  return std::pair<TT::LowDimensional::MatrixD, double>{
      std::move(basis), t0 + window_size * max_windows};
}

std::pair<TT::LowDimensional::MatrixD, double> find_windowed_basis_for(
    const double t0, const double tmax, const double dt,
    TT::LowDimensional::VectorRefD x0, Direct::Smoluchowski::Operator &S,
    const double window_size, const size_t window_samples,
    const double extension_eps, const double finished_eps,
    std::function<void(size_t, bool, double, double)> progress_log) {
  return find_windowed_basis_from_initial_approx(
      t0, tmax, dt, x0, S, window_size, window_samples, extension_eps,
      finished_eps, TT::LowDimensional::MatrixD{{x0.size(), 0}}, progress_log);
}

TT::LowDimensional::MatrixD rescale_basis_for_size(
    TT::LowDimensional::StridedMatrixRef<const double> old_basis,
    const size_t new_size) {
  const size_t rank = old_basis.size(1);
  const size_t old_size = old_basis.size(0);

  if (new_size < rank) {
    ttlog_error("New size %zu can't be below rank %zu", new_size, rank);
  }

  TT::LowDimensional::MatrixD rescaled_basis{{new_size, rank}};
  std::fill(rescaled_basis.begin(), rescaled_basis.end(), 0.0);

  for (size_t j = 0; j < rank; ++j) {
    for (size_t i = 0; i < old_size && i < new_size; ++i) {
      rescaled_basis(i, j) = old_basis(i, j);
    }
  }

  if (new_size < old_size) {
    VectorD tau{{rank}};
    TT::LAPACK::geqrf(rescaled_basis.ref().strided(), tau.ref());
    TT::LAPACK::orgqr(rescaled_basis.ref().strided(), tau.cref());
  }

  return rescaled_basis;
}

TT::LowDimensional::MatrixD rescale_and_recompress_basis(
    TT::LowDimensional::StridedMatrixRef<const double> old_basis,
    const size_t new_size, const double eps) {
  const size_t old_size = old_basis.size(0);
  const size_t old_rank = old_basis.size(1);

  if (new_size > old_size)
    return rescale_basis_for_size(old_basis, new_size);

  TT::LowDimensional::MatrixD new_basis{{new_size, old_rank}};

  new_basis.ijfor(
      [&old_basis](size_t i, size_t j, double &x) { x = old_basis(i, j); });

  VectorD s{{std::min(new_basis.size(0), new_basis.size(1))}};
  VectorD superb{{s.size()}};

  TT::LAPACK::gesvd(TT::LAPACK::svd_vectors::overwrite,
                    TT::LAPACK::svd_vectors::none, new_basis.ref().strided(),
                    s.ref(), nullptr, nullptr, superb.ref());

  size_t new_rank = std::find_if(s.cbegin(), s.cend(),
                                 [eps](double sv) { return sv < eps; }) -
                    s.cbegin();

  new_basis.resize_last(new_rank);

  return new_basis;
}

TT::LowDimensional::MatrixD
merge_bases(std::vector<TT::LowDimensional::MatrixRef<const double>> bases,
            const double eps) {
  const size_t N = bases.front().size(0);
  size_t total_vectors = 0;
  for (auto &&m : bases) {
    total_vectors += m.size(1);
  }

  TT::LowDimensional::MatrixD result{{N, total_vectors}};

  size_t size_so_far = 0;
  for (auto &&m : bases) {
    std::copy(m.cbegin(), m.cend(), result.begin() + size_so_far);
    size_so_far += m.size();
  }

  TT::LowDimensional::VectorD s{{std::min(N, total_vectors)}},
      superb{{std::min(N, total_vectors)}};

  TT::LAPACK::gesvd(TT::LAPACK::svd_vectors::overwrite,
                    TT::LAPACK::svd_vectors::none, result.ref().strided(),
                    s.ref(), nullptr, nullptr, superb.ref());
  const size_t resulting_rank =
      std::find_if(s.cbegin(), s.cend(),
                   [eps](double sv) { return sv < eps; }) -
      s.cbegin();
  result.resize_last(resulting_rank);
  return result;
}

} // namespace Reduction
