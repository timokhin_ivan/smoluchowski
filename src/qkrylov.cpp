#include "../include/qkrylov.hpp"
#include "../include/ttsuite/blapack.hpp"
#include "../include/ttsuite/log.hpp"
#include <algorithm>
#include <iterator>

namespace Direct {
namespace Smoluchowski {
namespace Reduction {
namespace QKrylov {

  // The code in this file has *not* been revised when Smoluchowski's
  // Partial became symmetric.  Expect some eyebrow-raising inefficiencies.

void expand_basis(TT::LowDimensional::MatrixD &basis, Operator &op,
                  const size_t max_basis_size, const size_t max_iterations,
                  const double eps) {
  using namespace TT::LowDimensional;
  const size_t N = basis.size(0);
  size_t processed = 0;

  ttlog_info("Starting quasi-krylov basis expansion with N = %u and %u initial "
             "vectors",
             static_cast<unsigned>(N), static_cast<unsigned>(basis.size(1)));

  {
    const size_t initial_basis_size = basis.size(1);

    VectorD s({std::min(initial_basis_size, N)});
    VectorD superb({s.size()});

    // TODO: Should I normalise basis vectors beforehand?
    TT::LAPACK::gesvd(TT::LAPACK::svd_vectors::overwrite,
                      TT::LAPACK::svd_vectors::none, basis.ref().strided(),
                      s.ref(), nullptr, nullptr, superb.ref());
    const double reps = eps * s(0);
    const size_t rank = std::find_if(s.cbegin(), s.cend(),
                                     [reps](double sv) { return sv < reps; }) -
                        s.cbegin();

    if (rank < initial_basis_size) {
      ttlog_info("Compressed the initial basis to %u vectors from %u",
                 static_cast<unsigned>(rank),
                 static_cast<unsigned>(initial_basis_size));
      basis.resize_last(rank);
    }
  }
  const double aeps = eps;

  MatrixD candidates({N, 0});
  std::vector<Partial> right_parts;

  right_parts.reserve(basis.size(1));
  for (size_t i = 0; i < basis.size(1); ++i) {
    right_parts.push_back(op.partial(basis.cref().index_last(i)));
  }

  for (size_t iteration = 0; iteration < max_iterations; ++iteration) {
    const size_t current_basis_size = basis.size(1);
    const size_t next_batch_size =
        (current_basis_size * (1 + current_basis_size) -
         processed * (1 + processed)) /
        2;

    ttlog_info("Starting iteration #%02u; current basis size is %u, %u vectors "
               "are processed, %u to be considered",
               static_cast<unsigned>(iteration),
               static_cast<unsigned>(current_basis_size),
               static_cast<unsigned>(processed),
               static_cast<unsigned>(next_batch_size));

    candidates.resize_last(next_batch_size);

    size_t candidate = 0;
    for (size_t j = processed; j < current_basis_size; ++j) {
      Partial left_part = op.partial(basis.cref().index_last(j));
      for (size_t i = 0; i <= j; ++i, ++candidate) {
        if (candidate >= next_batch_size)
          ttlog_error(
              "Miscalculation! Attempting to process candidate #%u out of %u",
              static_cast<unsigned>(candidate),
              static_cast<unsigned>(next_batch_size));

        ttlog_debug("Generating candidate #%u using basis vectors %u and %u",
                    static_cast<unsigned>(candidate), static_cast<unsigned>(j),
                    static_cast<unsigned>(i));
        op.combine(left_part, right_parts.at(i),
                   candidates.ref().index_last(candidate));
      }
    }

    if (candidate != next_batch_size)
      ttlog_error("Miscalculation! Got only %u candidates instead of %u",
                  static_cast<unsigned>(candidate),
                  static_cast<unsigned>(next_batch_size));

    MatrixD tmp({current_basis_size, next_batch_size});
    TT::BLAS::L3::gemm(TT::BLAS::transpose::yes, TT::BLAS::transpose::no, 1.0,
                       basis.cref().strided(), candidates.cref().strided(), 0.0,
                       tmp.ref().strided());
    TT::BLAS::L3::gemm(TT::BLAS::transpose::no, TT::BLAS::transpose::no, -1.0,
                       basis.cref().strided(), tmp.cref().strided(), 1.0,
                       candidates.ref().strided());

    VectorD s({std::min(next_batch_size, N)});
    VectorD superb({s.size()});
    TT::LAPACK::gesvd(TT::LAPACK::svd_vectors::overwrite,
                      TT::LAPACK::svd_vectors::none, candidates.ref().strided(),
                      s.ref(), nullptr, nullptr, superb.ref());

    const size_t max_rank = max_basis_size - current_basis_size;
    const size_t eps_rank =
        std::find_if(s.cbegin(), s.cend(),
                     [aeps](double sv) { return sv < aeps; }) -
        s.cbegin();

    bool oversize = eps_rank > max_rank;
    if (oversize)
      ttlog_warn("Basis truncated! Generated %u vectors, can only fit %u",
                 static_cast<unsigned>(eps_rank),
                 static_cast<unsigned>(max_rank));

    const size_t rank = std::min(eps_rank, max_rank);

    MatrixRef<const double> new_basis_columns =
        candidates.cref().slice_last(0, rank);
    basis.resize_last(basis.size(1) + rank);
    TT::BLAS::L1::copy(
        new_basis_columns.flatten().strided(),
        basis.ref().slice_last(current_basis_size, rank).flatten().strided());

    ttlog_info("Added %u new vectors to the basis, for a total of %u",
               static_cast<unsigned>(rank),
               static_cast<unsigned>(basis.size(1)));

    if (oversize)
      break;

    right_parts.reserve(basis.size(1));
    for (size_t i = current_basis_size; i < basis.size(1); ++i) {
      right_parts.push_back(op.partial(basis.cref().index_last(i)));
    }

    processed = current_basis_size;
  }
}

} // namespace QKrylov
} // namespace Reduction
} // namespace Smoluchowski
} // namespace Direct
