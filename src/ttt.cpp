#include "../include/ttsuite/ttt.hpp"

namespace TT {
std::vector<size_t> factorise(size_t n) {
  std::vector<size_t> result;

  ttlog_debug("Factorising %d", static_cast<int>(n));

  while (n % 2 == 0) {
    result.push_back(2);
    n /= 2;
    ttlog_debug("Found divisor 2, now factorising %d", static_cast<int>(n));
  }

  for (size_t divisor = 3; divisor * divisor <= n; divisor += 2) {
    while (n % divisor == 0) {
      result.push_back(divisor);
      n /= divisor;
      ttlog_debug("Found divisor %d, now factorising %d",
                  static_cast<int>(divisor), static_cast<int>(n));
    }
  }

  if (n != 1)
    result.push_back(n);
  ttlog_debug("Done factorising, ended up with %d", static_cast<int>(n));

  return result;
}
} // namespace TT
