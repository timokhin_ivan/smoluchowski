#include "../include/convolution.hpp"
#include "../include/cubic.hpp"
#include "../include/ttsuite/blapack.hpp"
#include "../include/ttsuite/log.hpp"
#include <algorithm>
#include <iostream>
#include <stdexcept>

#define DFTI_CALL(call)                                                        \
  ([&]() {                                                                     \
    auto status = call;                                                        \
    if (status && !DftiErrorClass(status, DFTI_NO_ERROR)) {                    \
      throw ::std::runtime_error(DftiErrorMessage(status));                    \
    }                                                                          \
  })()

using namespace TT::LowDimensional;

namespace {

// Given a rank decomposition A = UV^T for a symmetric matrix A,
// construct for it an ‘abbreviated’ eigendecomposition
// A = X diag(D) X^T of the same rank; crucially, the result
// enforces symmetry structurally.
void symmetricise(MatrixRef<const double> U, MatrixRef<const double> V,
                  MatrixD &X, VectorD &D) {
  const size_t N = U.size(0);
  const size_t r = U.size(1);
  assert(N == V.size(0));
  assert(r == V.size(1));

  // A = A^T ==> A = (A + A^T)/2 = (UV^T + VU^T) / 2 =
  //         [  0  I/2 ] [ U^T ]
  // [ U V ] [         ] [     ] = Y G Y^T
  //         [ I/2  0  ] [ V^T ]

  MatrixD Y{{N, 2 * r}};
  TT::BLAS::L1::copy(
      U.cref().template reshape<1>({N * r}).strided(),
      Y.ref().slice_last(0, r).template reshape<1>({N * r}).strided());
  TT::BLAS::L1::copy(
      V.cref().template reshape<1>({N * r}).strided(),
      Y.ref().slice_last(r, r).template reshape<1>({N * r}).strided());

  MatrixD G{{2 * r, 2 * r}};
  std::fill(G.begin(), G.end(), 0.0);
  for (size_t i = 0; i < r; ++i) {
    G(i + r, i) = G(i, i + r) = 0.5;
  }

  // Y = Q R ===> A = Q R G R^T Q^T
  VectorD tau{{2 * r}};
  TT::LAPACK::geqrf(Y.ref().strided(), tau.ref());
  auto R = Y.cref().strided().subslice({0, 0}, {2 * r, 2 * r});

  // G' = R G R^T ===> A = Q G' Q^T
  TT::BLAS::L3::trmm(TT::BLAS::side::left, TT::BLAS::uplo::upper,
                     TT::BLAS::transpose::no, TT::BLAS::diag::nonunit, 1.0, R,
                     G.ref().strided());
  TT::BLAS::L3::trmm(TT::BLAS::side::right, TT::BLAS::uplo::upper,
                     TT::BLAS::transpose::yes, TT::BLAS::diag::nonunit, 1.0, R,
                     G.ref().strided());

  // G' = Z diag(W) Z^T ===> A = Q Z diag(W) Z^T Q^T
  // Note that Z ends up in G's storage.
  VectorD W{{2 * r}};
  TT::LAPACK::syev(TT::LAPACK::eigenvectors::yes, TT::BLAS::uplo::upper,
                   G.ref().strided(), W.ref());

  // W are now eigenvalues of A, as well as G'.  Since A has rank r,
  // we expect only r of them to be non-zero.  Given that this is all
  // numeric, in practice we're interested in r largest (by absolute
  // value) eigenvalues.  Finally, since syev produces eigenvalues in
  // ascending order (or so the MKL documentation claims), the largest
  // values we seek are located at the extremes of the array (most
  // negative and most positive).  Hence the funny selection
  // algorithm.

  // We start by assuming all largest eigenvalues are positive
  // (i.e. they are the last r eigenvalues), and then replace the
  // smallest of them with negative ones as long as the absolute
  // values are larger.
  size_t negative_eigenvalues = 0;
  for (; negative_eigenvalues < r; ++negative_eigenvalues) {
    //  the eigenvalue under consideration   the eigv. it would replace
    if (std::abs(W(negative_eigenvalues)) < W(negative_eigenvalues + r))
      break;
  }

  // And now we drop (approximately) zero eigenvalues.

  // std::copy actually allows overlapping regions, as long as the
  // destination's start is not within the source
  std::copy(W.cbegin() + negative_eigenvalues + r, W.cend(),
            W.begin() + negative_eigenvalues);
  W.resize_last(r);

  // using BLAS::copy might be slightly faster, but it has stricter
  // overlap requirements, and these matrices are small anyway
  std::copy(G.cbegin() + (negative_eigenvalues + r) * G.size(0), G.cend(),
            G.begin() + negative_eigenvalues * G.size(0));
  G.resize_last(r);

  // As a reminder,
  // A = (Q Z) diag(W) (Q Z)^T,
  // so D = (truncated) W, and X = Q Z
  D = std::move(W);

  X = MatrixD{{N, r}};
  // Thus far, we have considered Q to be N × r, but our trusty ormqr
  // only allows multiplication by the full N × N orthogonal matrix
  // (although it need not be formed in full, and can consist of fewer
  // than N reflectors).  Therefore, we need to, essentially, pad Z to
  // N × r matrix.  Luckily, we can do that inside X.
  std::fill(X.begin(), X.end(), 0.0);
  for (size_t j = 0; j < G.size(1); ++j)
    for (size_t i = 0; i < G.size(0); ++i)
      X(i, j) = G(i, j);

  TT::LAPACK::ormqr(TT::BLAS::side::left, TT::BLAS::transpose::no,
                    Y.cref().strided(), tau.cref(), X.ref().strided());
}

} // namespace

namespace Direct {

FFT::FFT(size_t size) : m_handle(nullptr) {
  // NB: I need to free the handle if any of the SetValue
  // functions throws.  Oops.
  DFTI_CALL(DftiCreateDescriptor(&m_handle, DFTI_DOUBLE, DFTI_REAL, 1, size));

  DFTI_CALL(DftiSetValue(m_handle, DFTI_PLACEMENT, DFTI_INPLACE));

  DFTI_CALL(DftiSetValue(m_handle, DFTI_BACKWARD_SCALE, 1.0 / size));

  DFTI_CALL(DftiSetValue(m_handle, DFTI_CONJUGATE_EVEN_STORAGE,
                         DFTI_COMPLEX_COMPLEX));

  // I get a 'Functionality is not implemented' error from MKL.  Weird.
  // DFTI_CALL(DftiSetValue(m_handle, DFTI_ORDERING, DFTI_BACKWARD_SCRAMBLED));

  DFTI_CALL(DftiCommitDescriptor(m_handle));
}

FFT::~FFT() {
  // No DFTI_CALL because it's not like we can throw an exception out
  // of a destructor anyway.
  if (m_handle != nullptr)
    DftiFreeDescriptor(&m_handle);
}

FFT::FFT(FFT &&other) : m_handle(other.m_handle) { other.m_handle = nullptr; }

FFT &FFT::operator=(FFT &&other) {
  if (m_handle != nullptr)
    DftiFreeDescriptor(&m_handle);
  m_handle = other.m_handle;
  other.m_handle = nullptr;

  return *this;
}

void FFT::forward(
    TT::LowDimensional::VectorRef<std::complex<double>> data) const {
  // TODO: check that dimensions match
  DFTI_CALL(DftiComputeForward(m_handle, data.data()));
}

void FFT::inverse(
    TT::LowDimensional::VectorRef<std::complex<double>> data) const {
  // TODO: check that dimensions match
  DFTI_CALL(DftiComputeBackward(m_handle, data.data()));
}

size_t FFT::size() const {
  MKL_LONG n;
  DFTI_CALL(DftiGetValue(m_handle, DFTI_LENGTHS, &n));
  return n;
}

namespace Smoluchowski {

Operator::Operator(TT::LowDimensional::MatrixRef<const double> u,
                   TT::LowDimensional::MatrixRef<const double> v, double lambda)
    : m_lambda(lambda), m_fft(2 * u.size(0)) {
  ::symmetricise(u, v, m_kernel_X, m_kernel_D);
}

Partial Operator::partial(VectorRef<const double> x) const {
  const size_t N = m_kernel_X.size(0), R = m_kernel_X.size(1);
  const std::array<size_t, 2> size = {N, R};

  Partial result;

  result.scaled = MatrixD{size};
  for (size_t j = 0; j < R; ++j) {
    vdMul(N, m_kernel_X.cref().index_last(j).data(), x.data(),
          result.scaled.ref().index_last(j).data());
  }

  result.contracted = VectorD{std::array<size_t, 1>{R}};
  TT::BLAS::L2::gemv(TT::BLAS::transpose::yes, 1.0, m_kernel_X.cref().strided(),
                     x.strided(), 0.0, result.contracted.ref().strided());
  for (size_t j = 0; j < R; ++j) {
    result.contracted(j) *= m_kernel_D(j);
  }

  result.conv_part = Matrix<std::complex<double>>{{N + 1, R}};

  for (size_t a = 0; a < R; ++a) {
    auto col = result.scaled.cref().index_last(a);

    auto conv_real = as_real(result.conv_part.ref().index_last(a));
    std::copy(col.begin(), col.end(), conv_real.begin());
    std::fill(conv_real.begin() + N, conv_real.end(), 0.0);
    m_fft.forward(result.conv_part.ref().index_last(a));
  }

  auto indexed = VectorD{{N}};
  for (size_t i = 0; i < N; ++i)
    indexed(i) = (i + 1) * x(i);

  result.indexed_contracted = VectorD{{R}};
  TT::BLAS::L2::gemv(TT::BLAS::transpose::yes, 1.0, m_kernel_X.cref().strided(),
                     indexed.cref().strided(), 0.0,
                     result.indexed_contracted.ref().strided());

  return result;
}

void Operator::combine(const Partial &l, const Partial &r, VectorRefD out) const {
  const size_t N = m_kernel_X.size(0);
  const size_t R = m_kernel_X.size(1);

  Matrix<std::complex<double>> tmps{{N + 1, R}};

  for (size_t i = 0; i < R; ++i) {
    vzMul(N + 1,
          reinterpret_cast<const MKL_Complex16 *>(
              l.conv_part.cref().index_last(i).data()),
          reinterpret_cast<const MKL_Complex16 *>(
              r.conv_part.cref().index_last(i).data()),
          reinterpret_cast<MKL_Complex16 *>(tmps.ref().index_last(i).data()));
    m_fft.inverse(tmps.ref().index_last(i));
  }
  TT::BLAS::L2::gemv(
      TT::BLAS::transpose::no, 1.0,
      as_real(tmps.cref()).strided().subslice({0, 0}, {N - 1, R}),
      m_kernel_D.cref().strided(), 0.0, out.strided().subslice({1}, {N - 1}));
  out(0) = 0.0;

  const double k = 1.0 + m_lambda;

  TT::BLAS::L2::gemv(TT::BLAS::transpose::no, -k, l.scaled.cref().strided(),
                     r.contracted.cref().strided(), 1.0, out.strided());
  TT::BLAS::L2::gemv(TT::BLAS::transpose::no, -k, r.scaled.cref().strided(),
                     l.contracted.cref().strided(), 1.0, out.strided());

  out(0) +=
      m_lambda * (TT::BLAS::L1::dot(l.contracted.cref().strided(),
                                    r.indexed_contracted.cref().strided()) +
                  TT::BLAS::L1::dot(r.contracted.cref().strided(),
                                    l.indexed_contracted.cref().strided()));

  TT::BLAS::L1::scal(0.5, out.strided());
}

double choose_newton_step_size(VectorRef<const double> source, Operator &op,
                               const double Icoef, const double Jcoef,
                               VectorRef<const double> at,
                               VectorRef<const double> offset,
                               VectorRef<const double> val_at,
                               const double norm_val_at,
                               VectorRef<const double> direction) {
  VectorD test = at;
  TT::BLAS::L1::axpy(-1.0, direction.cref().strided(), test.ref().strided());

  VectorD arg = test;
  TT::BLAS::L1::axpy(1.0, offset.strided(), arg.ref().strided());
  VectorD test_val{{source.size()}};
  op.eval_quadratic(arg.cref(), test_val.ref());

  TT::BLAS::L1::axpy(1.0, source.strided(), test_val.ref().strided());
  TT::BLAS::L1::scal(Jcoef, test_val.ref().strided());
  TT::BLAS::L1::axpy(Icoef, test.cref().strided(), test_val.ref().strided());

  const double a =
      TT::BLAS::L1::dot(test_val.cref().strided(), test_val.cref().strided());
  const double b =
      2 * TT::BLAS::L1::dot(test_val.cref().strided(), val_at.strided());
  const double e = TT::BLAS::L1::dot(val_at.strided(), val_at.strided());
  const double c = e + b;
  const double d = 2 * e;

  ttlog_debug("Quartic: %g x^4 %+g x^3 %+g x^2 %+g x %+g", a, b, c, d, e);
  if (a != a || b != b || c != c || d != d || e != e) {
    ttlog_error("One of the quartic coefficients is NaN: %g, %g, %g, %g, %g", a,
                b, c, d, e);
  }

  double root, fmin;
  std::tie(root, fmin) = Poly::minimise_quartic(a, b, c, d, e);
  ttlog_debug("Recommending step size %g with predicted norm %g", root,
              std::sqrt(fmin));

  return root;
}

} // namespace Smoluchowski

} // namespace Direct
