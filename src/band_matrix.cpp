#include "../include/band_matrix.hpp"
#include <sstream>
#include <fstream>
#include <mkl.h>

#define TRACKING 0

BandMatrix::BandMatrix(const double *vals, size_t N, size_t width)
  : a(vals, vals + width * N), n(N), k(width - 1) {}

void BandMatrix::apply(double *x, bool inverse) const {
#if TRACKING
  static unsigned invocation = 0;
  std::ostringstream ostr;
  ostr << "snapshots/band/" << invocation << "/";

  {
    std::ofstream x_f(ostr.str() + "source.data");
    for (size_t i = 0; i < n; ++i)
      x_f << i << '\t' << x[i] << std::endl;
  }
#endif

  if (inverse) {
    cblas_dtbsv(CblasColMajor, CblasLower, CblasNoTrans, CblasNonUnit, n, k,
                a.data(), k + 1, x, 1);
  } else {
    cblas_dtbmv(CblasColMajor, CblasLower, CblasNoTrans, CblasNonUnit, n, k,
                a.data(), k + 1, x, 1);
  }

#if TRACKING
  {
    std::ofstream x_f(ostr.str() + "result.data");
    for (size_t i = 0; i < n; ++i)
      x_f << i << '\t' << x[i] << std::endl;
  }
  ++invocation;
#endif
}
