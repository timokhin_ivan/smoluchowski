#include "../../include/ttsuite/blapack.hpp"
#include "include/ttsuite/log.hpp"
#include "include/ttsuite/low_dimensional.hpp"
#include <algorithm>
#include <iostream>
#include <mkl.h>

namespace {
char svdjob(TT::LAPACK::svd_vectors job) {
  switch (job) {
  case TT::LAPACK::svd_vectors::all:
    return 'A';
  case TT::LAPACK::svd_vectors::significant:
    return 'S';
  case TT::LAPACK::svd_vectors::overwrite:
    return 'O';
  case TT::LAPACK::svd_vectors::none:
    return 'N';
  }
  assert(false);
}

char gees_schur_job(TT::LAPACK::schur_vectors job) {
  switch (job) {
  case TT::LAPACK::schur_vectors::yes:
    return 'V';
  case TT::LAPACK::schur_vectors::no:
    return 'N';
  }
  assert(false);
}

char gees_sort_eigenvalues(TT::LAPACK::sort_eigenvalues job) {
  switch (job) {
  case TT::LAPACK::sort_eigenvalues::yes:
    return 'S';
  case TT::LAPACK::sort_eigenvalues::no:
    return 'N';
  }
  assert(false);
}

} // namespace

namespace TT {
namespace LAPACK {

void gesvd(svd_vectors jobu, svd_vectors jobv,
           LD::StridedTensorRef<double, 2> a, LD::TensorRef<double, 1> s,
           LD::StridedTensorRef<double, 2> *u,
           LD::StridedTensorRef<double, 2> *v,
           LD::TensorRef<double, 1> superb) {
  assert(a.multiplier(0) == 1);

  assert(s.size() == std::min(a.size(0), a.size(1)));
  auto max_size = std::max(a.size(0), a.size(1));

  double *u_ptr = nullptr, *v_ptr = nullptr;
  size_t u_ld = max_size, v_ld = max_size;

  if (u == nullptr) {
    assert(jobu == svd_vectors::overwrite || jobu == svd_vectors::none);
  } else {
    assert(u->multiplier(0) == 1);
    assert(u->size(0) == a.size(0));
    u_ptr = u->data();
    u_ld = u->multiplier(1);

    if (jobu == svd_vectors::all) {
      assert(u->size(1) == a.size(1));
    } else if (jobu == svd_vectors::significant) {
      assert(u->size(1) == s.size());
    }
  }

  if (v == nullptr) {
    assert(jobv == svd_vectors::overwrite || jobv == svd_vectors::none);
  } else {
    assert(v->multiplier(0) == 1);
    assert(v->size(1) == a.size(1));
    v_ptr = v->data();
    v_ld = v->multiplier(1);

    if (jobv == svd_vectors::all) {
      assert(v->size(0) == a.size(0));
    } else if (jobv == svd_vectors::significant) {
      assert(v->size(0) == s.size());
    }
  }

  auto info = LAPACKE_dgesvd(LAPACK_COL_MAJOR, ::svdjob(jobu), ::svdjob(jobv),
                             a.size(0), a.size(1), a.data(), a.multiplier(1),
                             s.data(), u_ptr, u_ld, v_ptr, v_ld, superb.data());
  assert(info == 0);
}

void gesdd(svd_vectors jobz, LD::StridedTensorRef<double, 2> a,
           LD::VectorRefD s, LD::StridedTensorRef<double, 2> *u,
           LD::StridedTensorRef<double, 2> *v) {
  assert(a.multiplier(0) == 1);
  const auto max_size = std::max(a.size(0), a.size(1)),
             min_size = std::min(a.size(0), a.size(1));

  assert(s.size() == min_size);

  double *u_ptr = nullptr, *v_ptr = nullptr;
  size_t u_ld = max_size, v_ld = max_size;

  switch (jobz) {
  case svd_vectors::all:
    assert(u != nullptr);
    assert(u->size(0) == u->size(1));
    assert(u->size(0) == a.size(0));
    assert(u->multiplier(0) == 1);

    u_ptr = u->data();
    u_ld = u->multiplier(1);

    assert(v != nullptr);
    assert(v->size(0) == v->size(1));
    assert(v->size(0) == a.size(1));
    assert(v->multiplier(0) == 1);

    v_ptr = v->data();
    v_ld = v->multiplier(1);
    break;
  case svd_vectors::significant:
    assert(u != nullptr);
    assert(v != nullptr);
    assert(u->size(0) == a.size(0));
    assert(u->size(1) == min_size);
    assert(v->size(0) == min_size);
    assert(v->size(1) == a.size(1));
    assert(u->multiplier(0) == 1);
    assert(v->multiplier(0) == 1);

    u_ptr = u->data();
    u_ld = u->multiplier(1);
    v_ptr = v->data();
    v_ld = v->multiplier(1);
    break;
  case svd_vectors::overwrite:
    if (a.size(0) >= a.size(1)) {
      assert(v != nullptr);
      assert(v->size(0) == a.size(1));
      assert(v->size(1) == a.size(1));
      assert(v->multiplier(0) == 1);

      v_ptr = v->data();
      v_ld = v->multiplier(1);
    } else {
      assert(u != nullptr);
      assert(u->size(0) == a.size(0));
      assert(u->size(1) == a.size(0));
      assert(u->multiplier(0) == 1);

      u_ptr = u->data();
      u_ld = u->multiplier(1);
    }
    break;
  case svd_vectors::none:
    break;
  default:
    ttlog_error("Unrecognised jobz option: %d", static_cast<int>(jobz));
  }

  long long info = LAPACKE_dgesdd(
      LAPACK_COL_MAJOR, ::svdjob(jobz), static_cast<lapack_int>(a.size(0)),
      static_cast<lapack_int>(a.size(1)), a.data(),
      static_cast<lapack_int>(a.multiplier(1)), s.data(), u_ptr,
      static_cast<lapack_int>(u_ld), v_ptr, static_cast<lapack_int>(v_ld));

  if (info != 0)
    ttlog_error("dgesdd failed, info = %lld", info);
}

void gelqf(LD::StridedTensorRef<double, 2> a, LD::TensorRef<double, 1> tau) {
  assert(a.multiplier(0) == 1);
  assert(tau.size() == std::min(a.size(0), a.size(1)));
  auto info = LAPACKE_dgelqf(LAPACK_COL_MAJOR, a.size(0), a.size(1), a.data(),
                             a.multiplier(1), tau.data());
  assert(info == 0);
}

void orglq(int k, LD::StridedTensorRef<double, 2> a,
           LD::TensorRef<const double, 1> tau) {
  assert(a.multiplier(0) == 1);
  assert(tau.size() >= k);
  assert(k <= a.size(0));
  assert(a.size(1) >= a.size(0));
  auto info = LAPACKE_dorglq(LAPACK_COL_MAJOR, a.size(0), a.size(1), k,
                             a.data(), a.multiplier(1), tau.data());
  assert(info == 0);
}

void geqrf(LD::StridedTensorRef<double, 2> a, LD::TensorRef<double, 1> tau) {
  assert(a.multiplier(0) == 1);
  assert(tau.size() == std::min(a.size(0), a.size(1)));
  auto info = LAPACKE_dgeqrf(LAPACK_COL_MAJOR, a.size(0), a.size(1), a.data(),
                             a.multiplier(1), tau.data());
  assert(info == 0);
}

void orgqr(int k, LD::StridedTensorRef<double, 2> a,
           LD::TensorRef<const double, 1> tau) {
  assert(a.multiplier(0) == 1);
  assert(a.size(1) <= a.size(0));
  assert(k <= a.size(1));
  assert(tau.size() >= k);
  auto info = LAPACKE_dorgqr(LAPACK_COL_MAJOR, a.size(0), a.size(1), k,
                             a.data(), a.multiplier(1), tau.data());
  assert(info == 0);
}

void ormqr(BLAS::side side, BLAS::transpose trans,
           LD::StridedTensorRef<const double, 2> a,
           LD::TensorRef<const double, 1> tau,
           LD::StridedTensorRef<double, 2> c) {
  assert(a.multiplier(0) == 1);
  assert(c.multiplier(0) == 1);

  if (side == BLAS::side::left) {
    assert(a.size(0) == c.size(0));
  } else {
    assert(a.size(0) == c.size(1));
  }
  assert(a.size(1) <= a.size(0));
  assert(tau.size() == a.size(1));

  auto info = LAPACKE_dormqr(
      LAPACK_COL_MAJOR, side == BLAS::side::left ? 'L' : 'R',
      trans == BLAS::transpose::no ? 'N' : 'T', c.size(0), c.size(1), a.size(1),
      a.data(), a.multiplier(1), tau.data(), c.data(), c.multiplier(1));
  assert(info == 0);
}

void gees(schur_vectors jobs, sort_eigenvalues sort, select_d2 select,
          LD::StridedTensorRef<double, 2> a, MKL_INT &sdim,
          LD::TensorRef<double, 1> wr, LD::TensorRef<double, 1> wi,
          LD::StridedTensorRef<double, 2> *v) {
  assert(a.multiplier(0) == 1);
  assert(a.size(0) == a.size(1));
  assert(wr.size() == a.size(0));
  assert(wi.size() == a.size(0));

  double *v_ptr = nullptr;
  size_t v_ld = 1;

  if (v == nullptr) {
    assert(jobs == schur_vectors::no);
  } else {
    assert(v->multiplier(0) == 1);
    assert(v->size(0) == a.size(0));
    assert(v->size(1) == a.size(1));

    v_ptr = v->data();
    v_ld = v->multiplier(1);
  }

  if (select == nullptr) {
    assert(sort == sort_eigenvalues::no);
  }

  auto info =
      LAPACKE_dgees(LAPACK_COL_MAJOR, ::gees_schur_job(jobs),
                    ::gees_sort_eigenvalues(sort), select, a.size(0), a.data(),
                    a.multiplier(1), &sdim, wr.data(), wi.data(), v_ptr, v_ld);
  assert(info == 0);
}

void syev(eigenvectors jobz, BLAS::uplo kind, LD::StridedTensorRef<double, 2> a,
          LD::VectorRefD w) {
  assert(a.multiplier(0) == 1);
  assert(a.size(1) == a.size(0));
  assert(a.size(0) == w.size());

  auto info =
      LAPACKE_dsyev(LAPACK_COL_MAJOR, jobz == eigenvectors::yes ? 'V' : 'N',
                    kind == BLAS::uplo::lower ? 'L' : 'U', a.size(0), a.data(),
                    a.multiplier(1), w.data());
  assert(info == 0);
}

} // namespace LAPACK
} // namespace TT
