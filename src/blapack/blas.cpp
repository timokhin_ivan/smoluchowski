#include "../../include/ttsuite/blapack.hpp"

#include <mkl.h>

namespace {
CBLAS_TRANSPOSE tcblas(TT::BLAS::transpose t) {
  switch (t) {
  case TT::BLAS::transpose::no:
    return CblasNoTrans;
  case TT::BLAS::transpose::yes:
    return CblasTrans;
  case TT::BLAS::transpose::conj:
    return CblasConjTrans;
  default:
    assert(false);
  }
}

CBLAS_UPLO ulcblas(TT::BLAS::uplo ul) {
  switch (ul) {
  case TT::BLAS::uplo::upper:
    return CblasUpper;
  case TT::BLAS::uplo::lower:
    return CblasLower;
  default:
    assert(false);
  }
}

CBLAS_DIAG dcblas(TT::BLAS::diag d) {
  switch (d) {
  case TT::BLAS::diag::nonunit:
    return CblasNonUnit;
  case TT::BLAS::diag::unit:
    return CblasUnit;
  default:
    assert(false);
  }
}

CBLAS_SIDE scblas(TT::BLAS::side s) {
  switch (s) {
  case TT::BLAS::side::left:
    return CblasLeft;
  case TT::BLAS::side::right:
    return CblasRight;
  default:
    assert(false);
  }
}
} // namespace

namespace TT {

namespace BLAS {

namespace L1 {
double asum(LD::StridedTensorRef<const double, 1> v) {
  return cblas_dasum(v.size(), v.data(), v.multiplier(0));
}

void axpy(double a, LD::StridedTensorRef<const double, 1> x,
          LD::StridedTensorRef<double, 1> y) {
  assert(x.size() == y.size());
  cblas_daxpy(x.size(), a, x.data(), x.multiplier(0), y.data(),
              y.multiplier(0));
}

void copy(LD::StridedTensorRef<const double, 1> x,
          LD::StridedTensorRef<double, 1> y) {
  assert(x.size() == y.size());
  cblas_dcopy(x.size(), x.data(), x.multiplier(0), y.data(), y.multiplier(0));
}

double dot(LD::StridedTensorRef<const double, 1> x,
           LD::StridedTensorRef<const double, 1> y) {
  assert(x.size() == y.size());
  return cblas_ddot(x.size(), x.data(), x.multiplier(0), y.data(),
                    y.multiplier(0));
}

double nrm2(LD::StridedTensorRef<const double, 1> x) {
  return cblas_dnrm2(x.size(), x.data(), x.multiplier(0));
}

void rot(LD::StridedTensorRef<double, 1> x, LD::StridedTensorRef<double, 1> y,
         double c, double s) {
  assert(x.size() == y.size());
  cblas_drot(x.size(), x.data(), x.multiplier(0), y.data(), y.multiplier(0), c,
             s);
}

void scal(double a, LD::StridedTensorRef<double, 1> x) {
  cblas_dscal(x.size(), a, x.data(), x.multiplier(0));
}

void swap(LD::StridedTensorRef<double, 1> x,
          LD::StridedTensorRef<double, 1> y) {
  assert(x.size() == y.size());
  cblas_dswap(x.size(), x.data(), x.multiplier(0), y.data(), y.multiplier(0));
}

size_t iamax(LD::StridedTensorRef<const double, 1> x) {
  return cblas_idamax(x.size(), x.data(), x.multiplier(0));
}

void rotg(double &a, double &b, double &c, double &s) {
  cblas_drotg(&a, &b, &c, &s);
}
} // namespace L1

namespace L2 {
void gbmv(transpose trans, size_t dl, size_t du, double alpha,
          LD::StridedTensorRef<const double, 2> a,
          LD::StridedTensorRef<const double, 1> x, double beta,
          LD::StridedTensorRef<double, 1> y) {
  assert(a.multiplier(0) == 1);

  size_t m = 0;
  if (trans == transpose::no) {
    assert(a.size(1) == x.size());
    m = y.size();
  } else {
    assert(a.size(1) == y.size());
    m = x.size();
  }
  assert(a.size(0) == dl + du + 1);

  cblas_dgbmv(CblasColMajor, tcblas(trans), m, a.size(1), dl, du, alpha,
              a.data(), a.multiplier(1), x.data(), x.multiplier(0), beta,
              y.data(), y.multiplier(0));
}

void gemv(transpose trans, double alpha,
          LD::StridedTensorRef<const double, 2> a,
          LD::StridedTensorRef<const double, 1> x, double beta,
          LD::StridedTensorRef<double, 1> y) {
  assert(a.multiplier(0) == 1);

  if (trans == transpose::no) {
    assert(a.size(0) == y.size());
    assert(a.size(1) == x.size());
  } else {
    assert(a.size(0) == x.size());
    assert(a.size(1) == y.size());
  }

  cblas_dgemv(CblasColMajor, tcblas(trans), a.size(0), a.size(1), alpha,
              a.data(), a.multiplier(1), x.data(), x.multiplier(0), beta,
              y.data(), y.multiplier(0));
}

void ger(double alpha, LD::StridedTensorRef<const double, 1> x,
         LD::StridedTensorRef<const double, 1> y,
         LD::StridedTensorRef<double, 2> a) {
  assert(a.multiplier(0) == 1);
  assert(a.size(0) == x.size());
  assert(a.size(1) == y.size());

  cblas_dger(CblasColMajor, a.size(0), a.size(1), alpha, x.data(),
             x.multiplier(0), y.data(), y.multiplier(0), a.data(),
             a.multiplier(1));
}

void sbmv(uplo part, double alpha, LD::StridedTensorRef<const double, 2> a,
          LD::StridedTensorRef<const double, 1> x, double beta,
          LD::StridedTensorRef<double, 1> y) {
  assert(a.multiplier(0) == 1);
  assert(a.size(1) == x.size());
  assert(a.size(1) == y.size());

  cblas_dsbmv(CblasColMajor, ulcblas(part), a.size(1), a.size(0) - 1, alpha,
              a.data(), a.multiplier(1), x.data(), x.multiplier(0), beta,
              y.data(), y.multiplier(0));
}

void symv(uplo part, double alpha, LD::StridedTensorRef<const double, 2> a,
          LD::StridedTensorRef<const double, 1> x, double beta,
          LD::StridedTensorRef<double, 1> y) {
  assert(a.multiplier(0) == 1);
  assert(a.size(0) == y.size());
  assert(a.size(1) == x.size());
  assert(a.size(0) == a.size(1));

  cblas_dsymv(CblasColMajor, ulcblas(part), a.size(1), alpha, a.data(),
              a.multiplier(1), x.data(), x.multiplier(0), beta, y.data(),
              y.multiplier(0));
}

void syr(uplo part, double alpha, LD::StridedTensorRef<const double, 1> x,
         LD::StridedTensorRef<double, 2> a) {
  assert(a.multiplier(0) == 1);
  assert(a.size(0) == a.size(1));
  assert(a.size(0) == x.size());

  cblas_dsyr(CblasColMajor, ulcblas(part), a.size(0), alpha, x.data(),
             x.multiplier(0), a.data(), a.multiplier(1));
}

void syr2(uplo part, double alpha, LD::StridedTensorRef<const double, 1> x,
          LD::StridedTensorRef<const double, 1> y,
          LD::StridedTensorRef<double, 2> a) {
  assert(a.multiplier(0) == 1);
  assert(a.size(0) == a.size(1));
  assert(x.size() == a.size(0));
  assert(y.size() == a.size(1));

  cblas_dsyr2(CblasColMajor, ulcblas(part), a.size(), alpha, x.data(),
              x.multiplier(0), y.data(), y.multiplier(0), a.data(),
              a.multiplier(1));
}

void tbmv(uplo kind, transpose trans, diag d,
          LD::StridedTensorRef<const double, 2> a,
          LD::StridedTensorRef<double, 1> x) {
  assert(a.multiplier(0) == 1);
  assert(x.size() == a.size(1));
  cblas_dtbmv(CblasColMajor, ulcblas(kind), tcblas(trans), dcblas(d), a.size(1),
              a.size(0) - 1, a.data(), a.multiplier(1), x.data(),
              x.multiplier(0));
}

void tbsv(uplo kind, transpose trans, diag d,
          LD::StridedTensorRef<const double, 2> a,
          LD::StridedTensorRef<double, 1> x) {
  assert(a.multiplier(0) == 1);
  assert(x.size() == a.size(1));
  cblas_dtbsv(CblasColMajor, ulcblas(kind), tcblas(trans), dcblas(d), a.size(1),
              a.size(0) - 1, a.data(), a.multiplier(1), x.data(),
              x.multiplier(0));
}

void trmv(uplo kind, transpose trans, diag d,
          LD::StridedTensorRef<const double, 2> a,
          LD::StridedTensorRef<double, 1> x) {
  assert(a.multiplier(0) == 1);
  assert(a.size(0) == a.size(1));
  assert(a.size(0) == x.size());

  cblas_dtrmv(CblasColMajor, ulcblas(kind), tcblas(trans), dcblas(d), a.size(0),
              a.data(), a.multiplier(1), x.data(), x.multiplier(0));
}

void trsv(uplo kind, transpose trans, diag d,
          LD::StridedTensorRef<const double, 2> a,
          LD::StridedTensorRef<double, 1> x) {
  assert(a.multiplier(0) == 1);
  assert(a.size(0) == a.size(1));
  assert(a.size(0) == x.size());

  cblas_dtrsv(CblasColMajor, ulcblas(kind), tcblas(trans), dcblas(d), a.size(0),
              a.data(), a.multiplier(1), x.data(), x.multiplier(0));
}
} // namespace L2

namespace L3 {
void gemm(transpose transa, transpose transb, double alpha,
          LD::StridedTensorRef<const double, 2> a,
          LD::StridedTensorRef<const double, 2> b, double beta,
          LD::StridedTensorRef<double, 2> c) {
  assert(a.multiplier(0) == 1);
  assert(b.multiplier(0) == 1);
  assert(b.multiplier(1) >= b.size(0));
  assert(c.multiplier(0) == 1);

  size_t m = c.size(0), n = c.size(1);
  size_t k = transa == TT::BLAS::transpose::no ? a.size(1) : a.size(0);

  if (m == 0 || n == 0 || k == 0)
    return;

  if (transa == TT::BLAS::transpose::no) {
    assert(a.size(0) == m);
  } else {
    assert(a.size(1) == m);
  }

  if (transb == TT::BLAS::transpose::no) {
    assert(b.size(0) == k);
    assert(b.size(1) == n);
  } else {
    assert(b.size(0) == n);
    assert(b.size(1) == k);
  }

  cblas_dgemm(CblasColMajor, tcblas(transa), tcblas(transb), m, n, k, alpha,
              a.data(), a.multiplier(1), b.data(), b.multiplier(1), beta,
              c.data(), c.multiplier(1));
}

void symm(side sym, uplo part, double alpha,
          LD::StridedTensorRef<const double, 2> a,
          LD::StridedTensorRef<const double, 2> b, double beta,
          LD::StridedTensorRef<double, 2> c) {
  assert(a.multiplier(0) == 1);
  assert(b.multiplier(0) == 1);
  assert(c.multiplier(0) == 1);

  assert(c.size(0) == b.size(0));
  assert(c.size(1) == b.size(1));
  assert(a.size(0) == a.size(1));
  assert(a.size(0) == (sym == side::left ? b.size(0) : b.size(1)));

  cblas_dsymm(CblasColMajor, scblas(sym), ulcblas(part), c.size(0), c.size(1),
              alpha, a.data(), a.multiplier(1), b.data(), b.multiplier(1), beta,
              c.data(), c.multiplier(1));
}

void syrk(uplo part, transpose trans, double alpha,
          LD::StridedTensorRef<const double, 2> a, double beta,
          LD::StridedTensorRef<double, 2> c) {
  assert(a.multiplier(0) == 1);
  assert(c.multiplier(0) == 1);
  assert(c.size(0) == c.size(1));

  if (trans == transpose::no) {
    assert(a.size(0) == c.size(0));
  } else {
    assert(a.size(1) == c.size(0));
  }

  cblas_dsyrk(CblasColMajor, ulcblas(part), tcblas(trans), c.size(0),
              trans == transpose::no ? a.size(0) : a.size(1), alpha, a.data(),
              a.multiplier(1), beta, c.data(), c.multiplier(1));
}

void syr2k(uplo part, transpose trans, double alpha,
           LD::StridedTensorRef<const double, 2> a,
           LD::StridedTensorRef<const double, 2> b, double beta,
           LD::StridedTensorRef<double, 2> c) {
  assert(a.multiplier(0) == 1);
  assert(b.multiplier(0) == 1);
  assert(c.multiplier(0) == 1);

  assert(c.size(0) == c.size(1));
  size_t n = c.size(0), k = trans == transpose::no ? a.size(1) : a.size(0);
  assert(n == (trans == transpose::no ? a.size(0) : a.size(1)));
  assert(a.size(0) == b.size(0));
  assert(a.size(1) == b.size(1));

  cblas_dsyr2k(CblasColMajor, ulcblas(part), tcblas(trans), n, k, alpha,
               a.data(), a.multiplier(1), b.data(), b.multiplier(1), beta,
               c.data(), c.multiplier(1));
}

void trmm(side tr, uplo kind, transpose transa, diag d, double alpha,
          LD::StridedTensorRef<const double, 2> a,
          LD::StridedTensorRef<double, 2> b) {
  assert(a.multiplier(0) == 1);
  assert(b.multiplier(0) == 1);

  assert(a.size(0) == a.size(1));
  if (tr == side::left) {
    assert(a.size(0) == b.size(0));
  } else {
    assert(a.size(0) == b.size(1));
  }

  cblas_dtrmm(CblasColMajor, scblas(tr), ulcblas(kind), tcblas(transa),
              dcblas(d), b.size(0), b.size(1), alpha, a.data(), a.multiplier(1),
              b.data(), b.multiplier(1));
}

void trsm(side tr, uplo kind, transpose transa, diag d, double alpha,
          LD::StridedTensorRef<const double, 2> a,
          LD::StridedTensorRef<double, 2> b) {
  assert(a.multiplier(0) == 1);
  assert(b.multiplier(0) == 1);

  assert(a.size(0) == a.size(1));
  if (tr == side::left) {
    assert(a.size(0) == b.size(0));
  } else {
    assert(a.size(0) == b.size(1));
  }

  cblas_dtrsm(CblasColMajor, scblas(tr), ulcblas(kind), tcblas(transa),
              dcblas(d), b.size(0), b.size(1), alpha, a.data(), a.multiplier(1),
              b.data(), b.multiplier(1));
}
} // namespace L3
} // namespace BLAS
} // namespace TT
