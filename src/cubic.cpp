#include "../include/ttsuite/log.hpp"
#include "../include/cubic.hpp"
#include <algorithm>
#include <array>
#include <cassert>
#include <cmath>
#include <complex>
#include <limits>

namespace Poly {
std::tuple<double, double> solve_quadric(double a, double b, double c) {
  const double D = b * b - 4 * a * c;

  if (D < 0)
    return std::make_tuple(std::numeric_limits<double>::quiet_NaN(),
                           std::numeric_limits<double>::quiet_NaN());

  const double K = b <= 0 ? (-b + std::sqrt(D)) : (-b - std::sqrt(D));

  const double x1 = 2 * c / K;
  const double x2 = K / (2 * a);

  if (x1 <= x2)
    return std::make_tuple(x1, x2);
  else
    return std::make_tuple(x2, x1);
}

std::tuple<unsigned, std::array<double, 3>> solve_reduced_cubic(double p,
                                                                double q) {
  const double qnan = std::numeric_limits<double>::quiet_NaN();

  std::array<double, 3> roots = {qnan, qnan, qnan};

  const double Q = p * p * p / 27 + q * q / 4;

  if (Q > 0) {
    const double alpha =
        std::cbrt((q <= 0) ? (-q / 2 + std::sqrt(Q)) : (-q / 2 - std::sqrt(Q)));
    const double beta = -p / (3 * alpha);

    if (p <= 0) {
      roots[0] = alpha + beta;
    } else {
      const double denom = (std::abs(alpha) >= std::abs(beta))
                               ? (alpha * (alpha - beta) + beta * beta)
                               : (alpha * alpha - beta * (alpha - beta));
      roots[0] = -q / denom;
    }

    return std::make_tuple(1u, roots);
  } else {
    // Q <= 0
    const std::complex<double> alpha =
        std::pow(std::complex<double>(-q / 2, std::sqrt(-Q)), 1.0 / 3.0);

    roots[0] = 2 * alpha.real();
    roots[1] = -alpha.real() +
               (((alpha.real() >= 0) == (alpha.imag() >= 0)) ? (-1.0) : 1.0) *
                   (std::sqrt(3.0) * alpha.imag());
    roots[2] = -q / (roots[0] * roots[1]);
    return std::make_tuple(3u, roots);
  }
}

std::tuple<unsigned, std::array<double, 3>>
solve_cubic(const double a, const double b, const double c, const double d) {
  const double qnan = std::numeric_limits<double>::quiet_NaN();
  const double shift = b / (3 * a);
  const double tiny =
      std::sqrt(std::numeric_limits<double>::epsilon()) * std::abs(shift);

  const double p = (3 * a * c - b * b) / (3 * a * a);
  const double q =
      (2 * b * b * b - 9 * a * b * c + 27 * a * a * d) / (27 * a * a * a);

  unsigned count = 0;
  std::array<double, 3> roots = {qnan, qnan, qnan};
  std::tie(count, roots) = solve_reduced_cubic(p, q);

  const double y1 = roots[0];

  for (auto &x : roots) {
    x -= shift;
  }

  if (std::none_of(roots.cbegin(), roots.cbegin() + count,
                   [tiny](const double x) { return std::abs(x) <= tiny; })) {
    // All roots are large enough!
    std::sort(roots.begin(), roots.end());
    return std::make_tuple(count, roots);
  } else if (count == 1) {
    // There's one small real root, and two large complex ones.  They
    // have to both be large, since they're conjugates and add up to
    // (almost) -b/a.
    const double y2ty3 = -q / y1;
    const double y2py3 = -y1;
    const double x2tx3 = y2ty3 + (shift - y2py3) * shift;
    roots[0] = -d / (x2tx3 * a);
    return std::make_tuple(count, roots);
  } else {
    // All three roots are real, and some lose precision due to
    // catastrophic cancellation.
    std::partition(roots.begin(), roots.end(),
                   [tiny](const double x) { return std::abs(x) <= tiny; });

    // All three roots can't possibly end up tiny, because they add up
    // to -b/a, and at least one is.  So we need to figure out if
    // there's two or one.
    if (std::abs(roots[1]) <= tiny) {
      // There are two tiny roots. Rats. We'll find their sum and
      // product, and then find themselves from an auxiliary quadratic
      // equation.
      const double x1tx2 = -d / (a * roots[2]);
      // x3 * (x1 + x2) + x1*x2 = c / a
      // There's no (significant) cancellation here, because, as we
      // know, x1*x2 is tiny compared to x3 * (x1 + x2);
      const double x1px2 = (c / a - x1tx2) / roots[2];

      std::tie(roots[0], roots[1]) = solve_quadric(1.0, -x1px2, x1tx2);

      if (roots[0] != roots[0]) {
        // We had roots, and we lost them.  This means D is almost
        // zero, so they're essentially equal.
        roots[0] = roots[1] = x1px2 / 2;
      }

    } else {
      // There's only one tiny root, and we can find it from Viet.
      roots[0] = -d / (a * roots[1] * roots[2]);
    }

    std::sort(roots.begin(), roots.end());
    return std::make_tuple(count, roots);
  }
}

double eval_quartic(const double x, const double a, const double b,
                    const double c, const double d, const double e) {
  return e + x * (d + x * (c + x * (b + x * a)));
}

std::tuple<double, double> minimise_quartic(const double a, const double b,
                                            const double c, const double d,
                                            const double e) {
  unsigned root_count = 0;
  std::array<double, 3> deriv_roots;
  std::tie(root_count, deriv_roots) = solve_cubic(4 * a, 3 * b, 2 * c, d);

  double x = 0.0, fv = e;
  for (unsigned root = 0; root < root_count; ++root) {
    double candidate = eval_quartic(deriv_roots[root], a, b, c, d, e);
    if (candidate < fv) {
      x = deriv_roots[root];
      fv = candidate;
    }
  }

  return std::make_tuple(x, fv);
}
} // namespace Poly
