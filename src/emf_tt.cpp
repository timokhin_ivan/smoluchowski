#include "include/emf_tt.hpp"
#include "include/convolution.hpp"
#include "include/csv.hpp"
#include "include/cubic.hpp"
#include "include/integration.hpp"
#include "include/jacobi_solver.hpp"
#include "include/qkrylov.hpp"
#include "include/reduction.hpp"
#include "include/smoluchowski.hpp"
#include "include/ttsuite/blapack.hpp"
#include "include/ttsuite/log.hpp"
#include <algorithm>
#include <chrono>
#include <fstream>
#include <iomanip>
#include <iterator>
#include <sstream>
#include <string>

double find_best_step(const TT::TTT<double, 3> &op,
                      const TT::TTT<double, 1> &current,
                      const TT::TTT<double, 1> &current_rhs,
                      const double current_rhs_sqr_norm,
                      const TT::TTT<double, 1> &direction,
                      const TT::TTT<double, 1> &source, double eps,
                      size_t max_rank);

int ttt::run() {
  const size_t N = size_t{1} << logN;
  const size_t maxR = max_rank;
  const size_t K = max_iters;

#if 0
  auto kernel_f = [a](std::array<size_t, 2> idx) {
    auto i = idx[0] + 1, j = idx[1] + 1;
    auto i_a = std::pow(i, a), j_a = std::pow(j, a);
    return i_a / j_a + j_a / i_a;
  };
#else
  auto kernel_f = [](std::array<size_t, 2>) -> double {
    return 1; // idx[0] + idx[1];
  };
#endif

  std::mt19937_64 gen{};

  auto start = std::chrono::steady_clock::now();
  log(emf::severity::info, "main", "Starting kernel cross approximation");
  auto kernel =
      TT::TTT<double, 2>::dmrg_cross(kernel_f, {N, N}, 1e-8, 100, 200, gen);
  auto end = std::chrono::steady_clock::now();
  log(emf::severity::info, "main", "Done with kernel cross approximation");
  std::chrono::duration<double> diff = end - start;
  kernel.round(0.0, maxR);
  logf(emf::severity::info, "main",
       "Kernel max rank %zu, time taken by cross %g", kernel.max_rank(),
       diff.count());

  std::uniform_int_distribution<size_t> dist{0, N - 1};

  const size_t M = 100000;
  double diff_nrm = 0.0, nrm = 0.0;
  for (size_t i = 0; i < M; ++i) {
    std::array<size_t, 2> idx = {dist(gen), dist(gen)};
    double ref = kernel_f(idx);
    double d = kernel.element_at(idx.begin(), idx.end()) - ref;
    diff_nrm += d * d;
    nrm += ref * ref;
  }

  logf(emf::severity::info, "main",
       "Estimated relative error of kernel approximation %g",
       std::sqrt(diff_nrm / nrm));

  start = std::chrono::steady_clock::now();
  log(emf::severity::info, "main", "Starting operator calculation");
  auto op = kernel.extend_repeating(N) *
            (Smoluchowski::production<double>(N) -
             Smoluchowski::consumption<double>(N, false) * (1 + lambda) -
             Smoluchowski::consumption<double>(N, true) * (1 + lambda) +
             lambda * Smoluchowski::fragmentation_products<double>(N)) /
            2;
  auto err = op.round(1e-10);
  op.flip3dim();
  end = std::chrono::steady_clock::now();
  logf(emf::severity::info, "main",
       "Done calculating operator; rank is %zu, rounding error is %g",
       op.max_rank(), err);
  diff = end - start;

  auto mass_tester = TT::TTT<double, 1>::dmrg_cross(
      [](std::array<size_t, 1> idx) -> double { return idx[0] + 1; }, {N}, 1e-8,
      100, 200, gen);
  auto sum_tester = TT::TTT<double, 1>::dmrg_cross(
      [](std::array<size_t, 1>) -> double { return 1; }, {N}, 1e-8, 100, 200,
      gen);

  if (sources.size() != source_powers.size())
    throw "Source count inconsistency";

  if (sources.empty())
    throw "Need at least one source";

  auto source =
      source_powers.front() * TT::TTT<double, 1>::delta({N}, {sources.front()});

  for (size_t i = 1; i < sources.size(); ++i) {
    source = source + source_powers.at(i) *
                          TT::TTT<double, 1>::delta({N}, {sources.at(i)});
  }
  // auto source = TT::TTT<double, 1>::delta({N}, {0}) +
  //               (1.0 / 8) * TT::TTT<double, 1>::delta({N}, {256});
  source.round(1e-18);

  auto x = sum_tester / (static_cast<double>(N));
  logf(emf::severity::info, "main", "Initial mass: %g", x.dot(mass_tester));
  auto olddir = x;

  start = std::chrono::steady_clock::now();
  log(emf::severity::notice, "main", "Starting Newton loop");
  for (size_t k = 0; k < K; ++k) {
    // x = Smoluchowski::ttt_rk4_step(op_f, dt, x, 1e-12, 200);
    auto interm = op.tdot(x);
    interm.round(1e-18);

    auto S = interm.tdot(x) + source;
    S.round(1e-18);

    auto J = 2.0 * interm;

    if (k == 0)
      olddir = S;
    auto dir = olddir;
    dir.round(1e-18);
    double rhs_sqr_norm = S.dot(S);
    auto rhs_norm = std::sqrt(rhs_sqr_norm);
    if (rhs_norm < target_rhs_norm)
      break;
    J.amen(S, dir, 1e-6 * rhs_norm, 1e-10 * rhs_norm, 6, 16, 256,
           1e-10 * rhs_norm, [](size_t pass, double norm) {
             logf(emf::severity::info, "amen", "Starting pass %zu with norm %g",
                  pass, norm);
           });

    const double step_size =
        find_best_step(op, x, S, rhs_sqr_norm, dir, source,
                       std::min(1e-8, rhs_norm * 1e-5), maxR);
    x = x + step_size * dir;
    //x = x - dir;
    x.round(std::min(1e-8, rhs_norm * 1e-5), maxR);

    logf(emf::severity::info, "main",
         "Iteration %zu, rank %zu, rhs norm %g, step_size %g", k, x.max_rank(),
         rhs_norm, step_size);

    olddir = dir;
  }
  end = std::chrono::steady_clock::now();
  diff = end - start;
  logf(emf::severity::notice, "main", "Newton loop done; time taken %g",
       diff.count());

  auto fout = data_file("solution");
  for (std::array<size_t, 1> i = {0}; i[0] < std::min(size_t{8192}, N);
       ++i[0]) {
    fout << i[0] + 1 << '\t' << x.element_at(i.cbegin(), i.cend()) << std::endl;
  }

  for (std::array<size_t, 1> i = {8192}; i[0] < N; i[0] = (i[0] * 513) / 512) {
    fout << i[0] + 1 << '\t' << x.element_at(i.cbegin(), i.cend()) << std::endl;
  }

  std::array<size_t, 1> i = {N - 1};
  fout << N << '\t' << x.element_at(i.cbegin(), i.cend()) << std::endl;

  return 0;
}

double find_best_step(const TT::TTT<double, 3> &op,
                      const TT::TTT<double, 1> &current,
                      const TT::TTT<double, 1> &current_rhs,
                      const double current_rhs_sqr_norm,
                      const TT::TTT<double, 1> &direction,
                      const TT::TTT<double, 1> &source, double eps,
                      size_t max_rank) {
  // Part 1: Find quadratic vector coefficients via interpolation
  TT::TTT<double, 1> cur_minus_dir = current - direction,
                     cur_plus_dir = current + direction;
  cur_minus_dir.round(eps, max_rank);
  cur_plus_dir.round(eps, max_rank);

  TT::TTT<double, 2> tmp = op.tdot(cur_minus_dir);
  tmp.round(eps, max_rank);
  TT::TTT<double, 1> at_cur_minus_dir = tmp.tdot(cur_minus_dir);
  at_cur_minus_dir.round(eps, max_rank);
  at_cur_minus_dir = at_cur_minus_dir + source;
  at_cur_minus_dir.round(eps, max_rank);

  tmp = op.tdot(cur_plus_dir);
  tmp.round(eps, max_rank);
  TT::TTT<double, 1> at_cur_plus_dir = tmp.tdot(cur_plus_dir);
  at_cur_plus_dir.round(eps, max_rank);
  at_cur_plus_dir = at_cur_plus_dir + source;
  at_cur_plus_dir.round(eps, max_rank);

  at_cur_plus_dir = at_cur_plus_dir - current_rhs;
  at_cur_plus_dir.round(eps, max_rank);
  at_cur_minus_dir = at_cur_minus_dir - current_rhs;
  at_cur_minus_dir.round(eps, max_rank);

  TT::TTT<double, 1> a = (at_cur_plus_dir + at_cur_minus_dir) / 2,
                     b = (at_cur_plus_dir - at_cur_minus_dir) / 2;
  a.round(eps, max_rank);
  b.round(eps, max_rank);
  const TT::TTT<double, 1> &c = current_rhs;

  // Part 2: find quartic coefficients as dot products

  // For minima-finding purposes, the last term need not be accurate.
  const double qa = a.dot(a), qb = 2 * a.dot(b), qc = b.dot(b) + 2 * a.dot(c),
               qd = 2 * b.dot(c), qe = current_rhs_sqr_norm;

  const double classic = qa - qb + qc - qd + qe;

  emf::experiment::logf(
      emf::severity::debug, "main",
      "qa = %g, qb = %g, qc = %g, qd = %g, qe = %g, √f(-1) = %g", qa, qb, qc,
      qd, qe, std::sqrt(qa - qb + qc - qd + qe));

  if (classic < qe) {
    return -1;
  } else {
    auto [x, _] = Poly::minimise_quartic(qa, qb, qc, qd, qe);

    return x;
  }
}
