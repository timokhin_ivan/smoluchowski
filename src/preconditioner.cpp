#include "../include/preconditioner.hpp"
#include "../include/ttsuite/log.hpp"
#include <mkl.h>
#include <iostream>

namespace Jacobi {

BandMatrix makeBand(size_t N, size_t p, TT::MatrixCross<double> &kernel_cross,
                    double lambda,
                    TT::LowDimensional::VectorRef<const double> n, double Icoef, double Jcoef) {
  if (p < 1)
    p = 1;
  if (p >= N)
    p = N - 2;
  const std::array<size_t, 1> size{N}, rank{kernel_cross.rank()};
  TT::LowDimensional::VectorD diag(size), tmp(rank);

  TT::BLAS::L2::gemv(TT::BLAS::transpose::yes, 1.0, kernel_cross.v().strided(),
                     n.strided(), 0.0, tmp.ref().strided());
  TT::BLAS::L2::gemv(TT::BLAS::transpose::no, 1.0, kernel_cross.u().strided(),
                     tmp.cref().strided(), 0.0, diag.ref().strided());

  const std::array<size_t, 1> bsize{(N - 1) * p};
  TT::LowDimensional::VectorD band(bsize);
  TT::BLAS::L1::scal(-(1 + lambda)*Jcoef, diag.ref().strided());
  for (size_t i = 0; i < N - 1; ++i) {
    band(i * p) = Icoef + diag(i + 1);
  }
  // scal(N - 1, -(1 + lambda), diag + 1, 1);
  // copy(N - 1, diag + 1, 1, band.data(), p);

  for (size_t k = 1; k < p; ++k) {
    for (size_t j = 0; j < N - 1 - k; ++j) {
      band(j * p + k) = n(k - 1) * kernel_cross.calculate_element(j + 1, k - 1) * Jcoef;
    }
  }

  return BandMatrix(band.data(), N - 1, p);
}

Preconditioner::Preconditioner(size_t N, size_t R,
                               TT::MatrixCross<double> &kernel_cross,
                               double lambda,
                               TT::LowDimensional::VectorRef<const double> nv,
                               size_t p, double Icoef, double Jcoef)
    : n(N), rank(R), X((R + 2) * N), Y((R + 2) * N), C((R + 2) * (R + 2)),
      D(0.0), ipiv(2 * (R + 2)),
      band(Jacobi::makeBand(N, p, kernel_cross, lambda, nv, Icoef, Jcoef)) {
  auto nvec = nv.data();
  std::array<size_t, 1> vN{N}; //, vR{R};
  TT::LowDimensional::VectorD indexed_n(vN);
  // std::vector<double> indexed_n(n);
  // double *indexed_n_p = indexed_n.data();
  // indexed_n(0) = 0.0;
  // for (size_t k = 1; k < n; ++k)
  //   indexed_n(k) = (k + 1) * nvec[k];

  /*
  TT::LowDimensional::VectorD cropped_n(nvec, vN);
  //double *cropped_n_p = cropped_n.data();
  cropped_n(0) = 0.0;

  TT::LowDimensional::VectorD indexed_n_prod(vN), n_prod(vN), tmp(vR);
  TT::BLAS::L2::gemv(TT::BLAS::transpose::yes, 1.0, kernel_cross.v().strided(),
  indexed_n.cref().strided(), 0.0, tmp.ref().strided());
  TT::BLAS::L2::gemv(TT::BLAS::transpose::no, 1.0, kernel_cross.u().strided(),
  tmp.cref().strided(), 0.0, indexed_n_prod.ref().strided());
  //double *indexed_n_prod = kernel_cross.matvec(indexed_n_p);
  TT::BLAS::L2::gemv(TT::BLAS::transpose::yes, 1.0, kernel_cross.v().strided(),
  cropped_n.cref().strided(), 0.0, tmp.ref().strided());
  TT::BLAS::L2::gemv(TT::BLAS::transpose::no, 1.0, kernel_cross.u().strided(),
  tmp.cref().strided(), 0.0, n_prod.ref().strided());
  //double *n_prod = kernel_cross.matvec(cropped_n_p);
  */

  Y[0] = 0.0;

  for (size_t k = 1; k < n; ++k) {
    double v = (lambda * (k + 1) - 1) * kernel_cross.calculate_element(0, k);
    Y[k] = v * nvec[0];
    Y[0] += v * nvec[k];
    // n_prod(k) *= k + 1;
  }

  // axpy(n - 1, lambda, indexed_n_prod + 1, 1, Y.data() + 1, 1);
  // axpy(n - 1, lambda, n_prod + 1, 1, Y.data() + 1, 1);

  Y[0] -= 2 * kernel_cross.calculate_element(0, 0) * nvec[0];

  if (std::abs(Y[0] - 1.0) > 0.5) {
    D = 1.0;
    Y[0] -= 1.0;
  } else {
    D = -1.0;
    Y[0] += 1.0;
  }

  X[0] = 1;
  // done with first row

  Y[n] = 1;

  for (size_t k = 1; k < n; ++k) {
    X[k + n] = (nvec[k - 1] * kernel_cross.calculate_element(0, k - 1));
  }

  for (size_t i = 0; i < rank; ++i) {
    X[n * (i + 2)] = 0.0;
    for (size_t j = 0; j < n - 1; ++j) {
      X.data()[n * (i + 2) + 1 + j] =
          nvec[1 + j] * kernel_cross.u().data()[n * i + 1 + j];
    }
    // vdMul(n - 1, nvec + 1, kernel_cross.export_U() + n * i + 1,
    //       X.data() + n * (i + 2) + 1);
  }

  cblas_dscal(n * rank, -(1 + lambda), X.data() + 2 * n, 1);
  cblas_dcopy(n * rank, kernel_cross.v().data(), 1, Y.data() + 2 * n, 1);

  cblas_dscal(X.size(), Jcoef, X.data(), 1);

  for (size_t i = 0; i < rank + 2; ++i) {
    X[i * n] *= D;
    band.apply(X.data() + i * n + 1, true);
  }

  for (size_t j = 0; j < rank + 2; ++j) {
    for (size_t i = 0; i < rank + 2; ++i) {
      {
        if (i == j)
          C[i + j * (rank + 2)] = 1.0;
        else
          C[i + j * (rank + 2)] = 0.0;
      }
    }
  }

  cblas_dgemm(CblasColMajor, CblasTrans, CblasNoTrans, rank + 2, rank + 2, n,
              1.0, Y.data(), n, X.data(), n, 1.0, C.data(), rank + 2);
  int info = LAPACKE_dgetrf(LAPACK_COL_MAJOR, static_cast<lapack_int>(rank + 2),
                 static_cast<lapack_int>(rank + 2), C.data(),
                 static_cast<lapack_int>(rank + 2), ipiv.data());
  if (info != 0)
    ttlog_error("dgetrf terminated with info %d", info);

  rank += 2;
}

void Preconditioner::apply(double *x) const {
  x[0] *= D;
  band.apply(x + 1, true);

  std::vector<double> tmp(rank);
  cblas_dgemv(CblasColMajor, CblasTrans, n, rank, 1.0, Y.data(), n, x, 1, 0.0,
              tmp.data(), 1);
  LAPACKE_dgetrs(LAPACK_COL_MAJOR, 'N', rank, 1, C.data(), rank, ipiv.data(),
                 tmp.data(), rank);
  cblas_dgemv(CblasColMajor, CblasNoTrans, n, rank, -1.0, X.data(), n,
              tmp.data(), 1, 1.0, x, 1);
}

} // namespace Jacobi
