#include "../include/experiment.hpp"

namespace Experiment {

namespace Impl {

Localtime::Localtime() {
  std::time_t timer = std::time(nullptr);
  localtime_r(&timer, &time);
}

std::basic_ostream<char> &operator<<(std::basic_ostream<char> &ostr,
                                     const Localtime &tm) {
  enum { BUFFER_SIZE = 21 };
  char buffer[BUFFER_SIZE] = {};
  // ISO 8601
  std::strftime(buffer, BUFFER_SIZE, "%Y-%m-%dT%H:%M:%SZ", &tm.time);
  return ostr << buffer;
}

Duration::Duration(std::chrono::duration<double> duration) {
  double seconds = duration.count();

  h = static_cast<int>(seconds / 3600);
  seconds -= h * 3600;

  m = static_cast<int>(seconds / 60);
  seconds -= m * 60;

  s = seconds;
}

std::ostream &operator<<(std::ostream &ostr, const Duration &d) {
  // ISO 8601
  ostr << "PT";
  if (d.h != 0) {
    ostr << 'H' << d.h;
  }
  if (d.m != 0 || d.h != 0) {
    ostr << 'M' << d.m;
  }
  ostr << 'S' << d.s;
  return ostr;
}

} // namespace Impl

} // namespace Experiment
