#include "include/emf_reduction.hpp"
#include "include/convolution.hpp"
#include "include/csv.hpp"
#include "include/reduction.hpp"
#include "include/ttsuite/low_dimensional.hpp"
#include "include/ttsuite/matrix_cross.hpp"
#include <random>

namespace {
template <typename Gen>
Direct::Smoluchowski::Operator prepare_operator(double a, size_t N, Gen &gen) {
  auto kernel_cross = TT::MatrixCross<double>::cross(
      Direct::Smoluchowski::Kernel::Diffusion{a}, N, N, 1e-10, 20, gen);

  return Direct::Smoluchowski::Operator(kernel_cross.u(), kernel_cross.v());
}
} // namespace

int reduction_size_change_table::run() {
  const double window_size = total_time / window_count;
  auto gen = std::mt19937_64{};

  using namespace TT::LowDimensional;
  VectorD x0 = VectorD::kdelta({0}, {primary_size});
  VectorD x = x0;
  VectorD source = x0;

  auto S = prepare_operator(a, x0.size(0), gen);

  auto primary_basis_log_f = data_file("primary_basis_log.csv");
  primary_basis_log_f << "size,extended,norm_ratio,t\n";
  logf(emf::severity::info, "main", "Finding basis for primary size %zu",
       static_cast<size_t>(primary_size));
  auto [primary_basis, primary_time] = Reduction::find_windowed_basis_for(
      0.0, total_time, dt, x.ref(), S, window_size, window_samples, 1e-10,
      1e-12,
      [csv_log =
           CSV::Writer<size_t, bool, double, double>(primary_basis_log_f)](
          size_t size, bool ext, double norm_ratio, double t) mutable {
        csv_log.write(size, ext, norm_ratio, t);
        logf(emf::severity::info, "basis",
             "Time is %g, basis size is %zu, norm ratio is %g", t, size,
             norm_ratio);
      });
  primary_basis_log_f.close();

  const size_t primary_rank = primary_basis.size(1);
  logf(emf::severity::info, "main", "Found basis, rank = %zu", primary_rank);

  for (auto &&secondary_size :
       static_cast<const std::vector<size_t> &>(secondary_sizes)) {
    if (secondary_size < primary_rank) {
      logf(emf::severity::notice, "main",
           "Skipping secondary size %zu on the account of it being below basis "
           "size %zu",
           secondary_size, primary_rank);
      continue;
    }

    if (secondary_size > x0.size()) {
      size_t old_size = x0.size();
      x0.resize_last(secondary_size);
      std::fill(x0.begin() + old_size, x0.end(), 0.0);
      source.resize_last(secondary_size);
      std::fill(source.begin() + old_size, source.end(), 0.0);
    }

    auto rescaled_basis = Reduction::rescale_basis_for_size(
        primary_basis.cref().strided(), secondary_size);
    auto sized_S = prepare_operator(a, secondary_size, gen);
    auto [reduced_source, reduced_x0, reduced_op] =
        Reduction::prepare_reduced_problem(
            rescaled_basis.cref(),
            source.cref().slice_last(0, secondary_size).strided(),
            x0.cref().slice_last(0, secondary_size).strided(), sized_S);

    VectorD reduced_x = reduced_x0;

    MatrixD reduced_samples{{primary_rank, num_samples}};
    auto reduced_samples_ref = reduced_samples.ref();
    log(emf::severity::info, "main", "Starting integration of reduced system");
    Reduction::integrate_reduced_from_to(
        0, total_time, dt, reduced_x0.ref(), reduced_op.cref(),
        reduced_source.cref(), &reduced_samples_ref);
    log(emf::severity::info, "main", "Finished integration of reduced system");

    MatrixD full_samples{{secondary_size, num_samples}};
    auto full_samples_ref = full_samples.ref();
    VectorD scaled_x0 = x0.cref().slice_last(0, secondary_size);
    log(emf::severity::info, "main", "Starting integration of full system");
    Reduction::integrate_full_from_to(0, total_time, dt, scaled_x0.ref(),
                                      sized_S, &full_samples_ref);
    log(emf::severity::info, "main", "Finished integration of full system");

    MatrixD reconstructed_reduced_samples{{secondary_size, num_samples}};

    TT::BLAS::L3::gemm(TT::BLAS::transpose::no, TT::BLAS::transpose::no, 1.0,
                       rescaled_basis.cref().strided(),
                       reduced_samples.cref().strided(), 0.0,
                       reconstructed_reduced_samples.ref().strided());

    TT::BLAS::L1::axpy(
        -1.0,
        full_samples.cref()
            .template reshape<1>({full_samples.size()})
            .strided(),
        reconstructed_reduced_samples.ref()
            .template reshape<1>({reconstructed_reduced_samples.size()})
            .strided());

    auto diffs_file = data_file_f("errs_%zu.csv", secondary_size);
    diffs_file << "err,terr,nrm,tnrm,sample,sample_t\n";
    CSV::Writer<double, double, double, double, size_t, double> diffs_csv{
        diffs_file};
    const double sample_dt = total_time / (num_samples - 1);
    for (size_t i = 0; i < num_samples; ++i) {
      const double ref_norm =
          TT::BLAS::L1::nrm2(full_samples.cref().index_last(i).strided());
      const double diff_norm = TT::BLAS::L1::nrm2(
          reconstructed_reduced_samples.cref().index_last(i).strided());
      const double truncated_diff_norm = TT::BLAS::L1::nrm2(
          reconstructed_reduced_samples.cref()
              .index_last(i)
              .slice_last(0, std::min(static_cast<size_t>(primary_size),
                                      secondary_size))
              .strided());
      const double truncated_norm = TT::BLAS::L1::nrm2(
          full_samples.cref()
              .index_last(i)
              .slice_last(0, std::min(static_cast<size_t>(primary_size),
                                      secondary_size))
              .strided());
      diffs_csv.write(diff_norm, truncated_diff_norm, ref_norm, truncated_norm,
                      i, i * sample_dt);
    }
  }

  return 0;
}

int uniform_reference_solution::run() {
  auto gen = std::mt19937_64{};
  auto S = prepare_operator(a, size, gen);
  using namespace TT::LowDimensional;
  VectorD x0 = VectorD::kdelta({0}, {size});

  MatrixD samples{{x0.size(0), num_snapshots}};
  MatrixRefD samples_ref = samples.ref();

  Reduction::integrate_full_from_to(0, total_time, dt, x0.ref(), S,
                                    &samples_ref);

  for (size_t i = 0; i < samples.size(1); ++i) {
    auto file = data_file_f("snapshot_%05zu.csv", i);
    file << "k,n\n";

    samples.cref().index_last(i).iterate(
        [writer = CSV::Writer<size_t, double>{file}](
            double n, size_t k) mutable { writer.write(k, n); });
  }

  const double snapshot_dt = total_time / (num_snapshots - 1);

  auto file = data_file("timings.csv");

  file << "number,time\n";
  auto writer = CSV::Writer<size_t, double>{file};
  for (size_t i = 0; i < num_snapshots; ++i) {
    writer.write(i, i * snapshot_dt);
  }

  return 0;
}
