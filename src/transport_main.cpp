#include "../include/experiment.hpp"
#include "../include/transport.hpp"
#include "../include/ttsuite/log.hpp"
#include "../include/ttsuite/matrix_cross.hpp"
#include "include/convolution.hpp"
#include "include/ttsuite/low_dimensional.hpp"
#include <cmath>
#include <cstring>
#include <iostream>
#include <random>

namespace ArgNames {
const char N[] = "N";
const char a[] = "a";
const char L[] = "L";
const char dx[] = "dx";
const char T[] = "T";
const char dt[] = "dt";
const char type[] = "type";
const char vp[] = "vp";
const char eps[] = "eps";
}; // namespace ArgNames

constexpr size_t N_SNAPSHOTS = 16;

enum class type { full, low_rank };

std::ostream &operator<<(std::ostream &os, type ty) {
  switch (ty) {
  case type::full:
    os << "full";
    break;
  case type::low_rank:
    os << "low_rank";
    break;
  default:
    os << "<unknown>";
  }

  return os;
}

namespace Experiment {
namespace Impl {
template <> inline ::type parse<::type>(const char *str, const char *&str_end) {
  if (std::strcmp(str, "full") == 0) {
    str_end = str + 4;
    return ::type::full;
  } else if (std::strcmp(str, "low_rank") == 0) {
    str_end = str + 8;
    return ::type::low_rank;
  } else {
    str_end = str;
    throw str;
  }
}
} // namespace Impl
} // namespace Experiment

using E = Experiment::Experiment<
    Experiment::KV<ArgNames::N, size_t>, Experiment::KV<ArgNames::a, double>,
    Experiment::KV<ArgNames::L, double>, Experiment::KV<ArgNames::dx, double>,
    Experiment::KV<ArgNames::T, double>, Experiment::KV<ArgNames::dt, double>,
    Experiment::KV<ArgNames::type, type>, Experiment::KV<ArgNames::vp, double>,
    Experiment::KV<ArgNames::eps, double>>;

struct Params {
  size_t N;
  double a, L, dx, T, dt, vp, eps;
  type ty;

  Params(const E &exp)
      : N(exp.args().get<ArgNames::N>()), a(exp.args().get<ArgNames::a>()),
        L(exp.args().get<ArgNames::L>()), dx(exp.args().get<ArgNames::dx>()),
        T(exp.args().get<ArgNames::T>()), dt(exp.args().get<ArgNames::dt>()),
        vp(exp.args().get<ArgNames::vp>()),
        eps(exp.args().get<ArgNames::eps>()),
        ty(exp.args().get<ArgNames::type>()) {}
};

void record_snapshot(E &experiment, const Direct::Transfer::Simulation &sim,
                     size_t no);
void record_snapshot(E &experiment,
                     const Direct::Transfer::LowRank::Simulation &sim,
                     size_t no);

TT::LowDimensional::VectorD
    singular_values(TT::LowDimensional::MatrixRef<const double>);

void run_experiment_full(E &experiment, const Params &params,
                         Direct::Smoluchowski::Operator &&S,
                         const Direct::Transfer::PML &pml);

void run_experiment_low_rank(E &experiment, const Params &params,
                             Direct::Smoluchowski::Operator &&S,
                             const Direct::Transfer::PML &pml);

int main(int argc, const char **argv) {
  auto experiment = E("transport", argc, argv);

  // const size_t N = experiment.args().get<ArgNames::N>();
  // const double a = experiment.args().get<ArgNames::a>();
  // const double L = experiment.args().get<ArgNames::L>();
  // const double dx = experiment.args().get<ArgNames::dx>();
  // const double T = experiment.args().get<ArgNames::T>();
  // const double dt = experiment.args().get<ArgNames::dt>();
  // const type ty = experiment.args().get<ArgNames::type>();

  Params params{experiment};

  std::mt19937_64 rng{};

  experiment.checkpoint("Constructing cross approximation");
  auto cross = TT::MatrixCross<double>::cross(
      [params](size_t i, size_t j) -> double {
        // Hackety hack.
        if (params.a >= 0) {
          double k = std::pow(static_cast<double>(i + 1) / (j + 1), params.a);
          return k + 1 / k;
        } else {
          const double u = i + 1;
          const double v = j + 1;
          const double c = std::cbrt(u) + std::cbrt(v);
          return c * c * std::sqrt(1 / u + 1 / v);
        }
      },
      params.N, params.N, 1e-10, 100, rng);
  experiment.checkpoint("Constructed cross approximation, rank is ",
                        cross.rank());

  experiment.checkpoint("Constructing operator");
  auto S = Direct::Smoluchowski::Operator(cross.u(), cross.v());
  experiment.checkpoint("Constructed operator");
  auto PML = Direct::Transfer::PML{2.5, 10, params.L / 20, params.L};

  switch (params.ty) {
  case type::full:
    run_experiment_full(experiment, params, std::move(S), PML);
    break;
  case type::low_rank:
    run_experiment_low_rank(experiment, params, std::move(S), PML);
    break;
  default:
    std::cerr << "Unsupported experiment type; terminating" << std::endl;
  }

  return 0;
}

void run_experiment_full(E &experiment, const Params &params,
                         Direct::Smoluchowski::Operator &&S,
                         const Direct::Transfer::PML &pml) {
  auto velocity = [params](size_t i) { return std::pow(i + 1, params.vp); };
  auto initial_conditions = [](size_t k, double x) {
    return std::exp(-(x + k));
  };
  auto border_condition = [](size_t k) {
    return std::exp(-static_cast<double>(k));
  };

  experiment.checkpoint("Constructing simulation");
  auto sim =
      Direct::Transfer::Simulation(std::move(S), velocity, initial_conditions,
                                   border_condition, params.L, params.dx, pml);
  experiment.checkpoint("Constructed simulation");

  // double dt = sim.courant_dt();
  if (params.dt > sim.courant_dt()) {
    ttlog_error("Requested dt %g is above Courant dt %g", params.dt,
                sim.courant_dt());
  }
  const size_t Nsteps = std::ceil(params.T / params.dt);
  const size_t snapshot_at = Nsteps / N_SNAPSHOTS;

  experiment.checkpoint("Running simulation");
  for (size_t step = 0; step < Nsteps; ++step) {
    // sim.snapshot_full().display("n");
    if (step % snapshot_at == 0) {
      experiment.checkpoint("Step no ", step, ", t = ", step * params.dt);
      record_snapshot(experiment, sim, step);
      experiment.checkpoint("Recorded step no ", step,
                            ", t = ", step * params.dt);
    }
    sim.step(params.dt);
  }

  experiment.checkpoint("Finished simulation");
  record_snapshot(experiment, sim, Nsteps);
}

void run_experiment_low_rank(E &experiment, const Params &params,
                             Direct::Smoluchowski::Operator &&S,
                             const Direct::Transfer::PML &pml) {
  auto velocity = [params](size_t i) { return std::pow(i + 1, params.vp); };
  auto ic_u = TT::LD::VectorD::generate(
      [](size_t k) -> double { return std::exp(-static_cast<double>(k)); },
      {params.N});
  auto ic_v = TT::LD::VectorD::generate(
      [&params](size_t i) -> double { return std::exp(-(i * params.dx)); },
      {static_cast<size_t>(params.L / params.dx)});
  // auto ic_v = TT::LD::VectorD::kdelta({0}, {1});
  auto initial_conditions =
      Direct::Transfer::LowRank::Matrix::rank_1(ic_u.cref(), ic_v.cref());

  auto sim = Direct::Transfer::LowRank::Simulation{
      std::move(S), velocity, initial_conditions, params.L, params.dx, pml};

  if (params.dt > sim.courant_dt()) {
    ttlog_error("Requested dt %g is above Courant dt %g", params.dt,
                sim.courant_dt());
  }
  const size_t Nsteps = std::ceil(params.T / params.dt);
  const size_t snapshot_at = Nsteps / N_SNAPSHOTS;

  experiment.checkpoint("Running simulation");
  for (size_t step = 0; step < Nsteps; ++step) {
    if (step % snapshot_at == 0) {
      experiment.checkpoint("Step no ", step, ", t = ", step * params.dt,
                            ", rank = ", sim.snapshot_full().rank());
      record_snapshot(experiment, sim, step);
      experiment.checkpoint("Recorded step no ", step,
                            ", t = ", step * params.dt);
    }
    sim.step(params.dt, params.eps);
  }

  experiment.checkpoint("Finished simulation");
  record_snapshot(experiment, sim, Nsteps);
}

void record_snapshot(E &experiment, const Direct::Transfer::Simulation &sim,
                     size_t no) {
  std::string step_s = std::to_string(no);
  if (step_s.size() < 4)
    step_s.insert(0, 4 - step_s.size(), '0');
  experiment.write_tensor_tab(sim.snapshot_full().strided(), "snapshot_full",
                              step_s, ".tab");

  auto svs = singular_values(sim.snapshot_full());
  experiment.write_tensor_tab(svs.cref().strided(),
                              "snapshot_full_singular_values", step_s, ".tab");
}

void record_snapshot(E &experiment,
                     const Direct::Transfer::LowRank::Simulation &sim,
                     size_t no) {
  std::string step_s = std::to_string(no);
  if (step_s.size() < 4)
    step_s.insert(0, 4 - step_s.size(), '0');
  experiment.write_tensor_tab(
      sim.snapshot_full().materialise().cref().strided(), "snapshot_full",
      step_s, ".tab");
}

TT::LowDimensional::VectorD
singular_values(TT::LowDimensional::MatrixRef<const double> m) {
  const size_t M = m.size(0);
  const size_t N = m.size(1);
  const size_t K = std::min(M, N);

  TT::LowDimensional::VectorD svs{{K}}, superb{{K}};
  TT::LowDimensional::MatrixD tmp{m};

  TT::LAPACK::gesvd(TT::LAPACK::svd_vectors::none,
                    TT::LAPACK::svd_vectors::none, tmp.ref().strided(),
                    svs.ref(), nullptr, nullptr, superb.ref());
  return svs;
}
