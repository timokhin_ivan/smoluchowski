#ifndef __CONVOLUTION__TTT__HPP__
#define __CONVOLUTION__TTT__HPP__

#include "ttsuite/low_dimensional.hpp"
#include <algorithm>
#include <complex>
#include <functional>
#include <memory>
#include <mkl.h>
#include <vector>

namespace Direct {

struct FFT {
  void forward(TT::LowDimensional::VectorRef<std::complex<double>> data) const;
  void inverse(TT::LowDimensional::VectorRef<std::complex<double>> data) const;

  FFT(size_t size);
  ~FFT();

  FFT(const FFT &) = delete;
  void operator=(const FFT &) = delete;

  FFT(FFT &&);
  FFT &operator=(FFT &&);

  size_t size() const;

private:
  DFTI_DESCRIPTOR_HANDLE m_handle;
};

namespace Smoluchowski {

struct Partial {
private:
  TT::LowDimensional::Matrix<std::complex<double>> conv_part;

  TT::LowDimensional::VectorD contracted, indexed_contracted;
  TT::LowDimensional::MatrixD scaled;

public:
  friend class Operator;
};

class Operator {
private:
  TT::LowDimensional::MatrixD m_kernel_X;
  TT::LowDimensional::VectorD m_kernel_D;
  double m_lambda;
  FFT m_fft;

public:
  Operator(TT::LowDimensional::MatrixRef<const double> u,
           TT::LowDimensional::MatrixRef<const double> v, double lambda = 0.0);

  size_t size() const { return m_kernel_X.size(0); }

  Partial partial(TT::LowDimensional::VectorRef<const double> x) const;

  void eval_quadratic(TT::LowDimensional::VectorRef<const double> n,
                      TT::LowDimensional::VectorRefD out) const {
    auto part = partial(n);
    combine(part, part, out);
  }

  void combine(const Partial &l, const Partial &r,
               TT::LowDimensional::VectorRefD out) const;
};

// This function assumes the direction is obtained from newton's
// method, so (dS/dn)(dir) = S(n)
double
choose_newton_step_size(TT::LowDimensional::VectorRef<const double> source,
                        Operator &op, const double Icoef, const double Jcoef,
                        TT::LowDimensional::VectorRef<const double> at,
                        TT::LowDimensional::VectorRef<const double> offset,
                        TT::LowDimensional::VectorRef<const double> val_at,
                        const double norm_val_at,
                        TT::LowDimensional::VectorRef<const double> direction);

namespace Kernel {
struct Constant {
  double c;
  double operator()(size_t, size_t) { return c; }
};

struct Diffusion {
  double a;
  double operator()(size_t i, size_t j) {
    double k = static_cast<double>(i + 1) / (j + 1);
    k = std::pow(k, a);
    return k + 1 / k;
  }
};
} // namespace Kernel

} // namespace Smoluchowski

} // namespace Direct

#endif // __CONVOLUTION__TTT__HPP__
