#ifndef JACOBI_CUBIC_HPP_
#define JACOBI_CUBIC_HPP_

#include <array>
#include <complex>
#include <tuple>

namespace Poly {

std::tuple<double, double> solve_quadric(double a, double b, double c);

std::tuple<unsigned, std::array<double, 3>>
solve_cubic(const double a, const double b, const double c, const double d);

std::tuple<double, double> minimise_quartic(const double a, const double b,
                                            const double c, const double d,
                                            const double e);

} // namespace Poly

#endif // JACOBI_CUBIC_HPP_
