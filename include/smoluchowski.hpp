#ifndef __SMOLUCHOWSKI__TTT__HPP__
#define __SMOLUCHOWSKI__TTT__HPP__

#include "ttsuite/ttt.hpp"
#include <complex>
#include <random>

namespace Smoluchowski {

template <typename T> TT::TTT<T, 3> production(size_t n) {
  using TT::TTT;
  using TT::LowDimensional::Tensor;

  auto factors = TT::factorise(n);
  const size_t dimensionality = factors.size();

  std::vector<Tensor<T, 3>> cores;
  cores.reserve(dimensionality);

  {
    std::array<size_t, 3> core_sizes = {
        1, factors.front() * factors.front() * factors.front(), 2};
    cores.emplace_back(core_sizes);

    auto core = cores.back().ref().template reshape<4>(
        {factors.front(), factors.front(), factors.front(), 2});

    for (size_t k = 0; k < core.size(2); ++k)
      for (size_t j = 0; j < core.size(1); ++j)
        for (size_t i = 0; i < core.size(0); ++i) {
          core(i, j, k, 0) = (i + j + 1 == k) ? 1 : 0;
        }

    for (size_t k = 0; k < core.size(2); ++k)
      for (size_t j = 0; j < core.size(1); ++j)
        for (size_t i = 0; i < core.size(0); ++i) {
          core(i, j, k, 1) = (i + j + 1 == factors.front() + k) ? 1 : 0;
        }
  }

  for (size_t c = 1; c + 1 < dimensionality; ++c) {
    std::array<size_t, 3> core_sizes = {
        2, factors.at(c) * factors.at(c) * factors.at(c), 2};
    cores.emplace_back(core_sizes);

    auto core = cores.back().ref().template reshape<5>(
        {2, factors.at(c), factors.at(c), factors.at(c), 2});

    for (size_t k = 0; k < core.size(3); ++k)
      for (size_t j = 0; j < core.size(2); ++j)
        for (size_t i = 0; i < core.size(1); ++i) {
          core(0, i, j, k, 0) = (i + j == k) ? 1 : 0;
          core(1, i, j, k, 0) = (i + j + 1 == k) ? 1 : 0;
        }

    for (size_t k = 0; k < core.size(3); ++k)
      for (size_t j = 0; j < core.size(2); ++j)
        for (size_t i = 0; i < core.size(1); ++i) {
          core(0, i, j, k, 1) = (i + j == factors.at(c) + k) ? 1 : 0;
          core(1, i, j, k, 1) = (i + j + 1 == factors.at(c) + k) ? 1 : 0;
        }
  }

  {
    std::array<size_t, 3> core_sizes = {
        2, factors.back() * factors.back() * factors.back(), 1};
    cores.emplace_back(core_sizes);

    auto core = cores.back().ref().template reshape<4>(
        {2, factors.back(), factors.back(), factors.back()});

    for (size_t k = 0; k < core.size(3); ++k)
      for (size_t j = 0; j < core.size(2); ++j)
        for (size_t i = 0; i < core.size(1); ++i) {
          core(0, i, j, k) = (i + j == k) ? 1 : 0;
          core(1, i, j, k) = (i + j + 1 == k) ? 1 : 0;
        }
  }

  std::array<std::vector<size_t>, 3> ttt_factors = {factors, factors, factors};
  return TTT<T, 3>::from_components(
      TT::TensorTrain<T>::from_cores(std::move(cores)), std::move(ttt_factors));
}

template <typename T> TT::TTT<T, 3> fragmentation_products(size_t n) {
  std::mt19937_64 gen{};
  auto d2 =
    TT::TTT<T, 2>::dmrg_cross([](std::array<size_t, 2> ix) { return ix[0] + ix[1] + 2; },
                                {n, n}, T(1e-10), 5, 10, gen);
  return d2.extend_with_zeroes(n);

}

template <typename T> TT::TTT<T, 3> consumption(size_t n, bool side) {
  using TT::TTT;
  using TT::LowDimensional::Tensor;

  auto factors = TT::factorise(n);
  const size_t dimensionality = factors.size();

  std::vector<Tensor<T, 3>> cores;
  cores.reserve(dimensionality);

  for (auto factor : factors) {
    std::array<size_t, 3> core_sizes = {1, factor * factor * factor, 1};
    cores.emplace_back(core_sizes);
    auto core =
        cores.back().ref().template reshape<3>({factor, factor, factor});

    for (size_t k = 0; k < core.size(2); ++k)
      for (size_t j = 0; j < core.size(1); ++j)
        for (size_t i = 0; i < core.size(0); ++i) {
          core(i, j, k) = side ? (i == k ? 1 : 0) : (j == k ? 1 : 0);
        }
  }

  std::array<std::vector<size_t>, 3> ttt_factors = {factors, factors, factors};
  return TTT<T, 3>::from_components(
      TT::TensorTrain<T>::from_cores(std::move(cores)), std::move(ttt_factors));
}

template <typename T, size_t N, typename Fn>
TT::TTT<T, N> ttt_rk4_step(Fn fn, T dt, TT::TTT<T, N> x, T eps,
                           size_t max_rank) {
  const auto two = T(2), six = T(6);

  auto k1 = dt * fn(T(0), x);
  k1.round(eps, max_rank);

  auto x2 = x + k1 / two;
  x2.round(eps, max_rank);
  auto k2 = dt * fn(dt / two, x2);
  k2.round(eps, max_rank);

  auto x3 = x + k2 / two;
  x3.round(eps, max_rank);
  auto k3 = dt * fn(dt / two, x3);
  k3.round(eps, max_rank);

  auto x4 = x + k3;
  x4.round(eps, max_rank);
  auto k4 = dt * fn(dt, x4);
  k4.round(eps, max_rank);

  auto s1 = k1 + two * k2;
  s1.round(eps, max_rank);
  auto s2 = two * k3 + k4;
  s2.round(eps, max_rank);
  auto s = s1 + s2;
  s.round(eps, max_rank);

  auto new_x = x + s / six;
  new_x.round(eps, max_rank);

  return new_x;
}

template <typename T>
std::pair<std::array<T, 3>, bool> reduced_cubic(T p, T q, T eps) {
  using namespace std;
  if (abs(p) < eps && abs(q) < eps) {
    std::array<T, 3> solutions = {T(0), T(0), T(0)};
    return std::make_pair(solutions, true);
  }

  auto Q = p * p * p / 27 + q * q / 4;

  if (Q > 0) {
    T d = (q < 0) ? (-q / 2 + sqrt(Q)) : (-q / 2 - sqrt(Q));
    T alpha = cbrt(d);
    T beta = -p / (3 * alpha);

    T root;
    if (p <= 0) {
      root = alpha + beta;
    } else {
      root = -q / (alpha * alpha - alpha * beta + beta * beta);
    }

    std::array<T, 3> roots = {root, root, root};
    return std::make_pair(roots, false);
  } else {
    complex<T> d{-q / 2, sqrt(-Q)};
    complex<T> alpha = pow(d, T(1) / 3);

    T y1 = 2 * alpha.real();
    T y2 = (alpha.real() < 0) ? (-alpha.real() + alpha.imag() * sqrt(T(3)))
                              : (-alpha.real() - alpha.imag() * sqrt(T(3)));
    T y3 = -q / (y1 * y2);
    std::array<T, 3> roots = {y1, y2, y3};
    return std::make_pair(roots, true);
  }
}

template <typename T>
std::pair<std::array<T, 3>, bool> cubic(T b, T c, T d, T small, T eps) {
  T p = c - b * b / 3;
  T q = (2 * b * b * b - 9 * b * c + 27 * d) / 27;
  auto reduced_roots = reduced_cubic(p, q, eps);
  T shift = -b / 3;
  std::array<T, 3> results = {reduced_roots.first[0] + shift,
                              reduced_roots.first[1] + shift,
                              reduced_roots.first[2] + shift};
  return std::make_pair(results, reduced_roots.second);
}

template <typename T> inline T eval_quartic(T a, T b, T c, T d, T x) {
  return (((x + a) * x + b) * x + c) + d;
}

template <typename T>
T minimise_quartic(T a, T b, T c, T d, T def, T eps, T small) {
  auto stationary = cubic(3 * a / 4, b / 2, c / 4, eps, small).first;

  T argmin = def;
  T min = eval_quartic(a, b, c, d, argmin);

  for (auto x : stationary) {
    T min_cand = eval_quartic(a, b, c, d, x);
    if (min_cand < min) {
      argmin = x;
      min = min_cand;
    }
  }

  return argmin;
}

template <typename T, typename Op, typename Dot, size_t N, size_t M>
T optimise_step(const TT::TTT<T, N> &x, const TT::TTT<T, M> &y,
                const TT::TTT<T, N> &direction, Op op, Dot dot, T eps,
                size_t max_rank) {
  auto xp1 = x + direction;
  xp1.round(eps, max_rank);
  auto xm1 = x - direction;
  xm1.round(eps, max_rank);

  auto a = y;
  auto yp1 = op(xp1);
  yp1.round(eps, max_rank);
  auto ym1 = op(xm1);
  ym1.round(eps, max_rank);

  auto b = (yp1 - ym1) / 2;
  b.round(eps, max_rank);
  auto c = (yp1 + ym1 - a * 2) / 2;
  c.round(eps, max_rank);

  T qa = dot(c, c);
  T qb = 2 * dot(b, c);
  T qc = 2 * dot(a, c) + dot(b, b);
  T qd = 2 * dot(a, b);
  T qe = dot(a, a);

  if (qa < 0)
    qa = std::abs(qa);

  return minimise_quartic(qb / qa, qc / qa, qd / qa, qe / qa, T(1), T(1e-10),
                          T(1e-5));
}

} // namespace Smoluchowski

#endif
