#ifndef __JACOBI_SOLVER__HPP__
#define __JACOBI_SOLVER__HPP__

#include "ttsuite/low_dimensional.hpp"
#include "ttsuite/matrix_cross.hpp"
#include "convolution.hpp"

namespace Direct {

void solve_jacobi(double Icoef, double Jcoef,
                  TT::LowDimensional::VectorRef<const double> at,
                  TT::LowDimensional::VectorRef<const double> rhs,
                  TT::LowDimensional::VectorRefD x,
                  const size_t max_iters, const double eps, const size_t p,
                  TT::MatrixCross<double>& kernel,
                  Smoluchowski::Operator& op
                  );
}

#endif // __JACOBI_SOLVER__HPP__
