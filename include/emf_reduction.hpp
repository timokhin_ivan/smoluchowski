#ifndef _EMF__REDUCTION__HPP__
#define _EMF__REDUCTION__HPP__

#include "emf/emf.hpp"

struct reduction_size_change_table : emf::experiment {
  EMF_PARAMETER(double, a);
  EMF_PARAMETER(double, dt);
  EMF_PARAMETER(double, total_time);
  EMF_PARAMETER(size_t, window_count);
  EMF_PARAMETER_OPTIONAL(size_t, window_samples, static_cast<size_t>(65));
  EMF_PARAMETER_OPTIONAL(size_t, num_samples, static_cast<size_t>(512));
  EMF_PARAMETER(size_t, primary_size);
  EMF_PARAMETER(std::vector<size_t>, secondary_sizes);

  int run() override;

  const char *name() const override { return "rsct"; }
};

struct uniform_reference_solution : emf::experiment {
  EMF_PARAMETER(double, a);
  EMF_PARAMETER(double, dt);
  EMF_PARAMETER(size_t, size);
  EMF_PARAMETER(double, total_time);
  EMF_PARAMETER(size_t, num_snapshots);

  const char *name() const override { return "uniform_reference"; }

  int run() override;
};

#endif // _EMF__REDUCTION__HPP__
