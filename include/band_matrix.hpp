#ifndef JACOBI_BANDMATRIX_HPP
#define JACOBI_BANDMATRIX_HPP

#include <vector>
#include <cstdlib>

class BandMatrix {
private:
  std::vector<double> a;
  size_t n, k;

public:
  BandMatrix(const double *vals, size_t N, size_t width);

  void apply(double *x, bool inverse = false) const;
};

#endif //JACOBI_BANDMATRIX_HPP
