#ifndef __QKRYLOV__HPP__
#define __QKRYLOV__HPP__

#include "ttsuite/low_dimensional.hpp"
#include "convolution.hpp"

namespace Direct {
namespace Smoluchowski {
namespace Reduction {
namespace QKrylov {
void expand_basis(TT::LowDimensional::MatrixD &basis, Operator &op,
                  const size_t max_basis_size,
                  const size_t max_iterations, const double eps);
}
} // namespace Reduction
} // namespace Smoluchowski
} // namespace Direct

#endif // __QKRYLOV_HPP__
