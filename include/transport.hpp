#ifndef __TRANSPORT__HPP__
#define __TRANSPORT__HPP__

#include "convolution.hpp"
#include "ttsuite/log.hpp"
#include "ttsuite/low_dimensional.hpp"
#include "ttsuite/matrix_cross.hpp"
#include <algorithm>
#include <cmath>
#include <cstddef>
#include <cstdlib>
#include <limits>
#include <random>
#include <utility>

namespace Direct {

namespace Transfer {

struct PML {
  double m, W, width, offset;

  double operator()(double x) const {
    static const double LN10 = 2.302585092994045;
    return x <= offset
               ? 0.0
               : (m + 1) * W * LN10 / width * std::pow((x - offset) / width, m);
  }
};

class Simulation {
private:
  size_t Nm, Nx, Npml;
  TT::LD::Matrix<double> m_df, m_f;
  TT::LD::Vector<double> m_velocity;
  double m_dx;
  PML m_pml;
  Smoluchowski::Operator m_S;

public:
  template <typename Initial, typename Border, typename Velocity>
  Simulation(Smoluchowski::Operator op, Velocity velocity,
             Initial initial_conditions, Border border_condition,
             double simulation_width, double dx, PML pml)
      : Nm(op.size()), Nx(std::ceil(simulation_width / dx)),
        Npml(std::ceil(pml.width / dx)), m_df({Nm, Nx + Npml}),
        m_f({Nm, Nx + Npml}), m_velocity({Nm}), m_dx(simulation_width / Nx),
        m_pml(pml), m_S(std::move(op)) {
    for (size_t i = 0; i < m_velocity.size(); ++i) {
      m_velocity(i) = velocity(i);
    }

    for (size_t k = 0; k < Nm; ++k) {
      m_f(k, 0) = border_condition(k);
      m_f(k, m_f.size(1) - 1) = 0.0;
    }

    for (size_t ix = 1; ix < m_f.size(1) - 1; ++ix) {
      for (size_t k = 0; k < Nm; ++k) {
        m_f(k, ix) = initial_conditions(k, ix * m_dx);
      }
    }

    std::fill(m_df.begin(), m_df.end(), 0.0);

    m_pml.offset = simulation_width;
  }

  void step(double dt);

  TT::LD::MatrixRef<const double> snapshot_full() const { return m_f.cref(); }

  TT::LD::MatrixRef<const double> snapshot() const {
    return snapshot_full().slice_last(0, Nx);
  }

  double courant_dt() const;
};

namespace Impl {
void transfer(TT::LD::MatrixRef<double> df, TT::LD::MatrixRef<const double> f,
              TT::LD::VectorRef<const double> velocity, const double dt,
              const double dx, const PML &pml);
void aggregation(TT::LD::MatrixRef<double> df,
                 TT::LD::MatrixRef<const double> f, Smoluchowski::Operator &S);
} // namespace Impl

namespace LowRank {
class Matrix {
private:
  // A = U V^T
  TT::LD::Matrix<double> m_U, m_V;

  Matrix(TT::LD::Matrix<double> U, TT::LD::Matrix<double> V)
      : m_U(std::move(U)), m_V(std::move(V)) {}

  static Matrix lcom_padded(TT::LD::MatrixRef<const double> aU,
                            TT::LD::MatrixRef<const double> aV, double a,
                            TT::LD::MatrixRef<const double> bU,
                            TT::LD::MatrixRef<const double> bV, double b);

public:
  void round(double rtol = std::numeric_limits<double>::epsilon(),
             double atol = 0.0,
             size_t max_rank = std::numeric_limits<size_t>::max());
  void round_cross(double eps,
                   size_t max_rank = std::numeric_limits<size_t>::max());

  Matrix apply(const Smoluchowski::Operator &S) const;

  Matrix hadamard(const Matrix &m) const;

  size_t rank() const { return m_U.size(1); }
  size_t row_count() const { return m_U.size(0); }
  size_t column_count() const { return m_V.size(0); }

  Matrix lcom(double a, const Matrix &other, double b) const;
  Matrix lcom_padded(double a, const Matrix &other, double b) const;
  Matrix lcom_padded(double a, const TT::MatrixCross<double> &cross,
                     double b) const;

  Matrix pad(size_t M, size_t N) const;

  std::pair<size_t, size_t> nonzero_extent() const;

  void write_transport_df_row_partial(size_t row, TT::LD::VectorRefD dest,
                                      TT::LD::VectorRef<const double> velocity,
                                      const double dt, const double dx) const;
  void write_transport_df_col_partial(size_t col, TT::LD::VectorRefD dest,
                                      TT::LD::VectorRef<const double> velocity,
                                      const double dt, const double dx,
                                      TT::LD::VectorD &work) const;

  TT::LD::MatrixD materialise() const;

  static Matrix from_pml(size_t M, size_t N, double dx, const PML &pml,
                         TT::LD::VectorRef<const double> velocity);

  static Matrix rank_1(TT::LD::VectorRef<const double> u,
                       TT::LD::VectorRef<const double> v);

  static Matrix from_cross(const TT::MatrixCross<double> &cross);
};

class Simulation {
private:
  size_t m_Nm, m_Nx, m_Npml;
  TT::LD::VectorD m_velocity;
  Matrix m_f, m_pml;
  double m_dx;
  Smoluchowski::Operator m_S;

  // The type is currently hard-coded.  Alas.
  std::mt19937_64 m_gen;

  PML m_pml_orig;

public:
  template <typename Velocity>
  Simulation(Smoluchowski::Operator op, Velocity velocity,
             const Matrix &initial_conditions, double simulation_width,
             double dx, PML pml, std::mt19937_64 gen = std::mt19937_64{})
      : m_Nm(op.size()), m_Nx(std::ceil(simulation_width / dx)),
        m_Npml(std::ceil(pml.width / dx)),
        m_velocity(TT::LD::VectorD::generate(velocity, {m_Nm})),
        m_f(initial_conditions.pad(m_Nm, m_Nx + m_Npml)),
        m_pml(
            Matrix::from_pml(m_Nm, m_Nx + m_Npml, dx, pml, m_velocity.cref())),
        m_dx(dx), m_S(std::move(op)), m_gen(gen), m_pml_orig(pml){};

  void step(double dt, double rtol = std::numeric_limits<double>::epsilon(),
            double atol = 0.0,
            size_t max_rank = std::numeric_limits<size_t>::max());

  double courant_dt() const {
    return m_dx / *std::max_element(m_velocity.cbegin(), m_velocity.cend());
  }

  const Matrix &snapshot_full() const { return m_f; }
};

} // namespace LowRank

} // namespace Transfer

} // namespace Direct

#endif // __TRANSPORT__HPP__
