#ifndef __EXPERIMENT__HPP__
#define __EXPERIMENT__HPP__

#include <cerrno>
#include <chrono>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <fstream>
#include <sstream>
#include <string>
#include <sys/stat.h>
#include <sys/types.h>

#include "ttsuite/low_dimensional.hpp"

namespace Experiment {

namespace Impl {

template <typename T> T parse(const char *str, const char *&str_end);

template <> inline long parse<long>(const char *str, const char *&str_end) {
  return std::strtol(str, const_cast<char **>(&str_end), 0);
}

template <>
inline long long parse<long long>(const char *str, const char *&str_end) {
  return std::strtoll(str, const_cast<char **>(&str_end), 0);
}

template <>
inline unsigned long parse<unsigned long>(const char *str,
                                          const char *&str_end) {
  return std::strtoul(str, const_cast<char **>(&str_end), 0);
}

template <>
inline unsigned long long parse<unsigned long long>(const char *str,
                                                    const char *&str_end) {
  return std::strtoull(str, const_cast<char **>(&str_end), 0);
}

template <> inline float parse<float>(const char *str, const char *&str_end) {
  return std::strtof(str, const_cast<char **>(&str_end));
}

template <> inline double parse<double>(const char *str, const char *&str_end) {
  return std::strtod(str, const_cast<char **>(&str_end));
}

template <>
inline long double parse<long double>(const char *str, const char *&str_end) {
  return std::strtold(str, const_cast<char **>(&str_end));
}

template <const char *x, const char *y> constexpr bool cptreq = false;

template <const char *x> constexpr bool cptreq<x, x> = true;

template <typename T, typename F>
void write_tensor_tab_with(TT::LowDimensional::StridedTensorRef<T, 0> t, F f) {
  f() << t() << std::endl;
}

template <typename T, size_t N, typename F>
void write_tensor_tab_with(TT::LowDimensional::StridedTensorRef<T, N> t, F f) {
  for (size_t i = 0; i < t.size(0); ++i) {
    write_tensor_tab_with(t.template fix_idx<0>(i), [f, i]() -> decltype(auto) {
      return (f() << i << '\t');
    });
  }
}

inline void stringify_to(std::basic_ostream<char> &) {}

template <typename T, typename... Ts>
void stringify_to(std::basic_ostream<char> &ostr, T arg, Ts... args) {
  stringify_to(ostr << "_" << arg, args...);
}

template <typename... T> std::string stringify(const char *prefix, T... args) {
  std::ostringstream ostr;
  stringify_to(ostr << prefix, args...);
  return ostr.str();
}

struct Localtime {
  std::tm time;

  Localtime();
};

std::basic_ostream<char> &operator<<(std::basic_ostream<char> &ostr,
                                     const Localtime &tm);

struct Duration {
  int h, m;
  double s;

  Duration(std::chrono::duration<double> duration);
};

std::ostream &operator<<(std::ostream &ostr, const Duration &d);

} // namespace Impl

template <const char *name, typename T> struct KV { T value; };

template <typename... KVs> struct KVList {};

template <const char *name, typename T, typename... KVs>
class KVList<KV<name, T>, KVs...> : private KVList<KVs...> {
private:
  using Self = KVList<KV<name, T>, KVs...>;
  using Super = KVList<KVs...>;
  KV<name, T> kv;

  template <bool eq, const char *n> friend struct Getter;

  template <bool eq, const char *n> struct Getter {
    static decltype(auto) get(const Self &self) {
      return (static_cast<const Super &>(self).template get<n>());
    }
  };

  template <const char *n> struct Getter<true, n> {
    static decltype(auto) get(const Self &self) { return (self.kv.value); }
  };

  static T extract(int argc, const char **argv) {
    size_t namelen = std::strlen(name);
    for (int i = 0; i < argc; ++i) {
      if (std::strncmp(argv[i], name, namelen) == 0 &&
          argv[i][namelen] == '=') {
        const char *strend = nullptr; // FIXME: needs proper error
                                      // checking for incomplete parse
                                      // (and/or overflow).
        return Impl::parse<T>(argv[i] + namelen + 1, strend);
      }
    }

    throw name; // FIXME: needs better reporting of missing arguments.
  }

public:
  template <const char *other_name> decltype(auto) get() const {
    return (Getter<Impl::cptreq<name, other_name>, other_name>::get(*this));
  }

  KVList(int argc, const char **argv)
      : Super(argc, argv), kv{extract(argc, argv)} {}

  template <typename F> void traverse(F f) {
    f(name, kv.value);
    Super::traverse(f);
  }
};

template <> struct KVList<> {
  KVList(int, const char **) {}

  template <typename F> void traverse(F f) {}
};

template <typename... Args> class Experiment {
private:
  std::string m_prefix;
  KVList<Args...> m_args;
  std::chrono::time_point<std::chrono::steady_clock> start, last_checkpoint;

public:
  Experiment(const char *name, int argc, const char **argv)
      : m_args(argc, argv) {
    std::ostringstream ostr;
    ostr << "results/" << name;

    m_args.traverse([&ostr](const char *argname, const auto &val) {
      ostr << "_" << argname << "=" << val;
    });

    m_prefix = ostr.str();
    // TODO: check errors
    mkdir("results", 0755);
    mkdir(m_prefix.c_str(), 0755);

    m_prefix += '/';

    {
      std::ofstream meta(m_prefix + "meta");
      m_args.traverse([&meta](const char *argname, const auto &val) {
        meta << argname << " = " << val << std::endl;
      });
    }

    {
      std::ofstream eventlog(m_prefix + "eventlog");
      eventlog << "Experiment start: " << Impl::Localtime() << std::endl;
      start = last_checkpoint = std::chrono::steady_clock::now();
    }
  }

  ~Experiment() { checkpoint("Experiment finish: "); }

  const KVList<Args...> &args() const { return m_args; }

  template <typename... T> void checkpoint(const char *name, T... extras) {
    auto full_name = Impl::stringify(name, extras...);
    std::ofstream eventlog(m_prefix + "eventlog",
                           std::ios::app | std::ios::out);

    auto this_checkpoint = std::chrono::steady_clock::now();
    eventlog << full_name << " @ " << Impl::Localtime() << std::endl;

    std::chrono::duration<double> since_start = this_checkpoint - start;
    std::chrono::duration<double> since_last =
        this_checkpoint - last_checkpoint;
    eventlog << "\tSince experiment start: " << Impl::Duration(since_start)
             << " ( " << since_start.count() << " s )" << std::endl;
    eventlog << "\tSince last event: " << Impl::Duration(since_last) << " ( "
             << since_last.count() << " s )" << std::endl;
    last_checkpoint = this_checkpoint;
  }

  template <typename... T>
  std::ofstream open_file(const char *name, T... extras) {
    // FIXME: probably check that file names aren't reused.  Maybe add
    // option to create families of related files (aka
    // subdirectories)?
    return std::ofstream(m_prefix + Impl::stringify(name, extras...));
  }

  template <typename T, size_t N, typename... Extras>
  void write_tensor_tab(TT::LowDimensional::StridedTensorRef<T, N> values,
                        const char *name, Extras... extras) {
    auto out = open_file(name, extras...);
    Impl::write_tensor_tab_with(values,
                                [&out]() -> std::ofstream & { return out; });
  }
};

} // namespace Experiment

#endif
