#ifndef __INTEGRATION__HPP__
#define __INTEGRATION__HPP__

#include "ttsuite/blapack.hpp"
#include "ttsuite/log.hpp"
#include "ttsuite/low_dimensional.hpp"
#include <cmath>
#include <fstream>
#include <functional>
#include <limits>

namespace Integration {

template <typename Step, typename Callback>
void iterate(TT::LowDimensional::VectorRefD x, const double t0, const double t1,
             const double dt, Callback callback, Step stepF) {
  const size_t n = static_cast<size_t>(std::ceil((t1 - t0) / dt));

  for (size_t step = 0; step < n; ++step) {
    double t = t0 + step * dt;
    callback(x, t, step);
    stepF(x, t, dt, step);
  }
  callback(x, t0 + dt * n, n);
}

template <typename Step, typename Callback, typename StepCallback>
void dynamicStepIterate(TT::LowDimensional::VectorRefD x, const double t0,
                        const double t1, double dt, const double reps,
                        const double aeps, const size_t order,
                        Callback callback, StepCallback stepCallback,
                        Step stepF) {
  const size_t N = x.size();
  TT::LowDimensional::VectorD fine1({N}), fine2({N}), coarse({N});

  const double denom = 1.0 / (1ul << (order - 1));

  double t = t0;
  for (; t < t1; t += dt * 2) {
    ttlog_info("Current integration time: %g", t);
    callback(x, t, dt);

    // First, a single large step.
    stepF(x.cref(), t, 2 * dt, coarse.ref());

    bool shrinking = false;
    while (true) {
      // We make *two* small steps, so we can compare against the larger one.
      stepF(x.cref(), t, dt, fine1.ref());
      stepF(fine1.cref(), t + dt, dt, fine2.ref());

      TT::BLAS::L1::axpy(-1.0, fine2.cref().strided(), coarse.ref().strided());
      const double diff = TT::BLAS::L1::nrm2(coarse.cref().strided());
      const double nrm = TT::BLAS::L1::nrm2(fine2.cref().strided());

      ttlog_info("Difference between coarse and fine steps is %g / %g at %g",
                 diff, nrm, t);

      if ((diff / denom <= reps * nrm + aeps) && !shrinking) {
        stepCallback(t, dt, dt * 2);
        dt *= 2;
        stepF(x.cref(), t, 2 * dt, coarse.ref());
      } else if (diff * denom <= reps * nrm + aeps) {
        break;
      } else {
        stepCallback(t, dt, dt / 2);
        dt /= 2;
        shrinking = true;
        TT::BLAS::L1::copy(fine1.cref().strided(), coarse.ref().strided());
      }
    }

    TT::BLAS::L1::copy(fine2.cref().strided(), x.strided());
  }

  callback(x, t, dt);
}

namespace Explicit {
using namespace TT::LowDimensional;

template <typename F, typename Callback>
void midpoint(F f, VectorRefD x0, double t0, double t1, double dt,
              Callback callback) {
  const std::array<size_t, 1> size = {x0.size()};

  VectorD xmid{size}, fmid{size}, fend{size};

  iterate(x0, t0, t1, dt, callback,
          [&f, &xmid, &fmid, &fend](VectorRefD x, double t, double dt,
                                    size_t step) {
            f(t, x.cref(), fmid.ref());

            TT::BLAS::L1::copy(x.cref().strided(), xmid.ref().strided());
            TT::BLAS::L1::axpy(dt / 2, fmid.cref().strided(),
                               xmid.ref().strided());

            f(t + dt / 2, xmid.cref(), fend.ref());

            TT::BLAS::L1::axpy(dt, fend.cref().strided(), x.strided());
          });
}

template <typename F, typename Callback, typename StepChangeCallback>
void rk4(F f, VectorRefD x0, const double t0, const double t1, double dt,
         const double reps, const double aeps, Callback callback,
         StepChangeCallback stepChangeCallback) {
  const size_t N = x0.size();
  MatrixD k({N, 4});
  VectorD xp({N});

  const VectorD c{{1.0 / 6.0, 1.0 / 3.0, 1.0 / 3.0, 1.0 / 6.0}, {4}};
  ::Integration::dynamicStepIterate(
      x0, t0, t1, dt, reps, aeps, 4, callback, stepChangeCallback,
      [&k, &xp, &c, f](VectorRef<const double> x, const double t,
                       const double dt, VectorRef<double> y) mutable {
        f(t, x.cref(), k.ref().index_last(0));
        TT::BLAS::L1::scal(dt, k.ref().index_last(0).strided());

        TT::BLAS::L1::copy(x.strided(), xp.ref().strided());
        TT::BLAS::L1::axpy(0.5, k.cref().index_last(0).strided(),
                           xp.ref().strided());
        f(t + dt / 2, xp.cref(), k.ref().index_last(1));
        TT::BLAS::L1::scal(dt, k.ref().index_last(1).strided());

        TT::BLAS::L1::copy(x.strided(), xp.ref().strided());
        TT::BLAS::L1::axpy(0.5, k.cref().index_last(1).strided(),
                           xp.ref().strided());
        f(t + dt / 2, xp.cref(), k.ref().index_last(2));
        TT::BLAS::L1::scal(dt, k.ref().index_last(2).strided());

        TT::BLAS::L1::copy(x.strided(), xp.ref().strided());
        TT::BLAS::L1::axpy(1.0, k.cref().index_last(2).strided(),
                           xp.ref().strided());
        f(t + dt, xp.cref(), k.ref().index_last(3));
        TT::BLAS::L1::scal(dt, k.ref().index_last(3).strided());

        TT::BLAS::L1::copy(x.strided(), y.strided());
        TT::BLAS::L2::gemv(TT::BLAS::transpose::no, 1.0, k.cref().strided(),
                           c.cref().strided(), 1.0, y.strided());
      });
}

} // namespace Explicit

namespace Implicit {
using namespace TT::LowDimensional;

template <typename F, typename SolveJ, typename Callback>
void midpoint_newton(F f, SolveJ jacobi_solver, const size_t max_newton_iter,
                     const double newton_eps, VectorRefD x0, const double t0,
                     const double t1, const double dt, Callback callback) {
  const std::array<size_t, 1> N = {x0.size()};

  VectorD rhs{N}, nxt{N}, mid{N}, shift{N};
  iterate(
      x0, t0, t1, dt, callback,
      [&](VectorRefD x, double t, double dt, size_t step) {
        TT::BLAS::L1::copy(x.cref().strided(), nxt.ref().strided());

        for (size_t iter = 0; iter <= max_newton_iter; ++iter) {
          TT::BLAS::L1::copy(x.cref().strided(), mid.ref().strided());
          TT::BLAS::L1::scal(0.5, mid.ref().strided());
          TT::BLAS::L1::axpy(0.5, nxt.cref().strided(), mid.ref().strided());

          f(t, mid.cref(), rhs.ref());
          TT::BLAS::L1::scal(-dt, rhs.ref().strided());
          TT::BLAS::L1::axpy(1.0, nxt.cref().strided(), rhs.ref().strided());
          TT::BLAS::L1::axpy(-1.0, x.cref().strided(), rhs.ref().strided());

          double f_nrm = TT::BLAS::L1::nrm2(rhs.cref().strided());
          if (f_nrm < newton_eps)
            break;

          std::fill(shift.begin(), shift.end(), 0.0);
          jacobi_solver(1.0 /* coefficient for I */,
                        -dt / 2 /* coefficient for J */,
                        mid.cref() /* point at which J is evaluated */,
                        rhs.cref() /* RHS */, shift.ref() /* Solution */);
          TT::BLAS::L1::axpy(-1.0, shift.cref().strided(), nxt.ref().strided());
        }

        TT::BLAS::L1::copy(nxt.cref().strided(), x.strided());
      });
}

template <typename F, typename SolveJ>
void newton(F f, SolveJ jacobi_solver, const size_t max_iter, const double eps,
            VectorRefD x0) {
  newton_step_size(
      f, jacobi_solver,
      [](VectorRef<const double>, VectorRef<const double>, const double,
         VectorRef<const double>) { return -1.0; },
      max_iter, eps, x0);
}

template <typename F, typename SolveJ, typename StepSizeOracle>
void newton_step_size(F f, SolveJ jacobi_solver,
                      StepSizeOracle step_size_oracle, const size_t max_iter,
                      const double eps, VectorRefD x0) {
  newton_step_size_project(
      f, jacobi_solver, step_size_oracle, [](VectorRefD x) {}, max_iter, eps,
      x0);
}

template <typename F, typename SolveJ, typename StepSizeOracle>
void newton_step_size_bound_below(F f, SolveJ jacobi_solver,
                                  StepSizeOracle step_size_oracle,
                                  VectorRef<const double> lower_bound,
                                  const size_t max_iter, const double eps,
                                  VectorRefD x0) {
  auto project = [lower_bound](VectorRefD x) {
    const size_t N = x.size();
    for (size_t i = 0; i < N; ++i) {
      if (x(i) < lower_bound(i)) {
        x(i) = lower_bound(i);
      }
    }
  };

  newton_step_size_project(f, jacobi_solver, step_size_oracle, project,
                           max_iter, eps, x0);
}

template <typename F, typename SolveJ, typename StepSizeOracle,
          typename Project>
void newton_step_size_project(F f, SolveJ jacobi_solver,
                              StepSizeOracle step_size_oracle, Project project,
                              const size_t max_iter, const double eps,
                              VectorRefD x0) {
  const size_t N = x0.size();
  VectorD rhs{{N}}, shift{{N}}, oldx0{{N}};
  std::fill(shift.begin(), shift.end(), 0.0);

  ttlog_debug("%8s %10s(%7s) %10s %10s(%7s)", "Iter #", "Rhs amax", "at",
              "rhs nrm", "Shf amax", "at");

  project(x0);

  size_t iter = 0;
  for (iter = 0; iter <= max_iter; ++iter) {
    f(x0.cref(), rhs.ref());

#ifdef TTLOG_DEBUG
    auto i0 = TT::BLAS::L1::iamax(rhs.cref().strided());
#endif

    double nrm = TT::BLAS::L1::nrm2(rhs.cref().strided());
    if (nrm != nrm)
      ttlog_error("NaN residue on iteration %u/%u", (unsigned)iter,
                  (unsigned)max_iter);

    if (nrm < eps)
      break;

    jacobi_solver(x0.cref(), rhs.cref(), shift.ref());
#ifdef TTLOG_DEBUG
    auto i1 = TT::BLAS::L1::iamax(shift.cref().strided());
#endif

    double step_size =
        step_size_oracle(x0.cref(), rhs.cref(), nrm, shift.cref());

    TT::BLAS::L1::copy(x0.cref().strided(), oldx0.ref().strided());

    TT::BLAS::L1::axpy(step_size, shift.cref().strided(), x0.strided());
    project(x0);

    ttlog_debug("%03u/%03u: %10.3g(%7u) %10.3g %10.3g(%7u)",
                static_cast<unsigned>(iter), static_cast<unsigned>(max_iter),
                rhs(i0), static_cast<unsigned>(i0), nrm, shift(i1),
                static_cast<unsigned>(i1));
  }

  ttlog_info("Newton's method completed in %u iterations",
             static_cast<unsigned>(iter));
}

struct DIRKConfig {
  std::function<void(double, VectorRef<const double>, VectorRef<double>)> f;
  std::function<void(double, double, VectorRef<const double>,
                     VectorRef<const double>, VectorRef<double>)>
      jacobi_solver;
  std::function<double(double, double, VectorRef<const double>,
                       VectorRef<const double>, VectorRef<const double>, double,
                       VectorRef<const double>)>
      step_size_oracle = [](double, double, VectorRef<const double>,
                            VectorRef<const double>, VectorRef<const double>,
                            double, VectorRef<const double>) { return -1.0; };

  size_t max_newton_iter;
  double newton_eps;
  double reps;
  double aeps;

  std::function<void(VectorRef<double>, double, double)> callback =
      [](VectorRef<double>, double, double) {};
  std::function<void(double, double, double)> step_change_callback =
      [](double, double, double) {};

  std::function<void(VectorRef<const double>, VectorRef<double>)> project =
      [](VectorRef<const double>, VectorRef<double>) {};
};

inline void dirk_butcher(DIRKConfig config, VectorRefD x0, const double t0,
                         const double t1, double dt,
                         MatrixRef<const double> butcher, const size_t order) {
  const size_t N = x0.size();
  const size_t K = butcher.size(1) - 1;

  MatrixD k{{N, K}};
  VectorD xp{{N}};
  VectorD offset{{N}};

  ::Integration::dynamicStepIterate(
      x0, t0, t1, dt, config.reps, config.aeps, order, config.callback,
      config.step_change_callback,
      [&](VectorRef<const double> x, const double t, const double dt,
          VectorRef<double> y) mutable {
        std::fill(k.begin(), k.end(), 0.0);

        for (size_t stage = 0; stage < K; ++stage) {
          const double t_st = t + dt * butcher(stage, K);
          TT::BLAS::L1::copy(x.strided(), offset.ref().strided());
          TT::BLAS::L2::gemv(
              TT::BLAS::transpose::no, 1.0,
              k.cref().strided().subslice({0, 0}, {N, stage}),
              butcher.cref().strided().template fix_idx<0>(stage).subslice(
                  {0}, {stage}),
              1.0, offset.ref().strided());

          newton_step_size_project(
              [&](VectorRef<const double> u, VectorRefD val) mutable {
                TT::BLAS::L1::copy(offset.cref().strided(), xp.ref().strided());
                TT::BLAS::L1::axpy(1.0, u.strided(), xp.ref().strided());
                config.f(t_st, xp.cref(), val);
                TT::BLAS::L1::scal(-dt, val.strided());
                TT::BLAS::L1::axpy(1.0 / butcher(stage, stage), u.strided(),
                                   val.strided());
              },
              [&](VectorRef<const double> u, VectorRef<const double> rhs,
                  VectorRefD sol) mutable {
                TT::BLAS::L1::copy(offset.cref().strided(), xp.ref().strided());
                TT::BLAS::L1::axpy(1.0, u.strided(), xp.ref().strided());

                config.jacobi_solver(1.0 / butcher(stage, stage), -dt, xp.cref(), rhs,
                                     sol);
              },
              [&](VectorRef<const double> at, VectorRef<const double> val_at,
                  const double norm_val_at,
                  VectorRef<const double> direction) -> double {
                return config.step_size_oracle(1.0 / butcher(stage, stage), -dt,
                                               at, offset.cref(), val_at,
                                               norm_val_at, direction);
              },
              [&](VectorRef<double> u) { config.project(offset.cref(), u); },
              config.max_newton_iter, config.newton_eps,
              k.ref().index_last(stage));

          TT::BLAS::L1::scal(1.0 / butcher(stage, stage),
                             k.ref().index_last(stage).strided());
        }

        TT::BLAS::L1::copy(x.strided(), y.strided());
        TT::BLAS::L2::gemv(
            TT::BLAS::transpose::no, 1.0, k.cref().strided(),
            butcher.cref().strided().template fix_idx<0>(K).subslice({0}, {K}),
            1.0, y.strided());
      });
}

inline void norsett(DIRKConfig config, VectorRefD x0, const double t0,
                    const double t1, double dt) {
  const double C = 1.0685790213;
  const size_t N = x0.size();

  MatrixD k{{N, 3}};
  VectorD xp{{N}};
  VectorD offset{{N}};

  const double te1 = (1 - 2 * C), te2 = 1 / (6 * te1 * te1),
               te3 = 3 * te1 * te1, te4 = (te3 - 1) / te3;

  MatrixD butcher{{4, 4}};
  std::fill(butcher.begin(), butcher.end(), 0.0);
  butcher(0, 0) = C;
  butcher(0, 3) = C;
  butcher(1, 0) = 0.5 - C;
  butcher(1, 1) = C;
  butcher(1, 3) = 0.5;
  butcher(2, 0) = 2 * C;
  butcher(2, 1) = 1 - 4 * C;
  butcher(2, 2) = C;
  butcher(2, 3) = 1 - C;
  butcher(3, 0) = te2;
  butcher(3, 1) = te4;
  butcher(3, 2) = te2;

  dirk_butcher(config, x0, t0, t1, dt, butcher.cref(), 4);
}

inline void dirk43(DIRKConfig config, VectorRefD x0, const double t0,
                   const double t1, double dt) {
  const size_t N = x0.size();

  MatrixD k{{N, 4}};
  VectorD xp{{N}};
  VectorD offset{{N}};

  MatrixD butcher{{5, 5}};
  std::fill(butcher.begin(), butcher.end(), 0.0);
  butcher(0, 0) = 0.5;
  butcher(0, 4) = 0.5;
  butcher(1, 0) = 1.0 / 6.0;
  butcher(1, 1) = 0.5;
  butcher(1, 4) = 2.0 / 3.0;
  butcher(2, 0) = -0.5;
  butcher(2, 1) = 0.5;
  butcher(2, 2) = 0.5;
  butcher(2, 4) = 0.5;
  butcher(3, 0) = 1.5;
  butcher(3, 1) = -1.5;
  butcher(3, 2) = 0.5;
  butcher(3, 3) = 0.5;
  butcher(3, 4) = 1.0;
  butcher(4, 0) = 1.5;
  butcher(4, 1) = -1.5;
  butcher(4, 2) = 0.5;
  butcher(4, 3) = 0.5;

  dirk_butcher(config, x0, t0, t1, dt, butcher.cref(), 3);
}

} // namespace Implicit

namespace Rosenbrock {
using namespace TT::LowDimensional;

template <typename F, typename SolveJ, typename Callback>
void midpoint(F f, SolveJ jacobi_solver, VectorRefD x0, const double t0,
              const double t1, const double dt, Callback callback) {
  ::Integration::Implicit::midpoint_newton(f, jacobi_solver, 1, 0.0, x0, t0, t1,
                                           dt, callback);
}
} // namespace Rosenbrock

} // namespace Integration

#endif
