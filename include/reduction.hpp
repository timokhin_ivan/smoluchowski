#ifndef __REDUCTION__HPP__
#define __REDUCTION__HPP__

#include "convolution.hpp"
#include "include/ttsuite/low_dimensional.hpp"
#include <cstddef>
#include <functional>
#include <utility>

namespace Reduction {
TT::LowDimensional::Tensor<double, 3>
reduced_operator(const Direct::Smoluchowski::Operator &op,
                 TT::LowDimensional::MatrixRef<const double> base);

std::tuple<TT::LowDimensional::VectorD, TT::LowDimensional::VectorD,
           TT::LowDimensional::Tensor<double, 3>>
prepare_reduced_problem(
    TT::LowDimensional::MatrixRef<const double> base,
    TT::LowDimensional::StridedVectorRef<const double> full_source,
    TT::LowDimensional::StridedVectorRef<const double> full_x0,
    const Direct::Smoluchowski::Operator &full_op);

bool extend_basis(TT::LowDimensional::MatrixD &basis,
                  TT::LowDimensional::MatrixRef<const double> extension,
                  const double eps, double *const norm_ratio = nullptr);

void integrate_full_from_to(const double t0, const double t1, const double dt,
                            TT::LowDimensional::VectorRefD x0,
                            Direct::Smoluchowski::Operator &S,
                            TT::LowDimensional::MatrixRefD *samples = nullptr);

void integrate_reduced_from_to(
    const double t0, const double t1, const double dt,
    TT::LowDimensional::VectorRefD x0,
    TT::LowDimensional::TensorRef<const double, 3> reduced,
    TT::LowDimensional::VectorRef<const double> source,
    TT::LowDimensional::MatrixRefD *samples = nullptr);

std::pair<TT::LowDimensional::MatrixD, double> find_windowed_basis_for(
    const double t0, const double tmax, const double dt,
    TT::LowDimensional::VectorRefD x0, Direct::Smoluchowski::Operator &S,
    const double window_size, const size_t window_samples,
    const double extension_eps, const double finished_eps,
    std::function<void(size_t, bool, double, double)> progress_log =
        [](size_t, bool, double, double) {});

std::pair<TT::LowDimensional::MatrixD, double>
find_windowed_basis_from_initial_approx(
    const double t0, const double tmax, const double dt,
    TT::LowDimensional::VectorRefD x0, Direct::Smoluchowski::Operator &S,
    const double window_size, const size_t window_samples,
    const double extension_eps, const double finished_eps,
    TT::LowDimensional::MatrixD initial_basis,
    std::function<void(size_t, bool, double, double)> progress_log =
        [](auto...) {});

TT::LowDimensional::MatrixD rescale_basis_for_size(
    TT::LowDimensional::StridedMatrixRef<const double> old_basis,
    const size_t new_size);

TT::LowDimensional::MatrixD rescale_and_recompress_basis(
    TT::LowDimensional::StridedMatrixRef<const double> old_basis,
    const size_t new_size, const double eps);

TT::LowDimensional::MatrixD
merge_bases(std::vector<TT::LowDimensional::MatrixRef<const double>> bases,
            const double eps);
} // namespace Reduction

#endif // __REDUCTION__HPP__
