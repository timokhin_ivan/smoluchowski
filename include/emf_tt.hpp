#ifndef EMF_TT_HPP__
#define EMF_TT_HPP__

#include "emf/emf.hpp"

struct ttt : public emf::experiment {
  EMF_PARAMETER(double, a);
  EMF_PARAMETER(size_t, logN);
  EMF_PARAMETER(size_t, max_rank);
  EMF_PARAMETER_OPTIONAL(double, lambda, 0.0);
  EMF_PARAMETER_OPTIONAL(size_t, max_iters, static_cast<size_t>(50));
  EMF_PARAMETER_OPTIONAL(std::vector<size_t>, sources, std::vector<size_t>{1});
  EMF_PARAMETER_OPTIONAL(std::vector<double>, source_powers,
                         std::vector<double>{1.0});
  EMF_PARAMETER_OPTIONAL(double, target_rhs_norm, 5e-12);

  int run() override;

  const char *name() const override { return "ttt"; }
};

#endif // EMF_TT_HPP__
