#ifndef JACOBI_PRECONDITIONER_HPP
#define JACOBI_PRECONDITIONER_HPP

#include "band_matrix.hpp"
#include "ttsuite/blapack.hpp"
#include "ttsuite/matrix_cross.hpp"
#include <mkl.h>
#include <iostream>
#include <vector>

namespace Jacobi {

class Preconditioner {
private:
  size_t n, rank;
  std::vector<double> X, Y, C;
  double D;
  std::vector<MKL_INT> ipiv;
  BandMatrix band;

public:
  Preconditioner(size_t N, size_t R, TT::MatrixCross<double> &kernel_cross,
                 double lambda, TT::LowDimensional::VectorRef<const double> nv,
                 size_t p, double Icoef, double Jcoef);

  void apply(double *x) const;
};
} // namespace Jacobi

#endif /* JACOBI_PRECONDITIONER_HPP */
