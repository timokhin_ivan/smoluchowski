#ifndef __TTSUITE__BLAPACK__HPP__
#define __TTSUITE__BLAPACK__HPP__

#include "low_dimensional.hpp"
#include <mkl.h>

namespace TT {

namespace BLAS {

namespace L1 {
double asum(LD::StridedTensorRef<const double, 1> v);
void axpy(double a, LD::StridedTensorRef<const double, 1> x,
          LD::StridedTensorRef<double, 1> y);
void copy(LD::StridedTensorRef<const double, 1> x,
          LD::StridedTensorRef<double, 1> y);
double dot(LD::StridedTensorRef<const double, 1> x,
           LD::StridedTensorRef<const double, 1> y);
double nrm2(LD::StridedTensorRef<const double, 1> x);
void rot(LD::StridedTensorRef<double, 1> x, LD::StridedTensorRef<double, 1> y,
         double c, double s);
void scal(double a, LD::StridedTensorRef<double, 1> x);
void swap(LD::StridedTensorRef<double, 1> x, LD::StridedTensorRef<double, 1> y);
size_t iamax(LD::StridedTensorRef<const double, 1> x);

void rotg(double &a, double &b, double &c, double &s);
} // namespace L1

enum class transpose { no, yes, conj };
enum class uplo { upper, lower };
enum class diag { nonunit, unit };
enum class side { left, right };

namespace L2 {
void gbmv(transpose trans, size_t dl, size_t du, double alpha,
          LD::StridedTensorRef<const double, 2> a,
          LD::StridedTensorRef<const double, 1> x, double beta,
          LD::StridedTensorRef<double, 1> y);

void gemv(transpose trans, double alpha,
          LD::StridedTensorRef<const double, 2> a,
          LD::StridedTensorRef<const double, 1> x, double beta,
          LD::StridedTensorRef<double, 1> y);

void ger(double alpha, LD::StridedTensorRef<const double, 1> x,
         LD::StridedTensorRef<const double, 1> y,
         LD::StridedTensorRef<double, 2> a);

void sbmv(uplo part, double alpha, LD::StridedTensorRef<const double, 2> a,
          LD::StridedTensorRef<const double, 1> x, double beta,
          LD::StridedTensorRef<double, 1> y);

void symv(uplo part, double alpha, LD::StridedTensorRef<const double, 2> a,
          LD::StridedTensorRef<const double, 1> x, double beta,
          LD::StridedTensorRef<double, 1> y);

void syr(uplo part, double alpha, LD::StridedTensorRef<const double, 1> x,
         LD::StridedTensorRef<double, 2> a);

void syr2(uplo part, double alpha, LD::StridedTensorRef<const double, 1> x,
          LD::StridedTensorRef<const double, 1> y,
          LD::StridedTensorRef<double, 2> a);

void tbmv(uplo kind, transpose trans, diag d,
          LD::StridedTensorRef<const double, 2> a,
          LD::StridedTensorRef<double, 1> x);

void tbsv(uplo kind, transpose trans, diag d,
          LD::StridedTensorRef<const double, 2> a,
          LD::StridedTensorRef<double, 1> x);

void trmv(uplo kind, transpose trans, diag d,
          LD::StridedTensorRef<const double, 2> a,
          LD::StridedTensorRef<double, 1> x);

void trsv(uplo kind, transpose trans, diag d,
          LD::StridedTensorRef<const double, 2> a,
          LD::StridedTensorRef<double, 1> x);
} // namespace L2

namespace L3 {
void gemm(transpose transa, transpose transb, double alpha,
          LD::StridedTensorRef<const double, 2> a,
          LD::StridedTensorRef<const double, 2> b, double beta,
          LD::StridedTensorRef<double, 2> c);

void symm(side sym, uplo part, double alpha,
          LD::StridedTensorRef<const double, 2> a,
          LD::StridedTensorRef<const double, 2> b, double beta,
          LD::StridedTensorRef<double, 2> c);

void syrk(uplo part, transpose trans, double alpha,
          LD::StridedTensorRef<const double, 2> a, double beta,
          LD::StridedTensorRef<double, 2> c);

void syr2k(uplo part, transpose trans, double alpha,
           LD::StridedTensorRef<const double, 2> a,
           LD::StridedTensorRef<const double, 2> b, double beta,
           LD::StridedTensorRef<double, 2> c);

void trmm(side tr, uplo kind, transpose transa, diag d, double alpha,
          LD::StridedTensorRef<const double, 2> a,
          LD::StridedTensorRef<double, 2> b);

void trsm(side tr, uplo kind, transpose transa, diag d, double alpha,
          LD::StridedTensorRef<const double, 2> a,
          LD::StridedTensorRef<double, 2> b);
} // namespace L3
} // namespace BLAS

namespace LAPACK {

enum class svd_vectors { all, significant, overwrite, none };

void gesvd(svd_vectors jobu, svd_vectors jobv,
           LD::StridedTensorRef<double, 2> a, LD::TensorRef<double, 1> s,
           LD::StridedTensorRef<double, 2> *u,
           LD::StridedTensorRef<double, 2> *v, LD::TensorRef<double, 1> superb);

void gesdd(svd_vectors jobz, LD::StridedTensorRef<double, 2> a,
           LD::VectorRefD s, LD::StridedTensorRef<double, 2> *u,
           LD::StridedTensorRef<double, 2> *v);

void gelqf(LD::StridedTensorRef<double, 2> a, LD::TensorRef<double, 1> tau);

void orglq(int k, LD::StridedTensorRef<double, 2> a,
           LD::TensorRef<const double, 1> tau);

inline void orglq(LD::StridedTensorRef<double, 2> a,
                  LD::TensorRef<const double, 1> tau) {
  orglq(a.size(0), a, tau);
}

void geqrf(LD::StridedTensorRef<double, 2> a, LD::TensorRef<double, 1> tau);

void orgqr(int k, LD::StridedTensorRef<double, 2> a,
           LD::TensorRef<const double, 1> tau);

inline void orgqr(LD::StridedTensorRef<double, 2> a,
                  LD::TensorRef<const double, 1> tau) {
  orgqr(a.size(1), a, tau);
}

void ormqr(BLAS::side side, BLAS::transpose trans,
           LD::StridedTensorRef<const double, 2> a,
           LD::TensorRef<const double, 1> tau,
           LD::StridedTensorRef<double, 2> c);

enum class schur_vectors { no, yes };
enum class sort_eigenvalues { no, yes };

typedef MKL_INT (*select_d2)(const double *, const double *);

void gees(schur_vectors jobs, sort_eigenvalues sort, select_d2 select,
          LD::StridedTensorRef<double, 2> a, MKL_INT &sdim,
          LD::TensorRef<double, 1> wr, LD::TensorRef<double, 1> wi,
          LD::StridedTensorRef<double, 2> *v);

enum class eigenvectors { no, yes };

void syev(eigenvectors jobz, BLAS::uplo kind, LD::StridedTensorRef<double, 2> a,
          LD::VectorRefD w);

} // namespace LAPACK

} // namespace TT

#endif // __TTSUITE__BLAPACK__HPP__
