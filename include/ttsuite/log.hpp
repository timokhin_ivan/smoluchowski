#ifndef __TTSUITE__LOG__HPP__
#define __TTSUITE__LOG__HPP__

#include <cstdio>
#include <cstring>
#include <ctime>
#include <string>

namespace TTSuite {
namespace Logging {
enum class severity { debug, info, warn, error };

template <typename... Args>
void log(const char *filename, const unsigned line, const char *function,
         severity sev, const char *format, Args... args) {
  std::string sev_s;
  switch (sev) {
  case severity::debug:
    sev_s = "DEBUG";
    break;
  case severity::info:
    sev_s = "INFO";
    break;
  case severity::warn:
    sev_s = "WARN";
    break;
  case severity::error:
    sev_s = "ERROR";
    break;
  default:
    sev_s = "UNKNW";
  };

  std::time_t time = std::time(nullptr);
  // This is *so* not thread-safe
  char time_str[21];
  std::strftime(time_str, 20, "%F %T", std::localtime(&time));

  std::string full_format = "[%s][%-25s:%03u][%10s][%5s]\t";
  full_format += format;
  full_format += '\n';

  auto filename_length = std::strlen(filename);
  if (filename_length > 25)
    filename = filename + filename_length - 25;

  std::fprintf(stderr, full_format.c_str(), time_str, filename, line, function,
               sev_s.c_str(), args...);

  if (sev == severity::error)
    throw sev;
}
} // namespace Logging
} // namespace TTSuite

#define ttlog(sev, msg, ...)                                                   \
  (::TTSuite::Logging::log(__FILE__, __LINE__, __func__, (sev), (msg),         \
                           __VA_ARGS__))

#ifndef LOGGING_LEVEL
#define LOGGING_LEVEL 2
#endif

#if LOGGING_LEVEL >= 3
#define TTLOG_DEBUG
#define ttlog_debug(msg, ...)                                                  \
  ttlog(::TTSuite::Logging::severity::debug, (msg), __VA_ARGS__)
#else
#define ttlog_debug(...)
#endif

#if LOGGING_LEVEL >= 2
#define TTLOG_INFO
#define ttlog_info(msg, ...)                                                   \
  ttlog(::TTSuite::Logging::severity::info, (msg), __VA_ARGS__)
#else
#define ttlog_info(...)
#endif

#if LOGGING_LEVEL >= 1
#define TTLOG_WARN
#define ttlog_warn(msg, ...)                                                   \
  ttlog(::TTSuite::Logging::severity::warn, (msg), __VA_ARGS__)
#else
#define ttlog_warn(...)
#endif

#define ttlog_error(msg, ...)                                                  \
  ttlog(::TTSuite::Logging::severity::error, (msg), __VA_ARGS__)

#endif // __TTSUITE__LOG__HPP__
