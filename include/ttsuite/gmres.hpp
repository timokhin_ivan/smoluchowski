#ifndef __TTSUITE__GMRES__HPP__
#define __TTSUITE__GMRES__HPP__

#include "blapack.hpp"
#include "low_dimensional.hpp"
#include "log.hpp"

namespace TT {

template <typename T, typename A, typename P>
size_t gmres(A a, P preconditioner, LowDimensional::VectorRef<const T> rhs,
             LowDimensional::VectorRef<T> x, size_t max_iter,
             size_t norestart_iter, T eps) {
  const size_t N = rhs.size();
  std::array<size_t, 2> vsizes = {N, norestart_iter + 1},
                        hsizes = {norestart_iter + 2, norestart_iter + 2},
                        qsizes = {norestart_iter + 2, 2};
  LowDimensional::Matrix<T> v{vsizes}, h{hsizes}, q{qsizes};

  size_t iterations = 0;
  T target_norm = T(0);;
  bool tnorm_set = false;
  while (iterations < max_iter) {
    a(x.cref(), v.ref().index_last(0));

    BLAS::L1::axpy(T(-1), rhs.strided(),
                   v.ref().strided().template fix_idx<1>(0));
    preconditioner(v.ref().index_last(0));
    auto b = BLAS::L1::nrm2(v.cref().strided().template fix_idx<1>(0));

    if (!tnorm_set) {
      target_norm = b * eps;
      ttlog_debug("Target norm: %g", target_norm);
      tnorm_set = true;
    }

    BLAS::L1::scal(-1 / b, v.ref().strided().template fix_idx<1>(0));
    h(0, norestart_iter + 1) = b;

    size_t i = 0;
    for (i = 0; i < norestart_iter && iterations < max_iter;
         ++i, ++iterations) {
      a(v.cref().index_last(i), v.ref().index_last(i + 1));
      preconditioner(v.ref().index_last(i + 1));

      auto initial_norm =
          BLAS::L1::nrm2(v.cref().strided().template fix_idx<1>(i + 1));

      for (size_t j = 0; j < i + 1; ++j) {
        h(j, i) = BLAS::L1::dot(v.cref().strided().template fix_idx<1>(j),
                                v.cref().strided().template fix_idx<1>(i + 1));
        BLAS::L1::axpy(-h(j, i), v.cref().strided().template fix_idx<1>(j),
                       v.ref().strided().template fix_idx<1>(i + 1));
      }

      h(i + 1, i) =
          BLAS::L1::nrm2(v.cref().strided().template fix_idx<1>(i + 1));

      if (h(i + 1, i) < initial_norm / 10000) {
        for (size_t j = 0; j < i + 1; ++j) {
          auto dot =
              BLAS::L1::dot(v.cref().strided().template fix_idx<1>(j),
                            v.cref().strided().template fix_idx<1>(i + 1));
          BLAS::L1::axpy(dot, v.cref().strided().template fix_idx<1>(j),
                         v.ref().strided().template fix_idx<1>(i + 1));
          h(j, i) += dot;
        }

        h(i + 1, i) =
            BLAS::L1::nrm2(v.cref().strided().template fix_idx<1>(i + 1));
      }

      BLAS::L1::scal(1 / h(i + 1, i),
                     v.ref().strided().template fix_idx<1>(i + 1));

      for (size_t k = 0; k < i; ++k) {
        T temp = h(k, i) * q(k, 0) + h(k + 1, i) * q(k, 1);
        h(k + 1, i) = -h(k, i) * q(k, 1) + h(k + 1, i) * q(k, 0);
        h(k, i) = temp;
      }

      BLAS::L1::rotg(h(i, i), h(i + 1, i), q(i, 0), q(i, 1));

      h(i + 1, norestart_iter + 1) = -q(i, 1) * h(i, norestart_iter + 1);
      h(i, norestart_iter + 1) *= q(i, 0);

      //ttlog_debug("Residue norm at iteration #%u: %g", (unsigned)i, (double)std::abs(h(i + 1, norestart_iter + 1)));
      if (std::abs(h(i + 1, norestart_iter + 1)) < target_norm) {
        ttlog_debug("Final residue %g", h(i + 1, norestart_iter + 1));
        break;
      }
    }

    size_t dim = i < norestart_iter ? i + 1 : norestart_iter;
    BLAS::L2::trsv(BLAS::uplo::upper, BLAS::transpose::no, BLAS::diag::nonunit,
                   h.cref().strided().subslice({0, 0}, {dim, dim}),
                   h.ref()
                       .strided()
                       .template fix_idx<1>(norestart_iter + 1)
                       .subslice({0}, {dim}));

    BLAS::L2::gemv(BLAS::transpose::no, T(1),
                   v.cref().strided().subslice({0, 0}, {N, dim}),
                   h.cref()
                       .strided()
                       .template fix_idx<1>(norestart_iter + 1)
                       .subslice({0}, {dim}),
                   T(1), x.strided());

    if (std::abs(h(i + 1, norestart_iter + 1)) < target_norm) {
      break;
    }
  }

  return iterations;
}

} // namespace TT

#endif
