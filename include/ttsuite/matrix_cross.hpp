#ifndef __TTSUITE__MATRIX_CROSS__HPP__
#define __TTSUITE__MATRIX_CROSS__HPP__

#include "blapack.hpp"
#include "low_dimensional.hpp"

#include <cmath>
#include <random>
#include <unordered_set>

namespace TT {
template <typename T> class MatrixCross {
private:
  std::vector<size_t> m_row_indices, m_col_indices;
  LowDimensional::Matrix<T> m_columns, m_rows, m_pivot_matrix,
      m_pivot_matrix_lu, m_factor_u, m_factor_v;
  size_t m_rank;
  T m_eps;

  template <typename Fn>
  void select_row(Fn eval_row, size_t row, bool barebones) {
    m_row_indices[m_rank] = row;

    eval_row(row, m_factor_v.ref().index_last(m_rank));
    if (!barebones)
      TT::BLAS::L1::copy(m_factor_v.cref().index_last(m_rank).strided(),
                         m_rows.ref().index_last(m_rank).strided());

    if (m_rank > 0) {
      BLAS::L2::gemv(
          BLAS::transpose::no, -T(1),
          m_factor_v.cref().strided().subslice({0, 0},
                                               {m_factor_v.size(0), m_rank}),
          m_factor_u.cref().strided().template fix_idx<0>(row).subslice(
              {0}, {m_rank}),
          T(1), m_factor_v.ref().strided().template fix_idx<1>(m_rank));
    }
  }

  template <typename Fn>
  void select_column(Fn eval_column, size_t column, bool barebones) {
    m_col_indices[m_rank] = column;

    eval_column(column, m_factor_u.ref().index_last(m_rank));
    if (!barebones)
      BLAS::L1::copy(m_factor_u.cref().index_last(m_rank).strided(),
                     m_columns.ref().index_last(m_rank).strided());

    if (m_rank > 0) {
      BLAS::L2::gemv(
          BLAS::transpose::no, -T(1),
          m_factor_u.cref().strided().subslice({0, 0},
                                               {m_factor_u.size(0), m_rank}),
          m_factor_v.cref().strided().template fix_idx<0>(column).subslice(
              {0}, {m_rank}),
          T(1), m_factor_u.ref().strided().template fix_idx<1>(m_rank));
    }
  }

public:
  MatrixCross()
      : m_row_indices(), m_col_indices(), m_columns({0, 0}), m_rows({0, 0}),
        m_pivot_matrix({0, 0}), m_pivot_matrix_lu({0, 0}), m_factor_u({0, 0}),
        m_factor_v({0, 0}), m_rank(0), m_eps() {}

  template <typename Row, typename Col, typename Gen>
  static MatrixCross<T> cross(Row eval_row, Col eval_column, size_t nrows,
                              size_t ncols, T target_eps, size_t max_rank,
                              Gen &rnd_gen, bool barebones = false) {
    max_rank = std::min(nrows, std::min(ncols, max_rank));
    MatrixCross<T> result;
    result.m_row_indices.resize(max_rank);
    result.m_col_indices.resize(max_rank);

    if (!barebones) {
      result.m_columns = LowDimensional::Matrix<T>{{nrows, max_rank}};
      result.m_rows = LowDimensional::Matrix<T>{{ncols, max_rank}};
    }
    result.m_factor_u = LowDimensional::Matrix<T>{{nrows, max_rank}};
    result.m_factor_v = LowDimensional::Matrix<T>{{ncols, max_rank}};
    result.m_eps = T(0.0);

    std::unordered_set<size_t> selected_rows;

    std::uniform_int_distribution<size_t> rnd_row{0, nrows - 1},
        rnd_col{0, ncols - 1};

    for (result.m_rank = 0; result.m_rank < max_rank; ++result.m_rank) {
      size_t row = 0;
      do {
        row = rnd_row(rnd_gen);
      } while (selected_rows.find(row) != selected_rows.end());
      result.select_row(eval_row, row, barebones);

      size_t column = BLAS::L1::iamax(
          result.m_factor_v.cref().strided().template fix_idx<1>(
              result.m_rank));

      for (size_t s = 0; s < 50; ++s) {
        result.select_column(eval_column, column, barebones);

        size_t new_row = BLAS::L1::iamax(
            result.m_factor_u.cref().strided().template fix_idx<1>(
                result.m_rank));

        if (new_row == row)
          break;
        row = new_row;

        result.select_row(eval_row, row, barebones);

        size_t new_column = BLAS::L1::iamax(
            result.m_factor_v.cref().strided().template fix_idx<1>(
                result.m_rank));

        if (new_column == column)
          break;
        column = new_column;
      }

      result.m_eps = result.m_factor_u(row, result.m_rank);

      if (std::abs(result.m_eps) <= target_eps)
        break;

      selected_rows.insert(row);

      auto lpivot = std::sqrt(std::abs(result.m_eps));
      auto rpivot = result.m_eps / lpivot;
      if (result.m_eps != result.m_eps)
        throw result.m_eps;

      BLAS::L1::scal(
          1 / lpivot,
          result.m_factor_u.ref().strided().template fix_idx<1>(result.m_rank));
      BLAS::L1::scal(
          1 / rpivot,
          result.m_factor_v.ref().strided().template fix_idx<1>(result.m_rank));
    }

    result.m_factor_u.resize_last(result.m_rank);
    result.m_factor_v.resize_last(result.m_rank);
    result.m_col_indices.resize(result.m_rank);
    result.m_row_indices.resize(result.m_rank);

    if (!barebones) {
      result.m_columns.resize_last(result.m_rank);
      result.m_rows.resize_last(result.m_rank);
      result.m_pivot_matrix =
          LowDimensional::Matrix<T>{{result.m_rank, result.m_rank}};
      result.m_pivot_matrix_lu =
          LowDimensional::Matrix<T>{{result.m_rank, result.m_rank + 1}};

      auto pivot_matrix = result.m_pivot_matrix.ref();
      auto rows = result.m_rows.ref();
      for (size_t j = 0; j < result.m_rank; ++j)
        for (size_t i = 0; i < result.m_rank; ++i) {
          pivot_matrix(i, j) = rows(result.m_col_indices[j], i);
        }

      auto factor_u = result.m_factor_u.cref();
      auto factor_v = result.m_factor_v.cref();
      auto pivot_lu = result.m_pivot_matrix_lu.ref();

      for (size_t j = 0; j < result.m_rank + 1; ++j)
        for (size_t i = 0; i < result.m_rank; ++i) {
          if (i >= j) {
            pivot_lu(i, j) = factor_u(result.m_row_indices[i], j);
          } else {
            pivot_lu(i, j) = factor_v(result.m_col_indices[j - 1], i);
          }
        }
    }

    return result;
  }

  template <typename Fn, typename Gen>
  static MatrixCross<T> cross(Fn matrix, size_t nrows, size_t ncols,
                              T target_eps, size_t max_rank, Gen &rnd_gen,
                              bool barebones = false) {
    return MatrixCross<T>::cross(
        [&matrix](size_t row, TT::LD::VectorRef<T> x) {
          for (size_t i = 0; i < x.size(); ++i) {
            x(i) = matrix(row, i);
          }
        },
        [&matrix](size_t col, TT::LD::VectorRef<T> x) {
          for (size_t i = 0; i < x.size(); ++i) {
            x(i) = matrix(i, col);
          }
        },
        nrows, ncols, target_eps, max_rank, rnd_gen, barebones);
  }

  T calculate_element(size_t i, size_t j) const {
    return BLAS::L1::dot(
        m_factor_u.cref().strided().template fix_idx<0>(i).subslice({0},
                                                                    {m_rank}),
        m_factor_v.cref().strided().template fix_idx<0>(j).subslice({0},
                                                                    {m_rank}));
  }

  size_t rank() const { return m_rank; }

  LowDimensional::MatrixRef<const T> u() const { return m_factor_u.cref(); }

  LowDimensional::MatrixRef<const T> v() const { return m_factor_v.cref(); }

  LowDimensional::MatrixRef<const T> columns() const {
    return m_columns.cref();
  }

  LowDimensional::MatrixRef<const T> rows() const { return m_rows.cref(); }

  LowDimensional::MatrixRef<const T> pivot() const {
    return m_pivot_matrix.cref();
  }

  LowDimensional::StridedMatrixRef<const T> pivot_l() const {
    return m_pivot_matrix_lu.cref().strided().subslice({0, 0},
                                                       {m_rank, m_rank});
  }

  LowDimensional::StridedMatrixRef<const T> pivot_u() const {
    return m_pivot_matrix_lu.cref().strided().subslice({0, 1},
                                                       {m_rank, m_rank});
  }

  LowDimensional::VectorRef<const size_t> row_indices() const {
    return LowDimensional::TensorRef<const size_t, 1>{
        {m_rank}, m_row_indices.data(), m_rank};
  }

  LowDimensional::VectorRef<const size_t> column_indices() const {
    return LowDimensional::TensorRef<const size_t, 1>{
        {m_rank}, m_col_indices.data(), m_rank};
  }

  T eps() const { return m_eps; }
}; // namespace TT
} // namespace TT

#endif // __TTSUITE__MATRIX_CROSS__HPP__
