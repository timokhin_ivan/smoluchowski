#ifndef __TTSUITE__TENSOR_TRAIN__HPP__
#define __TTSUITE__TENSOR_TRAIN__HPP__

#include "blapack.hpp"
#include "log.hpp"
#include "matrix_cross.hpp"
#include "tensor.hpp"
#include <algorithm>
#include <cmath>
#include <limits>

namespace TT {

template <typename T> class TensorTrain {
private:
  std::vector<LowDimensional::Tensor<T, 3>> m_cores;

public:
  typename std::vector<LowDimensional::Tensor<T, 3>>::iterator cores_begin() {
    return m_cores.begin();
  }

  typename std::vector<LowDimensional::Tensor<T, 3>>::iterator cores_end() {
    return m_cores.end();
  }

  typename std::vector<LowDimensional::Tensor<T, 3>>::const_iterator
  cores_cbegin() const {
    return m_cores.cbegin();
  }

  typename std::vector<LowDimensional::Tensor<T, 3>>::const_iterator
  cores_cend() const {
    return m_cores.cend();
  }

  static TensorTrain<T>
  from_cores(std::vector<LowDimensional::Tensor<T, 3>> cores) {
    TensorTrain<T> result;
    result.m_cores = std::move(cores);
    return result;
  }

  static TensorTrain<T>
  ttsvd(Tensor<T> &full, T eps,
        size_t max_rank = std::numeric_limits<size_t>::max()) {
    size_t dimensionality = full.dimensionality();
    eps /= std::sqrt(dimensionality - 1);
    std::vector<size_t> sizes(dimensionality);
    for (size_t i = 0; i < dimensionality; ++i) {
      sizes[i] = full.size(i);
    }

    ttlog_debug("TTSVD with eps = %g, rank = %u, dimensionality = %u",
                (double)eps, (unsigned)max_rank, (unsigned)(dimensionality));

    std::array<size_t, 3> core_sizes;

    std::vector<T> elements{full.begin(), full.end()};
    std::size_t fullSize = elements.size();

    std::vector<T> s(std::min(sizes.back(), fullSize / sizes.back())),
        superb(s.size());
    LowDimensional::TensorRef<T, 1> s_ref{{s.size()}, s.data(), s.size()},
        superb_ref{{superb.size()}, superb.data(), superb.size()};

    std::vector<T> v(s.size() * sizes.back());
    LowDimensional::StridedTensorRef<T, 2> v_ref{
        {s.size(), sizes.back()}, {1, s.size()}, v.data()};

    LAPACK::gesvd(LAPACK::svd_vectors::overwrite,
                  LAPACK::svd_vectors::significant,
                  LowDimensional::StridedTensorRef<T, 2>(
                      {fullSize / sizes.back(), sizes.back()},
                      {1, fullSize / sizes.back()}, elements.data()),
                  s_ref, nullptr, &v_ref, superb_ref);
    size_t rank = std::min(
        max_rank,
        static_cast<std::size_t>(
            std::find_if(s.begin(), s.end(), [eps](T sv) { return sv < eps; }) -
            s.begin()));

    std::vector<LowDimensional::Tensor<T, 3>> cores;
    core_sizes = {rank, v_ref.size(1), 1};
    cores.emplace_back(core_sizes);
    for (size_t j = 0; j < cores.back().size(1); ++j)
      for (size_t i = 0; i < rank; ++i) {
        cores.back().ref()(i, j, 0) = v_ref(i, j);
      }

    for (size_t j = 0; j < rank; ++j) {
      BLAS::L1::scal(s[j], LowDimensional::StridedTensorRef<T, 1>(
                               {fullSize / sizes.back()}, {1},
                               elements.data() + j * fullSize / sizes.back()));
    }

    fullSize = fullSize / sizes.back() * rank;
    for (sizes.pop_back(); sizes.size() > 1; sizes.pop_back()) {
      s.resize(std::min(sizes.back() * rank, fullSize / (sizes.back() * rank)));
      s_ref = LowDimensional::TensorRef<T, 1>{{s.size()}, s.data(), s.size()};

      v.resize(s.size() * sizes.back() * rank);
      v_ref = LowDimensional::StridedTensorRef<T, 2>{
          {s.size(), sizes.back() * rank}, {1, s.size()}, v.data()};
      superb.resize(s.size());
      superb_ref = LowDimensional::TensorRef<T, 1>{
          {superb.size()}, superb.data(), superb.size()};

      LAPACK::gesvd(LAPACK::svd_vectors::overwrite,
                    LAPACK::svd_vectors::significant,
                    LowDimensional::StridedTensorRef<T, 2>(
                        {fullSize / (sizes.back() * rank), sizes.back() * rank},
                        {1, fullSize / (sizes.back() * rank)}, elements.data()),
                    s_ref, nullptr, &v_ref, superb_ref);
      size_t new_rank = std::min(
          max_rank,
          static_cast<size_t>(std::find_if(s.begin(), s.end(),
                                           [eps](T sv) { return sv < eps; }) -
                              s.begin()));

      core_sizes = {new_rank, sizes.back(), rank};
      cores.emplace_back(core_sizes);
      for (size_t k = 0; k < cores.back().size(2); ++k)
        for (size_t j = 0; j < cores.back().size(1); ++j)
          for (size_t i = 0; i < cores.back().size(0); ++i) {
            cores.back().ref()(i, j, k) =
                v_ref(i, j + k * cores.back().size(1));
          }

      for (size_t j = 0; j < new_rank; ++j) {
        BLAS::L1::scal(
            s[j], LowDimensional::StridedTensorRef<T, 1>(
                      {fullSize / (sizes.back() * rank)}, {1},
                      elements.data() + j * fullSize / (sizes.back() * rank)));
      }

      fullSize = fullSize / (sizes.back() * rank) * new_rank;
      rank = new_rank;
    }

    core_sizes = {1, sizes.back(), rank};
    cores.emplace_back(core_sizes);
    for (size_t j = 0; j < cores.back().size(2); ++j)
      for (size_t i = 0; i < cores.back().size(1); ++i) {
        cores.back().ref()(0, i, j) = elements[i + j * sizes.back()];
      }

    std::reverse(cores.begin(), cores.end());

    TensorTrain<T> result;
    result.m_cores = std::move(cores);
    return result;
  }

  template <typename Fn, typename Gen>
  static TensorTrain<T>
  dmrg_cross(Fn fn, LowDimensional::VectorRef<const size_t> sizes, T eps,
             size_t max_rank, size_t max_passes, Gen &gen) {
    const size_t dim = sizes.size();
    std::vector<std::vector<std::vector<size_t>>> left_indices, right_indices;
    left_indices.resize(dim - 2);
    right_indices.resize(dim - 2);
    ttlog_debug("%d", static_cast<int>(dim));

    // ========================================
    // Initial, random, guess for right indices
    // ========================================

    // leftmost unfolding, single index
    {
      const size_t search_space = std::min(sizes[dim - 1], max_rank);
      std::uniform_int_distribution<size_t> dist{0, sizes[dim - 1] - 1};
      while (right_indices.back().size() < search_space) {
        std::vector<size_t> guess = {dist(gen)};

        // Constructed in sorted order to simplify search for duplicates
        auto pos = std::lower_bound(right_indices.back().begin(),
                                    right_indices.back().end(), guess);

        if (pos == right_indices.back().end() || *pos != guess)
          right_indices.back().insert(pos, guess);
      }
    }

    // other unfoldings
    for (size_t i = dim - 3; i > 0; --i) {
      size_t search_space =
          std::min(max_rank, right_indices.at(i).size() * sizes[i + 1]);
      std::uniform_int_distribution<size_t> dist{0, sizes[i + 1] - 1},
          rest{0, right_indices.at(i).size() - 1};

      // First, we fill a vector consisting of the current index and
      // index into the previous index set
      std::vector<std::pair<size_t, size_t>> guesses;

      while (guesses.size() < search_space) {
        std::pair<size_t, size_t> guess{dist(gen), rest(gen)};

        auto pos = std::lower_bound(guesses.begin(), guesses.end(), guess);

        if (pos == guesses.end() || *pos != guess)
          guesses.insert(pos, guess);
      }

      // Now, we unwrap the reference
      right_indices.at(i - 1).resize(max_rank);
      std::transform(guesses.begin(), guesses.end(),
                     right_indices.at(i - 1).begin(),
                     [&right_indices, i](std::pair<size_t, size_t> guess) {
                       std::vector<size_t> result;
                       result.push_back(guess.first);

                       auto &rest = right_indices.at(i).at(guess.second);
                       result.insert(result.end(), rest.begin(), rest.end());

                       return result;
                     });
    }

    // =============================================================
    // Search passes.  We do not actually calculate an approximation
    // at this point, only search for good indices
    // =============================================================

    std::vector<size_t> index(dim);
    for (size_t pass = 0; pass < max_passes; ++pass) {
      // ------------------
      // Left-to-right pass
      // ------------------
      decltype(left_indices) old_left_indices(left_indices.size());
      old_left_indices.swap(left_indices);

      // First unfolding is special because there's no left index set
      {
        auto cross = MatrixCross<T>::cross(
            [fn, &right_indices, &index, sizes](size_t row,
                                                size_t column) mutable {
              index[0] = row;
              index[1] = column % sizes[1];
              auto &rest = right_indices.front().at(column / sizes[1]);
              std::copy(rest.begin(), rest.end(), index.begin() + 2);

              return fn(index.cbegin(), index.cend());
            },
            sizes[0], sizes[1] * right_indices.front().size(), eps, max_rank,
            gen);

        auto row_indices = cross.row_indices();
        left_indices.front().resize(row_indices.size());
        std::transform(row_indices.begin(), row_indices.end(),
                       left_indices.front().begin(), [](size_t i) {
                         std::vector<size_t> res = {i};
                         return res;
                       });
      }

      // Most unfoldings have both left and right index sets
      for (size_t i = 1; i < dim - 2; ++i) {
        auto cross = MatrixCross<T>::cross(
            [fn, &right_indices, &left_indices, &index, sizes,
             i](size_t row, size_t column) mutable {
              auto &init = left_indices.at(i - 1).at(
                  row % left_indices.at(i - 1).size());
              std::copy(init.begin(), init.end(), index.begin());

              index[i] = row / left_indices.at(i - 1).size();
              index[i + 1] = column % sizes[i + 1];

              auto &tail = right_indices.at(i).at(column / sizes[i + 1]);
              std::copy(tail.begin(), tail.end(), index.begin() + i + 2);

              return fn(index.cbegin(), index.cend());
            },
            sizes[i] * left_indices.at(i - 1).size(),
            sizes[i + 1] * right_indices.at(i).size(), eps, max_rank, gen);

        auto row_indices = cross.row_indices();
        left_indices.at(i).resize(row_indices.size());
        std::transform(row_indices.begin(), row_indices.end(),
                       left_indices.at(i).begin(),
                       [&left_indices, sizes, i](size_t row) {
                         std::vector<size_t> res(i + 1);

                         auto &init = left_indices.at(i - 1).at(
                             row % left_indices.at(i - 1).size());
                         std::copy(init.begin(), init.end(), res.begin());

                         res.back() = row / left_indices.at(i - 1).size();
                         return res;
                       });
      }

      if (old_left_indices == left_indices) {
        break;
      }
      // ------------------
      // Right-to-left pass
      // ------------------

      decltype(right_indices) old_right_indices(right_indices.size());
      old_right_indices.swap(right_indices);
      // Last unfolding does not have a right index set
      {
        auto cross = MatrixCross<T>::cross(
            [fn, &left_indices, &index, sizes, dim](size_t row,
                                                    size_t column) mutable {
              auto &init =
                  left_indices.back().at(row % left_indices.back().size());
              std::copy(init.begin(), init.end(), index.begin());
              index[dim - 2] = row / left_indices.back().size();
              index.back() = column;

              return fn(index.cbegin(), index.cend());
            },
            sizes[dim - 2] * left_indices.back().size(), sizes[dim - 1], eps,
            max_rank, gen);

        auto column_indices = cross.column_indices();
        right_indices.back().resize(column_indices.size());
        std::transform(column_indices.begin(), column_indices.end(),
                       right_indices.back().begin(), [](size_t i) {
                         std::vector<size_t> res = {i};
                         return res;
                       });
      }

      for (size_t i = dim - 3; i > 0; --i) {
        auto cross = MatrixCross<T>::cross(
            [fn, &right_indices, &left_indices, &index, sizes,
             i](size_t row, size_t column) mutable {
              auto &init = left_indices.at(i - 1).at(
                  row % left_indices.at(i - 1).size());
              std::copy(init.begin(), init.end(), index.begin());

              index[i] = row / left_indices.at(i - 1).size();
              index[i + 1] = column % sizes[i + 1];

              auto &tail = right_indices.at(i).at(column / sizes[i + 1]);
              std::copy(tail.begin(), tail.end(), index.begin() + i + 2);

              return fn(index.cbegin(), index.cend());
            },
            sizes[i] * left_indices.at(i - 1).size(),
            sizes[i + 1] * right_indices.at(i).size(), eps, max_rank, gen);

        auto column_indices = cross.column_indices();
        right_indices.at(i - 1).resize(column_indices.size());
        std::transform(column_indices.begin(), column_indices.end(),
                       right_indices.at(i - 1).begin(),
                       [&right_indices, sizes, i, dim](size_t column) {
                         std::vector<size_t> res(dim - 1 - i);
                         res.front() = column % sizes[i + 1];

                         auto &tail =
                             right_indices.at(i).at(column / sizes[i + 1]);
                         std::copy(tail.begin(), tail.end(), res.begin() + 1);

                         return res;
                       });
      }

      if (old_right_indices == right_indices)
        break;
    }

    // ============================================================
    // An approximation pass; we assume the right index set is good
    // enough; we still recompute the left one, though, since who
    // knows what we will pick.
    // ============================================================

    TensorTrain<T> result;
    result.m_cores.resize(dim);

    MatrixCross<T> prev_cross;

    // -------------------------------------
    // First unfolding is, as usual, special
    // -------------------------------------
    {
      auto cross = MatrixCross<T>::cross(
          [fn, &right_indices, &index, sizes](size_t row,
                                              size_t column) mutable {
            index[0] = row;
            index[1] = column % sizes[1];
            auto &rest = right_indices.front().at(column / sizes[1]);
            std::copy(rest.begin(), rest.end(), index.begin() + 2);

            return fn(index.cbegin(), index.cend());
          },
          sizes[0], sizes[1] * right_indices.front().size(), eps, max_rank,
          gen);

      auto row_indices = cross.row_indices();
      left_indices.front().resize(row_indices.size());
      std::transform(row_indices.begin(), row_indices.end(),
                     left_indices.front().begin(), [](size_t i) {
                       std::vector<size_t> res = {i};
                       return res;
                     });

      result.m_cores.front() =
          LowDimensional::Tensor<T, 3>{cross.u().template reshape<3>(
              {1, cross.u().size(0), cross.u().size(1)})};

      prev_cross = std::move(cross);
    }

    // ------------------------
    // Unfoldings in the middle
    // ------------------------
    for (size_t i = 1; i < dim - 2; ++i) {
      auto cross = MatrixCross<T>::cross(
          [fn, &right_indices, &left_indices, &index, sizes,
           i](size_t row, size_t column) mutable {
            auto &init =
                left_indices.at(i - 1).at(row % left_indices.at(i - 1).size());
            std::copy(init.begin(), init.end(), index.begin());

            index[i] = row / left_indices.at(i - 1).size();
            index[i + 1] = column % sizes[i + 1];

            auto &tail = right_indices.at(i).at(column / sizes[i + 1]);
            std::copy(tail.begin(), tail.end(), index.begin() + i + 2);

            return fn(index.cbegin(), index.cend());
          },
          sizes[i] * left_indices.at(i - 1).size(),
          sizes[i + 1] * right_indices.at(i).size(), eps, max_rank, gen);

      auto row_indices = cross.row_indices();
      left_indices.at(i).resize(row_indices.size());
      std::transform(
          row_indices.begin(), row_indices.end(), left_indices.at(i).begin(),
          [&left_indices, sizes, i](size_t row) {
            std::vector<size_t> res(i + 1);

            auto &init =
                left_indices.at(i - 1).at(row % left_indices.at(i - 1).size());
            std::copy(init.begin(), init.end(), res.begin());

            res.back() = row / left_indices.at(i - 1).size();
            return res;
          });

      result.m_cores.at(i) =
          LowDimensional::Tensor<T, 3>{cross.u().template reshape<3>(
              {left_indices.at(i - 1).size(), sizes[i], row_indices.size()})};

      BLAS::L3::trsm(
          BLAS::side::left, BLAS::uplo::lower, BLAS::transpose::no,
          BLAS::diag::nonunit, T(1), prev_cross.pivot_l(),
          result.m_cores.at(i)
              .ref()
              .template reshape<2>(
                  {result.m_cores.at(i).size(0),
                   result.m_cores.at(i).size(1) * result.m_cores.at(i).size(2)})
              .strided());

      prev_cross = std::move(cross);
    }

    // Last unfolding; phew!
    {
      auto cross = MatrixCross<T>::cross(
          [fn, &left_indices, &index, sizes, dim](size_t row,
                                                  size_t column) mutable {
            auto &init =
                left_indices.back().at(row % left_indices.back().size());
            std::copy(init.begin(), init.end(), index.begin());
            index[dim - 2] = row / left_indices.back().size();
            index.back() = column;

            return fn(index.cbegin(), index.cend());
          },
          sizes[dim - 2] * left_indices.back().size(), sizes[dim - 1], eps,
          max_rank, gen);

      auto row_indices = cross.row_indices();
      result.m_cores.at(dim - 2) = LowDimensional::Tensor<T, 3>{
          cross.u().template reshape<3>({left_indices.back().size(),
                                         sizes[dim - 2], row_indices.size()})};

      BLAS::L3::trsm(
          BLAS::side::left, BLAS::uplo::lower, BLAS::transpose::no,
          BLAS::diag::nonunit, T(1), prev_cross.pivot_l(),
          result.m_cores.at(dim - 2)
              .ref()
              .template reshape<2>({result.m_cores.at(dim - 2).size(0),
                                    result.m_cores.at(dim - 2).size(1) *
                                        result.m_cores.at(dim - 2).size(2)})
              .strided());

      result.m_cores.back() =
          LowDimensional::Tensor<T, 3>{{cross.rank(), sizes[dim - 1], 1}};

      for (size_t j = 0; j < sizes[dim - 1]; ++j)
        for (size_t i = 0; i < cross.rank(); ++i) {
          result.m_cores.back()(i, j, 0) = cross.v()(j, i);
        }
    }

    return result;
  }

  size_t rank(size_t i) const { return m_cores.at(i).size(2); }

  size_t dimensionality() const { return m_cores.size(); }

  template <typename Pred> bool any_core(Pred pred) const {
    return std::any_of(m_cores.cbegin(), m_cores.cend(), pred);
  }

  Tensor<T> materialise() const {
    auto fullSize =
        std::accumulate(m_cores.begin(), m_cores.end(), static_cast<size_t>(1),
                        [](size_t accum, const LowDimensional::Tensor<T, 3> &core) {
                          return core.size(1) * accum;
                        });

    std::vector<T> tmp[2];
    tmp[0].resize(fullSize);
    tmp[1].resize(fullSize);

    const size_t dim = dimensionality();
    size_t accumulated_index = 0;
    size_t next_index = 1;

    size_t accumulated_size = 1;
    tmp[accumulated_index].front() = T(1);

    for (size_t processed = 0; processed < dim; ++processed) {
      std::array<size_t, 2> accumulated_sizes = {accumulated_size,
                                                 m_cores.at(processed).size(0)};
      LowDimensional::MatrixRef<const T> accumulated{
          accumulated_sizes, tmp[accumulated_index].data(),
          accumulated_sizes[0] * accumulated_sizes[1]};

      std::array<size_t, 2> next_sizes = {accumulated_size,
                                          m_cores.at(processed).size(2) *
                                              m_cores.at(processed).size(1)};
      LowDimensional::MatrixRef<T> next{next_sizes, tmp[next_index].data(),
                                        next_sizes[0] * next_sizes[1]};

      std::array<size_t, 2> core_sizes = {m_cores.at(processed).size(0),
                                          m_cores.at(processed).size(1) *
                                              m_cores.at(processed).size(2)};
      auto core_m = m_cores.at(processed).cref().template reshape<2>(core_sizes);

      BLAS::L3::gemm(BLAS::transpose::no, BLAS::transpose::no, T(1),
                     accumulated.strided(), core_m.strided(), T(0),
                     next.strided());

      accumulated_size *= m_cores.at(processed).size(1);
      accumulated_index = 1 - accumulated_index;
      next_index = 1 - next_index;
    }

    std::vector<size_t> full_sizes(dim);
    for (size_t i = 0; i < dim; ++i) {
      full_sizes.at(i) = m_cores.at(i).size(1);
    }

    return Tensor<T>{std::move(tmp[accumulated_index]), std::move(full_sizes)};
  }

  template <typename Iter> T element_at(Iter begin, Iter end) const {
    if (m_cores.size() == 1) {
      return (m_cores.front().cref())(0, *begin, 0);
    } else if (m_cores.size() == 2) {
      auto i = *begin++;
      auto j = *begin;
      return BLAS::L1::dot(m_cores[0]
                               .cref()
                               .strided()
                               .template fix_idx<0>(0)
                               .template fix_idx<0>(i),
                           m_cores[1]
                               .cref()
                               .strided()
                               .template fix_idx<2>(0)
                               .template fix_idx<1>(j));
    } else {
      std::vector<T> tmp[2];
      auto v = m_cores[0]
                   .cref()
                   .strided()
                   .template fix_idx<0>(0)
                   .template fix_idx<0>(*begin);
      ++begin;

      for (size_t dim = 1; dim < m_cores.size(); ++dim, ++begin) {
        auto m = m_cores[dim].cref().strided().template fix_idx<1>(*begin);
        tmp[dim % 2].resize(m.size(1));

        auto new_v = LowDimensional::StridedTensorRef<T, 1>(
            {m.size(1)}, {1}, tmp[dim % 2].data());
        BLAS::L2::gemv(BLAS::transpose::yes, 1.0, m, v, 0.0, new_v);
        v = new_v.cref();
      }

      return v[0];
    }
  }

  void left_orthogonalise() {
    std::vector<T> R, tau;
    for (size_t dim = m_cores.size() - 1; dim > 0; --dim) {
      auto core = m_cores[dim].ref();
      auto a =
          core.template reshape<2>({core.size(0), core.size(1) * core.size(2)});
      tau.resize(std::min(a.size(0), a.size(1)));
      auto tau_ref =
          LowDimensional::TensorRef<T, 1>{{tau.size()}, tau.data(), tau.size()};

      LAPACK::gelqf(a.strided(), tau_ref);

      auto prev_core = m_cores[dim - 1].ref();
      auto b = prev_core.template reshape<2>(
          {prev_core.size(0) * prev_core.size(1), prev_core.size(2)});

      if (a.size(0) <= a.size(1)) {
        BLAS::L3::trmm(
            BLAS::side::right, BLAS::uplo::lower, BLAS::transpose::no,
            BLAS::diag::nonunit, T(1.0),
            a.strided().cref().subslice({0, 0}, {a.size(0), a.size(0)}),
            b.strided());

        LAPACK::orglq(a.strided(), tau_ref.cref());
      } else {
        // Our matrix ended up with more rows than columns, which
        // means we get to do some early (and lossless!) recompression
        LowDimensional::Tensor<T, 3> new_prev_core{
            {prev_core.size(0), prev_core.size(1), a.size(1)}};

        for (size_t k = 0; k < new_prev_core.size(2); ++k)
          for (size_t j = 0; j < new_prev_core.size(1); ++j)
            for (size_t i = 0; i < new_prev_core.size(0); ++i) {
              new_prev_core(i, j, k) = prev_core(i, j, k);
            }

        auto new_b =
            new_prev_core.ref().template reshape<2>({b.size(0), a.size(1)});

        BLAS::L3::trmm(
            BLAS::side::right, BLAS::uplo::lower, BLAS::transpose::no,
            BLAS::diag::nonunit, T(1.0),
            a.strided().cref().subslice({0, 0}, {a.size(1), a.size(1)}),
            new_b.strided());
        BLAS::L3::gemm(BLAS::transpose::no, BLAS::transpose::no, T(1),
                       b.strided().cref().subslice(
                           {0, a.size(1)}, {b.size(0), b.size(1) - a.size(1)}),
                       a.strided().cref().subslice(
                           {a.size(1), 0}, {a.size(0) - a.size(1), a.size(1)}),
                       T(1), new_b.strided());

        m_cores[dim - 1] = std::move(new_prev_core);

        LowDimensional::Tensor<T, 3> new_this_core{
            {a.size(1), core.size(1), core.size(2)}};

        LAPACK::orglq(a.strided().subslice({0, 0}, {a.size(1), a.size(1)}),
                      tau_ref.cref());
        // A gentle reminder that `a` is a reference to `core`.
        for (size_t k = 0; k < new_this_core.size(2); ++k)
          for (size_t j = 0; j < new_this_core.size(1); ++j)
            for (size_t i = 0; i < new_this_core.size(0); ++i) {
              new_this_core(i, j, k) = core(i, j, k);
            }

        m_cores[dim] = std::move(new_this_core);
      }
    }
  }

  T compress_prev_core(size_t dim, T eps, size_t max_rank) {
    std::vector<T> vt, s, superb;
    auto core = m_cores[dim - 1].ref();
    auto a =
        core.template reshape<2>({core.size(0) * core.size(1), core.size(2)});
    vt.resize(std::min(a.size(0), a.size(1)) * a.size(1));
    s.resize(std::min(a.size(0), a.size(1)));
    superb.resize(s.size());

    auto vt_ref =
        (LowDimensional::TensorRef<T, 2>{
             {std::min(a.size(0), a.size(1)), a.size(1)}, vt.data(), vt.size()})
            .strided();
    auto s_ref =
        LowDimensional::TensorRef<T, 1>{{s.size()}, s.data(), s.size()};
    auto superb_ref = LowDimensional::TensorRef<T, 1>{
        {superb.size()}, superb.data(), superb.size()};

    LAPACK::gesvd(LAPACK::svd_vectors::overwrite,
                  LAPACK::svd_vectors::significant, a.strided(), s_ref, nullptr,
                  &vt_ref, superb_ref);

    size_t rank = std::min(
        max_rank,
        static_cast<std::size_t>(
            std::find_if(s.begin(), s.end(), [eps](T sv) { return sv < eps; }) -
            s.begin()));
    if (rank == 0)
      rank = 1;

    T err = rank == s.size() ? T(0) : s.at(rank);

    m_cores[dim - 1].resize_last(rank);

    for (size_t j = 0; j < vt_ref.size(1); ++j)
      for (size_t i = 0; i < rank; ++i) {
        vt_ref(i, j) *= s_ref[i];
      }

    auto next_core = m_cores[dim].cref();
    auto b = next_core.template reshape<2>(
        {next_core.size(0), next_core.size(1) * next_core.size(2)});

    auto new_next_core = LowDimensional::Tensor<T, 3>(
        {rank, next_core.size(1), next_core.size(2)});
    auto new_b = new_next_core.ref().template reshape<2>({rank, b.size(1)});

    BLAS::L3::gemm(BLAS::transpose::no, BLAS::transpose::no, T(1),
                   vt_ref.cref().subslice({0, 0}, {rank, vt_ref.size(1)}),
                   b.strided(), T(0), new_b.strided());

    m_cores[dim] = std::move(new_next_core);

    return err;
  }

  T round(T eps, size_t max_rank = std::numeric_limits<size_t>::max()) {
    if (m_cores.size() <= 1)
      return T(0);

    // Pass 1: orthogonalise the cores
    left_orthogonalise();
    // Pass 2: compression

    T total_err = T(0);
    for (size_t dim = 1; dim < m_cores.size(); ++dim) {
      T err = compress_prev_core(dim, eps, max_rank);
      total_err += err * err;
    }
    return std::sqrt(total_err);
  }

  TensorTrain<T> operator+(const TensorTrain<T> &other) const {
    assert(this->dimensionality() == other.dimensionality());

    std::vector<LowDimensional::Tensor<T, 3>> new_cores;

    for (size_t i = 0; i < m_cores.size(); ++i) {
      auto this_core = m_cores.at(i).cref();
      auto other_core = other.m_cores.at(i).cref();
      assert(this_core.size(1) == other_core.size(1));

      std::array<size_t, 3> offsets = {this_core.size(0), 0, this_core.size(2)};
      if (i == 0)
        offsets[0] = 0;
      if (i == m_cores.size() - 1)
        offsets[2] = 0;

      std::array<size_t, 3> new_sizes = {offsets[0] + other_core.size(0),
                                         offsets[1] + other_core.size(1),
                                         offsets[2] + other_core.size(2)};

      new_cores.emplace_back(new_sizes);

      auto new_core = new_cores.back().ref();
      std::fill(new_core.begin(), new_core.end(), T(0));

      for (size_t k = 0; k < this_core.size(2); ++k)
        for (size_t j = 0; j < this_core.size(1); ++j)
          for (size_t i = 0; i < this_core.size(0); ++i)
            new_core(i, j, k) = this_core(i, j, k);

      for (size_t k = 0; k < other_core.size(2); ++k)
        for (size_t j = 0; j < other_core.size(1); ++j)
          for (size_t i = 0; i < other_core.size(0); ++i)
            new_core(i + offsets[0], j + offsets[1], k + offsets[2]) =
                other_core(i, j, k);
    }

    TensorTrain<T> result;
    result.m_cores = std::move(new_cores);
    return result;
  }

  TensorTrain<T> operator-(const TensorTrain<T> &other) const {
    assert(this->dimensionality() == other.dimensionality());

    std::vector<LowDimensional::Tensor<T, 3>> new_cores;

    for (size_t dim = 0; dim < m_cores.size(); ++dim) {
      auto this_core = m_cores.at(dim).cref();
      auto other_core = other.m_cores.at(dim).cref();
      assert(this_core.size(1) == other_core.size(1));

      std::array<size_t, 3> offsets = {this_core.size(0), 0, this_core.size(2)};
      if (dim == 0)
        offsets[0] = 0;
      if (dim == m_cores.size() - 1)
        offsets[2] = 0;

      std::array<size_t, 3> new_sizes = {offsets[0] + other_core.size(0),
                                         offsets[1] + other_core.size(1),
                                         offsets[2] + other_core.size(2)};

      new_cores.emplace_back(new_sizes);

      auto new_core = new_cores.back().ref();
      std::fill(new_core.begin(), new_core.end(), T(0));

      for (size_t k = 0; k < this_core.size(2); ++k)
        for (size_t j = 0; j < this_core.size(1); ++j)
          for (size_t i = 0; i < this_core.size(0); ++i)
            new_core(i, j, k) = this_core(i, j, k);

      for (size_t k = 0; k < other_core.size(2); ++k)
        for (size_t j = 0; j < other_core.size(1); ++j)
          for (size_t i = 0; i < other_core.size(0); ++i)
            new_core(i + offsets[0], j + offsets[1], k + offsets[2]) =
                dim == m_cores.size() - 1 ? other_core(i, j, k)
                                          : -other_core(i, j, k);
    }

    TensorTrain<T> result;
    result.m_cores = std::move(new_cores);
    return result;
  }

  TensorTrain<T> &operator+=(const TensorTrain<T> &other) {
    return (*this = *this + other);
  }

  TensorTrain<T> &operator-=(const TensorTrain<T> &other) {
    return (*this = *this - other);
  }

  TensorTrain<T> &operator*=(T val) {
    BLAS::L1::scal(val, m_cores.back()
                            .ref()
                            .template reshape<1>({m_cores.back().size()})
                            .strided());

    return *this;
  }

  TensorTrain<T> operator*(T val) const {
    auto result = *this;
    result *= val;
    return result;
  }

  TensorTrain<T> &operator/=(T val) { return (*this *= (1 / val)); }

  TensorTrain<T> operator/(T val) const {
    auto result = *this;
    result /= val;
    return result;
  }

  // Elementwise
  TensorTrain<T> operator*(const TensorTrain<T> &other) const {
    assert(this->dimensionality() == other.dimensionality());

    std::vector<LowDimensional::Tensor<T, 3>> new_cores;
    new_cores.reserve(dimensionality());

    for (size_t dim = 0; dim < m_cores.size(); ++dim) {
      auto this_core = m_cores.at(dim).cref();
      auto other_core = other.m_cores.at(dim).cref();
      assert(this_core.size(1) == other_core.size(1));

      std::array<size_t, 3> new_sizes = {
          this_core.size(0) * other_core.size(0), this_core.size(1),
          this_core.size(2) * other_core.size(2)};

      new_cores.emplace_back(new_sizes);
      auto new_core = new_cores.back().ref();

      for (size_t k2 = 0; k2 < other_core.size(2); ++k2)
        for (size_t k1 = 0; k1 < this_core.size(2); ++k1)
          for (size_t j = 0; j < this_core.size(1); ++j)
            for (size_t i2 = 0; i2 < other_core.size(0); ++i2)
              for (size_t i1 = 0; i1 < this_core.size(0); ++i1) {
                new_core(i1 + i2 * this_core.size(0), j,
                         k1 + k2 * this_core.size(2)) =
                    this_core(i1, j, k1) * other_core(i2, j, k2);
              }
    }

    TensorTrain<T> result;
    result.m_cores = std::move(new_cores);
    return result;
  }

  TensorTrain<T> &operator*=(const TensorTrain<T> &other) const {
    return (*this = *this * other);
  }

  T dot(const TensorTrain<T> &other) const {
    const size_t dim = dimensionality();
    assert(dim == other.dimensionality());
    assert(dim >= 1);
    std::vector<T> tmp[2];
    std::vector<T> intermediate;

    auto last_core = m_cores.back().cref();
    auto other_last_core = other.m_cores.back().cref();
    assert(last_core.size(1) == other_last_core.size(1));
    tmp[0].resize(last_core.size(0) * other_last_core.size(0));

    LowDimensional::MatrixRef<T> current{
        {last_core.size(0), other_last_core.size(0)},
        tmp[0].data(),
        tmp[0].size()};

    {
      auto a =
          last_core.template reshape<2>({last_core.size(0), last_core.size(1)});
      auto b = other_last_core.template reshape<2>(
          {other_last_core.size(0), other_last_core.size(1)});

      BLAS::L3::gemm(BLAS::transpose::no, BLAS::transpose::yes, T(1),
                     a.strided(), b.strided(), T(0), current.strided());
    }

    for (size_t i = dim - 1; i > 0; --i) {
      size_t tmp_ix = (dim - i) % 2;

      auto current_core = m_cores.at(i - 1).cref();
      auto other_current_core = other.m_cores.at(i - 1).cref();

      assert(current_core.size(1) == other_current_core.size(1));
      tmp[tmp_ix].resize(current_core.size(0) * other_current_core.size(0));
      intermediate.resize(other_current_core.size(0) * current_core.size(1) *
                          current_core.size(2));

      auto a = current_core.template reshape<2>(
          {current_core.size(0), current_core.size(1) * current_core.size(2)});
      auto b = other_current_core.template reshape<2>(
          {other_current_core.size(0) * other_current_core.size(1),
           other_current_core.size(2)});

      // C and D are different unfoldings of the same intermediate tensor
      auto c = LowDimensional::MatrixRef<T>{
          {other_current_core.size(0) * current_core.size(1),
           current_core.size(2)},
          intermediate.data(),
          intermediate.size()};
      auto d = LowDimensional::MatrixRef<const T>{
          {other_current_core.size(0),
           current_core.size(1) * current_core.size(2)},
          intermediate.data(),
          intermediate.size()};

      auto next = LowDimensional::MatrixRef<T>{
          {current_core.size(0), other_current_core.size(0)},
          tmp[tmp_ix].data(),
          tmp[tmp_ix].size()};

      BLAS::L3::gemm(BLAS::transpose::no, BLAS::transpose::yes, T(1),
                     b.strided(), current.cref().strided(), T(0), c.strided());
      BLAS::L3::gemm(BLAS::transpose::no, BLAS::transpose::yes, T(1),
                     a.strided(), d.strided(), T(0), next.strided());

      current = next;
    }

    return current(0, 0);
  }

  TensorTrain<T> &outer_assign(const TensorTrain<T> &other) {
    m_cores.insert(m_cores.end(), other.m_cores.begin(), other.m_cores.end());
    return *this;
  }

  TensorTrain<T> &outer_assign(TensorTrain<T> &&other) {
    size_t insertion_point = dimensionality();
    m_cores.resize(m_cores.size() + other.m_cores.size());
    std::move(other.m_cores.begin(), other.m_cores.end(),
              m_cores.begin() + insertion_point);
    other.m_cores.clear();
    return *this;
  }

  TensorTrain<T> outer(const TensorTrain<T> &other) const {
    auto result = *this;
    result.outer_assign(other);
    return result;
  }

  TensorTrain<T> outer(TensorTrain<T> &&other) const {
    auto result = *this;
    result.outer_assign(std::move(other));
    return result;
  }
};

template <typename T>
TensorTrain<T> operator*(T val, const TensorTrain<T> &tt) {
  return tt * val;
}

} // namespace TT

#endif // __TTSUITE__TENSOR_TRAIN__HPP__
