#ifndef __TTSUITE__TTT__HPP__
#define __TTSUITE__TTT__HPP__

#include "gmres.hpp"
#include "log.hpp"
#include "tensor_train.hpp"

namespace TT {

std::vector<size_t> factorise(size_t n);

template <typename T, size_t N> class TTT {
private:
  TensorTrain<T> m_tt;
  std::array<std::vector<size_t>, N> m_size_factors;

  static void tensorise_index(size_t idx, const std::vector<size_t> &factors,
                              std::vector<size_t> &indices) {
    indices.clear();
    indices.reserve(factors.size());

    for (auto n : factors) {
      indices.push_back(idx % n);
      idx /= n;
    }

    assert(idx == 0);
  }

  static size_t linearise_index(const std::vector<size_t> &idx,
                                const std::vector<size_t> &factors) {
    assert(idx.size() == factors.size());
    size_t linear = 0;

    for (auto ix = idx.crbegin(), factor = factors.crbegin(); ix != idx.crend();
         ++ix, ++factor) {
      linear = *ix + linear * *factor;
    }

    return linear;
  }

  static LowDimensional::Tensor<T, 3>
  product_core(LowDimensional::TensorRef<const T, 4> a,
               LowDimensional::TensorRef<const T, 4> b) {
    const size_t r1 = a.size(0);
    const size_t m = a.size(1);
    const size_t k = a.size(2);
    const size_t r2 = a.size(3);

    const size_t R1 = b.size(0);
    assert(k == b.size(1));
    const size_t n = b.size(2);
    const size_t R2 = b.size(3);

    LowDimensional::Tensor<T, 3> res_core{{r1 * R1, m * n, r2 * R2}};

    auto c = res_core.ref().template reshape<6>({r1, R1, m, n, r2, R2});

    LowDimensional::Tensor<T, 4> tmpa({r1, m, r2, k}), tmpb({R1, n, R2, k});
    LowDimensional::Tensor<T, 6> tmpc({r1, m, r2, R1, n, R2});

    for (size_t i4 = 0; i4 < k; ++i4)
      for (size_t i3 = 0; i3 < r2; ++i3)
        for (size_t i2 = 0; i2 < m; ++i2)
          for (size_t i1 = 0; i1 < r1; ++i1)
            tmpa(i1, i2, i3, i4) = a(i1, i2, i4, i3);

    for (size_t i4 = 0; i4 < k; ++i4)
      for (size_t i3 = 0; i3 < R2; ++i3)
        for (size_t i2 = 0; i2 < n; ++i2)
          for (size_t i1 = 0; i1 < R1; ++i1)
            tmpb(i1, i2, i3, i4) = b(i1, i4, i2, i3);

    BLAS::L3::gemm(
        BLAS::transpose::no, BLAS::transpose::yes, T(1),
        tmpa.cref().template reshape<2>({r1 * m * r2, k}).strided(),
        tmpb.cref().template reshape<2>({R1 * n * R2, k}).strided(), T(0),
        tmpc.ref().template reshape<2>({r1 * m * r2, R1 * n * R2}).strided());

    for (size_t i6 = 0; i6 < R2; ++i6)
      for (size_t i3 = 0; i3 < r2; ++i3)
        for (size_t i5 = 0; i5 < n; ++i5)
          for (size_t i2 = 0; i2 < m; ++i2)
            for (size_t i4 = 0; i4 < R1; ++i4)
              for (size_t i1 = 0; i1 < r1; ++i1)
                c(i1, i4, i2, i5, i3, i6) = tmpc(i1, i2, i3, i4, i5, i6);

    return res_core;
  }

public:
  static TTT<T, N> from_components(TensorTrain<T> tt,
                                   std::array<std::vector<size_t>, N> factors) {
    TTT<T, N> result;
    result.m_tt = std::move(tt);
    result.m_size_factors = std::move(factors);
    return result;
  }

  static TTT<T, N> delta(std::array<size_t, N> sizes,
                         std::array<size_t, N> pos) {
    TTT<T, N> result;

    for (size_t i = 0; i < N; ++i) {
      result.m_size_factors[i] = factorise(sizes[i]);
    }

    size_t dimensionality = 0;
    for (size_t i = 0; i < N; ++i) {
      dimensionality =
          std::max(result.m_size_factors[i].size(), dimensionality);
    }

    for (auto &factors : result.m_size_factors) {
      if (factors.size() < dimensionality) {
        factors.insert(factors.end(), dimensionality - factors.size(), 1);
      }
    }

    std::array<std::vector<size_t>, N> pos_tensorised;
    for (size_t i = 0; i < N; ++i) {
      tensorise_index(pos[i], result.m_size_factors[i], pos_tensorised[i]);
    }

    static_assert(N == 1, "NYI");
    std::vector<LowDimensional::Tensor<T, 3>> cores;
    cores.reserve(dimensionality);
    for (size_t i = 0; i < dimensionality; ++i) {
      std::array<size_t, 3> core_sizes = {1, result.m_size_factors[0][i], 1};
      cores.emplace_back(core_sizes);
      std::fill(cores.back().begin(), cores.back().end(), T(0));
      cores.back()(0, pos_tensorised[0][i], 0) = T(1);
    }

    result.m_tt = TensorTrain<T>::from_cores(std::move(cores));
    return result;
  }

  static TTT<T, N> identity(size_t n) {
    static_assert(N == 2, "Identity _matrix_");

    auto factors = factorise(n);
    const size_t dimensionality = factors.size();
    std::vector<LowDimensional::Tensor<T, 3>> cores;
    cores.reserve(dimensionality);

    for (auto factor : factors) {
      std::array<size_t, 3> core_sizes = {1, factor * factor, 1};
      cores.emplace_back(core_sizes);
      auto core = cores.back().ref().template reshape<2>({factor, factor});

      for (size_t j = 0; j < core.size(1); ++j)
        for (size_t i = 0; i < core.size(0); ++i) {
          core(i, j) = i == j ? T(1) : T(0);
        }
    }

    std::array<std::vector<size_t>, 2> ttt_factors = {factors, factors};
    return from_components(TensorTrain<T>::from_cores(std::move(cores)),
                           std::move(ttt_factors));
  }

  static TTT<T, N>
  ttt_svd(LowDimensional::TensorRef<const T, N> tensor, T eps,
          size_t max_rank = std::numeric_limits<size_t>::max()) {
    static_assert(N == 1, "Only vectors supported ATM");
    std::vector<T> els(tensor.cbegin(), tensor.cend());

    const size_t n = tensor.size();
    auto factors = factorise(n);
    std::array<std::vector<size_t>, 1> ttt_factors = {factors};

    auto full = Tensor<T>{std::move(els), factors};

    return from_components(TensorTrain<T>::ttsvd(full, eps, max_rank),
                           std::move(ttt_factors));
  }

  template <typename Fn, typename Gen>
  static TTT<T, N> dmrg_cross(Fn fn, std::array<size_t, N> sizes, T eps,
                              size_t max_rank, size_t max_passes, Gen &gen) {

    ttlog_debug("%d", static_cast<int>(sizes[0]));
    TTT<T, N> result;
    for (size_t i = 0; i < N; ++i) {
      result.m_size_factors[i] = factorise(sizes[i]);
      ttlog_debug("%d, %d", static_cast<int>(sizes[i]),
                  static_cast<int>(result.m_size_factors[i].size()));
    }

    size_t dimensionality = 0;
    for (size_t i = 0; i < N; ++i) {
      dimensionality =
          std::max(result.m_size_factors[i].size(), dimensionality);
    }
    ttlog_debug("%d", static_cast<int>(dimensionality));

    for (auto &factors : result.m_size_factors) {
      if (factors.size() < dimensionality) {
        factors.insert(factors.end(), dimensionality - factors.size(), 1);
      }
    }

    auto tfn = [&fn, &result,
                dimensionality](std::vector<size_t>::const_iterator begin,
                                std::vector<size_t>::const_iterator end) -> T {
      assert(static_cast<size_t>(end - begin) == dimensionality);

      std::array<size_t, N> idx = {0}, multipliers = {0};

      for (auto &m : multipliers)
        m = 1;

      for (size_t i = 0; begin != end; ++i, ++begin) {
        size_t current = *begin;
        for (size_t j = 0; j < N; ++j) {
          idx[j] += multipliers[j] * (current % result.m_size_factors[j][i]);
          current /= result.m_size_factors[j][i];

          multipliers[j] *= result.m_size_factors[j][i];
        }
      }

      return fn(idx);
    };

    LowDimensional::Vector<size_t> tsizes{{dimensionality}};
    for (size_t i = 0; i < dimensionality; ++i) {
      size_t tsize = 1;
      for (size_t j = 0; j < N; ++j)
        tsize *= result.m_size_factors[j][i];
      tsizes(i) = tsize;
    }
    ttlog_debug("%d", static_cast<int>(dimensionality));

    result.m_tt = TensorTrain<T>::dmrg_cross(tfn, tsizes.cref(), eps, max_rank,
                                             max_passes, gen);

    return result;
  }

  template <typename RAIter> T element_at(RAIter begin, RAIter end) const {
    assert(end - begin == N);
    std::vector<size_t> tensorised_idx(m_tt.dimensionality());
    std::array<size_t, N> idx;
    std::copy(begin, end, idx.begin());

    for (size_t i = 0; i < m_tt.dimensionality(); ++i) {
      tensorised_idx[i] = 0;
      for (size_t j = N; j > 0; --j) {
        tensorised_idx[i] = tensorised_idx[i] * m_size_factors[j - 1][i] +
                            idx[j - 1] % m_size_factors[j - 1][i];
        idx[j - 1] /= m_size_factors[j - 1][i];
      }
    }

    return m_tt.element_at(tensorised_idx.cbegin(), tensorised_idx.cend());
  }

  LowDimensional::Tensor<T, N> materialise() const {
    static_assert(N == 1, "Only vectors supported currently");

    auto const materialised_dynamic = m_tt.materialise();

    return LowDimensional::Tensor<T, 1>{materialised_dynamic.data(),
                                        {materialised_dynamic.size(0)}};
  }

  T dot(const TTT<T, N> &other) const { return m_tt.dot(other.m_tt); }

  TTT<T, N + 1> extend_with_zeroes(size_t n) {
    std::array<std::vector<size_t>, N + 1> factors;
    std::copy(m_size_factors.cbegin(), m_size_factors.cend(), factors.begin());
    factors[N] = factorise(n);

    if (internal_dimensionality() > factors[N].size()) {
      factors[N].insert(factors[N].end(),
                        internal_dimensionality() - factors[N].size(), 1);
    } else if (factors[N].size() > internal_dimensionality()) {
      for (auto f = factors.begin(); f != factors.begin() + N; ++f) {
        f->insert(f->end(), factors[N].size() - f->size(), 1);
      }
    }

    std::vector<LowDimensional::Tensor<T, 3>> cores;

    size_t i = 0;
    for (auto core = cores_cbegin(); core != cores_cend(); ++core, ++i) {
      std::array<size_t, 3> new_sizes{
          {core->size(0), core->size(1) * factors[N][i], core->size(2)}};
      cores.emplace_back(new_sizes);

      auto current_core = cores.back().ref().template reshape<4>(
          {core->size(0), core->size(1), factors[N][i], core->size(2)});

      for (size_t k = 0; k < current_core.size(3); ++k)
        for (size_t j = 0; j < current_core.size(1); ++j)
          for (size_t i = 0; i < current_core.size(0); ++i) {
            current_core(i, j, 0, k) = (*core)(i, j, k);
          }

      for (size_t l = 0; l < current_core.size(3); ++l)
        for (size_t k = 1; k < current_core.size(2); ++k)
          for (size_t j = 0; j < current_core.size(1); ++j)
            for (size_t i = 0; i < current_core.size(0); ++i) {
              current_core(i, j, k, l) = T(0);
            }
    }

    return TTT<T, N + 1>::from_components(
        TensorTrain<T>::from_cores(std::move(cores)), std::move(factors));
  }

  TTT<T, N + 1> extend_repeating(size_t n) {
    std::array<std::vector<size_t>, N + 1> factors;
    std::copy(m_size_factors.cbegin(), m_size_factors.cend(), factors.begin());
    factors[N] = factorise(n);

    if (internal_dimensionality() > factors[N].size()) {
      factors[N].insert(factors[N].end(),
                        internal_dimensionality() - factors[N].size(), 1);
    } else if (factors[N].size() > internal_dimensionality()) {
      for (auto f = factors.begin(); f != factors.begin() + N; ++f) {
        f->insert(f->end(), factors[N].size() - f->size(), 1);
      }
    }

    std::vector<LowDimensional::Tensor<T, 3>> cores;

    size_t i = 0;
    for (auto core = cores_cbegin(); core != cores_cend(); ++core, ++i) {
      std::array<size_t, 3> new_sizes{
          {core->size(0), core->size(1) * factors[N][i], core->size(2)}};
      cores.emplace_back(new_sizes);

      auto current_core = cores.back().ref().template reshape<4>(
          {core->size(0), core->size(1), factors[N][i], core->size(2)});

      for (size_t l = 0; l < current_core.size(3); ++l)
        for (size_t k = 0; k < current_core.size(2); ++k)
          for (size_t j = 0; j < current_core.size(1); ++j)
            for (size_t i = 0; i < current_core.size(0); ++i) {
              current_core(i, j, k, l) = (*core)(i, j, l);
            }
    }

    return TTT<T, N + 1>::from_components(
        TensorTrain<T>::from_cores(std::move(cores)), std::move(factors));
  }

  void flip3dim() {
    static_assert(N == 3, "3D only");

    auto f1p = m_size_factors[0].cbegin();
    auto f2p = m_size_factors[1].cbegin();
    auto f3p = m_size_factors[2].cbegin();
    auto core_p = cores_cbegin();

    std::vector<LowDimensional::Tensor<T, 3>> new_cores;
    for (size_t i = 0; i < internal_dimensionality();
         ++i, ++f1p, ++f2p, ++f3p, ++core_p) {
      const size_t m = *f1p, n = *f2p, k = *f3p;
      auto tcore = core_p->cref();
      const size_t r1 = tcore.size(0);
      assert(tcore.size(1) == m * n * k);
      const size_t r2 = tcore.size(2);
      auto core = tcore.template reshape<5>({r1, m, n, k, r2});

      std::array<size_t, 3> new_sizes = {r1, m * n * k, r2};
      new_cores.emplace_back(new_sizes);
      auto new_core =
          new_cores.back().ref().template reshape<5>({r1, k, n, m, r2});

      for (size_t i5 = 0; i5 < r2; ++i5)
        for (size_t i4 = 0; i4 < m; ++i4)
          for (size_t i3 = 0; i3 < n; ++i3)
            for (size_t i2 = 0; i2 < k; ++i2)
              for (size_t i1 = 0; i1 < r1; ++i1)
                new_core(i1, i2, i3, i4, i5) = core(i1, i4, i3, i2, i5);
    }

    std::reverse(m_size_factors.begin(), m_size_factors.end());
    m_tt = TensorTrain<T>::from_cores(std::move(new_cores));
  }

  size_t internal_dimensionality() const { return m_tt.dimensionality(); }

  typename std::vector<LowDimensional::Tensor<T, 3>>::const_iterator
  cores_cbegin() const {
    return m_tt.cores_cbegin();
  }

  typename std::vector<LowDimensional::Tensor<T, 3>>::const_iterator
  cores_cend() const {
    return m_tt.cores_cend();
  }

  typename std::vector<LowDimensional::Tensor<T, 3>>::iterator cores_begin() {
    return m_tt.cores_begin();
  }

  typename std::vector<LowDimensional::Tensor<T, 3>>::iterator cores_end() {
    return m_tt.cores_end();
  }

  const std::array<std::vector<size_t>, N> &size_factors() const {
    return m_size_factors;
  }

  T round(T eps, size_t max_rank = std::numeric_limits<size_t>::max()) {
    return m_tt.round(eps, max_rank);
  }

  size_t max_rank() const {
    size_t R = 0;
    for (size_t i = 0; i < m_tt.dimensionality() - 1; ++i) {
      if (m_tt.rank(i) > R)
        R = m_tt.rank(i);
    }

    return R;
  }

  size_t tt_rank(size_t i) const { return m_tt.rank(i); }

  bool has_sign_variability() const {
    return m_tt.any_core([](const LowDimensional::Tensor<T, 3> &core) -> bool {
      return !std::all_of(core.cbegin(), core.cend(), [](const T &v) -> bool {
        return v <= 0;
      }) && !std::all_of(core.cbegin(), core.cend(), [](const T &v) -> bool {
        return v >= 0;
      });
    });
  }

  template <size_t M> TTT<T, M + N - 2> tdot(const TTT<T, M> &other) const {
    assert(other.internal_dimensionality() == this->internal_dimensionality());
    std::vector<LowDimensional::Tensor<T, 3>> cores;
    std::array<std::vector<size_t>, M + N - 2> factors;

    for (size_t j = 0; j < N - 1; ++j)
      factors[j].assign(m_size_factors[j].begin(), m_size_factors[j].end());
    for (size_t j = 0; j < M - 1; ++j)
      factors[N - 1 + j].assign(other.size_factors()[j + 1].begin(),
                                other.size_factors()[j + 1].end());

    for (auto this_core = cores_cbegin(), other_core = other.cores_cbegin();
         this_core != cores_cend() && other_core != other.cores_cend();
         ++this_core, ++other_core) {
      const size_t i = this_core - cores_cbegin();

      size_t m = 1, n = 1, k = 1;
      for (size_t j = 0; j < N - 1; ++j)
        m *= m_size_factors[j][i];
      for (size_t j = 0; j < M - 1; ++j)
        n *= other.size_factors()[j + 1][i];
      k = m_size_factors[N - 1][i];
      assert(k == other.size_factors()[0][i]);

      size_t r1 = this_core->size(0), r2 = this_core->size(2),
             R1 = other_core->size(0), R2 = other_core->size(2);

      auto a = this_core->cref().template reshape<4>({r1, m, k, r2});
      auto b = other_core->cref().template reshape<4>({R1, k, n, R2});

      cores.emplace_back(product_core(a, b));
    }

    return TTT<T, M + N - 2>::from_components(
        TensorTrain<T>::from_cores(std::move(cores)), std::move(factors));
  }

  TTT<T, N> operator+(const TTT<T, N> &other) const {
    return from_components(m_tt + other.m_tt, m_size_factors);
  }

  TTT<T, N> &operator+=(const TTT<T, N> &other) {
    m_tt += other.m_tt;
    return *this;
  }

  TTT<T, N> operator*(const TTT<T, N> &other) const {
    return from_components(m_tt * other.m_tt, m_size_factors);
  }

  TTT<T, N> operator-(const TTT<T, N> &other) const {
    return from_components(m_tt - other.m_tt, m_size_factors);
  }

  TTT<T, N> operator*(T val) const {
    return from_components(m_tt * val, m_size_factors);
  }

  TTT<T, N> operator/(T val) const {
    return from_components(m_tt / val, m_size_factors);
  }

  void left_orthogonalise_tt() { m_tt.left_orthogonalise(); }

  void amen(const TTT<T, 1> &rhs, TTT<T, 1> &x, T eps, T working_eps,
            size_t max_passes, size_t working_rank, size_t gmres_iterations,
            T gmres_eps,
            std::function<void(size_t, double)> pass_callback = nullptr) const {
    static_assert(N == 2, "Can only solve system with a matrix");
    size_t dim = internal_dimensionality();
    assert(dim == rhs.internal_dimensionality());
    assert(dim == x.internal_dimensionality());

    for (size_t pass = 0; pass < max_passes; ++pass) {
      x.left_orthogonalise_tt();

      auto full_residue = rhs - this->tdot(x);
      auto compressed_residue = full_residue;
      compressed_residue.round(working_eps, working_rank);
      auto rnorm2 = compressed_residue.dot(compressed_residue);
      if (rnorm2 < eps * eps)
        return;
      double norm = std::sqrt(rnorm2);
      if (pass_callback == nullptr) {
        ttlog_info("Starting pass %zu with norm %g", pass, norm);
      } else {
        pass_callback(pass, std::sqrt(rnorm2));
      }

      compressed_residue.round(T(0), 5);
      compressed_residue.left_orthogonalise_tt();

      std::vector<LowDimensional::Tensor<T, 3>> right_reduced_cores,
          left_reduced_cores;
      right_reduced_cores.resize(dim);
      left_reduced_cores.resize(dim);

      std::vector<LowDimensional::Tensor<T, 2>> rhs_right_reduced_cores(dim),
          rhs_left_reduced_cores(dim);

      // Construct right part of X* A X
      {
        right_reduced_cores.at(dim - 1) =
            LowDimensional::Tensor<T, 3>(std::vector<T>({T(1)}), {1, 1, 1});

        auto x_core_p = x.cores_cend();
        --x_core_p;
        auto a_core_p = this->cores_cend();
        --a_core_p;
        for (size_t k = dim - 1; k > 0; --k, --x_core_p, --a_core_p) {
          auto x_core = x_core_p->cref();
          const size_t r1 = x_core.size(0);
          const size_t r2 = x_core.size(2);
          const size_t n = x_core.size(1);

          auto a_core = a_core_p->cref();
          const size_t R1 = a_core.size(0);
          const size_t R2 = a_core.size(2);
          assert(n * n == a_core.size(1));
          assert(n == m_size_factors[0][k]);
          assert(n == m_size_factors[1][k]);

          auto a_m_core = a_core.template reshape<4>({R1, n, n, R2});

          auto prev_R = right_reduced_cores.at(k).cref();
          assert(prev_R.size(0) == r2);
          assert(prev_R.size(1) == R2);
          assert(prev_R.size(2) == r2);

          LowDimensional::Tensor<T, 4> temp1{{r2, R2, r1, n}};
          BLAS::L3::gemm(
              BLAS::transpose::no, BLAS::transpose::yes, T(1),
              prev_R.template reshape<2>({r2 * R2, r2}).strided(),
              x_core.template reshape<2>({r1 * n, r2}).strided(), T(0),
              temp1.ref().template reshape<2>({r2 * R2, r1 * n}).strided());
          LowDimensional::Tensor<T, 4> temp1p{{r2, r1, n, R2}};
          for (size_t i4 = 0; i4 < R2; ++i4)
            for (size_t i3 = 0; i3 < n; ++i3)
              for (size_t i2 = 0; i2 < r1; ++i2)
                for (size_t i1 = 0; i1 < r2; ++i1)
                  temp1p(i1, i2, i3, i4) = temp1(i1, i4, i2, i3);

          LowDimensional::Tensor<T, 4> temp2{{r2, r1, R1, n}};
          BLAS::L3::gemm(
              BLAS::transpose::no, BLAS::transpose::yes, T(1),
              temp1p.cref().template reshape<2>({r2 * r1, n * R2}).strided(),
              a_m_core.template reshape<2>({R1 * n, n * R2}).strided(), T(0),
              temp2.ref().template reshape<2>({r2 * r1, R1 * n}).strided());
          LowDimensional::Tensor<T, 4> temp2p{{n, r2, R1, r1}};
          for (size_t i4 = 0; i4 < r1; ++i4)
            for (size_t i3 = 0; i3 < R1; ++i3)
              for (size_t i2 = 0; i2 < r2; ++i2)
                for (size_t i1 = 0; i1 < n; ++i1)
                  temp2p(i1, i2, i3, i4) = temp2(i2, i4, i3, i1);

          right_reduced_cores.at(k - 1) =
              LowDimensional::Tensor<T, 3>({r1, R1, r1});
          BLAS::L3::gemm(
              BLAS::transpose::no, BLAS::transpose::no, T(1),
              x_core.template reshape<2>({r1, n * r2}).strided(),
              temp2p.cref().template reshape<2>({n * r2, R1 * r1}).strided(),
              T(0),
              right_reduced_cores.at(k - 1)
                  .ref()
                  .template reshape<2>({r1, R1 * r1})
                  .strided());
        }
      }

      // Construct right part of X* b
      {
        rhs_right_reduced_cores.at(dim - 1) =
            LowDimensional::Tensor<T, 2>(std::vector<T>({T(1)}), {1, 1});

        auto x_core_p = x.cores_cend();
        --x_core_p;
        auto rhs_core_p = rhs.cores_cend();
        --rhs_core_p;
        for (size_t k = dim - 1; k > 0; --k, --x_core_p, --rhs_core_p) {
          auto x_core = x_core_p->cref();
          const size_t r1 = x_core.size(0);
          const size_t n = x_core.size(1);
          const size_t r2 = x_core.size(2);

          auto rhs_core = rhs_core_p->cref();
          const size_t R1 = rhs_core.size(0);
          assert(rhs_core.size(1) == n);
          const size_t R2 = rhs_core.size(2);

          auto prevR = rhs_right_reduced_cores.at(k).cref();
          assert(prevR.size(0) == r2);
          assert(prevR.size(1) == R2);

          LowDimensional::Tensor<T, 3> tmp{{R1, n, r2}};
          BLAS::L3::gemm(BLAS::transpose::no, BLAS::transpose::yes, T(1),
                         rhs_core.template reshape<2>({R1 * n, R2}).strided(),
                         prevR.strided(), T(0),
                         tmp.ref().template reshape<2>({R1 * n, r2}).strided());

          rhs_right_reduced_cores.at(k - 1) =
              LowDimensional::Tensor<T, 2>({r1, R1});
          BLAS::L3::gemm(BLAS::transpose::no, BLAS::transpose::yes, T(1),
                         x_core.template reshape<2>({r1, n * r2}).strided(),
                         tmp.cref().template reshape<2>({R1, n * r2}).strided(),
                         T(0),
                         rhs_right_reduced_cores.at(k - 1).ref().strided());
        }
      }

      std::vector<LowDimensional::Tensor<T, 2>> residue_right_reduced_cores(
          dim),
          residue_left_reduced_cores(dim);

      // Construct right part of Z* (b - A X)
      {
        residue_right_reduced_cores.at(dim - 1) =
            LowDimensional::Tensor<T, 2>(std::vector<T>({T(1)}), {1, 1});

        auto full_residue_core_p = full_residue.cores_cend();
        --full_residue_core_p;
        auto compressed_residue_core_p = compressed_residue.cores_cend();
        --compressed_residue_core_p;
        for (size_t k = dim - 1; k > 0;
             --k, --full_residue_core_p, --compressed_residue_core_p) {
          auto R = full_residue_core_p->cref();
          auto Z = compressed_residue_core_p->cref();
          const size_t R1 = R.size(0), R2 = R.size(2), r1 = Z.size(0),
                       r2 = Z.size(2), n = R.size(1);
          assert(n == Z.size(1));

          auto prev = residue_right_reduced_cores.at(k).cref();
          assert(prev.size(0) == R2);
          assert(prev.size(1) == r2);

          LowDimensional::Tensor<T, 3> tmp({R1, n, r2});
          BLAS::L3::gemm(BLAS::transpose::no, BLAS::transpose::no, T(1),
                         R.template reshape<2>({R1 * n, R2}).strided(),
                         prev.strided(), T(0),
                         tmp.ref().template reshape<2>({R1 * n, r2}).strided());

          residue_right_reduced_cores.at(k - 1) =
              LowDimensional::Matrix<T>({R1, r1});
          BLAS::L3::gemm(BLAS::transpose::no, BLAS::transpose::yes, T(1),
                         tmp.cref().template reshape<2>({R1, n * r2}).strided(),
                         Z.template reshape<2>({r1, n * r2}).strided(), T(0),
                         residue_right_reduced_cores.at(k - 1).ref().strided());
        }
      }

      {
        left_reduced_cores.at(0) =
            LowDimensional::Tensor<T, 3>(std::vector<T>({1}), {1, 1, 1});
        rhs_left_reduced_cores.at(0) =
            LowDimensional::Tensor<T, 2>(std::vector<T>({1}), {1, 1});
        residue_left_reduced_cores.at(0) =
            LowDimensional::Tensor<T, 2>(std::vector<T>({1}), {1, 1});

        auto x_core_p = x.cores_begin();
        auto a_core_p = this->cores_cbegin();
        auto rhs_core_p = rhs.cores_cbegin();
        auto z_core_p = compressed_residue.cores_cbegin();
        for (size_t k = 0; k < dim;
             ++k, ++x_core_p, ++a_core_p, ++rhs_core_p, ++z_core_p) {
          auto x_core = x_core_p->ref();
          auto a_core = a_core_p->cref();
          auto rhs_core = rhs_core_p->cref();
          auto z_core = z_core_p->cref();

          const size_t xr1 = x_core.size(0);
          const size_t n = x_core.size(1);
          const size_t xr2 = x_core.size(2);

          const size_t ar1 = a_core.size(0);
          const size_t ar2 = a_core.size(2);
          assert(a_core.size(1) == n * n);

          const size_t rr1 = rhs_core.size(0);
          const size_t rr2 = rhs_core.size(2);
          assert(rhs_core.size(1) == n);

          // const size_t zr1 = z_core.size(0);
          const size_t zr2 = z_core.size(2);
          assert(z_core.size(1) == n);

          LowDimensional::Tensor<T, 3> local_rhs{{xr1, n, xr2}};
          {
            LowDimensional::Tensor<T, 3> tmp{{xr1, n, rr2}};
            BLAS::L3::gemm(
                BLAS::transpose::no, BLAS::transpose::no, T(1),
                rhs_left_reduced_cores.at(k).cref().strided(),
                rhs_core.template reshape<2>({rr1, n * rr2}).strided(), T(0),
                tmp.ref().template reshape<2>({xr1, n * rr2}).strided());
            BLAS::L3::gemm(
                BLAS::transpose::no, BLAS::transpose::yes, T(1),
                tmp.cref().template reshape<2>({xr1 * n, rr2}).strided(),
                rhs_right_reduced_cores.at(k).cref().strided(), T(0),
                local_rhs.ref().template reshape<2>({xr1 * n, xr2}).strided());
          }

          {
            LowDimensional::Tensor<T, 4> temp1{{xr2, ar2, xr1, n}},
                temp1p{{xr2, xr1, n, ar2}}, temp2{{ar1, n, xr2, xr1}},
                temp2p{{ar1, xr1, n, xr2}};

            auto R = right_reduced_cores.at(k).cref();
            auto L = left_reduced_cores.at(k).cref();
            auto a = a_core.template reshape<4>({ar1, n, n, ar2});
            gmres(
                [R, L, a, &temp1, &temp1p, &temp2, &temp2p, xr1, n, xr2, ar1,
                 ar2](LowDimensional::VectorRef<const T> vx,
                      LowDimensional::VectorRef<T> vy) mutable {
                  auto x = vx.template reshape<3>({xr1, n, xr2});
                  auto y = vy.template reshape<3>({xr1, n, xr2});

                  BLAS::L3::gemm(
                      BLAS::transpose::no, BLAS::transpose::yes, T(1),
                      R.template reshape<2>({xr2 * ar2, xr2}).strided(),
                      x.template reshape<2>({xr1 * n, xr2}).strided(), T(0),
                      temp1.ref()
                          .template reshape<2>({xr2 * ar2, xr1 * n})
                          .strided());

                  for (size_t i4 = 0; i4 < ar2; ++i4)
                    for (size_t i3 = 0; i3 < n; ++i3)
                      for (size_t i2 = 0; i2 < xr1; ++i2)
                        for (size_t i1 = 0; i1 < xr2; ++i1)
                          temp1p(i1, i2, i3, i4) = temp1(i1, i4, i2, i3);

                  BLAS::L3::gemm(
                      BLAS::transpose::no, BLAS::transpose::yes, T(1),
                      a.template reshape<2>({ar1 * n, n * ar2}).strided(),
                      temp1p.cref()
                          .template reshape<2>({xr2 * xr1, n * ar2})
                          .strided(),
                      T(0),
                      temp2.ref()
                          .template reshape<2>({ar1 * n, xr2 * xr1})
                          .strided());

                  for (size_t i4 = 0; i4 < xr2; ++i4)
                    for (size_t i3 = 0; i3 < n; ++i3)
                      for (size_t i2 = 0; i2 < xr1; ++i2)
                        for (size_t i1 = 0; i1 < ar1; ++i1)
                          temp2p(i1, i2, i3, i4) = temp2(i1, i3, i4, i2);

                  BLAS::L3::gemm(
                      BLAS::transpose::no, BLAS::transpose::no, T(1),
                      L.template reshape<2>({xr1, ar1 * xr1}).strided(),
                      temp2p.cref()
                          .template reshape<2>({ar1 * xr1, n * xr2})
                          .strided(),
                      T(0), y.template reshape<2>({xr1, n * xr2}).strided());
                },
                [](LowDimensional::VectorRef<T>) {},
                local_rhs.cref().template reshape<1>({xr1 * n * xr2}),
                x_core.template reshape<1>({xr1 * n * xr2}), gmres_iterations,
                gmres_iterations, gmres_eps);
          }

          // extend via rhs
          // Recalculate left
          if (k + 1 < dim) {
            auto ax = product_core(
                a_core.template reshape<4>({ar1, n, n, ar2}),
                x_core.cref().template reshape<4>({xr1, n, 1, xr2}));
            assert(ax.size(0) == ar1 * xr1);
            assert(ax.size(1) == n);
            assert(ax.size(2) == ar2 * xr2);

            std::array<size_t, 3> offsets = {rr1, 0, rr2};
            if (k == 0)
              offsets[0] = 0;
            LowDimensional::Tensor<T, 3> residue_core(
                {ar1 * xr1 + offsets[0], n, ar2 * xr2 + rr2});
            for (size_t ix3 = 0; ix3 < rr2; ++ix3)
              for (size_t ix2 = 0; ix2 < n; ++ix2)
                for (size_t ix1 = 0; ix1 < rr1; ++ix1) {
                  residue_core(ix1, ix2, ix3) = rhs_core(ix1, ix2, ix3);
                }
            for (size_t ix3 = 0; ix3 < ar2 * xr2; ++ix3)
              for (size_t ix2 = 0; ix2 < n; ++ix2)
                for (size_t ix1 = 0; ix1 < ar1 * xr1; ++ix1) {
                  residue_core(ix1 + offsets[0], ix2, ix3 + rr2) =
                      ax(ix1, ix2, ix3);
                }

            LowDimensional::Tensor<T, 3> local_expander({xr1, n, zr2});
            {
              LowDimensional::Tensor<T, 3> tmp({xr1, n, residue_core.size(2)});
              BLAS::L3::gemm(
                  BLAS::transpose::no, BLAS::transpose::no, T(1),
                  residue_left_reduced_cores.at(k).cref().strided(),
                  residue_core.cref()
                      .template reshape<2>(
                          {residue_core.size(0), n * residue_core.size(2)})
                      .strided(),
                  T(0),
                  tmp.ref()
                      .template reshape<2>({xr1, n * residue_core.size(2)})
                      .strided());
              BLAS::L3::gemm(
                  BLAS::transpose::no, BLAS::transpose::no, T(1),
                  tmp.cref()
                      .template reshape<2>({xr1 * n, residue_core.size(2)})
                      .strided(),
                  residue_right_reduced_cores.at(k).cref().strided(), T(0),
                  local_expander.ref()
                      .template reshape<2>({xr1 * n, zr2})
                      .strided());
            }

            LowDimensional::Tensor<T, 3> expanded_x_core({xr1, n, xr2 + zr2});

            {
              BLAS::L1::copy(
                  x_core.cref().template reshape<1>({x_core.size()}).strided(),
                  expanded_x_core.ref()
                      .template reshape<1>({expanded_x_core.size()})
                      .strided()
                      .subslice({0}, {xr1 * n * xr2}));
              BLAS::L1::copy(local_expander.cref()
                                 .template reshape<1>({local_expander.size()})
                                 .strided(),
                             expanded_x_core.ref()
                                 .template reshape<1>({expanded_x_core.size()})
                                 .strided()
                                 .subslice({xr1 * n * xr2}, {xr1 * n * zr2}));
            }

            {
              LowDimensional::Matrix<T> vt(
                  {std::min(xr1 * n, xr2 + zr2), xr2 + zr2});
              LowDimensional::Vector<T> s({vt.size(0)}), superb({vt.size(0)});
              auto vt_ref = vt.ref().strided();

              LAPACK::gesvd(LAPACK::svd_vectors::overwrite,
                            LAPACK::svd_vectors::significant,
                            expanded_x_core.ref()
                                .template reshape<2>({xr1 * n, xr2 + zr2})
                                .strided(),
                            s.ref(), nullptr, &vt_ref, superb.ref());
              const size_t rank =
                  std::max(static_cast<size_t>(1),
                           std::min(working_rank,
                                    static_cast<size_t>(
                                        std::find_if(s.cbegin(), s.cend(),
                                                     [working_eps](T sv) {
                                                       return sv < working_eps;
                                                     }) -
                                        s.cbegin())));
              expanded_x_core.resize_last(rank);

              *x_core_p = std::move(expanded_x_core);
              x_core = x_core_p->ref();

              for (size_t i = 0; i < rank; ++i)
                BLAS::L1::scal(s(i), vt.ref().strided().template fix_idx<0>(i));

              auto next_x_core_p = x_core_p + 1;
              auto next_x_core = next_x_core_p->cref();

              LowDimensional::Tensor<T, 3> new_next_x_core(
                  {rank, next_x_core.size(1), next_x_core.size(2)});

              BLAS::L3::gemm(
                  BLAS::transpose::no, BLAS::transpose::no, T(1),
                  vt.cref().strided().subslice({0, 0},
                                               {rank, next_x_core.size(0)}),
                  next_x_core.cref()
                      .template reshape<2>(
                          {next_x_core.size(0),
                           next_x_core.size(1) * next_x_core.size(2)})
                      .strided(),
                  T(0),
                  new_next_x_core.ref()
                      .template reshape<2>(
                          {rank, next_x_core.size(1) * next_x_core.size(2)})
                      .strided());

              *next_x_core_p = std::move(new_next_x_core);
            }

            const size_t new_xr2 = x_core.size(2);

            // Update left part of X*AX
            {
              auto prevL = left_reduced_cores.at(k).cref();

              LowDimensional::Tensor<T, 4> tmp1({n, new_xr2, ar1, xr1});
              BLAS::L3::gemm(
                  BLAS::transpose::yes, BLAS::transpose::no, T(1),
                  x_core.cref()
                      .template reshape<2>({xr1, n * new_xr2})
                      .strided(),
                  prevL.template reshape<2>({xr1, ar1 * xr1}).strided(), T(0),
                  tmp1.ref()
                      .template reshape<2>({n * new_xr2, ar1 * xr1})
                      .strided());
              LowDimensional::Tensor<T, 4> tmp1p({new_xr2, xr1, ar1, n});
              for (size_t i4 = 0; i4 < n; ++i4)
                for (size_t i3 = 0; i3 < ar1; ++i3)
                  for (size_t i2 = 0; i2 < xr1; ++i2)
                    for (size_t i1 = 0; i1 < new_xr2; ++i1) {
                      tmp1p(i1, i2, i3, i4) = tmp1(i4, i1, i3, i2);
                    }
              LowDimensional::Tensor<T, 4> tmp2({new_xr2, xr1, n, ar2});
              BLAS::L3::gemm(
                  BLAS::transpose::no, BLAS::transpose::no, T(1),
                  tmp1p.cref()
                      .template reshape<2>({new_xr2 * xr1, ar1 * n})
                      .strided(),
                  a_core.template reshape<2>({ar1 * n, n * ar2}).strided(),
                  T(0),
                  tmp2.ref()
                      .template reshape<2>({new_xr2 * xr1, n * ar2})
                      .strided());
              LowDimensional::Tensor<T, 4> tmp2p({new_xr2, ar2, xr1, n});
              for (size_t i4 = 0; i4 < n; ++i4)
                for (size_t i3 = 0; i3 < xr1; ++i3)
                  for (size_t i2 = 0; i2 < ar2; ++i2)
                    for (size_t i1 = 0; i1 < new_xr2; ++i1) {
                      tmp2p(i1, i2, i3, i4) = tmp2(i1, i3, i4, i2);
                    }

              left_reduced_cores.at(k + 1) =
                  LowDimensional::Tensor<T, 3>({new_xr2, ar2, new_xr2});
              BLAS::L3::gemm(BLAS::transpose::no, BLAS::transpose::no, T(1),
                             tmp2p.cref()
                                 .template reshape<2>({new_xr2 * ar2, xr1 * n})
                                 .strided(),
                             x_core.cref()
                                 .template reshape<2>({xr1 * n, new_xr2})
                                 .strided(),
                             T(0),
                             left_reduced_cores.at(k + 1)
                                 .ref()
                                 .template reshape<2>({new_xr2 * ar2, new_xr2})
                                 .strided());
            }

            // Update left part of X*b
            {
              // TODO: the first part of this is identical with the
              // local_rhs calculation above
              LowDimensional::Tensor<T, 3> tmp{{xr1, n, rr2}};
              BLAS::L3::gemm(
                  BLAS::transpose::no, BLAS::transpose::no, T(1),
                  rhs_left_reduced_cores.at(k).cref().strided(),
                  rhs_core.template reshape<2>({rr1, n * rr2}).strided(), T(0),
                  tmp.ref().template reshape<2>({xr1, n * rr2}).strided());
              rhs_left_reduced_cores.at(k + 1) =
                  LowDimensional::Tensor<T, 2>{{new_xr2, rr2}};
              BLAS::L3::gemm(
                  BLAS::transpose::yes, BLAS::transpose::no, T(1),
                  x_core.cref()
                      .template reshape<2>({xr1 * n, new_xr2})
                      .strided(),
                  tmp.cref().template reshape<2>({xr1 * n, rr2}).strided(),
                  T(0), rhs_left_reduced_cores.at(k + 1).ref().strided());
            }

            // Update left part of residue
            {
              // Since the core has changed, we need to recalculate residue
              ax = product_core(
                  a_core.template reshape<4>({ar1, n, n, ar2}),
                  x_core.cref().template reshape<4>({xr1, n, 1, new_xr2}));
              assert(ax.size(0) == ar1 * xr1);
              assert(ax.size(1) == n);
              assert(ax.size(2) == ar2 * new_xr2);

              std::array<size_t, 3> offsets = {rr1, 0, rr2};
              if (k == 0)
                offsets[0] = 0;
              residue_core = LowDimensional::Tensor<T, 3>(
                  {ar1 * xr1 + offsets[0], n, ar2 * new_xr2 + rr2});
              for (size_t ix3 = 0; ix3 < rr2; ++ix3)
                for (size_t ix2 = 0; ix2 < n; ++ix2)
                  for (size_t ix1 = 0; ix1 < rr1; ++ix1) {
                    residue_core(ix1, ix2, ix3) = rhs_core(ix1, ix2, ix3);
                  }
              for (size_t ix3 = 0; ix3 < ar2 * new_xr2; ++ix3)
                for (size_t ix2 = 0; ix2 < n; ++ix2)
                  for (size_t ix1 = 0; ix1 < ar1 * xr1; ++ix1) {
                    residue_core(ix1 + offsets[0], ix2, ix3 + rr2) =
                        ax(ix1, ix2, ix3);
                  }

              LowDimensional::Tensor<T, 3> tmp{{xr1, n, residue_core.size(2)}};
              BLAS::L3::gemm(
                  BLAS::transpose::no, BLAS::transpose::no, T(1),
                  residue_left_reduced_cores.at(k).cref().strided(),
                  residue_core.cref()
                      .template reshape<2>(
                          {residue_core.size(0), n * residue_core.size(2)})
                      .strided(),
                  T(0),
                  tmp.ref()
                      .template reshape<2>({xr1, n * residue_core.size(2)})
                      .strided());
              residue_left_reduced_cores.at(k + 1) =
                  LowDimensional::Tensor<T, 2>({new_xr2, residue_core.size(2)});
              BLAS::L3::gemm(
                  BLAS::transpose::yes, BLAS::transpose::no, T(1),
                  x_core.cref()
                      .template reshape<2>({xr1 * n, new_xr2})
                      .strided(),
                  tmp.cref()
                      .template reshape<2>({xr1 * n, residue_core.size(2)})
                      .strided(),
                  T(0), residue_left_reduced_cores.at(k + 1).ref().strided());
            }
          }
        }
      }
    }
  }
};

template <typename T, size_t N>
inline TTT<T, N> operator*(T val, const TTT<T, N> &ttt) {
  return ttt * val;
}

} // namespace TT

#endif // __TTSUITE__TTT__HPP__
