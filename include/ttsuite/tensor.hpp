#ifndef __TTSUITE__TENSOR__HPP__
#define __TTSUITE__TENSOR__HPP__

#include <functional>
#include <numeric>
#include <stdexcept>
#include <vector>

#include "low_dimensional.hpp"

namespace TT {
template <typename T> class Tensor {
private:
  using V = std::vector<T>;
  V m_elements;
  std::vector<size_t> m_sizes;

  template <typename BidirectionalIterator>
  size_t linearIdx(BidirectionalIterator beginIx, BidirectionalIterator endIx) {
    size_t linear = 0;
    auto sizeIter = m_sizes.rbegin();
    for (--endIx; sizeIter != m_sizes.rend(); --endIx, ++sizeIter) {
      linear = *endIx + linear * *sizeIter;
    }
    return linear;
  }

public:
  Tensor(std::vector<T> elements, std::vector<size_t> sizes)
      : m_elements(std::move(elements)), m_sizes(std::move(sizes)) {
    assert(m_elements.size() == std::accumulate(m_sizes.begin(), m_sizes.end(),
                                                1ul, std::multiplies<size_t>()));
  }

  Tensor(std::vector<size_t> sizes) : m_sizes(std::move(sizes)) {
    m_elements.resize(std::accumulate(m_sizes.begin(), m_sizes.end(), 1ul,
                                      std::multiplies<size_t>()));
  }

  using value_type = typename V::value_type;
  using size_type = typename V::size_type;
  using difference_type = typename V::difference_type;
  using reference = typename V::reference;
  using const_reference = typename V::const_reference;
  using pointer = typename V::pointer;
  using const_pointer = typename V::const_pointer;
  using iterator = typename V::iterator;
  using const_iterator = typename V::const_iterator;
  using reverse_iterator = typename V::reverse_iterator;
  using const_reverse_iterator = typename V::const_reverse_iterator;

  template <typename BidirectionalIterator>
  reference operator()(BidirectionalIterator beginIx,
                       BidirectionalIterator endIx) {
    return m_elements[linearIdx(beginIx, endIx)];
  }

  template <typename BidirectionalIterator>
  const_reference operator()(BidirectionalIterator beginIx,
                             BidirectionalIterator endIx) const {
    return m_elements[linearIdx(beginIx, endIx)];
  }

  pointer data() { return m_elements.data(); }
  const_pointer data() const { return m_elements.data(); }
  const_pointer cdata() const { return m_elements.data(); }

  template <typename ForwardIterator>
  void reshape(ForwardIterator beginSizes, ForwardIterator endSizes) {
    if (m_elements.size() !=
        std::accumulate(beginSizes, endSizes, 1, std::multiplies<size_t>()))
      throw std::invalid_argument("Can't change sizes in reshape");
    m_sizes.assign(beginSizes, endSizes);
  }

  void popLastSize() {
    if (m_sizes.back() != 1)
      throw std::invalid_argument("Cant't change size");

    m_sizes.pop_back();
  }

  size_t dimensionality() const { return m_sizes.size(); }

  LowDimensional::TensorRef<T, 2> unfolding(size_t dim) {
    assert(dim < dimensionality());
    size_t m = std::accumulate(m_sizes.begin(), m_sizes.begin() + dim, 1,
                               std::multiplies<size_t>()),
           n = std::accumulate(m_sizes.begin() + dim, m_sizes.end(), 1,
                               std::multiplies<size_t>());
    return LowDimensional::TensorRef<T, 2>({m, n}, m_elements.data(), m * n);
  }

  size_type size(size_t dim) const { return m_sizes[dim]; }
  size_type size() const { return m_elements.size(); }
  size_type max_size() const { return m_elements.max_size(); }
  bool empty() const { return m_elements.empty(); }

  iterator begin() { return m_elements.begin(); }
  iterator end() { return m_elements.end(); }
  const_iterator begin() const { return m_elements.begin(); }
  const_iterator end() const { return m_elements.end(); }
  const_iterator cbegin() const { return m_elements.cbegin(); }
  const_iterator cend() const { return m_elements.cend(); }

  reverse_iterator rbegin() { return m_elements.rbegin(); }
  reverse_iterator rend() { return m_elements.rend(); }
  const_reverse_iterator rbegin() const { return m_elements.rbegin(); }
  const_reverse_iterator rend() const { return m_elements.rend(); }
  const_reverse_iterator crbegin() const { return m_elements.crbegin(); }
  const_reverse_iterator crend() const { return m_elements.crend(); }
};
} // namespace TT

#endif // __TSUITE__TENSOR__HPP__
