#ifndef __TTSUITE__LOW_DIMENSIONAL__HPP__
#define __TTSUITE__LOW_DIMENSIONAL__HPP__

#include <array>
#include <cassert>
#include <cmath>
#include <complex>
#include <cstddef>
#include <iterator>
#include <limits>
#include <numeric>
#include <type_traits>
#include <vector>

#include <iomanip>
#include <iostream>

namespace TT {
namespace LowDimensional {

namespace Internal {
inline size_t linearIdx(const size_t *) { return 0; }

template <typename... Indices>
inline size_t linearIdx(const size_t *sizes, size_t idx, Indices... indices) {
  return idx + *sizes * Internal::linearIdx(sizes + 1, indices...);
}

template <size_t M>
inline size_t linearIdxPtr(const size_t *sizes, const size_t *indices) {
  return *indices + *sizes * linearIdxPtr<M - 1>(sizes + 1, indices + 1);
}

template <> inline size_t linearIdxPtr<0>(const size_t *, const size_t *) {
  return 0;
}

inline size_t dot_product(const size_t *) { return 0; }

template <typename... Indices>
inline size_t dot_product(const size_t *multipliers, size_t idx,
                          Indices... indices) {
  return *multipliers * idx +
         Internal::dot_product(multipliers + 1, indices...);
}

template <size_t N> inline size_t product(const size_t *v) {
  return *v * product<N - 1>(v + 1);
}

template <> inline size_t product<0>(const size_t *) { return 1; }

template <typename T> struct RealType {};
template <typename T> struct RealType<std::complex<T>> { using t = T; };
template <typename T> struct RealType<const std::complex<T>> {
  using t = const T;
};
template <typename T> struct RealType<volatile std::complex<T>> {
  using t = volatile T;
};
template <typename T> struct RealType<const volatile std::complex<T>> {
  using t = const volatile T;
};

template <typename T> using real_type = typename RealType<T>::t;

template <size_t M> struct IndexTraversal {
  const size_t *sizes;

  template <typename F> inline void traverse(F f) {
    IndexTraversal<M - 1> subtraversal{sizes};
    for (size_t i = 0; i < sizes[M - 1]; ++i) {
      subtraversal.traverse(
          [i, &f](auto... indices) mutable { f(indices..., i); });
    }
  }
};

template <> struct IndexTraversal<0> {
  const size_t *sizes;

  template <typename F> inline void traverse(F f) { f(); }
};

} // namespace Internal

template <typename T, size_t N> class StridedTensorRef {
private:
  std::array<size_t, N> m_sizes, m_scales;
  T *m_elements;
  size_t m_size;

public:
  using value_type = T;
  using size_type = size_t;
  using difference_type = ptrdiff_t;
  using reference = T &;
  using const_reference = const T &;
  using pointer = T *;
  using const_pointer = const T *;

  StridedTensorRef(std::array<size_t, N> sizes,
                   std::array<size_t, N> storage_multipliers, T *elements)
      : m_sizes(sizes), m_scales(storage_multipliers), m_elements(elements),
        m_size(Internal::product<N>(sizes.data())) {}

  StridedTensorRef<T, N> subslice(std::array<size_t, N> lower_bounds,
                                  std::array<size_t, N> new_sizes) const {
    size_t offset = 0;
    for (size_t i = 0; i < N; ++i) {
      offset += lower_bounds[i] * m_scales[i];
    }

    return StridedTensorRef<T, N>{new_sizes, m_scales, m_elements + offset};
  }

  StridedTensorRef<T, N> strided(std::array<size_t, N> strides) const {
    std::array<size_t, N> new_scales, new_sizes;
    for (size_t i = 0; i < N; ++i) {
      new_scales[i] = m_scales[i] * strides[i];
      new_sizes[i] = (m_sizes[i] + strides[i] - 1) / strides[i];
    }

    return StridedTensorRef<T, N>{new_sizes, new_scales, m_elements};
  }

  reference operator[](size_type i) const noexcept {
    static_assert(N == 1,
                  "Linear indexing only available for 1-dimensional tensors");
    assert(i < m_sizes[0]);
    return m_elements[i * m_scales[0]];
  }

  template <typename... UInts>
  typename std::enable_if<N == sizeof...(UInts), reference>::type
  operator()(UInts... indices) const noexcept {
    return m_elements[Internal::dot_product(m_scales.data(), indices...)];
  }

  template <size_t Idx> StridedTensorRef<T, N - 1> fix_idx(size_t ix) const {
    static_assert(Idx < N, "Index out of bounds");
    assert(ix < m_sizes[Idx]);
    std::array<size_t, N - 1> new_sizes, new_multipliers;

    for (size_t i = 0; i < Idx; ++i) {
      new_sizes[i] = m_sizes[i];
      new_multipliers[i] = m_scales[i];
    }

    for (size_t i = Idx + 1; i < N; ++i) {
      new_sizes[i - 1] = m_sizes[i];
      new_multipliers[i - 1] = m_scales[i];
    }

    return StridedTensorRef<T, N - 1>(new_sizes, new_multipliers,
                                      m_elements + m_scales[Idx] * ix);
  }

  StridedTensorRef<const T, N> cref() const {
    return StridedTensorRef<const T, N>(m_sizes, m_scales, m_elements);
  }

  size_type size() const noexcept { return m_size; }

  size_type size(size_t dim) const noexcept { return m_sizes[dim]; }

  std::array<size_t, N> shape() const noexcept { return m_sizes; }

  size_type multiplier(size_t dim) const noexcept { return m_scales[dim]; }

  pointer data() const noexcept { return m_elements; }

  template <typename Fn> void ifor(Fn fn) {
    static_assert(N == 1, "Currently supported only for vectors");

    for (size_t i = 0; i < m_size; ++i) {
      fn(i, (*this)[i]);
    }
  }

  template <typename Fn> void ijfor(Fn fn) {
    static_assert(N == 2, "Only supported for matrices");

    if (m_scales[1] > m_scales[0]) {
      for (size_t j = 0; j < m_sizes[1]; ++j) {
        for (size_t i = 0; i < m_sizes[0]; ++i) {
          fn(i, j, (*this)(i, j));
        }
      }
    } else {
      for (size_t i = 0; i < m_sizes[0]; ++i) {
        for (size_t j = 0; j < m_sizes[1]; ++j) {
          fn(i, j, (*this)(i, j));
        }
      }
    }
  }
};

template <typename T, size_t N> class TensorRef {
private:
  std::array<size_t, N> m_sizes;
  T *m_elements;
  size_t m_size;

public:
  using value_type = T;
  using size_type = size_t;
  using difference_type = ptrdiff_t;
  using reference = T &;
  using const_reference = const T &;
  using pointer = T *;
  using const_pointer = const T *;
  using iterator = T *;
  using const_iterator = const T *;
  using reverse_iterator = std::reverse_iterator<iterator>;
  using const_reverse_iterator = std::reverse_iterator<const_iterator>;

  // TODO: figure out a generalisation of lda/incx thing for tensors.
  TensorRef(std::array<size_t, N> sizes, T *elements, size_t sz)
      : m_sizes(sizes), m_elements(elements), m_size(sz) {}

  TensorRef<const T, N> cref() const {
    return TensorRef<const T, N>{m_sizes, m_elements, m_size};
  }

  reference operator[](size_type i) const noexcept {
    static_assert(N == 1,
                  "Linear indexing only available for 1-dimensional tensors");
    return m_elements[i];
  }

  template <typename... UInts>
  typename std::enable_if<N == sizeof...(UInts), reference>::type
  operator()(UInts... indices) const noexcept {
    auto linear = Internal::linearIdx(m_sizes.data(), indices...);

    return m_elements[linear];
  }

  StridedTensorRef<T, N> strided() const noexcept {
    static_assert(N > 0, "");
    std::array<size_t, N> multipliers = {};
    multipliers[0] = 1;

    for (size_t i = 1; i < N; ++i) {
      multipliers[i] = multipliers[i - 1] * m_sizes[i - 1];
    }

    return StridedTensorRef<T, N>(m_sizes, multipliers, m_elements);
  }

  template <size_t M>
  TensorRef<T, M> reshape(std::array<size_t, M> new_sizes) const {
    assert(Internal::product<M>(new_sizes.data()) == m_size);
    return TensorRef<T, M>(new_sizes, m_elements, m_size);
  }

  TensorRef<T, 1> flatten() const {
    return this->template reshape<1>({m_size});
  }

  TensorRef<T, N - 1> index_last(size_t i) const {
    assert(i <= m_sizes[N - 1]);

    std::array<size_t, N - 1> new_sizes;
    std::copy(m_sizes.cbegin(), m_sizes.cbegin() + N - 1, new_sizes.begin());

    size_t new_size = std::accumulate(new_sizes.cbegin(), new_sizes.cend(), 1ul,
                                      std::multiplies<size_t>());

    return TensorRef<T, N - 1>(new_sizes, m_elements + i * new_size, new_size);
  }

  TensorRef<T, N> slice_last(size_t start, size_t size) const {
    assert(start <= m_sizes[N - 1]);
    assert(start + size <= m_sizes[N - 1]);

    std::array<size_t, N> new_sizes;
    std::copy(m_sizes.cbegin(), m_sizes.cbegin() + N - 1, new_sizes.begin());
    new_sizes[N - 1] = size;

    size_t block_size =
        std::accumulate(new_sizes.cbegin(), new_sizes.cbegin() + N - 1, 1ul,
                        std::multiplies<size_t>());
    size_t new_size = block_size * size;

    return TensorRef<T, N>(new_sizes, m_elements + start * block_size,
                           new_size);
  }

  size_type size(size_type dim) const { return m_sizes[dim]; }

  std::array<size_type, N> shape() const { return m_sizes; }

  pointer data() const { return m_elements; }

  size_type size() const { return m_size; }
  size_type max_size() const { return std::numeric_limits<size_type>::max(); }
  bool empty() const { return m_size == 0; }

  iterator begin() const { return m_elements; }
  iterator end() const { return m_elements + m_size; }
  const_iterator cbegin() const { return begin(); }
  const_iterator cend() const { return end(); }

  reverse_iterator rbegin() const { return reverse_iterator{end()}; }
  reverse_iterator rend() const { return reverse_iterator{begin()}; }
  const_reverse_iterator crbegin() const { return rbegin(); }
  const_reverse_iterator crend() const { return rend(); }

  reference front() const { return *m_elements; }
  reference back() const { return *m_elements + m_size - 1; }

  template <typename Fn> void ifor(Fn fn) {
    static_assert(N == 1, "Currently supported only for vectors");

    for (size_t i = 0; i < m_size; ++i) {
      fn(i, m_elements[i]);
    }
  }

  template <typename Fn> void ijfor(Fn fn) {
    static_assert(N == 2, "Only supported for matrices");

    for (size_t j = 0; j < m_sizes[1]; ++j) {
      for (size_t i = 0; i < m_sizes[0]; ++i) {
        fn(i, j, (*this)(i, j));
      }
    }
  }

  template <typename Fn> void iterate_indices(Fn fn) const {
    Internal::IndexTraversal<N> traversal{m_sizes.data()};
    traversal.traverse(fn);
  }

  template <typename Fn> void iterate(Fn fn) const {
    pointer v = m_elements;
    iterate_indices([&fn, &v](auto... indices) mutable {
      fn(*v, indices...);
      ++v;
    });
  }

  void display(const char *name, std::ostream &out = std::cout,
               std::streamsize precision = 2) const {
    auto oldflags = out.setf(std::ios_base::right | std::ios_base::scientific |
                             std::ios_base::showpoint);
    auto oldprecision = out.precision(precision);

    auto width = 3 + precision + 5;

    if (N == 1) {
      out << name << "(:) = \n";
      for (size_t i = 0; i < m_size; ++i) {
        out << " " << std::setw(width) << m_elements[i] << '\n';
      }
      out << std::flush;
    } else if (N == 2) {
      out << name << "(:, :) = \n";
      for (size_t i = 0; i < m_sizes[0]; ++i) {
        for (size_t j = 0; j < m_sizes[1]; ++j) {
          out << ' ' << std::setw(width) << m_elements[i + j * m_sizes[0]];
        }
        out << '\n';
      }
      out << std::flush;
    } else {
      std::array<size_t, N> idx = {};

      for (size_t slice_idx = 0; slice_idx < m_size / (m_sizes[0] * m_sizes[1]);
           ++slice_idx) {
        for (size_t dim = N - 1; dim < N; ++dim) {
          ++idx[dim];
          if (idx[dim] >= m_sizes[dim]) {
            idx[dim] = 0;
          } else {
            break;
          }
        }

        out << name << "(:, :";
        for (size_t dim = 2; dim < N; ++dim) {
          out << ", " << idx[dim];
        }
        out << ") = \n";

        for (size_t i = 0; i < m_sizes[0]; ++i) {
          for (size_t j = 0; j < m_sizes[1]; ++j) {
            out << ' ' << std::setw(width)
                << m_elements[i + m_sizes[0] * (j + m_sizes[1] * slice_idx)];
          }
          out << '\n';
        }
        out << std::flush;
      }
    }

    out.flags(oldflags);
    out.precision(oldprecision);
  }
};

template <typename CT, size_t N>
TensorRef<Internal::real_type<CT>, N> as_real(const TensorRef<CT, N> &ref) {
  auto shape = ref.shape();
  shape[0] *= 2;
  return TensorRef<Internal::real_type<CT>, N>{
      shape, reinterpret_cast<Internal::real_type<CT> *>(ref.data()),
      2 * ref.size()};
}

template <typename T, size_t N> class Tensor {
private:
  using V = std::vector<T>;

  V m_elements;
  std::array<size_t, N> m_sizes;

public:
  using value_type = typename V::value_type;
  using size_type = typename V::size_type;
  using difference_type = typename V::difference_type;
  using reference = typename V::reference;
  using const_reference = typename V::const_reference;
  using pointer = typename V::pointer;
  using const_pointer = typename V::const_pointer;
  using iterator = typename V::iterator;
  using const_iterator = typename V::const_iterator;
  using reverse_iterator = typename V::reverse_iterator;
  using const_reverse_iterator = typename V::const_reverse_iterator;

  Tensor(std::initializer_list<T> values, std::array<size_t, N> sizes)
      : m_elements(values), m_sizes(sizes) {
    // assert Internal::product<N>(sizes.data()) == m_elements.size()
  }

  Tensor(const T *elements, std::array<size_t, N> sizes)
      : m_elements(elements, elements + Internal::product<N>(sizes.data())),
        m_sizes(sizes) {}

  Tensor(std::vector<T> elements, std::array<size_t, N> sizes)
      : m_elements(std::move(elements)), m_sizes(sizes) {}

  Tensor(std::array<size_t, N> sizes)
      : m_elements(Internal::product<N>(sizes.data())), m_sizes(sizes) {}

  template <typename U>
  Tensor(TensorRef<U, N> other)
      : m_elements(other.begin(), other.end()), m_sizes(other.shape()) {}

  Tensor() : m_elements() { m_sizes.fill(0); }

  template <typename U, size_t M>
  Tensor(StridedTensorRef<U, M> other,
         typename std::enable_if<M == 1 && N == M>::type * = nullptr)
      : m_elements(other.size()), m_sizes(other.shape()) {
    for (size_t i = 0; i < other.size(); ++i)
      m_elements[i] = other[i];
  }

  template <typename U, size_t M>
  Tensor(StridedTensorRef<U, M> other,
         typename std::enable_if<M == 2 && N == M>::type * = nullptr)
      : m_elements(other.size()), m_sizes(other.shape()) {
    for (size_t j = 0; j < other.size(1); ++j)
      for (size_t i = 0; i < other.size(0); ++i)
        (*this)(i, j) = other(i, j);
  }

  template <typename F>
  static Tensor<T, N> generate(F f, std::array<size_t, N> sizes) {
    Tensor result(sizes);
    result.ref().iterate([&f](T &v, auto... indices) { v = f(indices...); });
    return result;
  }

  static Tensor<T, N> kdelta(std::array<size_t, N> index,
                             std::array<size_t, N> sizes, T value = T(1),
                             T backfill = T(0)) {
    Tensor result(sizes);
    std::fill(result.begin(), result.end(), backfill);
    result.m_elements[Internal::linearIdxPtr<N>(sizes.data(), index.data())] =
        value;
    return result;
  }

  void resize_last(size_t new_last_size) {
    size_t block_size = 1;
    for (size_t i = 0; i + 1 < N; ++i)
      block_size *= m_sizes[i];

    m_elements.resize(block_size * new_last_size);
    m_elements.shrink_to_fit();
    m_sizes.back() = new_last_size;
  }

  TensorRef<T, N> ref() {
    return TensorRef<T, N>{m_sizes, m_elements.data(), m_elements.size()};
  }

  TensorRef<const T, N> ref() const {
    return TensorRef<const T, N>{m_sizes, m_elements.data(), m_elements.size()};
  }

  TensorRef<const T, N> cref() const { return ref(); }

  template <typename... UInts>
  typename std::enable_if<N == sizeof...(UInts), reference>::type
  operator()(UInts... indices) noexcept {
    auto linear = Internal::linearIdx(m_sizes.data(), indices...);

    return m_elements[linear];
  }

  template <typename... UInts>
  typename std::enable_if<N == sizeof...(UInts), const_reference>::type
  operator()(UInts... indices) const noexcept {
    auto linear = Internal::linearIdx(m_sizes.data(), indices...);

    return m_elements[linear];
  }

  pointer data() { return m_elements.data(); }
  const_pointer data() const { return m_elements.data(); }
  const_pointer cdata() const { return data(); }

  size_type size(size_t dim) const { return m_sizes[dim]; }
  size_type size() const { return m_elements.size(); }
  size_type max_size() const { return m_elements.max_size(); }
  bool empty() const { return m_elements.empty(); }

  iterator begin() { return m_elements.begin(); }
  iterator end() { return m_elements.end(); }
  const_iterator begin() const { return m_elements.begin(); }
  const_iterator end() const { return m_elements.end(); }
  const_iterator cbegin() const { return m_elements.cbegin(); }
  const_iterator cend() const { return m_elements.cend(); }

  reverse_iterator rbegin() { return m_elements.rbegin(); }
  reverse_iterator rend() { return m_elements.rend(); }
  const_reverse_iterator rbegin() const { return m_elements.rbegin(); }
  const_reverse_iterator rend() const { return m_elements.rend(); }
  const_reverse_iterator crbegin() const { return m_elements.crbegin(); }
  const_reverse_iterator crend() const { return m_elements.crend(); }

  template <typename Fn> void ifor(Fn fn) { ref().ifor(fn); }

  template <typename Fn> void ijfor(Fn fn) { ref().ijfor(fn); }
};

template <typename T> using Vector = Tensor<T, 1>;
template <typename T> using VectorRef = TensorRef<T, 1>;
template <typename T> using StridedVectorRef = StridedTensorRef<T, 1>;

using VectorF = Vector<float>;
using VectorD = Vector<double>;
using VectorRefF = VectorRef<float>;
using VectorRefD = VectorRef<double>;

template <typename T> using Matrix = Tensor<T, 2>;
template <typename T> using MatrixRef = TensorRef<T, 2>;
template <typename T> using StridedMatrixRef = StridedTensorRef<T, 2>;

using MatrixF = Matrix<float>;
using MatrixD = Matrix<double>;
using MatrixRefF = MatrixRef<float>;
using MatrixRefD = MatrixRef<double>;

} // namespace LowDimensional

namespace LD = LowDimensional;
} // namespace TT

#endif // __TTSUITE__LOW_DIMENSIONAL__HPP__
