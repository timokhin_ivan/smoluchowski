#ifndef __CSV__HPP__
#define __CSV__HPP__

#include <ostream>

namespace CSV {

template <typename... T> class Writer {
private:
  std::ostream &m_sink;

  template <typename H, typename... U>
  inline void write_impl(bool first, const H &h, const U &... tl) {
    if (!first)
      m_sink.put(',');
    m_sink << h;

    write_impl(false, tl...);
  }

  inline void write_impl(bool) { m_sink << std::endl; }

public:
  Writer(std::ostream &sink) : m_sink(sink) {}

  void write(const T &... args) { write_impl(true, args...); }
};

} // namespace CSV

#endif // __CSV__HPP__
