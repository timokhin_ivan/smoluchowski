#include "../include/csv.hpp"
#include <boost/test/unit_test.hpp>
#include <sstream>

BOOST_AUTO_TEST_SUITE(csv)

BOOST_AUTO_TEST_CASE(no_fields) {
  std::ostringstream ostr;

  {
    CSV::Writer<> w{ostr};

    w.write();
    w.write();
  }

  BOOST_TEST(ostr.str() == "\n\n");
}

BOOST_AUTO_TEST_CASE(single_field) {
  std::ostringstream ostr;

  {
    CSV::Writer<int> w{ostr};

    w.write(3);
    w.write(4);
  }

  BOOST_TEST(ostr.str() == "3\n4\n");
}

BOOST_AUTO_TEST_CASE(many_fields) {
  std::ostringstream ostr;

  {
    CSV::Writer<int, double, bool> w{ostr};

    w.write(3, 2.17, false);
    w.write(5, 3.14, true);
  }

  BOOST_TEST(ostr.str() == "3,2.17,0\n5,3.14,1\n");
}

BOOST_AUTO_TEST_SUITE_END()
