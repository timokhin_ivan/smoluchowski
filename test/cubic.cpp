#include "../include/cubic.hpp"
#include <boost/test/unit_test.hpp>
#include <numeric>

BOOST_AUTO_TEST_SUITE(cubic, *boost::unit_test::tolerance(1e-14))

double neg_p2(unsigned n) {
  double x = 1.0;
  for (unsigned i = 0; i < n; ++i)
    x /= 2.0;
  return x;
}

BOOST_AUTO_TEST_SUITE(roots)

BOOST_AUTO_TEST_CASE(three_real_roots) {
  auto sol = Poly::solve_cubic(2.0, -12.0, 22.0, -12.0);
  std::array<double, 3> roots = {1.0, 2.0, 3.0};

  BOOST_TEST(std::get<0>(sol) == 3u);
  BOOST_TEST(std::get<1>(sol) == roots, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(one_real_root) {
  auto sol = Poly::solve_cubic(3.0, -6.0, 6.0, -3.0);

  BOOST_TEST(std::get<0>(sol) == 1u);
  BOOST_TEST(std::get<1>(sol)[0] == 1.0);
}

BOOST_AUTO_TEST_CASE(one_tiny_real_root) {
  const double x = 2.91038304567e-11;
  const double a = 1.0;
  const double b = -(1.0 + x);
  const double c = (2.0 + x);
  const double d = -2 * x;
  auto sol = Poly::solve_cubic(a, b, c, d);

  BOOST_TEST(std::get<0>(sol) == 1u);
  BOOST_TEST(std::get<1>(sol)[0] == x);
}

BOOST_AUTO_TEST_CASE(two_tiny_real_roots) {
  const double x1 = neg_p2(38);
  const double x2 = neg_p2(35);
  const double x3 = 1.0;
  const double a = 1.0, b = -(x1 + x2 + x3), c = x3 * (x1 + x2) + x1 * x2,
               d = -x1 * x2 * x3;
  auto sol = Poly::solve_cubic(a, b, c, d);

  BOOST_TEST(std::get<0>(sol) == 3u);
  BOOST_TEST(std::get<1>(sol)[0] == x1);
  BOOST_TEST(std::get<1>(sol)[1] == x2);
  BOOST_TEST(std::get<1>(sol)[2] == x3);
}

BOOST_AUTO_TEST_CASE(one_tiny_real_root_of_three) {
  const std::array<double, 3> x = {neg_p2(35), 1.0, 2.0};
  auto sol = Poly::solve_cubic(1.0, -(x[0] + x[1] + x[2]),
                               x[0] * x[1] + x[0] * x[2] + x[1] * x[2],
                               -x[0] * x[1] * x[2]);

  BOOST_TEST(std::get<0>(sol) == 3u);
  BOOST_TEST(std::get<1>(sol) == x, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(minimse)

BOOST_AUTO_TEST_CASE(irrat) {
  auto sol = Poly::minimise_quartic(1.0, -4.0, 7.0, -4.0, 1.0);

  BOOST_TEST(std::get<0>(sol) == 0.410245487698542);
  BOOST_TEST(std::get<1>(sol) == 0.289273423937778);
}

BOOST_AUTO_TEST_CASE(simple_roots) {
  auto sol = Poly::minimise_quartic(3.0, 8.0, -78.0, 120.0, 1600.0);

  BOOST_TEST(std::get<0>(sol) == -5.0);
  BOOST_TEST(std::get<1>(sol) == -75.0);
}

BOOST_AUTO_TEST_CASE(small) {
  auto sol = Poly::minimise_quartic(1.0, 0.0, 0.0, -neg_p2(30), 0.0);

  BOOST_TEST(std::get<0>(sol) == 0.000615195825143981);
  BOOST_TEST(std::get<1>(sol) == -4.29709319824340e-13);
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE_END()
