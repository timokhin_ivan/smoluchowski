#include "../include/smoluchowski.hpp"
#include <boost/test/unit_test.hpp>
#include <numeric>

BOOST_AUTO_TEST_SUITE(smoluchowski, *boost::unit_test::tolerance(1e-10))

BOOST_AUTO_TEST_SUITE(components)

BOOST_AUTO_TEST_CASE(production) {
  const size_t N = 120;
  auto prod = Smoluchowski::production<double>(N);

  std::array<size_t, 3> idx;
  for (idx[2] = 0; idx[2] < N; ++idx[2])
    for (idx[1] = 0; idx[1] < N; ++idx[1])
      for (idx[0] = 0; idx[0] < N; ++idx[0])
        BOOST_TEST_CONTEXT("i = " << idx[0] << "; j = " << idx[1]
                                  << "; k = " << idx[2]) {
          BOOST_TEST(prod.element_at(idx.cbegin(), idx.cend()) ==
                     ((idx[0] + idx[1] + 1 == idx[2]) ? 1.0 : 0.0));
        }
}

BOOST_AUTO_TEST_CASE(fragmentation_products) {
  const size_t N = 120;
  auto frag = Smoluchowski::fragmentation_products<double>(N);

  BOOST_TEST_REQUIRE(frag.max_rank() == 2);

  std::array<size_t, 3> idx;
  for (idx[2] = 0; idx[2] < N; ++idx[2])
    for (idx[1] = 0; idx[1] < N; ++idx[1])
      for (idx[0] = 0; idx[0] < N; ++idx[0])
        BOOST_TEST_CONTEXT("i = " << idx[0] << "; j = " << idx[1]
                                  << "; k = " << idx[2]) {
          BOOST_TEST(frag.element_at(idx.cbegin(), idx.cend()) ==
                     ((idx[2] == 0) ? idx[0] + idx[1] + 2 : 0.0));
        }
}

BOOST_AUTO_TEST_CASE(consumption) {
  const size_t N = 120;
  auto cons1 = Smoluchowski::consumption<double>(N, true),
       cons2 = Smoluchowski::consumption<double>(N, false);

  std::array<size_t, 3> idx;
  for (idx[2] = 0; idx[2] < N; ++idx[2])
    for (idx[1] = 0; idx[1] < N; ++idx[1])
      for (idx[0] = 0; idx[0] < N; ++idx[0])
        BOOST_TEST_CONTEXT("i = " << idx[0] << "; j = " << idx[1]
                                  << "; k = " << idx[2]) {
          BOOST_TEST(cons1.element_at(idx.cbegin(), idx.cend()) ==
                     ((idx[0] == idx[2]) ? 1.0 : 0.0));
          BOOST_TEST(cons2.element_at(idx.cbegin(), idx.cend()) ==
                     ((idx[1] == idx[2]) ? 1.0 : 0.0));
        }
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_CASE(holistic) {
  const size_t N = size_t{1} << 10;

  auto op1 = Smoluchowski::production<double>(N);
  auto op2 = Smoluchowski::consumption<double>(N, false);
  auto op3 = Smoluchowski::consumption<double>(N, true);
  auto op = (op1 - op2 - op3) / 2;
  op.round(1e-10);

  std::mt19937_64 gen{};
  auto x = TT::TTT<double, 1>::dmrg_cross(
      [](std::array<size_t, 1> idx) { return std::sin(idx[0]); }, {N}, 1e-12,
      100, 200, gen);
  auto y = TT::TTT<double, 1>::dmrg_cross(
      [](std::array<size_t, 1> idx) { return std::cos(idx[0]); }, {N}, 1e-12,
      100, 200, gen);

  BOOST_TEST_REQUIRE(x.max_rank() == 2);
  BOOST_TEST_REQUIRE(y.max_rank() == 2);

  auto u = x.tdot(op);
  auto z = y.tdot(u);

  for (std::array<size_t, 1> i = {0}; i[0] < N; ++i[0])
    BOOST_TEST_CONTEXT("i = " << i[0]) {
      double expected = i[0] * std::sin(i[0] - 1) / 4 -
                        0.5 * std::sin(N / 2.0) / std::sin(0.5) *
                            std::sin((N + 2.0 * i[0] - 1) / 2);

      BOOST_TEST(z.element_at(i.cbegin(), i.cend()) == expected);
    }
}

BOOST_AUTO_TEST_SUITE_END()
