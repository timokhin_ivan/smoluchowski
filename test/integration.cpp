#include "../include/integration.hpp"
#include <boost/test/unit_test.hpp>
#include <random>

BOOST_AUTO_TEST_SUITE(integration, *boost::unit_test::tolerance(2e-7))

BOOST_AUTO_TEST_SUITE(rotation)

BOOST_AUTO_TEST_CASE(rk4) {
  const size_t N = 2;
  TT::LowDimensional::VectorD x0{{N}};
  x0(0) = 0.0;
  x0(1) = 1.0;

  Integration::Explicit::rk4(
      [](double t, TT::LowDimensional::VectorRef<const double> x,
         TT::LowDimensional::VectorRefD y) {
        y(0) = x(1);
        y(1) = -x(0);
      },
      x0.ref(), 0.0, 10.0, 1.0, 1e-12, 1e-12,
      [](TT::LowDimensional::VectorRefD x, double t, double dt) {
        BOOST_TEST(x(0) == std::sin(t));
        BOOST_TEST(x(1) == std::cos(t));
      },
      [](double t, double old_dt, double new_dt) {});
}

BOOST_AUTO_TEST_CASE(norsett) {
  const size_t N = 2;
  TT::LowDimensional::VectorD x0{{N}};
  x0(0) = 0.0;
  x0(1) = 1.0;

  Integration::Implicit::DIRKConfig config;
  config.f = [](double t, TT::LowDimensional::VectorRef<const double> x,
                TT::LowDimensional::VectorRefD y) {
    y(0) = x(1);
    y(1) = -x(0);
  };
  config.jacobi_solver = [](double Icoef, double Jcoef,
                            TT::LowDimensional::VectorRef<const double> u,
                            TT::LowDimensional::VectorRef<const double> rhs,
                            TT::LowDimensional::VectorRefD sol) {
    const double det = Icoef * Icoef + Jcoef * Jcoef;
    sol(0) = (Icoef * rhs(0) - Jcoef * rhs(1)) / det;
    sol(1) = (Jcoef * rhs(0) + Icoef * rhs(1)) / det;
  };
  config.max_newton_iter = 10;
  config.newton_eps = 1e-10;
  config.reps = 1e-12;
  config.aeps = 1e-12;
  config.callback = [](TT::LowDimensional::VectorRefD x, double t, double dt) {
    BOOST_TEST(x(0) == std::sin(t));
    BOOST_TEST(x(1) == std::cos(t));
  };

  Integration::Implicit::norsett(config,

                                 x0.ref(), 0.0, 10.0, 1.0

  );
}

BOOST_AUTO_TEST_CASE(dirk43) {
  const size_t N = 2;
  TT::LowDimensional::VectorD x0{{N}};
  x0(0) = 0.0;
  x0(1) = 1.0;

  Integration::Implicit::DIRKConfig config;
  config.f = [](double t, TT::LowDimensional::VectorRef<const double> x,
                TT::LowDimensional::VectorRefD y) {
    y(0) = x(1);
    y(1) = -x(0);
  };
  config.jacobi_solver = [](double Icoef, double Jcoef,
                            TT::LowDimensional::VectorRef<const double> u,
                            TT::LowDimensional::VectorRef<const double> rhs,
                            TT::LowDimensional::VectorRefD sol) {
    const double det = Icoef * Icoef + Jcoef * Jcoef;
    sol(0) = (Icoef * rhs(0) - Jcoef * rhs(1)) / det;
    sol(1) = (Jcoef * rhs(0) + Icoef * rhs(1)) / det;
  };
  config.max_newton_iter = 10;
  config.newton_eps = 1e-10;
  config.reps = 1e-12;
  config.aeps = 1e-12;
  config.callback = [](TT::LowDimensional::VectorRefD x, double t, double dt) {
    BOOST_TEST(x(0) == std::sin(t));
    BOOST_TEST(x(1) == std::cos(t));
  };

  Integration::Implicit::dirk43(config, x0.ref(), 0.0, 10.0, 1.0);
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(stiff_exponent)

BOOST_AUTO_TEST_CASE(rk4) {
  const size_t N = 2;
  TT::LowDimensional::VectorD x0{{N}};
  x0(0) = 1.0e-3;
  x0(1) = 1.0;

  Integration::Explicit::rk4(
      [](double t, TT::LowDimensional::VectorRef<const double> x,
         TT::LowDimensional::VectorRefD y) {
        y(0) = -500.0 * x(0);
        y(1) = -0.8 * x(1);
      },
      x0.ref(), 0.0, 1.0, 0.25, 1e-12, 1e-12,
      [](TT::LowDimensional::VectorRefD x, double t, double dt) {
        BOOST_TEST(x(1) + x(0) == x(1) + 1.0e-3 * std::exp(-500.0 * t));
        BOOST_TEST(x(1) == std::exp(-0.8 * t));
      },
      [](double t, double old_t, double new_dt) {});
}

BOOST_AUTO_TEST_CASE(norsett) {
  const size_t N = 2;
  TT::LowDimensional::VectorD x0{{N}};
  x0(0) = 1.0e-3;
  x0(1) = 1.0;

  Integration::Implicit::DIRKConfig config;
  config.f = [](double t, TT::LowDimensional::VectorRef<const double> x,
                TT::LowDimensional::VectorRefD y) {
    y(0) = -500.0 * x(0);
    y(1) = -0.8 * x(1);
  };
  config.jacobi_solver = [](double Icoef, double Jcoef,
                            TT::LowDimensional::VectorRef<const double> u,
                            TT::LowDimensional::VectorRef<const double> rhs,
                            TT::LowDimensional::VectorRefD sol) {
    sol(0) = rhs(0) / (Icoef - 500.0 * Jcoef);
    sol(1) = rhs(1) / (Icoef - 0.8 * Jcoef);
  };
  config.max_newton_iter = 10;
  config.newton_eps = 1e-10;
  config.reps = 1e-12;
  config.aeps = 1e-12;
  config.callback = [](TT::LowDimensional::VectorRefD x, double t, double dt) {
    BOOST_TEST(x(1) + x(0) == x(1) + 1.0e-3 * std::exp(-500.0 * t));
    BOOST_TEST(x(1) == std::exp(-0.8 * t));
  };

  Integration::Implicit::norsett(config, x0.ref(), 0.0, 1.0, 0.25);
}

BOOST_AUTO_TEST_CASE(dirk43) {
  const size_t N = 2;
  TT::LowDimensional::VectorD x0{{N}};
  x0(0) = 1.0e-3;
  x0(1) = 1.0;

  Integration::Implicit::DIRKConfig config;
  config.f = [](double t, TT::LowDimensional::VectorRef<const double> x,
                TT::LowDimensional::VectorRefD y) {
    y(0) = -500.0 * x(0);
    y(1) = -0.8 * x(1);
  };
  config.jacobi_solver = [](double Icoef, double Jcoef,
                            TT::LowDimensional::VectorRef<const double> u,
                            TT::LowDimensional::VectorRef<const double> rhs,
                            TT::LowDimensional::VectorRefD sol) {
    sol(0) = rhs(0) / (Icoef - 500.0 * Jcoef);
    sol(1) = rhs(1) / (Icoef - 0.8 * Jcoef);
  };
  config.max_newton_iter = 10;
  config.newton_eps = 1e-10;
  config.reps = 1e-12;
  config.aeps = 1e-12;
  config.callback = [](TT::LowDimensional::VectorRefD x, double t, double dt) {
    BOOST_TEST(x(1) + x(0) == x(1) + 1.0e-3 * std::exp(-500.0 * t));
    BOOST_TEST(x(1) == std::exp(-0.8 * t));
  };

  Integration::Implicit::dirk43(config, x0.ref(), 0.0, 1.0, 0.25);
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE_END()
