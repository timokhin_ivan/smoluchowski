#include "../include/transport.hpp"
#include "include/convolution.hpp"
#include "include/ttsuite/blapack.hpp"
#include "include/ttsuite/log.hpp"
#include "include/ttsuite/low_dimensional.hpp"
#include "include/ttsuite/matrix_cross.hpp"
#include <array>
#include <boost/test/unit_test.hpp>
#include <random>

BOOST_AUTO_TEST_SUITE(transport)

struct LowRankFixture {
  std::mt19937_64 gen;
  TT::LD::MatrixD f_full;
  Direct::Transfer::LowRank::Matrix f_lr;

  static double f_fn(size_t i, size_t j) {
    return std::exp(-std::cbrt(i)) * std::cos(j) +
           1 / std::sqrt(1 + i) * std::sin(j);
  }

  enum { M = 16, N = 12 };

  static constexpr std::array<size_t, 2> shape() noexcept { return {M, N}; }

  LowRankFixture()
      : f_full(TT::LD::MatrixD::generate(f_fn, shape())),
        f_lr(Direct::Transfer::LowRank::Matrix::from_cross(
            TT::MatrixCross<double>::cross(f_fn, M, N, 1e-12, 50, gen))) {}

  virtual ~LowRankFixture() = default;
};

BOOST_AUTO_TEST_SUITE(low_rank, *boost::unit_test::tolerance(1e-12))

struct TransferFixture : virtual public LowRankFixture {
  TT::LD::MatrixD transfer_full;
  TT::LD::VectorD velocity;
  double dt;
  double dx;

  TransferFixture()
      : transfer_full{shape()}, velocity{TT::LD::VectorD::generate(
                                    [](size_t i) {
                                      return 1 / std::sqrt(1 + i);
                                    },
                                    {N})},
        dt(0.25), dx(0.5) {
    std::fill(transfer_full.begin(), transfer_full.end(), 0.0);
    const Direct::Transfer::PML pml = {1.0, 1.0, 1.0, N + 1};

    Direct::Transfer::Impl::transfer(transfer_full.ref(), f_full.cref(),
                                     velocity.cref(), dt, dx, pml);
    TT::BLAS::L1::scal(dt, transfer_full.ref().flatten().strided());
    TT::BLAS::L1::axpy(1.0, f_full.cref().flatten().strided(),
                       transfer_full.ref().flatten().strided());
  }
};

BOOST_FIXTURE_TEST_SUITE(transfer, TransferFixture)

BOOST_AUTO_TEST_CASE(columns) {
  TT::LD::MatrixD transfer_lr{shape()};
  TT::LD::VectorD work;

  for (size_t col = 0; col < N; ++col) {
    f_lr.write_transport_df_col_partial(col, transfer_lr.ref().index_last(col),
                                        velocity.cref(), dt, dx, work);
  }

  for (size_t j = 0; j < N; ++j) {
    for (size_t i = 0; i < M; ++i) {
      BOOST_TEST_CONTEXT("i = " << i << ", j = " << j) {
        BOOST_TEST(transfer_lr(i, j) == transfer_full(i, j));
      }
    }
  }
}

BOOST_AUTO_TEST_CASE(columns_partial) {
  TT::LD::MatrixD transfer_lr{{M / 2, N}};
  TT::LD::VectorD work;

  for (size_t col = 0; col < N; ++col) {
    f_lr.write_transport_df_col_partial(col, transfer_lr.ref().index_last(col),
                                        velocity.cref(), dt, dx, work);
  }

  for (size_t j = 0; j < N; ++j) {
    for (size_t i = 0; i < M / 2; ++i) {
      BOOST_TEST_CONTEXT("i = " << i << ", j = " << j) {
        BOOST_TEST(transfer_lr(i, j) == transfer_full(i, j));
      }
    }
  }
}

BOOST_AUTO_TEST_CASE(rows) {
  TT::LD::MatrixD transfer_lr_t{{N, M}};

  for (size_t row = 0; row < M; ++row) {
    f_lr.write_transport_df_row_partial(
        row, transfer_lr_t.ref().index_last(row), velocity.cref(), dt, dx);
  }

  for (size_t j = 0; j < N; ++j) {
    for (size_t i = 0; i < M; ++i) {
      BOOST_TEST_CONTEXT("i = " << i << ", j = " << j) {
        BOOST_TEST(transfer_lr_t(j, i) == transfer_full(i, j));
      }
    }
  }
}

BOOST_AUTO_TEST_CASE(rows_partial) {
  TT::LD::MatrixD transfer_lr_t{{N / 2, M}};

  for (size_t row = 0; row < M; ++row) {
    f_lr.write_transport_df_row_partial(
        row, transfer_lr_t.ref().index_last(row), velocity.cref(), dt, dx);
  }

  for (size_t j = 0; j < N / 2; ++j) {
    for (size_t i = 0; i < M; ++i) {
      BOOST_TEST_CONTEXT("i = " << i << ", j = " << j) {
        BOOST_TEST(transfer_lr_t(j, i) == transfer_full(i, j));
      }
    }
  }
}

BOOST_AUTO_TEST_SUITE_END()

struct AggregationFixture : virtual public LowRankFixture {
  Direct::Smoluchowski::Operator S;
  TT::LD::MatrixD aggregation_full;

  Direct::Smoluchowski::Operator make_operator() {
    constexpr double a = 0.2;
    auto fn = [](size_t i, size_t j) -> double {
      double k = std::pow(static_cast<double>(i + 1) / (j + 1), a);
      return k + 1 / k;
    };

    auto cross = TT::MatrixCross<double>::cross(fn, M, M, 1e-12, 10, gen);
    return {cross.u(), cross.v()};
  }

  AggregationFixture() : S{make_operator()}, aggregation_full{shape()} {
    Direct::Transfer::Impl::aggregation(aggregation_full.ref(), f_full.cref(),
                                        S);
  }
};

BOOST_FIXTURE_TEST_CASE(aggregation, AggregationFixture) {
  auto aggregation_lr = f_lr.apply(S);
  auto aggregation_lr_materialised = aggregation_lr.materialise();

  for (size_t j = 0; j < N; ++j) {
    for (size_t i = 0; i < M; ++i) {
      BOOST_TEST_CONTEXT("i = " << i << "; j = " << j) {
        BOOST_TEST(aggregation_lr_materialised(i, j) == aggregation_full(i, j));
      }
    }
  }
}

BOOST_AUTO_TEST_CASE(lcom_padded) {
  std::mt19937_64 gen;
  auto fn1 = [](size_t i, size_t j) -> double { return i + j; };
  auto fn2 = [](size_t i, size_t j) -> double {
    return (i + 1) * (j + 1) + i * i * j * j;
  };

  std::array<size_t, 2> shape1 = {10, 5}, shape2 = {6, 12};

  auto m1 = TT::LD::MatrixD::generate(fn1, shape1),
       m2 = TT::LD::MatrixD::generate(fn2, shape2);

  auto lrm1 = Direct::Transfer::LowRank::Matrix::from_cross(
      TT::MatrixCross<double>::cross(fn1, shape1[0], shape1[1], 1e-12, 10,
                                     gen));
  auto lrm2 = Direct::Transfer::LowRank::Matrix::from_cross(
      TT::MatrixCross<double>::cross(fn2, shape2[0], shape2[1], 1e-12, 10,
                                     gen));

  ttlog_debug("rank 1 = %zu, rank 2 = %zu", lrm1.rank(), lrm2.rank());

  std::array<size_t, 2> shape = {std::max(shape1[0], shape2[0]),
                                 std::max(shape1[1], shape2[1])};

  const double a1 = 3.0, a2 = -2.0;

  TT::LD::MatrixD lcom_full{shape};

  m1.cref().iterate([&lcom_full, a1](double v, size_t i, size_t j) mutable {
    lcom_full(i, j) += a1 * v;
  });
  m2.cref().iterate([&lcom_full, a2](double v, size_t i, size_t j) mutable {
    lcom_full(i, j) += a2 * v;
  });

  auto lcom_lr = lrm1.lcom_padded(a1, lrm2, a2).materialise();

  BOOST_TEST_REQUIRE(lcom_lr.size(0) == lcom_full.size(0));
  BOOST_TEST_REQUIRE(lcom_lr.size(1) == lcom_full.size(1));

  lcom_full.cref().iterate([&lcom_lr](double ref, size_t i, size_t j) {
    BOOST_TEST_CONTEXT("i = " << i << ", j = " << j) {
      BOOST_TEST(lcom_lr(i, j) == ref);
    }
  });
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE_END()
