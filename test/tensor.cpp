#include "../include/ttsuite/tensor.hpp"
#include <boost/test/unit_test.hpp>
#include <numeric>

BOOST_AUTO_TEST_SUITE(tensor)

BOOST_AUTO_TEST_CASE(initialisation) {

  std::vector<double> elements(5 * 4 * 3 * 2);
  std::iota(elements.begin(), elements.end(), 0);
  TT::Tensor<double> tensor{elements, {5, 4, 3, 2}};

  BOOST_TEST(tensor.size() == 5 * 4 * 3 * 2);
  BOOST_TEST(tensor.size(0) == 5);
  BOOST_TEST(tensor.size(1) == 4);
  BOOST_TEST(tensor.size(2) == 3);
  BOOST_TEST(tensor.size(3) == 2);
  BOOST_TEST(tensor == elements, boost::test_tools::per_element());

  auto unf = tensor.unfolding(2);
  BOOST_TEST(unf.size() == tensor.size());
  BOOST_TEST(unf.data() == tensor.data());
  BOOST_TEST(unf.size(0) == tensor.size(0) * tensor.size(1));
  BOOST_TEST(unf.size(1) == tensor.size(2) * tensor.size(3));

  std::array<size_t, 4> index = {0, 0, 0, 0};
  size_t val = 0;
  for (index[3] = 0; index[3] < tensor.size(3); ++index[3])
    for (index[2] = 0; index[2] < tensor.size(2); ++index[2])
      for (index[1] = 0; index[1] < tensor.size(1); ++index[1])
        for (index[0] = 0; index[0] < tensor.size(0); ++index[0])
          BOOST_TEST_CONTEXT("i = " << index[0] << "; j = " << index[1]
                                    << "; k = " << index[2]
                                    << "; l = " << index[3]) {
            BOOST_TEST(tensor(index.begin(), index.end()) == val);
            BOOST_TEST(tensor(index.begin(), index.end()) ==
                       unf(index[0] + tensor.size(0) * index[1],
                           index[2] + tensor.size(2) * index[3]));
            ++val;
          }
}

BOOST_AUTO_TEST_SUITE_END() // tensor
