#include "../include/convolution.hpp"
#include "../include/preconditioner.hpp"
#include "../include/ttsuite/gmres.hpp"
#include "../include/ttsuite/matrix_cross.hpp"
#include <boost/test/unit_test.hpp>
#include <iostream>
#include <random>

BOOST_AUTO_TEST_SUITE(convolution, *boost::unit_test::tolerance(1e-10))

BOOST_AUTO_TEST_SUITE(smoluchowski)

BOOST_AUTO_TEST_CASE(simple) {
  const double a = 0.3;
  auto C = [a](size_t i, size_t j) {
    double i_a = std::pow(i + 1, a);
    double j_a = std::pow(j + 1, a);
    return i_a / j_a + j_a / i_a;
  };

  auto gen = std::mt19937_64{};
  const size_t N = 1024;

  auto Cc = TT::MatrixCross<double>::cross(C, N, N, 1e-10, 4, gen);
  auto S = Direct::Smoluchowski::Operator(Cc.u(), Cc.v());

  std::array<size_t, 1> size = {N};
  TT::LowDimensional::VectorD x{size}, y{size}, z{size}, u{size};
  for (size_t i = 0; i < N; ++i) {
    x(i) = std::sin(i);
    y(i) = std::cos(i);
  }

  for (size_t i = 0; i < N; ++i) {
    z(i) = 0.0;
    for (size_t j = 0; j < i; ++j) {
      z(i) += C(i - 1 - j, j) * x(i - j - 1) * y(j);
    }

    for (size_t j = 0; j < N; ++j) {
      z(i) -= C(i, j) * (x(i) * y(j) + y(i) * x(j));
    }

    z(i) /= 2.0;
  }

  auto left = S.partial(x.cref());
  auto right = S.partial(y.cref());
  S.combine(left, right, u.ref());

  BOOST_TEST(u == z, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(fragmentation) {
  const double a = 0.3, lambda = 0.05;
  auto C = [a](size_t i, size_t j) {
    double i_a = std::pow(i + 1, a);
    double j_a = std::pow(j + 1, a);
    return i_a / j_a + j_a / i_a;
  };

  auto gen = std::mt19937_64{};
  const size_t N = 1024;

  auto Cc = TT::MatrixCross<double>::cross(C, N, N, 1e-10, 4, gen);
  auto S = Direct::Smoluchowski::Operator(Cc.u(), Cc.v(), lambda);

  std::array<size_t, 1> size = {N};
  TT::LowDimensional::VectorD x{size}, y{size}, z{size}, u{size};
  for (size_t i = 0; i < N; ++i) {
    x(i) = std::sin(i);
    y(i) = std::cos(i);
  }

  for (size_t i = 0; i < N; ++i) {
    z(i) = 0.0;
    for (size_t j = 0; j < i; ++j) {
      z(i) += C(i - 1 - j, j) * x(i - j - 1) * y(j);
    }

    for (size_t j = 0; j < N; ++j) {
      z(i) -= (1 + lambda) * C(i, j) * (x(i) * y(j) + y(i) * x(j));
    }

    if (i == 0) {
      for (size_t j = 0; j < N; ++j)
        for (size_t k = 0; k < N; ++k) {
          z(i) += lambda * (j + k + 2) * C(j, k) * x(j) * y(k);
        }
    }

    z(i) /= 2.0;
  }

  auto left = S.partial(x.cref());
  auto right = S.partial(y.cref());
  S.combine(left, right, u.ref());

  BOOST_TEST(u == z, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(preconditioned_gmres) {
  const double a = 0.3;
  auto C = [a](size_t i, size_t j) {
    double i_a = std::pow(i + 1, a);
    double j_a = std::pow(j + 1, a);
    return i_a / j_a + j_a / i_a;
  };

  auto gen = std::mt19937_64{};
  const size_t N = 1024;

  auto Cc = TT::MatrixCross<double>::cross(C, N, N, 1e-10, 4, gen);
  auto S = Direct::Smoluchowski::Operator(Cc.u(), Cc.v());

  std::array<size_t, 1> size = {N};

  TT::LowDimensional::VectorD n{size}, x{size}, y{size}, x0{size};

  for (size_t i = 0; i < N; ++i) {
    n(i) = std::pow(i + 1, -0.5);
    x(i) = std::sin(i);
  }

  auto left = S.partial(n.cref());
  {
    auto right = S.partial(x.cref());
    S.combine(left, right, y.ref());
  }

  const double Icoef = 0.34;
  const double Jcoef = -0.72;
  TT::BLAS::L1::scal(Jcoef, y.ref().strided());
  TT::BLAS::L1::axpy(Icoef, x.cref().strided(), y.ref().strided());

  auto prec = Jacobi::Preconditioner(N, Cc.rank(), Cc, 0.0, n.cref(), 128,
                                     Icoef, Jcoef);

  size_t iters = TT::gmres(
      [&left, &S, Icoef, Jcoef](TT::LowDimensional::VectorRef<const double> x,
                                TT::LowDimensional::VectorRefD y) {
        auto right = S.partial(x);
        S.combine(left, right, y);
        TT::BLAS::L1::scal(Jcoef, y.strided());
        TT::BLAS::L1::axpy(Icoef, x.cref().strided(), y.strided());
      },
      [&prec](TT::LowDimensional::VectorRefD x) { prec.apply(x.data()); },
      y.cref(), x0.ref(), 256, 256, 1e-14);

  BOOST_TEST(iters < 50);

  BOOST_TEST(x0 == x, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE_END()
