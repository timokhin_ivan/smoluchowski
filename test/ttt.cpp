#include "../include/ttsuite/ttt.hpp"
#include <boost/test/unit_test.hpp>
#include <cmath>

BOOST_AUTO_TEST_SUITE(ttt)

BOOST_AUTO_TEST_CASE(simple_cross, *boost::unit_test::tolerance(1e-12)) {
  auto fn = [](const std::array<size_t, 2> &idx) -> double {
    return idx[0] + idx[1];
  };
  std::mt19937_64 gen{};
  std::array<size_t, 2> sizes = {2000, 1000};
  auto ttt = TT::TTT<double, 2>::dmrg_cross(fn, sizes, 1e-12, 10, 40, gen);

  std::array<std::uniform_int_distribution<size_t>, 2> dists = {
      {std::uniform_int_distribution<size_t>(0, sizes[0] - 1),
       std::uniform_int_distribution<size_t>(0, sizes[1] - 1)}};

  for (size_t i = 0; i < 10000; ++i) {
    std::array<size_t, 2> idx = {dists[0](gen), dists[1](gen)};

    BOOST_TEST_CONTEXT("i = " << idx[0] << "; j = " << idx[1]) {
      BOOST_TEST(ttt.element_at(idx.begin(), idx.end()) ==
                 static_cast<double>(idx[0] + idx[1]));
    }
  }
}

BOOST_AUTO_TEST_CASE(matmul, *boost::unit_test::tolerance(1e-8)) {
  auto matrix_fn = [](const std::array<size_t, 2> &idx) -> double {
    return idx[0] + idx[1];
  };
  auto vector_fn = [](const std::array<size_t, 1> &idx) -> double {
    return std::sin(idx[0]);
  };
  std::mt19937_64 gen{};
  std::array<size_t, 2> matrix_sizes = {20000, 20000};
  std::array<size_t, 1> vector_size = {matrix_sizes[1]};

  auto matrix = TT::TTT<double, 2>::dmrg_cross(matrix_fn, matrix_sizes, 1e-10,
                                               20, 80, gen);
  auto vector = TT::TTT<double, 1>::dmrg_cross(vector_fn, vector_size, 1e-10,
                                               20, 80, gen);

  std::array<std::uniform_int_distribution<size_t>, 2> dists = {
      {std::uniform_int_distribution<size_t>(0, matrix_sizes[0] - 1),
       std::uniform_int_distribution<size_t>(0, matrix_sizes[1] - 1)}};

  for (size_t i = 0; i < 100000; ++i) {
    std::array<size_t, 2> idx = {dists[0](gen), dists[1](gen)};

    BOOST_TEST_CONTEXT("i = " << idx[0] << "; j = " << idx[1]) {
      BOOST_TEST(matrix.element_at(idx.begin(), idx.end()) ==
                 static_cast<double>(idx[0] + idx[1]));
      BOOST_TEST(vector.element_at(idx.begin() + 1, idx.end()) ==
                 std::sin(idx[1]));
    }
  }

  TT::TTT<double, 1> product = matrix.tdot(vector);
  product.round(1e-10);

  double k1 = std::sin(matrix_sizes[0]), k2 = std::sin(1.0 - matrix_sizes[0]),
         k3 = -0.84147098480789650665, k4 = -1.0876713248350107054;

  for (size_t i = 0; i < 10000; ++i) {
    std::array<size_t, 1> idx = {dists[0](gen)};

    BOOST_TEST_CONTEXT("i = " << idx[0]) {
      BOOST_TEST(product.element_at(idx.begin(), idx.end()) ==
                 ((idx[0] + matrix_sizes[0] - 1) * k1 +
                  (idx[0] + matrix_sizes[0]) * k2 + idx[0] * k3) *
                     k4);
    }
  }
}

BOOST_AUTO_TEST_CASE(extend_zeros, *boost::unit_test::tolerance(1e-12)) {
  auto matrix_fn = [](const std::array<size_t, 2> &idx) -> double {
    return idx[0] + idx[1];
  };

  std::mt19937_64 gen{};
  std::array<size_t, 2> matrix_sizes = {20000, 20000};

  auto matrix = TT::TTT<double, 2>::dmrg_cross(matrix_fn, matrix_sizes, 1e-10,
                                               20, 80, gen);

  auto tensor = matrix.extend_with_zeroes(20000);

  std::uniform_int_distribution<size_t> dist(0, matrix_sizes[0] - 1);

  for (size_t i = 0; i < 100000; ++i) {
    std::array<size_t, 3> idx = {dist(gen), dist(gen), dist(gen)};

    while (idx[2] == 0)
      idx[2] = dist(gen);

    BOOST_TEST_CONTEXT("i = " << idx[0] << "; j = " << idx[1]
                              << "; k = " << idx[2]) {
      BOOST_TEST(tensor.element_at(idx.cbegin(), idx.cend()) == 0.0);

      idx[2] = 0;
      BOOST_TEST(tensor.element_at(idx.cbegin(), idx.cend()) ==
                 static_cast<double>(idx[0] + idx[1]));
    }
  }
}

BOOST_AUTO_TEST_CASE(extend_repeating, *boost::unit_test::tolerance(1e-12)) {
  auto matrix_fn = [](const std::array<size_t, 2> &idx) -> double {
    return idx[0] + idx[1];
  };

  std::mt19937_64 gen{};
  std::array<size_t, 2> matrix_sizes = {20000, 20000};

  auto matrix = TT::TTT<double, 2>::dmrg_cross(matrix_fn, matrix_sizes, 1e-10,
                                               20, 80, gen);

  auto tensor = matrix.extend_repeating(20000);

  std::uniform_int_distribution<size_t> dist(0, matrix_sizes[0] - 1);

  for (size_t i = 0; i < 100000; ++i) {
    std::array<size_t, 3> idx = {dist(gen), dist(gen), dist(gen)};
    BOOST_TEST_CONTEXT("i = " << idx[0] << "; j = " << idx[1]
                              << "; k = " << idx[2]) {
      BOOST_TEST(tensor.element_at(idx.cbegin(), idx.cend()) ==
                 static_cast<double>(idx[0] + idx[1]));
    }
  }
}

BOOST_AUTO_TEST_CASE(identity_matrix, *boost::unit_test::tolerance(1e-8)) {
  std::mt19937_64 gen{};

  std::array<size_t, 1> vec_size = {400000};

  auto x = TT::TTT<double, 1>::dmrg_cross(
      [](const std::array<size_t, 1> &idx) -> double {
        return std::sin(idx[0]);
      },
      vec_size, 1e-10, 40, 80, gen);
  auto id = TT::TTT<double, 2>::identity(vec_size[0]);
  auto y = id.tdot(x);

  std::uniform_int_distribution<size_t> dist(0, vec_size[0] - 1);

  for (size_t i = 0; i < 10000; ++i) {
    std::array<size_t, 1> idx = {dist(gen)};
    BOOST_TEST_CONTEXT("i = " << idx[0]) {
      BOOST_TEST(y.element_at(idx.cbegin(), idx.cend()) == std::sin(idx[0]));
    }
  }
}

BOOST_AUTO_TEST_SUITE(amen)

BOOST_AUTO_TEST_CASE(noop, *boost::unit_test::tolerance(1e-8)) {
  std::mt19937_64 gen{};

  std::array<size_t, 1> vec_size = {400000};

  auto x = TT::TTT<double, 1>::dmrg_cross(
      [](const std::array<size_t, 1> &idx) -> double {
        return std::sin(idx[0]);
      },
      vec_size, 1e-10, 40, 80, gen);
  auto id = TT::TTT<double, 2>::identity(vec_size[0]);
  auto y = x;
  id.amen(y, x, 1e-8, 1e-8, 10, 20, 50, 1e-8);

  std::uniform_int_distribution<size_t> dist(0, vec_size[0] - 1);

  for (size_t i = 0; i < 10000; ++i) {
    std::array<size_t, 1> idx = {dist(gen)};
    BOOST_TEST_CONTEXT("i = " << idx[0]) {
      BOOST_TEST(y.element_at(idx.cbegin(), idx.cend()) == std::sin(idx[0]));
    }
  }
}

BOOST_AUTO_TEST_CASE(half, *boost::unit_test::tolerance(1e-8)) {
  std::mt19937_64 gen{};

  std::array<size_t, 1> vec_size = {400000};

  auto x = TT::TTT<double, 1>::dmrg_cross(
      [](const std::array<size_t, 1> &idx) -> double {
        return std::sin(idx[0]);
      },
      vec_size, 1e-10, 40, 80, gen);
  auto id = TT::TTT<double, 2>::identity(vec_size[0]);
  auto y = x * 2.0;
  id.amen(y, x, 1e-8, 1e-8, 10, 20, 50, 1e-8);

  std::uniform_int_distribution<size_t> dist(0, vec_size[0] - 1);

  for (size_t i = 0; i < 10000; ++i) {
    std::array<size_t, 1> idx = {dist(gen)};
    BOOST_TEST_CONTEXT("i = " << idx[0]) {
      BOOST_TEST(y.element_at(idx.cbegin(), idx.cend()) ==
                 2.0 * std::sin(idx[0]));
    }
  }
}

BOOST_AUTO_TEST_CASE(from_ones, *boost::unit_test::tolerance(1e-8)) {
  std::mt19937_64 gen{};

  std::array<size_t, 1> vec_size = {400000};

  auto x = TT::TTT<double, 1>::dmrg_cross(
      [](const std::array<size_t, 1> &) -> double { return 1.0; }, vec_size,
      1e-10, 40, 80, gen);
  x.round(1e-10);
  auto id = TT::TTT<double, 2>::identity(vec_size[0]);
  auto y = TT::TTT<double, 1>::dmrg_cross(
      [](const std::array<size_t, 1> &idx) -> double {
        return std::sin(idx[0]);
      },
      vec_size, 1e-10, 40, 80, gen);
  y.round(1e-10);
  id.amen(y, x, 1e-8, 1e-8, 10, 20, 50, 1e-8);

  std::uniform_int_distribution<size_t> dist(0, vec_size[0] - 1);

  for (size_t i = 0; i < 10000; ++i) {
    std::array<size_t, 1> idx = {dist(gen)};
    BOOST_TEST_CONTEXT("i = " << idx[0]) {
      BOOST_TEST(y.element_at(idx.cbegin(), idx.cend()) == std::sin(idx[0]));
    }
  }
}

BOOST_AUTO_TEST_CASE(with_full_matrix, *boost::unit_test::tolerance(1e-8)) {
  std::mt19937_64 gen{};

  std::array<size_t, 1> vsize = {10000};
  std::array<size_t, 2> msize = {vsize[0], vsize[0]};

  auto base = TT::TTT<double, 2>::dmrg_cross(
      [vsize](const std::array<size_t, 2> &ix) -> double {
        return std::sin(ix[1]) +
               static_cast<double>(ix[0] * ix[0]) / (vsize[0] * vsize[0]);
      },
      msize, 1e-10, 40, 80, gen);
  auto a = TT::TTT<double, 2>::identity(msize[0]) - 1e-3 * base;
  a.round(1e-14);

  auto x = TT::TTT<double, 1>::dmrg_cross(
      [](const std::array<size_t, 1> &idx) -> double {
        return std::sin(idx[0]) + 2.0 + std::sin(idx[0] / 11.0)
          + std::sin(idx[0] / 17.0);
      },
      vsize, 1e-10, 40, 80, gen);
  x.round(1e-14);
  auto y = a.tdot(x);
  y.round(1e-14);

  auto guess = TT::TTT<double, 1>::dmrg_cross(
      [](const std::array<size_t, 1> &) -> double { return 1.0; }, vsize, 1e-10,
      40, 80, gen);
  a.amen(y, guess, 1e-11, 1e-11, 10, 20, 50, 1e-11);
  guess.round(1e-14);

  for (std::array<size_t, 1> i = {0}; i[0] < vsize[0]; ++i[0])
    BOOST_TEST_CONTEXT("i = " << i[0]) {
      BOOST_TEST(guess.element_at(i.cbegin(), i.cend()) ==
                 x.element_at(i.cbegin(), i.cend()));
    }
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE_END()
