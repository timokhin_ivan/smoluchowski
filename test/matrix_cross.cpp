#include "../include/ttsuite/matrix_cross.hpp"
#include <boost/test/unit_test.hpp>
#include <random>

BOOST_AUTO_TEST_SUITE(matrix_cross)

BOOST_AUTO_TEST_CASE(simple_exact, *boost::unit_test::tolerance(1e-12)) {
  auto gen = std::mt19937_64{};
  size_t n = 20, m = 30;
  auto cross = TT::MatrixCross<double>::cross(
      [](size_t i, size_t j) { return static_cast<double>(i + j); }, n, m,
      1e-13, 4, gen, true);

  BOOST_TEST(cross.rank() == 2);

  for (size_t i = 0; i < n; ++i)
    for (size_t j = 0; j < m; ++j)
      BOOST_TEST_CONTEXT("i = " << i << "; j = " << j) {
        BOOST_TEST(cross.calculate_element(i, j) == static_cast<double>(i + j));
      }
}

// Uncommenting this produces internal error in Boost
// BOOST_AUTO_TEST_CASE(simple_approx, *boost::unit_test::tolerance(1e-3)) {
//   auto gen = std::mt19937_64{};
//   size_t m = 50, n = 40;
//   auto cross = TT::MatrixCross<double>::cross(
//       [](size_t i, size_t j) { return i + j + i * i * j * j * 1e-8; }, m, n,
//       1e-2, 6, gen);

//   BOOST_TEST(cross.rank() == 2);

//   for (size_t i = 0; i < m; ++i)
//     for (size_t j = 0; j < n; ++j)
//       BOOST_TEST_CONTEXT("i = " << i << "; j = " << j) {
//         BOOST_TEST(cross.calculate_element(i, j) == 1.0*i + j);
//       }
// }

struct ExactDetailsFixture {
  TT::LowDimensional::MatrixD full;
  TT::MatrixCross<double> cross;

  ExactDetailsFixture() : full{{30, 40}} {
    for (size_t j = 0; j < full.size(1); ++j)
      for (size_t i = 0; i < full.size(0); ++i) {
        full(i, j) = i + j + 0.1 * i * i * j * j + 0.01 * i * i * i * j * j * j;
      }
    auto gen = std::mt19937_64{};

    cross = TT::MatrixCross<double>::cross(
        [this](size_t i, size_t j) { return full(i, j); }, full.size(0),
        full.size(1), 1e-8, 10, gen);
  }
};

BOOST_FIXTURE_TEST_SUITE(exact_details, ExactDetailsFixture,
                         *boost::unit_test::tolerance(1e-7))

BOOST_AUTO_TEST_CASE(rank) { BOOST_TEST(cross.rank() == 4); }

BOOST_AUTO_TEST_CASE(factors) {
  BOOST_TEST_REQUIRE(cross.u().size(0) == full.size(0));
  BOOST_TEST_REQUIRE(cross.u().size(1) == cross.rank());
  BOOST_TEST_REQUIRE(cross.v().size(0) == full.size(1));
  BOOST_TEST_REQUIRE(cross.v().size(1) == cross.rank());

  TT::LowDimensional::MatrixD reconstructed{{full.size(0), full.size(1)}};
  TT::BLAS::L3::gemm(TT::BLAS::transpose::no, TT::BLAS::transpose::yes, 1.0,
                     cross.u().strided(), cross.v().strided(), 0.0,
                     reconstructed.ref().strided());

  BOOST_TEST(reconstructed == full, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(columns) {
  auto column_indices = cross.column_indices();
  auto columns = cross.columns();
  BOOST_TEST_REQUIRE(column_indices.size() == cross.rank());
  BOOST_TEST_REQUIRE(columns.size(0) == full.size(0));
  BOOST_TEST_REQUIRE(columns.size(1) == cross.rank());

  for (size_t j = 0; j < columns.size(1); ++j)
    for (size_t i = 0; i < columns.size(0); ++i)
      BOOST_TEST_CONTEXT("i = " << i << "; j = " << j
                                << "; column_indices[j] = "
                                << column_indices[j]) {
        BOOST_TEST(columns(i, j) == full(i, column_indices[j]));
      }
}

BOOST_AUTO_TEST_CASE(rows) {
  auto row_indices = cross.row_indices();
  auto rows = cross.rows();
  BOOST_TEST_REQUIRE(row_indices.size() == cross.rank());
  BOOST_TEST_REQUIRE(rows.size(0) == full.size(1));
  BOOST_TEST_REQUIRE(rows.size(1) == cross.rank());

  for (size_t j = 0; j < rows.size(1); ++j)
    for (size_t i = 0; i < rows.size(0); ++i)
      BOOST_TEST_CONTEXT("i = " << i << "; j = " << j
                                << "; row_indices[j] = " << row_indices[j]) {
        BOOST_TEST(rows(i, j) == full(row_indices[j], i));
      }
}

BOOST_AUTO_TEST_CASE(pivot) {
  auto row_indices = cross.row_indices();
  auto column_indices = cross.column_indices();
  auto pivot_matrix = cross.pivot();

  BOOST_TEST_REQUIRE(column_indices.size() == cross.rank());
  BOOST_TEST_REQUIRE(row_indices.size() == cross.rank());
  BOOST_TEST_REQUIRE(pivot_matrix.size(0) == cross.rank());
  BOOST_TEST_REQUIRE(pivot_matrix.size(1) == cross.rank());

  for (size_t j = 0; j < pivot_matrix.size(0); ++j)
    for (size_t i = 0; i < pivot_matrix.size(1); ++i)
      BOOST_TEST_CONTEXT("i = " << i << "; j = " << j << "; row_indices[i] = "
                                << row_indices[i] << "; column_indices[j] = "
                                << column_indices[j]) {
        BOOST_TEST(pivot_matrix(i, j) ==
                   full(row_indices[i], column_indices[j]));
      }
}

BOOST_AUTO_TEST_CASE(pivot_factorisation) {
  auto l = cross.pivot_l();
  auto u = cross.pivot_u();
  auto pivot = cross.pivot();
  const auto rank = cross.rank();

  BOOST_TEST_REQUIRE(l.size(0) == rank);
  BOOST_TEST_REQUIRE(l.size(1) == rank);
  BOOST_TEST_REQUIRE(u.size(0) == rank);
  BOOST_TEST_REQUIRE(u.size(1) == rank);
  BOOST_TEST_REQUIRE(pivot.size(0) == rank);
  BOOST_TEST_REQUIRE(pivot.size(1) == rank);

  TT::LowDimensional::MatrixD reconstructed{{rank, rank}};
  for (size_t j = 0; j < rank; ++j)
    for (size_t i = 0; i < rank; ++i) {
      reconstructed(i, j) = i >= j ? l(i, j) : 0.0;
    }

  TT::BLAS::L3::trmm(TT::BLAS::side::right, TT::BLAS::uplo::upper,
                     TT::BLAS::transpose::no, TT::BLAS::diag::nonunit, 1.0, u,
                     reconstructed.ref().strided());

  BOOST_TEST(reconstructed == pivot, boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(pivot_l_rows) {
  auto l = cross.pivot_l();
  const auto rank = cross.rank();
  TT::LowDimensional::MatrixD rows{cross.rows()};

  BOOST_TEST_REQUIRE(l.size(0) == rank);
  BOOST_TEST_REQUIRE(l.size(1) == rank);
  BOOST_TEST_REQUIRE(rows.size(0) == full.size(1));
  BOOST_TEST_REQUIRE(rows.size(1) == rank);

  TT::BLAS::L3::trsm(TT::BLAS::side::right, TT::BLAS::uplo::lower,
                     TT::BLAS::transpose::yes, TT::BLAS::diag::nonunit, 1.0, l,
                     rows.ref().strided());

  BOOST_TEST(rows == cross.v(), boost::test_tools::per_element());
}

BOOST_AUTO_TEST_CASE(pivot_u_columns, *boost::unit_test::tolerance(2e-4)) {
  auto u = cross.pivot_u();
  const auto rank = cross.rank();
  TT::LowDimensional::MatrixD columns{cross.columns()};

  BOOST_TEST_REQUIRE(u.size(0) == rank);
  BOOST_TEST_REQUIRE(u.size(1) == rank);
  BOOST_TEST_REQUIRE(columns.size(0) == full.size(0));
  BOOST_TEST_REQUIRE(columns.size(1) == rank);

  TT::BLAS::L3::trsm(TT::BLAS::side::right, TT::BLAS::uplo::upper,
                     TT::BLAS::transpose::no, TT::BLAS::diag::nonunit, 1.0, u,
                     columns.ref().strided());

  BOOST_TEST(columns == cross.u(), boost::test_tools::per_element());
}

BOOST_AUTO_TEST_SUITE_END() // exact_details

BOOST_AUTO_TEST_CASE(tiny) {
  TT::LowDimensional::MatrixD full{{3, 3}};
  for (size_t j = 0; j < full.size(1); ++j)
    for (size_t i = 0; i < full.size(0); ++i) {
      full(i, j) = i + j + i*i * j*j;
    }

  auto gen = std::mt19937_64{};
  auto cross = TT::MatrixCross<double>::cross(
      [&full](size_t i, size_t j) { return full(i, j); }, full.size(0),
      full.size(1), 1e-10, 3, gen);

  BOOST_TEST(cross.rank() == full.size(0));
}

BOOST_AUTO_TEST_SUITE_END() // matrix_cross
