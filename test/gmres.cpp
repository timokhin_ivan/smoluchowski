#include "../include/ttsuite/gmres.hpp"
#include <boost/test/unit_test.hpp>
#include <numeric>
#include <cmath>

BOOST_AUTO_TEST_SUITE(gmres, *boost::unit_test::tolerance(1e-9))

BOOST_AUTO_TEST_CASE(small) {
  using namespace TT;
  const size_t N = 50;
  LowDimensional::Vector<double> b{{N}}, x{{N}};

  for (size_t i = 0; i < N; ++i) {
    b(i) = i + 1;
    x(i) = 0;
  }

  auto iter = TT::gmres(
      [](LowDimensional::VectorRef<const double> x,
         LowDimensional::VectorRef<double> y) {
        for (size_t i = 0; i < x.size(); ++i)
          y[i] = x[i] * (std::sin(i) + 5);
      },
      [](LowDimensional::VectorRef<double> x) {}, b.cref(), x.ref(), 50, 50,
      1e-12);

  BOOST_TEST(iter < 50);

  for (size_t i = 0; i < x.size(); ++i)
    BOOST_TEST_CONTEXT("i = " << i) {
      BOOST_TEST(x(i) * (std::sin(i) + 5) == b(i));
    }
}

BOOST_AUTO_TEST_CASE(clustered) {
  using namespace TT;
  using namespace TT::LowDimensional;
  const size_t N = 200, K = 5;

  Vector<double> b{{K * N}}, x{{K * N}};

  for (size_t i = 0; i < b.size(); ++i) {
    b(i) = std::sin(i);
    x(i) = 0;
  }

  auto iter = TT::gmres(
      [N, K](VectorRef<const double> x, VectorRef<double> y) {
        for (size_t i = 0; i < N; ++i)
          for (size_t cluster = 0; cluster < K; ++cluster) {
            y[cluster + i * K] = (cluster + 1) * x[cluster + i * K];
          }
      },
      [](VectorRef<double> x) {}, b.cref(), x.ref(), 10 * K, 10 * K, 1e-12);

  BOOST_TEST(iter <= K);

  for (size_t i = 0; i < N; ++i)
    for (size_t cluster = 0; cluster < K; ++cluster)
      BOOST_TEST_CONTEXT("i = " << i << "; cluster = " << cluster) {
        BOOST_TEST(x(cluster + i * K) * (cluster + 1) == b(cluster + i * K));
      }
}

BOOST_AUTO_TEST_CASE(quasi_laplace) {
  using namespace TT;
  using namespace TT::LowDimensional;
  const size_t N = 1000;

  Vector<double> b{{N}}, x{{N}};

  for (size_t i = 0; i < b.size(); ++i) {
    b(i) = std::sin(i);
    x(i) = 0;
  }

  auto iter = TT::gmres(
      [N](VectorRef<const double> x, VectorRef<double> y) {
        y[0] = -3 * x[0] + x[1];
        for (size_t i = 1; i < N - 1; ++i) {
          y[i] = x[i - 1] - 3 * x[i] + x[i + 1];
        }
        y[N - 1] = -3 * x[N - 1] + x[N - 2];
      },
      [](VectorRef<double> x) {}, b.cref(), x.ref(), 100, 100, 1e-12);

  BOOST_TEST(iter <= 100);

  BOOST_TEST_CONTEXT("i = 0") { BOOST_TEST(-3 * x(0) + x(1) == b(0)); }

  for (size_t i = 0; i < N - 1; ++i) {
    BOOST_TEST_CONTEXT("i = " << i) {
      BOOST_TEST(x(i - 1) - 3 * x(i) + x(i + 1) == b(i));
    }
  }

  BOOST_TEST_CONTEXT("i = " << N - 1) {
    BOOST_TEST(-3 * x(N - 1) + x(N - 2) == b(N - 1));
  }
}

BOOST_AUTO_TEST_CASE(preconditioned) {
  using namespace TT;
  const size_t N = 50;
  LowDimensional::Vector<double> b{{N}}, x{{N}};

  for (size_t i = 0; i < N; ++i) {
    b(i) = i + 1;
    x(i) = 0;
  }

  const size_t M = N / 2;

  auto iter = TT::gmres(
      [](LowDimensional::VectorRef<const double> x,
         LowDimensional::VectorRef<double> y) {
        for (size_t i = 0; i < x.size(); ++i)
          y[i] = x[i] * (i + 0.005);
      },
      [M](LowDimensional::VectorRef<double> x) {
        for (size_t i = 0; i < x.size(); ++i) {
          if (i < M) {
            x[i] /= (i + 0.005);
          } else {
            x[i] /= (M + 0.005 + 0.75 * (i - M));
          }
        }
      },
      b.cref(), x.ref(), 50, 50, 1e-12);

  BOOST_TEST(iter < 10);

  for (size_t i = 0; i < x.size(); ++i)
    BOOST_TEST_CONTEXT("i = " << i) { BOOST_TEST(x(i) * (i + 0.005) == b(i)); }
}

BOOST_AUTO_TEST_SUITE_END()
