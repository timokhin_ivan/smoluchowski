#include "../include/ttsuite/low_dimensional.hpp"
#include <boost/test/unit_test.hpp>
#include <cmath>
#include <numeric>

BOOST_AUTO_TEST_SUITE(low_dimensional)

BOOST_AUTO_TEST_SUITE(construction)

BOOST_AUTO_TEST_SUITE(tensor)

BOOST_AUTO_TEST_CASE(initializer_list) {
  auto tensor =
      TT::LD::Tensor<int, 3>{{0,  1,  2,  3,  4,  5,  6,  7,  8,  9,  10, 11,
                              12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23},
                             {2, 3, 4}};

  BOOST_TEST(tensor.size(0) == 2);
  BOOST_TEST(tensor.size(1) == 3);
  BOOST_TEST(tensor.size(2) == 4);
  BOOST_TEST(tensor.size() == 24);
  BOOST_TEST(!tensor.empty());
  BOOST_TEST(tensor.data() != nullptr);
  BOOST_TEST(tensor.data() == tensor.cdata());

  std::vector<int> v(tensor.size());
  std::iota(v.begin(), v.end(), 0);

  BOOST_TEST(tensor.max_size() == v.max_size());
  BOOST_TEST(tensor == v, boost::test_tools::per_element());

  for (size_t k = 0; k < tensor.size(2); ++k)
    for (size_t j = 0; j < tensor.size(1); ++j)
      for (size_t i = 0; i < tensor.size(0); ++i)
        BOOST_TEST_CONTEXT("i = " << i << "; j = " << j << "; k = " << k) {
          BOOST_TEST(tensor.cref()(i, j, k) ==
                     v.at(i + j * tensor.size(0) +
                          k * tensor.size(0) * tensor.size(1)));
        }
}

BOOST_AUTO_TEST_CASE(vector) {
  std::vector<int> v(24);
  std::iota(v.begin(), v.end(), 0);

  auto tensor = TT::LD::Tensor<int, 3>{v, {2, 3, 4}};

  BOOST_TEST(tensor.size(0) == 2);
  BOOST_TEST(tensor.size(1) == 3);
  BOOST_TEST(tensor.size(2) == 4);
  BOOST_TEST(tensor.size() == 24);
  BOOST_TEST(!tensor.empty());
  BOOST_TEST(tensor.data() != nullptr);
  BOOST_TEST(tensor.data() == tensor.cdata());

  BOOST_TEST(tensor.max_size() == v.max_size());
  BOOST_TEST(tensor == v, boost::test_tools::per_element());

  for (size_t k = 0; k < tensor.size(2); ++k)
    for (size_t j = 0; j < tensor.size(1); ++j)
      for (size_t i = 0; i < tensor.size(0); ++i)
        BOOST_TEST_CONTEXT("i = " << i << "; j = " << j << "; k = " << k) {
          BOOST_TEST(tensor.cref()(i, j, k) ==
                     v.at(i + j * tensor.size(0) +
                          k * tensor.size(0) * tensor.size(1)));
        }
}

BOOST_AUTO_TEST_CASE(pointer) {
  std::vector<int> v(24);
  std::iota(v.begin(), v.end(), 0);

  auto tensor = TT::LD::Tensor<int, 3>{v.data(), {2, 3, 4}};

  BOOST_TEST(tensor.size(0) == 2);
  BOOST_TEST(tensor.size(1) == 3);
  BOOST_TEST(tensor.size(2) == 4);
  BOOST_TEST(tensor.size() == 24);
  BOOST_TEST(!tensor.empty());
  BOOST_TEST(tensor.data() != nullptr);
  BOOST_TEST(tensor.data() == tensor.cdata());

  BOOST_TEST(tensor.max_size() == v.max_size());
  BOOST_TEST(tensor == v, boost::test_tools::per_element());

  for (size_t k = 0; k < tensor.size(2); ++k)
    for (size_t j = 0; j < tensor.size(1); ++j)
      for (size_t i = 0; i < tensor.size(0); ++i)
        BOOST_TEST_CONTEXT("i = " << i << "; j = " << j << "; k = " << k) {
          BOOST_TEST(tensor.cref()(i, j, k) ==
                     v.at(i + j * tensor.size(0) +
                          k * tensor.size(0) * tensor.size(1)));
        }
}

BOOST_AUTO_TEST_CASE(delta) {
  auto tensor = TT::LD::Tensor<int, 3>::kdelta({1, 2, 0}, {3, 5, 2}, 5, -2);
  for (size_t k = 0; k < tensor.size(2); ++k)
    for (size_t j = 0; j < tensor.size(1); ++j)
      for (size_t i = 0; i < tensor.size(0); ++i)
        BOOST_TEST_CONTEXT("i = " << i << "; j = " << j << "; k = " << k) {
          BOOST_TEST(tensor(i, j, k) == ((i == 1 && j == 2 && k == 0) ? 5 : -2));
        }
}

BOOST_AUTO_TEST_SUITE_END() // tensor

BOOST_AUTO_TEST_SUITE_END() // construction

BOOST_AUTO_TEST_SUITE(subtensors)

BOOST_AUTO_TEST_CASE(block) {
  std::vector<int> v(4 * 7 * 6);
  std::iota(v.begin(), v.end(), 0);

  auto tensor = TT::LD::Tensor<int, 3>(v, {4, 7, 6});
  auto subtensor = tensor.cref().strided().subslice({1, 2, 3}, {2, 3, 2});

  BOOST_TEST(subtensor.size() == 2 * 3 * 2);
  BOOST_TEST(subtensor.size(0) == 2);
  BOOST_TEST(subtensor.size(1) == 3);
  BOOST_TEST(subtensor.size(2) == 2);
  BOOST_TEST(subtensor.data() == tensor.data() + 1 + 2 * 4 + 3 * 4 * 7);
  BOOST_TEST(subtensor.multiplier(0) == 1);
  BOOST_TEST(subtensor.multiplier(1) == 4);
  BOOST_TEST(subtensor.multiplier(2) == 4 * 7);

  for (size_t k = 0; k < subtensor.size(2); ++k)
    for (size_t j = 0; j < subtensor.size(1); ++j)
      for (size_t i = 0; i < subtensor.size(0); ++i)
        BOOST_TEST_CONTEXT("i = " << i << "; j = " << j << "; k = " << k) {
          BOOST_TEST(tensor.cref()(i + 1, j + 2, k + 3) == subtensor(i, j, k));
        }
}

BOOST_AUTO_TEST_CASE(strided) {
  std::vector<int> v(10 * 10 * 10 * 10);
  std::iota(v.begin(), v.end(), 0);

  auto tensor = TT::LD::Tensor<int, 4>(std::move(v), {10, 10, 10, 10});
  auto subtensor = tensor.cref().strided().strided({2, 3, 4, 7});

  BOOST_TEST(subtensor.size(0) == 5);
  BOOST_TEST(subtensor.size(1) == 4);
  BOOST_TEST(subtensor.size(2) == 3);
  BOOST_TEST(subtensor.size(3) == 2);
  BOOST_TEST(subtensor.size() == 5 * 4 * 3 * 2);

  for (size_t l = 0; l < subtensor.size(3); ++l)
    for (size_t k = 0; k < subtensor.size(2); ++k)
      for (size_t j = 0; j < subtensor.size(1); ++j)
        for (size_t i = 0; i < subtensor.size(0); ++i)
          BOOST_TEST_CONTEXT("i = " << i << "; j = " << j << "; k = " << k
                                    << "; l = " << l) {
            BOOST_TEST(tensor.cref()(i * 2, j * 3, k * 4, l * 7) ==
                       subtensor(i, j, k, l));
          }
}

BOOST_AUTO_TEST_CASE(combined) {
  std::vector<int> v(30 * 30 * 30);
  std::iota(v.begin(), v.end(), 0);

  auto tensor = TT::LD::Tensor<int, 3>(std::move(v), {30, 30, 30});
  auto subtensor = tensor.cref()
                       .strided()
                       .subslice({0, 1, 2}, {30, 29, 28})
                       .strided({4, 3, 2})
                       .subslice({1, 3, 5}, {7, 7, 9})
                       .strided({2, 3, 4});

  BOOST_TEST(subtensor.size(0) == 4);
  BOOST_TEST(subtensor.size(1) == 3);
  BOOST_TEST(subtensor.size(2) == 3);

  for (size_t k = 0; k < subtensor.size(2); ++k)
    for (size_t j = 0; j < subtensor.size(1); ++j)
      for (size_t i = 0; i < subtensor.size(0); ++i)
        BOOST_TEST_CONTEXT("i = " << i << "; j = " << j << "; k = " << k) {
          BOOST_TEST(tensor.cref()((2 * i + 1) * 4, (3 * j + 3) * 3 + 1,
                                   (4 * k + 5) * 2 + 2) == subtensor(i, j, k));
        }
}

BOOST_AUTO_TEST_SUITE_END() // subtensors

BOOST_AUTO_TEST_SUITE(loops)

BOOST_AUTO_TEST_CASE(ifor) {
  auto v = TT::LD::VectorD{{5}};

  v.ifor([](size_t i, double &x) { x = i + 1; });

  for (size_t i = 0; i < v.size(0); ++i)
    BOOST_TEST_CONTEXT("i = " << i) { BOOST_TEST(v(i) == i + 1); }
}

BOOST_AUTO_TEST_CASE(ijfor) {
  auto m = TT::LD::MatrixD{{10, 6}};

  m.ijfor([](size_t i, size_t j, double &x) {
    x = std::pow(2.0, i) * std::pow(3.0, j);
  });

  for (size_t j = 0; j < m.size(1); ++j)
    for (size_t i = 0; i < m.size(0); ++i)
      BOOST_TEST_CONTEXT("i = " << i << "; j = " << j) {
        BOOST_TEST(m(i, j) == std::pow(2.0, i) * std::pow(3.0, j));
      }
}

BOOST_AUTO_TEST_CASE(strided_ifor) {
  auto v = TT::LD::VectorD{{5}};

  v.ref().strided().ifor([](size_t i, double &x) { x = i + 1; });

  for (size_t i = 0; i < v.size(0); ++i)
    BOOST_TEST_CONTEXT("i = " << i) { BOOST_TEST(v(i) == i + 1); }
}

BOOST_AUTO_TEST_CASE(strided_ijfor) {
  auto m = TT::LD::MatrixD{{10, 6}};

  m.ref().strided().ijfor([](size_t i, size_t j, double &x) {
    x = std::pow(2.0, i) * std::pow(3.0, j);
  });

  for (size_t j = 0; j < m.size(1); ++j)
    for (size_t i = 0; i < m.size(0); ++i)
      BOOST_TEST_CONTEXT("i = " << i << "; j = " << j) {
        BOOST_TEST(m(i, j) == std::pow(2.0, i) * std::pow(3.0, j));
      }
}

BOOST_AUTO_TEST_CASE(iterate) {
  auto m = TT::LD::Tensor<double, 3>{{3, 4, 7}};

  m.ref().iterate([](double &x, size_t i, size_t j, size_t k) {
    x = std::pow(2.0, i) * std::pow(3.0, j) * std::pow(5.0, k);
  });

  for (size_t k = 0; k < m.size(2); ++k)
    for (size_t j = 0; j < m.size(1); ++j)
      for (size_t i = 0; i < m.size(0); ++i)
        BOOST_TEST_CONTEXT("i = " << i << "; j = " << j << "; k = " << k) {
          BOOST_TEST(m(i, j, k) ==
                     std::pow(2.0, i) * std::pow(3.0, j) * std::pow(5.0, k));
        }
}

BOOST_AUTO_TEST_CASE(generate) {
  auto m = TT::LD::Tensor<double, 3>::generate(
      [](size_t i, size_t j, size_t k) -> double {
        return std::pow(2.0, i) * std::pow(3.0, j) * std::pow(5.0, k);
      },
      {3, 4, 7});

  for (size_t k = 0; k < m.size(2); ++k)
    for (size_t j = 0; j < m.size(1); ++j)
      for (size_t i = 0; i < m.size(0); ++i)
        BOOST_TEST_CONTEXT("i = " << i << "; j = " << j << "; k = " << k) {
          BOOST_TEST(m(i, j, k) ==
                     std::pow(2.0, i) * std::pow(3.0, j) * std::pow(5.0, k));
        }
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE_END() // low_dimensional
