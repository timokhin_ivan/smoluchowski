#include "../include/ttsuite/tensor_train.hpp"
#include <boost/test/unit_test.hpp>
#include <cmath>

BOOST_AUTO_TEST_SUITE(tensor_train)

BOOST_AUTO_TEST_SUITE(construction)

BOOST_AUTO_TEST_CASE(full, *boost::unit_test::tolerance(1e-12)) {
  TT::Tensor<double> full{{10, 10, 10, 10}};
  std::array<size_t, 4> idx = {0, 0, 0, 0};
  for (idx[3] = 0; idx[3] < 10; ++idx[3])
    for (idx[2] = 0; idx[2] < 10; ++idx[2])
      for (idx[1] = 0; idx[1] < 10; ++idx[1])
        for (idx[0] = 0; idx[0] < 10; ++idx[0]) {
          full(idx.begin(), idx.end()) =
              std::sin(idx[0] + idx[1] + idx[2] + idx[3]);
        }

  auto tt = TT::TensorTrain<double>::ttsvd(full, 1e-5, 10);

  BOOST_TEST(tt.rank(0) == 2);
  BOOST_TEST(tt.rank(1) == 2);
  BOOST_TEST(tt.rank(2) == 2);
  BOOST_TEST(tt.rank(3) == 1);

  for (idx[3] = 0; idx[3] < 10; ++idx[3])
    for (idx[2] = 0; idx[2] < 10; ++idx[2])
      for (idx[1] = 0; idx[1] < 10; ++idx[1])
        for (idx[0] = 0; idx[0] < 10; ++idx[0])
          BOOST_TEST_CONTEXT("i = " << idx[0] << "; j = " << idx[1] << "; k = "
                                    << idx[2] << "; l = " << idx[3]) {
            BOOST_TEST(full(idx.begin(), idx.end()) ==
                       tt.element_at(idx.begin(), idx.end()));
          }
}

BOOST_AUTO_TEST_CASE(full_approx, *boost::unit_test::tolerance(1e-2)) {
  TT::Tensor<double> full{{10, 10, 10, 10}};
  std::array<size_t, 4> idx = {0, 0, 0, 0};
  for (idx[3] = 0; idx[3] < 10; ++idx[3])
    for (idx[2] = 0; idx[2] < 10; ++idx[2])
      for (idx[1] = 0; idx[1] < 10; ++idx[1])
        for (idx[0] = 0; idx[0] < 10; ++idx[0]) {
          full(idx.begin(), idx.end()) =
              std::sin(idx[0] + idx[1] + idx[2] + idx[3]) +
              1e-6 * (idx[0] + idx[1] + idx[2] + idx[3]);
        }

  auto tt = TT::TensorTrain<double>::ttsvd(full, 1e-7, 10);

  BOOST_TEST(tt.rank(0) == 4);
  BOOST_TEST(tt.rank(1) == 4);
  BOOST_TEST(tt.rank(2) == 4);

  tt = TT::TensorTrain<double>::ttsvd(full, 1e-2, 10);
  BOOST_TEST(tt.rank(0) == 2);
  BOOST_TEST(tt.rank(1) == 2);
  BOOST_TEST(tt.rank(2) == 2);

  for (idx[3] = 0; idx[3] < 10; ++idx[3])
    for (idx[2] = 0; idx[2] < 10; ++idx[2])
      for (idx[1] = 0; idx[1] < 10; ++idx[1])
        for (idx[0] = 0; idx[0] < 10; ++idx[0])
          BOOST_TEST_CONTEXT("i = " << idx[0] << "; j = " << idx[1] << "; k = "
                                    << idx[2] << "; l = " << idx[3]) {
            BOOST_TEST(full(idx.begin(), idx.end()) ==
                       tt.element_at(idx.begin(), idx.end()));
          }
}

BOOST_AUTO_TEST_CASE(noisy_cross, *boost::unit_test::tolerance(1e-4)) {
  std::mt19937_64 gen{};
  std::uniform_real_distribution<double> dist{-1e-10, 1e-10};

  TT::Tensor<double> full{{10, 10, 10, 10}};
  std::array<size_t, 4> idx = {0, 0, 0, 0};
  for (idx[3] = 0; idx[3] < 10; ++idx[3])
    for (idx[2] = 0; idx[2] < 10; ++idx[2])
      for (idx[1] = 0; idx[1] < 10; ++idx[1])
        for (idx[0] = 0; idx[0] < 10; ++idx[0]) {
          full(idx.begin(), idx.end()) =
              0 + std::sin(idx[0] + idx[1] + idx[2] + idx[3]) + dist(gen);
        }

  TT::LowDimensional::Vector<size_t> sizes{{4}};
  sizes(0) = sizes(1) = sizes(2) = sizes(3) = 10;
  auto tt = TT::TensorTrain<double>::dmrg_cross(
      [&full](std::vector<size_t>::const_iterator begin,
              std::vector<size_t>::const_iterator end) {
        BOOST_TEST_REQUIRE((end - begin) == 4);
        return full(begin, end);
      },
      sizes.cref(), 1e-12, 80, 600, gen);

  // BOOST_TEST(tt.rank(0) == 2);
  // BOOST_TEST(tt.rank(1) == 2);
  // BOOST_TEST(tt.rank(2) == 2);
  // BOOST_TEST(tt.rank(3) == 1);

  for (idx[3] = 0; idx[3] < 10; ++idx[3])
    for (idx[2] = 0; idx[2] < 10; ++idx[2])
      for (idx[1] = 0; idx[1] < 10; ++idx[1])
        for (idx[0] = 0; idx[0] < 10; ++idx[0])
          BOOST_TEST_CONTEXT("i = " << idx[0] << "; j = " << idx[1] << "; k = "
                                    << idx[2] << "; l = " << idx[3]) {
            BOOST_TEST(full(idx.begin(), idx.end()) ==
                       tt.element_at(idx.begin(), idx.end()));
          }
}

BOOST_AUTO_TEST_CASE(cross, *boost::unit_test::tolerance(1e-12)) {
  TT::Tensor<double> full{{10, 10, 10, 10}};
  std::array<size_t, 4> idx = {0, 0, 0, 0};
  for (idx[3] = 0; idx[3] < 10; ++idx[3])
    for (idx[2] = 0; idx[2] < 10; ++idx[2])
      for (idx[1] = 0; idx[1] < 10; ++idx[1])
        for (idx[0] = 0; idx[0] < 10; ++idx[0]) {
          full(idx.begin(), idx.end()) =
              std::sin(idx[0] + idx[1] + idx[2] + idx[3]);
        }

  std::mt19937_64 gen{};

  TT::LowDimensional::Vector<size_t> sizes{{4}};
  sizes(0) = sizes(1) = sizes(2) = sizes(3) = 10;
  auto tt = TT::TensorTrain<double>::dmrg_cross(
      [&full](std::vector<size_t>::const_iterator begin,
              std::vector<size_t>::const_iterator end) {
        BOOST_TEST_REQUIRE((end - begin) == 4);
        return full(begin, end);
      },
      sizes.cref(), 1e-8, 4, 10, gen);

  BOOST_TEST(tt.rank(0) == 2);
  BOOST_TEST(tt.rank(1) == 2);
  BOOST_TEST(tt.rank(2) == 2);
  BOOST_TEST(tt.rank(3) == 1);

  for (idx[3] = 0; idx[3] < 10; ++idx[3])
    for (idx[2] = 0; idx[2] < 10; ++idx[2])
      for (idx[1] = 0; idx[1] < 10; ++idx[1])
        for (idx[0] = 0; idx[0] < 10; ++idx[0])
          BOOST_TEST_CONTEXT("i = " << idx[0] << "; j = " << idx[1] << "; k = "
                                    << idx[2] << "; l = " << idx[3]) {
            BOOST_TEST(full(idx.begin(), idx.end()) ==
                       tt.element_at(idx.begin(), idx.end()));
          }
}

BOOST_AUTO_TEST_CASE(long_cross, *boost::unit_test::tolerance(1e-12)) {
  std::mt19937_64 gen{};

  auto fn = [](std::vector<size_t>::const_iterator begin,
               std::vector<size_t>::const_iterator end) -> double {
    return std::accumulate(begin, end, size_t(0), std::plus<size_t>{});
  };

  TT::LowDimensional::Vector<size_t> sizes{{50}};
  std::fill(sizes.begin(), sizes.end(), 2);

  auto tt =
      TT::TensorTrain<double>::dmrg_cross(fn, sizes.cref(), 1e-8, 4, 50, gen);

  for (size_t i = 0; i < sizes.size() - 1; ++i)
    BOOST_TEST_CONTEXT("rank #" << i) { BOOST_TEST(tt.rank(i) == 2); }

  std::uniform_int_distribution<size_t> ix_dist{0, sizes(0) - 1};
  for (size_t i = 0; i < 15000; ++i) {
    std::vector<size_t> ix(sizes.size());
    std::generate(ix.begin(), ix.end(),
                  [&gen, &ix_dist]() { return ix_dist(gen); });

    BOOST_TEST(fn(ix.cbegin(), ix.cend()) ==
               tt.element_at(ix.cbegin(), ix.cend()));
  }
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(rounding)

BOOST_AUTO_TEST_CASE(idempotent, *boost::unit_test::tolerance(1e-12)) {
  TT::Tensor<double> full{{10, 10, 10, 10}};
  std::array<size_t, 4> idx = {0, 0, 0, 0};
  for (idx[3] = 0; idx[3] < 10; ++idx[3])
    for (idx[2] = 0; idx[2] < 10; ++idx[2])
      for (idx[1] = 0; idx[1] < 10; ++idx[1])
        for (idx[0] = 0; idx[0] < 10; ++idx[0]) {
          full(idx.begin(), idx.end()) =
              std::sin(idx[0] + idx[1] + idx[2] + idx[3]);
        }

  auto tt = TT::TensorTrain<double>::ttsvd(full, 1e-5, 10);

  tt.round(1e-5, 10);

  BOOST_TEST(tt.rank(0) == 2);
  BOOST_TEST(tt.rank(1) == 2);
  BOOST_TEST(tt.rank(2) == 2);
  BOOST_TEST(tt.rank(3) == 1);

  for (idx[3] = 0; idx[3] < 10; ++idx[3])
    for (idx[2] = 0; idx[2] < 10; ++idx[2])
      for (idx[1] = 0; idx[1] < 10; ++idx[1])
        for (idx[0] = 0; idx[0] < 10; ++idx[0])
          BOOST_TEST_CONTEXT("i = " << idx[0] << "; j = " << idx[1] << "; k = "
                                    << idx[2] << "; l = " << idx[3]) {
            BOOST_TEST(full(idx.begin(), idx.end()) ==
                       tt.element_at(idx.begin(), idx.end()));
          }
}

BOOST_AUTO_TEST_CASE(approx, *boost::unit_test::tolerance(1e-2)) {
  TT::Tensor<double> full{{10, 10, 10, 10}};
  std::array<size_t, 4> idx = {0, 0, 0, 0};
  for (idx[3] = 0; idx[3] < 10; ++idx[3])
    for (idx[2] = 0; idx[2] < 10; ++idx[2])
      for (idx[1] = 0; idx[1] < 10; ++idx[1])
        for (idx[0] = 0; idx[0] < 10; ++idx[0]) {
          full(idx.begin(), idx.end()) =
              std::sin(idx[0] + idx[1] + idx[2] + idx[3]) +
              1e-6 * (idx[0] + idx[1] + idx[2] + idx[3]);
        }

  auto tt = TT::TensorTrain<double>::ttsvd(full, 1e-7, 10);

  BOOST_TEST(tt.rank(0) == 4);
  BOOST_TEST(tt.rank(1) == 4);
  BOOST_TEST(tt.rank(2) == 4);

  tt.round(1e-2, 10);
  BOOST_TEST(tt.rank(0) == 2);
  BOOST_TEST(tt.rank(1) == 2);
  BOOST_TEST(tt.rank(2) == 2);

  for (idx[3] = 0; idx[3] < 10; ++idx[3])
    for (idx[2] = 0; idx[2] < 10; ++idx[2])
      for (idx[1] = 0; idx[1] < 10; ++idx[1])
        for (idx[0] = 0; idx[0] < 10; ++idx[0])
          BOOST_TEST_CONTEXT("i = " << idx[0] << "; j = " << idx[1] << "; k = "
                                    << idx[2] << "; l = " << idx[3]) {
            BOOST_TEST(full(idx.begin(), idx.end()) ==
                       tt.element_at(idx.begin(), idx.end()));
          }
}

BOOST_AUTO_TEST_CASE(huge_ranks, *boost::unit_test::tolerance(1e-14)) {
  TT::LowDimensional::Vector<size_t> sizes{{20}};
  std::fill(sizes.begin(), sizes.end(), 2);

  std::mt19937_64 gen;
  auto tt = TT::TensorTrain<double>::dmrg_cross(
      [](std::vector<size_t>::const_iterator,
         std::vector<size_t>::const_iterator) -> double { return 1.0; },
      sizes.cref(), 1e-12, 3, 50, gen);

  for (size_t i = 0; i < tt.dimensionality() - 1; ++i)
    BOOST_TEST_CONTEXT("rank #" << i) { BOOST_TEST_REQUIRE(tt.rank(i) == 1); }

  auto sum = tt;
  for (size_t i = 0; i < 19; ++i)
    sum = sum + tt;

  for (size_t i = 0; i < sum.dimensionality() - 1; ++i)
    BOOST_TEST_CONTEXT("expanded rank #" << i) {
      BOOST_TEST_REQUIRE(sum.rank(i) == 20);
    }

  sum.round(1e-12);
  for (size_t i = 0; i < sum.dimensionality() - 1; ++i)
    BOOST_TEST_CONTEXT("recompressed rank #" << i) {
      BOOST_TEST(sum.rank(i) == 1);
    }

  std::uniform_int_distribution<size_t> ix_dist{0, sizes(0) - 1};
  for (size_t i = 0; i < 1500; ++i) {
    std::vector<size_t> ix(sum.dimensionality());
    std::generate(ix.begin(), ix.end(),
                  [&gen, &ix_dist]() { return ix_dist(gen); });

    BOOST_TEST(sum.element_at(ix.cbegin(), ix.cend()) == 20.0);
  }
}

BOOST_AUTO_TEST_SUITE_END()

struct OperationsFixture {
  TT::Tensor<double> full1, full2;
  TT::TensorTrain<double> tt1, tt2;

  OperationsFixture() : full1{{10, 10, 10, 10}}, full2{{10, 10, 10, 10}} {
    std::array<size_t, 4> idx = {0, 0, 0, 0};
    for (idx[3] = 0; idx[3] < 10; ++idx[3])
      for (idx[2] = 0; idx[2] < 10; ++idx[2])
        for (idx[1] = 0; idx[1] < 10; ++idx[1])
          for (idx[0] = 0; idx[0] < 10; ++idx[0]) {
            full1(idx.begin(), idx.end()) =
                std::sin(idx[0] + idx[1] + idx[2] + idx[3]);
            full2(idx.begin(), idx.end()) =
                (idx[0] + idx[1] + idx[2] + idx[3]) / 20.0;
          }

    tt1 = TT::TensorTrain<double>::ttsvd(full1, 1e-7, 10);
    tt2 = TT::TensorTrain<double>::ttsvd(full2, 1e-7, 10);
  }
};

BOOST_FIXTURE_TEST_SUITE(operations, OperationsFixture,
                         *boost::unit_test::tolerance(1e-12))

BOOST_AUTO_TEST_CASE(plus) {
  auto tt3 = tt1 + tt2;

  std::array<size_t, 4> idx;
  for (idx[3] = 0; idx[3] < 10; ++idx[3])
    for (idx[2] = 0; idx[2] < 10; ++idx[2])
      for (idx[1] = 0; idx[1] < 10; ++idx[1])
        for (idx[0] = 0; idx[0] < 10; ++idx[0])
          BOOST_TEST_CONTEXT("i = " << idx[0] << "; j = " << idx[1] << "; k = "
                                    << idx[2] << "; l = " << idx[3]) {
            BOOST_TEST(full1(idx.begin(), idx.end()) +
                           full2(idx.begin(), idx.end()) ==
                       tt3.element_at(idx.begin(), idx.end()));
          }
}

BOOST_AUTO_TEST_CASE(minus) {
  auto tt3 = tt1 - tt2;

  std::array<size_t, 4> idx;
  for (idx[3] = 0; idx[3] < 10; ++idx[3])
    for (idx[2] = 0; idx[2] < 10; ++idx[2])
      for (idx[1] = 0; idx[1] < 10; ++idx[1])
        for (idx[0] = 0; idx[0] < 10; ++idx[0])
          BOOST_TEST_CONTEXT("i = " << idx[0] << "; j = " << idx[1] << "; k = "
                                    << idx[2] << "; l = " << idx[3]) {
            BOOST_TEST(full1(idx.begin(), idx.end()) -
                           full2(idx.begin(), idx.end()) ==
                       tt3.element_at(idx.begin(), idx.end()));
          }
}

BOOST_AUTO_TEST_CASE(scale) {
  auto tt3 = 12.3 * tt1;

  std::array<size_t, 4> idx;
  for (idx[3] = 0; idx[3] < 10; ++idx[3])
    for (idx[2] = 0; idx[2] < 10; ++idx[2])
      for (idx[1] = 0; idx[1] < 10; ++idx[1])
        for (idx[0] = 0; idx[0] < 10; ++idx[0])
          BOOST_TEST_CONTEXT("i = " << idx[0] << "; j = " << idx[1] << "; k = "
                                    << idx[2] << "; l = " << idx[3]) {
            BOOST_TEST(12.3 * full1(idx.begin(), idx.end()) ==
                       tt3.element_at(idx.begin(), idx.end()));
          }
}

BOOST_AUTO_TEST_CASE(downscale) {
  auto tt3 = tt1 / 12.3;

  std::array<size_t, 4> idx;
  for (idx[3] = 0; idx[3] < 10; ++idx[3])
    for (idx[2] = 0; idx[2] < 10; ++idx[2])
      for (idx[1] = 0; idx[1] < 10; ++idx[1])
        for (idx[0] = 0; idx[0] < 10; ++idx[0])
          BOOST_TEST_CONTEXT("i = " << idx[0] << "; j = " << idx[1] << "; k = "
                                    << idx[2] << "; l = " << idx[3]) {
            BOOST_TEST(full1(idx.begin(), idx.end()) / 12.3 ==
                       tt3.element_at(idx.begin(), idx.end()));
          }
}

BOOST_AUTO_TEST_CASE(hadamard) {
  auto tt3 = tt1 * tt2;

  std::array<size_t, 4> idx;
  for (idx[3] = 0; idx[3] < 10; ++idx[3])
    for (idx[2] = 0; idx[2] < 10; ++idx[2])
      for (idx[1] = 0; idx[1] < 10; ++idx[1])
        for (idx[0] = 0; idx[0] < 10; ++idx[0])
          BOOST_TEST_CONTEXT("i = " << idx[0] << "; j = " << idx[1] << "; k = "
                                    << idx[2] << "; l = " << idx[3]) {
            BOOST_TEST(full1(idx.begin(), idx.end()) *
                           full2(idx.begin(), idx.end()) ==
                       tt3.element_at(idx.begin(), idx.end()));
          }
}

BOOST_AUTO_TEST_CASE(dot) {
  auto prod = tt1.dot(tt2);
  auto v1 = TT::LowDimensional::VectorRef<const double>{
      {full1.size()}, full1.data(), full1.size()};
  auto v2 = TT::LowDimensional::VectorRef<const double>{
      {full2.size()}, full2.data(), full2.size()};
  auto ref = TT::BLAS::L1::dot(v1.strided(), v2.strided());

  BOOST_TEST(prod == ref);
}

BOOST_AUTO_TEST_CASE(outer) {
  auto tt3 = tt1.outer(tt2);

  std::array<size_t, 8> idx;
  std::mt19937_64 gen;
  std::uniform_int_distribution<size_t> idx_dist{0, 9};

  for (size_t i = 0; i < 15000; ++i) {
    std::generate(idx.begin(), idx.end(),
                  [&gen, &idx_dist]() { return idx_dist(gen); });

    BOOST_TEST(full1(idx.cbegin(), idx.cbegin() + 4) *
                   full2(idx.cbegin() + 4, idx.cend()) ==
               tt3.element_at(idx.cbegin(), idx.cend()));
  }
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(materialisation)

BOOST_AUTO_TEST_CASE(simple, *boost::unit_test::tolerance(1e-12)) {
    TT::Tensor<double> full{{10, 10, 10, 10}};
  std::array<size_t, 4> idx = {0, 0, 0, 0};
  for (idx[3] = 0; idx[3] < 10; ++idx[3])
    for (idx[2] = 0; idx[2] < 10; ++idx[2])
      for (idx[1] = 0; idx[1] < 10; ++idx[1])
        for (idx[0] = 0; idx[0] < 10; ++idx[0]) {
          full(idx.begin(), idx.end()) =
              std::sin(idx[0] + idx[1] + idx[2] + idx[3]);
        }

  auto const tt = TT::TensorTrain<double>::ttsvd(full, 1e-5, 10);

  auto reconstructed = tt.materialise();

  BOOST_TEST_REQUIRE(reconstructed.dimensionality() == 4);
  BOOST_TEST_REQUIRE(reconstructed.size(0) == 10);
  BOOST_TEST_REQUIRE(reconstructed.size(1) == 10);
  BOOST_TEST_REQUIRE(reconstructed.size(2) == 10);
  BOOST_TEST_REQUIRE(reconstructed.size(3) == 10);

  for (idx[3] = 0; idx[3] < 10; ++idx[3])
    for (idx[2] = 0; idx[2] < 10; ++idx[2])
      for (idx[1] = 0; idx[1] < 10; ++idx[1])
        for (idx[0] = 0; idx[0] < 10; ++idx[0]) {
          BOOST_TEST(full(idx.begin(), idx.end()) == reconstructed(idx.begin(), idx.end()));
        }
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE_END()
