#include "../include/reduction.hpp"
#include <boost/test/unit_test.hpp>

BOOST_AUTO_TEST_SUITE(reduction)

BOOST_AUTO_TEST_CASE(merge) {
  using namespace TT::LowDimensional;

  MatrixD x{{10, 3}}, y{{10, 2}}, z{{10, 4}};
  std::fill(x.begin(), x.end(), 0);
  std::fill(y.begin(), y.end(), 0);
  std::fill(z.begin(), z.end(), 0);
  x(0, 0) = x(1, 1) = x(2, 2) = y(0, 0) = y(9, 1) = z(0, 0) = z(1, 1) =
      z(2, 1) = z(3, 2) = 1.0;

  std::vector<MatrixRef<const double>> refs = {x.cref(), y.cref(), z.cref()};

  auto combined = Reduction::merge_bases(refs, 1e-12);

  BOOST_TEST(combined.size(1) == 5);
}

BOOST_AUTO_TEST_SUITE_END()
