#!/usr/bin/env bash

mapfile -t jobs < job_list

srun build/main ${jobs[$SLURM_ARRAY_TASK_ID]}
