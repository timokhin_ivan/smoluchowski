#!/usr/bin/env sh

RELFLAGS="-O3 -march=native -mtune=native -DNDEBUG"
RDBFLAGS="-O3 -march=native -mtune=native -ggdb -fno-omit-frame-pointer"
DBGFLAGS="-O0 -ggdb -U_FORTIFY_SOURCE -Wall"

mkdir -p build.release && (cd build.release && CXXFLAGS="${RELFLAGS}" ../configure)
mkdir -p build.reldeb && (cd build.reldeb && CXXFLAGS="${RDBFLAGS}" ../configure)
mkdir -p build.debug && (cd build.debug && CXXFLAGS="${DBGFLAGS}" ../configure)
