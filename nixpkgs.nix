let
  src = (import <nixpkgs> {}).fetchFromGitHub {
    owner = "NixOS";
    repo = "nixpkgs";
    rev = "e83b3f3394834c41c0d25017f6808d65c3d6f880";
    sha256 = "0wbkyz460547x58mrzvq52qpni38c6fcsjc8mb7v0311p8kyx8dd";
    #rev = "e0605b3174f4d786368f5cfc2086a74e66c3cc34";
    #sha256 = "1f964kdvxplh97mbajm7jxmh81p34v3v062l3f03s6abbh7kx9mn";
  };
in
  import src
