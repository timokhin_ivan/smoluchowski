{ nixpkgs ? import ./nixpkgs.nix {} }:

import ./common.nix {
  src = nixpkgs.nix-gitignore.gitignoreSource [./.gitignore] ./.;
  inherit nixpkgs;
}
