#!/bin/sh

for lambda in 0.0 0.01 0.005 0.001
do
    sbatch -n1 ./main.sh 65536 0.8 1 $lambda 1e-13 512 256
done

for a in 0.6 0.7 0.8
do
    for n in 32768 65536 131072
    do
        for eps in 1e-13 1e-12 1e-11 1e-10
        do
            sbatch -n1 ./main.sh $n $a 1 0.0 $eps 512 256
        done
    done
done
