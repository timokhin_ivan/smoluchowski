{ src, nixpkgs }:

with nixpkgs;
stdenv.mkDerivation {
  name = "tt-suite-0.1.0";
  inherit src;
  buildInputs = [
    boost
    gdb
    valgrind
    mkl
    pkgconfig
    autoconf
    automake
    autoconf-archive
    libtool
    rr
    ccls
    # clang_9
    # clang-tools
    bear
  ];

  MKL_LIBS = "-L${mkl}/lib -Wl,--no-as-needed -lmkl_intel_ilp64 -lmkl_gnu_thread -lmkl_core -lgomp -lpthread -lm -ldl";
  MKL_CFLAGS = "-DMKL_ILP64 -m64 -I${mkl}/include";
}
